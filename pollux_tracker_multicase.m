function [S] = pollux_tracker_multicase(C,X,P)
%   Remarque : tout ce qui est relatif au photon 1.157 est marqué par un "prime"
    %   Pour la CSR : V1p, V2p, E1a, E2a
    %   Pour la LOR : V1,V2 si LOR détectée
    %                 V1, (V1s,V2s,E1b,E2b) si DEMI-LOR avec V1 détecté
    %                 V2, (V1s,V2s,E1b,E2b) si DEMI-LOR avec V2 detecté
    % MANTRA : P = E + t * V  
    % => On peut tout calculer à partir de t (puisque l'on connait E et V)
    % => On ne s'embête plus à maintenir simultanément des retour de point et de coefficient t
    [S] = pollux_init_struct(C,P);     
    [C,verb_lim,ANIM] = draw_conditions(C);
    [pure_evt_idx] = get_pure_event_idx(C);
    % ---------------------------------------------------------------------
    if (C.draw)
        set(0,'DefaultFigureWindowStyle','docked')
        draw_xemis(X,P,ANIM);
    end
    % ---------------------------------------------------------------------
    terminal = false;
    n = 0;
    % ---------------------------------------------------------------------
    while (~terminal)
        n = n + 1;
        % Allocation de mémoire supplémentaire dans la structure S
        if (n >= S.cur_N_limit)
            [S] = update_S_struct(S);
        end
        % -----------------------------------------------------------------
        pts_list = [];     % Liste des points à conserver (émission, interaction) 
        % -----------------------------------------------------------------

        [E0,E0_prime,~,~] = init_photon_energy_count(C.pollux_case);
        % -----------------------------------------------------------------
        [S,M,Dir,info] = pollux_simulation_emission(P,C,S,n);
        % -----------------------------------------------------------------
        % Initialisation des trackers en dehors de la boucle 
        track_1157 = init_tracker(); 
        track_511_b = init_tracker();
        track_511_f = init_tracker();
        % -----------------------------------------------------------------
        switch C.pollux_case
            case "1"
            % >>>>>>>>>>>>>>>>>>>>> DEBUT CAS COR @1.157 >>>>>>>>>>>>>>>>>>
                % Photon à 1157;
                [t1_min_p_f,t1_min_p_b] = get_intersect_coord_Rin(X,M,Dir.D1);
                % >> PATCH CHOIX DE t1_min_p;
                T_min_p = [t1_min_p_f,t1_min_p_b];
                scal_vec = zeros(size(T_min_p));
                V_unit_dir = M + Dir.D1;
                MV_nrm_unit_dir = (V_unit_dir-M)/norm(V_unit_dir-M,2);
                % Pour la première composante t retournée par get_intersect_coord_Rin
                if (~isinf(t1_min_p_f))
                    V_t1_min_p_f = M + t1_min_p_f*Dir.D1;
                    MV_nrm_min_p_f = (V_t1_min_p_f - M)/norm(V_t1_min_p_f - M,2);
                    scal_vec(1) = dot(MV_nrm_unit_dir,MV_nrm_min_p_f);
                    %if (C.draw),pts_list = draw_store_pts(pts_list,V_t1_min_p_f,'^','m'); end
                end
                % Pour la seconde composante t retournée par get_intersect_coord_Rin
                if (~isinf(t1_min_p_b))
                    V_t1_min_p_b = M + t1_min_p_b*Dir.D1;
                    MV_nrm_min_p_b = (V_t1_min_p_b - M)/norm(V_t1_min_p_b - M,2);
                    scal_vec(2) = dot(MV_nrm_unit_dir,MV_nrm_min_p_b);
                    %if (C.draw), pts_list = draw_store_pts(pts_list,V_t1_min_p_b,'v','c'); end
                end
                [~,idx_t_min] = max(scal_vec);
                t1_min_p = T_min_p(idx_t_min);
                if (C.verbose)
                    ast_idx = ["",""];
                    ast_idx(idx_t_min) = "(*)";
                    fprintf("[INIT_1157 - %d]\t t1_min_p_f = %g %s \t t1_min_p_b = %g %s\n",n,t1_min_p_f,ast_idx(1),t1_min_p_b,ast_idx(2));
                end
                % <<
                % -----------------------------------------------------------------
                if (~isinf(t1_min_p))
                    % Point d'entrée dans la zone active
                    % V0 = get_point(M,Dir.D1,t1_min_p);
                    % [t1_max_p] = get_intersect_coord_Rout(X,M,Dir.D1,t1_min_p);
                    [track_1157,pts_list_1157] = photon_tracker(X,C,track_1157,E0_prime,M,Dir.D1,t1_min_p,"1157");
                    if (C.draw), pts_list = [pts_list,pts_list_1157]; end
    
                end
            % <<<<<<<<<<<<<<<<<<<<< FIN CAS COR @1.157 <<<<<<<<<<<<<<<<<<<<
            case "2"
            % >>>>>>>>>>>>>>>>>>>>> DEBUT CAS LOR ou COR @0.511 >>>>>>>>>>>
                % Photon à 511;
                % Application du PR et stockage
                [M,R_pr] = positron_range(M,Dir.D_pr,S.Conf.Pos_rng);
                S.results.M_pr(n,:) = M;
                S.results.R_pr(n,:) = R_pr;
                % Simulation
                [t_min_f,t_min_b] = get_intersect_coord_Rin(X,M,Dir.D2);  % b : back, f : forward
                if (C.verbose), fprintf("\n[INIT_LOR - %d] \tt_min_f = %g \t t_min_b = %g \n",n,t_min_f,t_min_b); end
                % forward - track_511_f
                if (~isinf(t_min_f))
                    % Point d'entrée dans la zone active
                    %V0 = get_point(M,Dir.D2,t_min_f);
                    %[t1_max_f] = get_intersect_coord_Rout(X,M,Dir.D2,t_min_f);
                    [track_511_f,pts_list_511_f] = photon_tracker(X,C,track_511_f,E0,M,Dir.D2,t_min_f,"511");
                    if (C.draw), pts_list = [pts_list,pts_list_511_f]; end
                end
                % backward - track_511_b
                if (~isinf(t_min_b))
                    % Point d'entrée dans la zone active
                    %V0 = get_point(M,Dir.D2,t_min_b);
                    %[t1_max_b] = get_intersect_coord_Rout(X,M,Dir.D2,t_min_b);
                    [track_511_b,pts_list_511_b] = photon_tracker(X,C,track_511_b,E0,M,Dir.D2,t_min_b,"511");
                    if (C.draw), pts_list = [pts_list,pts_list_511_b]; end
                end
            % <<<<<<<<<<<<<<<<<<<<< FIN CAS LOR ou COR @0.511 <<<<<<<<<<<<< 
            case "3"
            % >>>>>>>>>>>>>>>>>>>>> DEBUT CAS 3G >>>>>>>>>>>>>>>>>>>>>>>>>>
                % Photon à 1157;
                [t1_min_p_f,t1_min_p_b] = get_intersect_coord_Rin(X,M,Dir.D1);
                % >> PATCH CHOIX DE t1_min_p;
                T_min_p = [t1_min_p_f,t1_min_p_b];
                scal_vec = zeros(size(T_min_p));
                V_unit_dir = M + Dir.D1;
                MV_nrm_unit_dir = (V_unit_dir-M)/norm(V_unit_dir-M,2);
                % Pour la première composante t retournée par get_intersect_coord_Rin
                if (~isinf(t1_min_p_f))
                    V_t1_min_p_f = M + t1_min_p_f*Dir.D1;
                    MV_nrm_min_p_f = (V_t1_min_p_f - M)/norm(V_t1_min_p_f - M,2);
                    scal_vec(1) = dot(MV_nrm_unit_dir,MV_nrm_min_p_f);
                    %if (C.draw),pts_list = draw_store_pts(pts_list,V_t1_min_p_f,'^','m'); end
                end
                % Pour la seconde composante t retournée par get_intersect_coord_Rin
                if (~isinf(t1_min_p_b))
                    V_t1_min_p_b = M + t1_min_p_b*Dir.D1;
                    MV_nrm_min_p_b = (V_t1_min_p_b - M)/norm(V_t1_min_p_b - M,2);
                    scal_vec(2) = dot(MV_nrm_unit_dir,MV_nrm_min_p_b);
                    %if (C.draw), pts_list = draw_store_pts(pts_list,V_t1_min_p_b,'v','c'); end
                end
                [~,idx_t_min] = max(scal_vec);
                t1_min_p = T_min_p(idx_t_min);
                if (C.verbose)
                    ast_idx = ["",""];
                    ast_idx(idx_t_min) = "(*)";
                    fprintf("[INIT_1157 - %d]\t t1_min_p_f = %g %s \t t1_min_p_b = %g %s\n",n,t1_min_p_f,ast_idx(1),t1_min_p_b,ast_idx(2));
                end
                % <<
                % -----------------------------------------------------------------
                if (~isinf(t1_min_p))
                    % Point d'entrée dans la zone active
                    % V0 = get_point(M,Dir.D1,t1_min_p);
                    % [t1_max_p] = get_intersect_coord_Rout(X,M,Dir.D1,t1_min_p);
                    [track_1157,pts_list_1157] = photon_tracker(X,C,track_1157,E0_prime,M,Dir.D1,t1_min_p,"1157");
                    if (C.draw), pts_list = [pts_list,pts_list_1157]; end
    
                end
                % -----------------------------------------------------------------
                % Photon à 511;
                % Application du PR et stockage
                [M,R_pr] = positron_range(M,Dir.D_pr,S.Conf.Pos_rng);
                S.results.M_pr(n,:) = M;
                S.results.R_pr(n,:) = R_pr;
                % Simulation
                [t_min_f,t_min_b] = get_intersect_coord_Rin(X,M,Dir.D2);  % b : back, f : forward
                if (C.verbose), fprintf("\n[INIT_LOR - %d] \tt_min_f = %g \t t_min_b = %g \n",n,t_min_f,t_min_b); end
                % forward - track_511_f;
                if (~isinf(t_min_f))
                    % Point d'entrée dans la zone active
                    %V0 = get_point(M,Dir.D2,t_min_f);
                    %[t1_max_f] = get_intersect_coord_Rout(X,M,Dir.D2,t_min_f);
                    [track_511_f,pts_list_511_f] = photon_tracker(X,C,track_511_f,E0,M,Dir.D2,t_min_f,"511");
                    if (C.draw), pts_list = [pts_list,pts_list_511_f]; end
                end
                % backward - track_511_b
                if (~isinf(t_min_b))
                    % Point d'entrée dans la zone active
                    %V0 = get_point(M,Dir.D2,t_min_b);
                    %[t1_max_b] = get_intersect_coord_Rout(X,M,Dir.D2,t_min_b);
                    [track_511_b,pts_list_511_b] = photon_tracker(X,C,track_511_b,E0,M,Dir.D2,t_min_b,"511");
                    if (C.draw), pts_list = [pts_list,pts_list_511_b]; end
                end
            % <<<<<<<<<<<<<<<<<<<<< FIN CAS 3G <<<<<<<<<<<<<<<<<<<<<<<<<<<<
            otherwise
                warning(sprintf(" !!!! C.pollux_case = %s non reconnu !!!!",C.pollux_case)); break
        end



        % >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        % >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        % >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        % -----------------------------------------------------------------
        
        % <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        % <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        % <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

        % -----------------------------------------------------------------
        % Caractérisation de la détection; 
        [S]= process_event(X,C,S,n,track_1157,track_511_f,track_511_b);
        test = ""; 
        % -----------------------------------------------------------------
        
        
        
        
        
        if (mod(n,verb_lim) == 0 )
            info.n = n;
            info.N_allG =S.results.occ_config_count;
            info.pure_evt_idx = pure_evt_idx;
            print_one_simu_tracker(C,P,info);
        end
                
        if (C.draw)
            erase_all_elts(pts_list); 
        end
        
        
        %S.results.N_exp = 1; % PATCH
        switch C.preset_type
            case "count"
                if (S.results.occ_config_count(pure_evt_idx) == C.N), S.terminal=true; break;  end
            case "time"
                 disp("A FAIRE - TODO "); S.terminal = false; break;
            otherwise
        end
        %terminal = true; % PATCH 
    end % while (~terminal)
    
    if (C.draw)
        hold off;
    end

    S.results.N_exp = sum(S.results.occ_config_count);
    S.results.calib_factor = sum(S.results.activity,'all')/S.Conf.N;
    % ------------------------------------------------------------------            
    info.n = n;
    info.N_allG =S.results.occ_config_count;
    info.pure_evt_idx = pure_evt_idx;
    print_one_simu_tracker(C,P,info);
    % ------------------------------------------------------------------
    % Rabotage des allocations mémoire en trop ...
    [S] = plane_S_struct(S);






    test = ""; 
end


function [] = print_one_simu_tracker(C,P,info)   
    clc; 
    % info: Vox info, info : Tirage info; info : Simulation Info  
    N_focG = info.N_allG(info.pure_evt_idx);
    N_remG = sum(info.N_allG) - N_focG; 
    prop = info.N_allG./sum(info.N_allG)*100; 
    fprintf("method: %s\n",C.method);
    fprintf("n = %d/%d\t #(othG) = %g\t (%g %%)\n",N_focG,C.N,N_remG,((N_focG)/C.N)*100);
    fprintf('%s\t',C.Classif(:,1)'); fprintf("\n")
    fmt = repmat('%.2f\t', 1, length(prop));
    fprintf(fmt,prop); fprintf(" %%\n")
    test = ""; 
end


function [S] = process_event(X,C,S,n,T1157,T511f,T511b)
    % Rappel: Rayleigh_id = 1; Photo_id = 2; Compton_id = 3;      
    % INCOMPATIBLE AVEC UN AJOUT FUTUR DE L'EFFET RAYLEIGH (CALCUL DE LA SOMME)

    LOR = (T511f.N >= 1 && T511b.N >= 1);
    G1157 = (T1157.N>=2 ); % && sum(T1157.effect_id(1:2))>=5
    G511f = (~LOR && T511f.N>=2 ); % && sum(T511f.effect_id(1:2))>=5
    G511b = (~LOR && T511b.N>=2 ); % && sum(T511b.effect_id(1:2))>=5
%     if (LOR), disp("uLOR"); end 
%     if (G1157), disp("u1G1157"); end
%     if (G511f), disp("u1G511f"); end
%     if (G511b), disp("u1G511b"); end
%     disp("--")
    % ---------------------------------------------------------------------
    % ---------------------------------------------------------------------
    % ---------------------------------------------------------------------
    if (LOR)
        % Config initiale 
        config.idx=4;
        config.chr='3';
        config.label="2GLOR";
        % Données de la LOR
        S.export.data(n,S.rng.V1_rng) = T511f.inter_pts(1,:);   % V1;
        S.export.data(n,S.rng.E1b_rng) = T511f.dep_energ(1);    % E1b
        S.export.data(n,S.rng.V2_rng) = T511b.inter_pts(1,:);   % V2 
        S.export.data(n,S.rng.E2b_rng) = T511b.dep_energ(1);    % E2b 
        test = "";
        if (G1157)
            % Update de la config et des données associées
            config.idx=6;
            config.chr='5';
            config.label="3G";
            % Données du 3ème photon 
            S.export.data(n,S.rng.V1p_rng) = T1157.inter_pts(1,:);  % V1p
            S.export.data(n,S.rng.E1a_rng) = T1157.dep_energ(1);    % E1a
            S.export.data(n,S.rng.V2p_rng) = T1157.inter_pts(2,:);  % V2p
            S.export.data(n,S.rng.E2a_rng) = T1157.dep_energ(2);    % E2a
            test = "";
            % Calcul des points d'intersections 
            V1 = T511f.inter_pts(1,:); V2 = T511b.inter_pts(1,:);
            axe_lor = (V2-V1)/norm(V2-V1,2); 
            V1p = T1157.inter_pts(1,:); V2p = T1157.inter_pts(2,:); E1a = T1157.dep_energ(1);
            axe_csr = (V1p-V2p)/norm(V1p-V2p,2); apex = V1p; 
            beta =  acos(1-(E1a*X.mec2/(1.157*(1.157-E1a))));
            [t_inter_1,t_inter_2,tab_inter_status] = intersection_lor_cor(X,V1,axe_lor,apex,axe_csr,beta,false);
            nb_intersection = (t_inter_1~=0)+(t_inter_2~=0); 
            S.results.nb_intersection(n) = nb_intersection; 
            % Nature des intersections: [sgn(delta),t1,t1::in_fov,t1::sign(beta),t2,t2::in_fov,t2::sign(beta)]
            S.results.inter_status(n,:) = tab_inter_status; 
            if (t_inter_1~=0)
                S.export.intersection_points(n,S.rng.inter_I1_rng) = get_point(V1,axe_lor,t_inter_1);
            end
            if (t_inter_2~=0)
                S.export.intersection_points(n,S.rng.inter_I2_rng) = get_point(V1,axe_lor,t_inter_2);
            end

            test = ""; 
        end
    % ---------------------------------------------------------------------
    elseif (G511f)
        % Config initiale 
        config.idx=2;
        config.chr='1';
        config.label="1G511_f";
        % Données du photon 511 forward
        S.export.data(n,S.rng.V1s_rng) = T511f.inter_pts(1,:);
        S.export.data(n,S.rng.E1b_rng) = T511f.dep_energ(1);
        S.export.data(n,S.rng.V2s_rng) = T511f.inter_pts(2,:);
        S.export.data(n,S.rng.E2b_rng) = T511f.dep_energ(2);
        test = "";
        if (G1157)
            % Update de la config et des données associées
            config.idx=5;
            config.chr='4';
            config.label="2GMIX_f"; 
            % Données du 3ème photon
            S.export.data(n,S.rng.V1p_rng) = T1157.inter_pts(1,:);
            S.export.data(n,S.rng.E1a_rng) = T1157.dep_energ(1);
            S.export.data(n,S.rng.V2p_rng) = T1157.inter_pts(2,:);
            S.export.data(n,S.rng.E2a_rng) = T1157.dep_energ(2);
            test = "";
        end
    % ---------------------------------------------------------------------
    elseif (G511b)
        % Config initiale
        config.idx=2;
        config.chr='1';
        config.label="1G511_b";
        % Données du photon 511 backward
        S.export.data(n,S.rng.V1s_rng) = T511b.inter_pts(1,:);
        S.export.data(n,S.rng.E1b_rng) = T511b.dep_energ(1);
        S.export.data(n,S.rng.V2s_rng) = T511b.inter_pts(2,:);
        S.export.data(n,S.rng.E2b_rng) = T511b.dep_energ(2);
        test = "";
        if (G1157)
        % Update de la config et des données associées
            config.idx=5;
            config.chr='4';
            config.label="2GMIX_b";
            % Données du 3ème photon
            S.export.data(n,S.rng.V1p_rng) = T1157.inter_pts(1,:);
            S.export.data(n,S.rng.E1a_rng) = T1157.dep_energ(1);
            S.export.data(n,S.rng.V2p_rng) = T1157.inter_pts(2,:);
            S.export.data(n,S.rng.E2a_rng) = T1157.dep_energ(2);
            test = "";
        end
    % ---------------------------------------------------------------------
    elseif (G1157)
        config.idx=3;
        config.chr='2';
        config.label = "1G1157";
        % Données du 3ème photon 
        S.export.data(n,S.rng.V1p_rng) = T1157.inter_pts(1,:);
        S.export.data(n,S.rng.E1a_rng) = T1157.dep_energ(1);
        S.export.data(n,S.rng.V2p_rng) = T1157.inter_pts(2,:);
        S.export.data(n,S.rng.E2a_rng) = T1157.dep_energ(2);
        test = "";
        % -------------------------------------------------------------
    else 
        config.idx=1;
        config.chr='0';
        config.label = "0G";
    end
    % ---------------------------------------------------------------------
    % ---------------------------------------------------------------------
    % ---------------------------------------------------------------------
    S.export.chr(n)= config.chr;
    % Stockage des données de comptage
    S.results.occ_config_count(config.idx) = S.results.occ_config_count(config.idx) + 1;
    % ---------------------------------------------------------------------
    if (config.idx>1)
        S.nb_detection = S.nb_detection + 1;
    end
    % ---------------------------------------------------------------------

%    disp("it"+num2str(n)+"_"+config.label)
    if (C.verbose),fprintf("=> Process event : label=%s [chr=%s|idx=%d]\n",config.label,config.chr,config.idx); end 
    test = "";
end

function [track] = init_tracker()
    track.N = 0; 
    track.inter_pts = [];
    track.dep_energ = [];
    track.effect_id = []; 
end







% SAUVEGARDE STOCKAGE DONNEES REDONDANCE
%{
if (LOR)
        if (G1157)
            % -------------------------------------------------------------
            config.idx=6;
            config.chr='5';
            config.label="3G";
            % Données du 3ème photon 
            S.export.data(n,S.rng.V1p_rng) = T1157.inter_pts(1,:);
            S.export.data(n,S.rng.E1a_rng) = T1157.dep_energ(1);
            S.export.data(n,S.rng.V2p_rng) = T1157.inter_pts(2,:);
            S.export.data(n,S.rng.E2a_rng) = T1157.dep_energ(2);
            % Données de la LOR
            S.export.data(n,S.rng.V1_rng) = T511f.inter_pts(1,:);
            S.export.data(n,S.rng.E1b_rng) = T511f.dep_energ(1);
            S.export.data(n,S.rng.V2_rng) = T511b.inter_pts(1,:);
            S.export.data(n,S.rng.E2b_rng) = T511b.dep_energ(1);
            % -------------------------------------------------------------
        else
            % -------------------------------------------------------------
            config.idx=4;
            config.chr='3';
            config.label="2GLOR";
            % Données de la LOR
            S.export.data(n,S.rng.V1_rng) = T511f.inter_pts(1,:);
            S.export.data(n,S.rng.E1b_rng) = T511f.dep_energ(1);
            S.export.data(n,S.rng.V2_rng) = T511b.inter_pts(1,:);
            S.export.data(n,S.rng.E2b_rng) = T511b.dep_energ(1);
            % -------------------------------------------------------------
        end
    elseif (G511f)
        if (G1157)
            % -------------------------------------------------------------
            config.idx=5;
            config.chr='4';
            config.label="2GMIX_f"; 
            % Données du 3ème photon
            S.export.data(n,S.rng.V1p_rng) = T1157.inter_pts(1,:);
            S.export.data(n,S.rng.E1a_rng) = T1157.dep_energ(1);
            S.export.data(n,S.rng.V2p_rng) = T1157.inter_pts(2,:);
            S.export.data(n,S.rng.E2a_rng) = T1157.dep_energ(2);
            % Données du photon 511 forward
            S.export.data(n,S.rng.V1s_rng) = T511f.inter_pts(1,:);
            S.export.data(n,S.rng.E1b_rng) = T511f.dep_energ(1);
            S.export.data(n,S.rng.V2s_rng) = T511f.inter_pts(2,:);
            S.export.data(n,S.rng.E2b_rng) = T511f.dep_energ(2);
            % -------------------------------------------------------------
        else
            % -------------------------------------------------------------
            config.idx=2;
            config.chr='1';
            config.label="1G511_f";
            % Données du photon 511 forward
            S.export.data(n,S.rng.V1s_rng) = T511f.inter_pts(1,:);
            S.export.data(n,S.rng.E1b_rng) = T511f.dep_energ(1);
            S.export.data(n,S.rng.V2s_rng) = T511f.inter_pts(2,:);
            S.export.data(n,S.rng.E2b_rng) = T511f.dep_energ(2);
            % -------------------------------------------------------------
        end
    elseif (G511b)
        if (G1157)
            % -------------------------------------------------------------
            config.idx=5;
            config.chr='4';
            config.label="2GMIX_b";
            % Données du 3ème photon
            S.export.data(n,S.rng.V1p_rng) = T1157.inter_pts(1,:);
            S.export.data(n,S.rng.E1a_rng) = T1157.dep_energ(1);
            S.export.data(n,S.rng.V2p_rng) = T1157.inter_pts(2,:);
            S.export.data(n,S.rng.E2a_rng) = T1157.dep_energ(2);
            % Données du photon 511 backward
            S.export.data(n,S.rng.V1s_rng) = T511b.inter_pts(1,:);
            S.export.data(n,S.rng.E1b_rng) = T511b.dep_energ(1);
            S.export.data(n,S.rng.V2s_rng) = T511b.inter_pts(2,:);
            S.export.data(n,S.rng.E2b_rng) = T511b.dep_energ(2);
            test="";
            % -------------------------------------------------------------
        else
            % -------------------------------------------------------------
            config.idx=2;
            config.chr='1';
            config.label="1G511_b";
            % Données du photon 511 backward
            S.export.data(n,S.rng.V1s_rng) = T511b.inter_pts(1,:);
            S.export.data(n,S.rng.E1b_rng) = T511b.dep_energ(1);
            S.export.data(n,S.rng.V2s_rng) = T511b.inter_pts(2,:);
            S.export.data(n,S.rng.E2b_rng) = T511b.dep_energ(2);
            test = ""; 
            % -------------------------------------------------------------
        end
    elseif (G1157)
        % -------------------------------------------------------------
        config.idx=3;
        config.chr='2';
        config.label = "1G1157";
        % Données du 3ème photon 
        S.export.data(n,S.rng.V1p_rng) = T1157.inter_pts(1,:);
        S.export.data(n,S.rng.E1a_rng) = T1157.dep_energ(1);
        S.export.data(n,S.rng.V2p_rng) = T1157.inter_pts(2,:);
        S.export.data(n,S.rng.E2a_rng) = T1157.dep_energ(2);
        % -------------------------------------------------------------
    else 
        config.idx=1;
        config.chr='0';
        config.label = "0G";
    end

%}