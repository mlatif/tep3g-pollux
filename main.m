clear all; close all;
set(0,'DefaultFigureWindowStyle','docked') 
addpath("methods/");
addpath("methods/dessin/");
method = "DRAW";  % "HECTOR" ou "POLLUX"
%method = "LOAD"; 
%method = "POLLUX";
method = "TRACKER"; 
%method = "TRACKER_MC"; 
method ="REWRITE_CASTOR_DATA"; 
MODE= "DEV"; 
%MODE = "CASTOR"; 


% ../simulator/bin/mod_create_phantom.exe -d 90 90 1 -v 9. 9. 9. -o phantom_emission_simon -c 0. 0. 0. 150. 4. 100. -c 50. 10. 0. 20. 4. 400. -c -40. -40. 0. 40. 4. 0. -x 32 32 1

if (MODE == "CASTOR")
    F.folder = "castorification_phantoms/";         if (~isfolder(F.folder)), mkdir(F.folder); end 
    F.save_folder = "castorification_res_simu/";    if (~isfolder(F.save_folder)), mkdir(F.save_folder); end
    
    F.file = "cylindre_35_120_v1"; F.phantom_name = "cylindre_35_120_v1";
    F.file = "cylindre_35_120_v5"; F.phantom_name = "cylindre_35_120_v5";

else
    F.folder = "my_phantoms/";                      if (~isfolder(F.folder)), mkdir(F.folder); end 
    F.save_folder = "res_simu/";                    if (~exist(F.save_folder)), mkdir(F.save_folder); end

    F.file ="erno_3d_raw";
    %F.file ="erno_3d_smt";
    F.file = "phantom_3d_raw";                          % mod_create_phantom.exe -d 24 24 30 -v 4. 4. 4. -o phantom_3d_raw -c 0. 0. 0. 46. 120. 100. -c 15. 15. 0. 18. 120. 400. -c -20. -20. 0. 10. 60. 0.
    F.file = "phantom_3d_smt";                         % mod_create_phantom.exe -d 24 24 30 -v 4. 4. 4. -o phantom_3d_smt -c 0. 0. 0. 46. 120. 100. -c 15. 15. 0. 18. 120. 400. -c -20. -20. 0. 10. 60. 0. -x 32 32 1
    F.file = "achille_v1"; 
    F.file = "ulysse_v1";
    F.file = "cortex";  F.phantom_name = "cortex"; 
    %F.file = "thesee-5-5-10"
    %F.file = "myrmidon";
    %F.file = "phantom_3d_smt";                         % mod_create_phantom.exe -d 24 24 30 -v 4. 4. 4. -o phantom_3d_smt -c 0. 0. 0. 46. 120. 100. -c 15. 15. 0. 18. 120. 400. -c -20. -20. 0. 10. 60. 0. -x 32 32 1
    %F.file = "ajax_v1"
    %F.file ="thesee-2-2-2";  F.phantom_name = "thesee"; 
    %F.file ="thesee-5-5-5";  F.phantom_name = "thesee_2GLOR";
    F.file = "cylindre";      F.phantom_name = "cylindre_2GLOR"; 
    F.file = "cylindre_sphere"; F.phantom_name = "cylindre_sphere_2GLOR";
    F.file = "cylindre_35_120"; F.phantom_name = "cylindre_35_120";

end




% ----------------------------------------- --------------------------------



% -------------------------------------------------------------------------


% -------------------------------------------------------------------------
% Pour sauvegarder les résultats d'une simulation Pollux depuis le fichier
% main.
to_save_pollux_simu = true; 
% -------------------------------------------------------------------------
cs_file = "data/nist_data_xenon.txt";
cs_file = "data/nistdata_1_1200_kev_rng.txt";
% -------------------------------------------------------------------------
RNG = 'default';

rng(RNG);
% -------------------------------------------------------------------------
data = load(strcat(F.folder,F.file,".txt"));
% -------------------------------------------------------------------------
neg_rayleigh = true;                                % Est-ce qu'on veut négliger l'effet Rayleigh
[X,P,stop_init] = init_simulation(data,cs_file,neg_rayleigh);
% -------------------------------------------------------------------------
if (~stop_init)
    print_phantom_info(P,F);
    % -------------------------------------------------------------------------
    % C : Config
    C.method = method;
    C.rng = RNG;
    C.save_folder = strcat(F.save_folder,F.file);
    C.Classif = ["0G",0,"0\gamma"; "1G511",1,"1\gamma - 511"; "1G1157",1,"1\gamma - 1157"; "2GLOR",2,"2\gamma - LOR"; "2GMIX",2,"2\gamma- Mix"; "3G",3,"3\gamma"];
    C.Pos_rng.mu = 2.4;                      % Positron range (exprimé en mm)
    C.Pos_rng.sigma = 2;                     % => Deux paramètres pour le moment N(mu,sigma^2), on verra après
    C.Pos_rng.active = true;
    % Spécification de la source d'émission (IN: aléatoire depuis le voxel, CV: fixé au centre du voxel)
    C.Emi_pts = "IN"; 
    %C.Emi_pts = "CV"; 
    C.verbose = false;                      % Mode verbeux pour le suivi d'une itération (effet phyisiques, succès et echecs des LOR/CSR, hors de la zone active)
    C.draw = true;                         % dessine le résultat de la simulation (faux si N > N_max ou preset_time)
    C.wait_time = 0.1;                      % Temps d'attente entre deux tirage si dessiné
    C.rem_pts = true;
    C.create_anim = false;
    C.N_max =100;                       % N_max : nombre max d'itérations avant de désactiver le draw
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    C.preset_type = "count";            % Preset_count/Preset_time
    C.pollux_N = 10;                 % N : Nombre de tirage (preset_count)
    %C.N = ceil(sum(P.lambda))          % [!!!] L'activité d'un phantom est connue en simulation
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % A faire varier dans les cas TRACKER_MC (Multi_Case)
    C.pollux_case = "3";
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    C.hector_N = 10000;                     % Nombre de tirages voulu sur chaque voxel
    F.hector_partial_save_folder = ["/home/user/Nextcloud/HECTOR_PART_SAUVEGARDE/"];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fprintf("MODE: %s\n\tFOLDER: %s\n\tSAVE_FOLDER: %s\n",MODE,F.folder,F.save_folder); 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    switch method
        case "DRAW"
            fprintf("METHODE = %s\n",method);
            draw_phantom(P,F,X);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        case "POLLUX"
            fprintf("METHODE = %s\n",method);
            S = pollux(C,X,P);
            if (S.terminal)
                print_phantom_info(P,F,S);
                write_castordata(F,S);
                if ((C.pollux_N>=10 && C.pollux_N > C.N_max) || to_save_pollux_simu) % && false  
                    tok_pr = ""; if (C.Pos_rng.active), tok_pr = "_PR"; end  
                    save_name = strcat(F.save_folder,"pollux_",F.file,"_sim_",num2str(C.pollux_N),"_",C.pollux_case,"G",tok_pr);
                    save(save_name,"X","P","S","F","C",'-v7.3');
                    fprintf("Save as/in %s \n",save_name); 
                end
                hist_occ_events(C,S,[1,2,4],4)
                hist_occ_events(C,S,[1,2,3,4,5,6],6)
                hist_occ_events(C,S,[2,3,4,5,6],6)
            end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        case "TRACKER"
            fprintf("METHODE = %s\n",method)
            % >>
            % Sécurité supplémentaire au cas où
            % Bug constaté quand C.pollux_case = "1" ou "2" ajouté pour la
            % méthode "TRACKER_MC"
            C.pollux_case = "3";
            % <<
            S = pollux_tracker(C,X,P);
            if (S.terminal)
                print_phantom_info(P,F,S);
                % --------------------------------------------------------
                % Construction du nombre du suffixe: _N+"premier_chiffre"+E+"puissance_de_10"
                if (C.pollux_N < 1000)
                    n_msg=C.pollux_N;
                else
                    e=floor(log10(C.pollux_N)); f=floor(C.pollux_N/(10^e));
                    n_msg=sprintf("%dE%d",f,e);
                end
                tok_pr = ""; if (C.Pos_rng.active), tok_pr = "_PR"; end
                msg = C.method+"_C"+C.pollux_case+"_N"+n_msg+tok_pr;
                write_castordata(F,S,msg);
                if ((C.pollux_N>=10 && C.pollux_N > C.N_max) || to_save_pollux_simu)
                    tok_pr = ""; if (C.Pos_rng.active), tok_pr = "_PR"; end
                    save_name = strcat(F.save_folder,"pollux_tracker_",F.file,"_sim_",num2str(C.pollux_N),"_",C.pollux_case,"G",tok_pr);
                    save(save_name,"X","P","S","F","C",'-v7.3');
                    fprintf("Save as/in %s \n",save_name);
                end
            end
            hist_occ_events(C,S,[1,2,3,4,5,6],6)
            % TODO
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        case "TRACKER_MC"
            fprintf("METHODE = %s\n",method);
             S = pollux_tracker_multicase(C,X,P);
             if (S.terminal)
                 print_phantom_info(P,F,S);
                 if (C.pollux_N < 1000)
                    n_msg=C.pollux_N; 
                 else
                     e=floor(log10(C.pollux_N)); f=floor(C.pollux_N/(10^e)); 
                     n_msg=sprintf("%dE%d",f,e); 
                 end
                 tok_pr = ""; if (C.Pos_rng.active), tok_pr = "_PR"; end
                 msg = C.method+"_C"+C.pollux_case+"_N"+n_msg+tok_pr;
                 write_castordata(F,S,msg);
                 if ((C.pollux_N>=10 && C.pollux_N > C.N_max) || to_save_pollux_simu) 
                     tok_pr = ""; if (C.Pos_rng.active), tok_pr = "_PR"; end
                     save_name = strcat(F.save_folder,"pollux_tracker_mc_",F.file,"_sim_",num2str(C.pollux_N),"_",C.pollux_case,"G",tok_pr);
                     save(save_name,"X","P","S","F","C",'-v7.3');
                     fprintf("Save as/in %s \n",save_name);
                 end
             end
             hist_occ_events(C,S,[1,2,3,4,5,6],6)
             % TODO
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        case "HECTOR"
            fprintf("METHODE = %s\n",method);
            fprintf("Utiliser la fonction : run_hector(file,N,sv_folder,j_start,j_stop)\navec:\n\tfile=%s\n\tN=%d\n\tsv_folder=%s\n",F.file,C.hector_N,F.hector_partial_save_folder(1))
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        case "LOAD"
            fprintf("METHODE = %s\n",method);
            tok_pr = ""; if (C.Pos_rng.active), tok_pr = "_PR"; end
            save_name = strcat(F.save_folder,"pollux_",F.file,"_sim_",num2str(C.pollux_N),"_",C.pollux_case,"G",tok_pr);
            load(save_name); 
            print_phantom_info(P,F,S);
            write_castordata(F,S);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        case "REWRITE_CASTOR_DATA"
            fprintf("METHODE = %s\n",method)
            tok_pr = ""; if (C.Pos_rng.active), tok_pr = "_PR"; end
            save_name = strcat(F.save_folder,"pollux_tracker_",F.file,"_sim_",num2str(C.pollux_N),"_",C.pollux_case,"G",tok_pr); 
            fprintf("LOAD: %s \n",save_name); 
            t_start = tic(); 
            load(save_name); 
            t_stop = toc(t_start); 
            fprintf("Time to load %s\n\t t = %g (s)\n",save_name,t_stop); 
            if (S.terminal)
                 print_phantom_info(P,F,S);
                 if (C.pollux_N < 1000)
                    n_msg=C.pollux_N; 
                 else
                     e=floor(log10(C.pollux_N)); f=floor(C.pollux_N/(10^e));
                     n_msg=sprintf("%dE%d",f,e);
                 end
                 tok_pr = ""; if (C.Pos_rng.active), tok_pr = "_PR"; end
                 msg = C.method+"_C"+C.pollux_case+"_N"+n_msg+tok_pr;
                 fprintf("CREATE CASTOR DATA FOR %s\n",msg);
                 t_start = tic();
                 write_castordata(F,S,msg);
                 t_stop = toc(t_start);
            end
        otherwise 
            warning("OPTION NON DEFINIE !!!")
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%









% if (false)
% figure;
%     hold on; box on; grid on;
%     plot(P.lambda)
%     plot(S.results.lambda)
%     legend(strcat("P.lambda = ",num2str(sum(P.lambda))),strcat("S.lambda = ",num2str(sum(S.results.lambda))));
% end
% 
% if (exist('S'))
%     % figure;
%     %     hold on; box on; grid on;
%     %     plot(P.lambda,'b-');
%     %     plot(S.lambda,'r--');
%     %     legend('P.lambda','S.lambda')
%     % hold off
% end
