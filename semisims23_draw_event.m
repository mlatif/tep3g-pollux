% 
clear all 
close all
%load("semi_sims23.mat"); 
%draw_event(X,P,S)
%N = 155     % 1G511
%N = 45      % 1G1157
%N = 10      % 2GLOR
%N = 161     % 2GMIX
% N = 17      % 3G
%draw_unique_event(X,P,S,N)

%load("CSI_DATA.mat"); 
%draw_event(X,P,S); 
%N = 17; 
%draw_step_by_step(X,P,S,N)


load("pollux_cortex_sim_1000_3G_Pcount.mat")
filter = "5"; 
%draw_event(X,P,S,filter)
L_selected = [
    136,6,171,293,306,108,123,243,257,75,89,533,19,38,99,67,157,433,600   
];
L_selected = [136, 257, 89, 38, 433]

for (n=1:length(L_selected))
    draw_unique_event(X,P,S,L_selected(n),'png')
    draw_unique_event(X,P,S,L_selected(n),'eps')
end


function [] = draw_event(X,P,S,filter)
    close all; 
    draw_xemis(X,P,true); 
    draw_list = []; 
    % 
    [V0f,V0b,V1,V2,V0p,V1p,V2p,V0s,V1s,V2s,E1a,E2a] = reset_interaction_point();
    liste_3g = find(S.export.chr=="1");
    for (n = 1:length(S.export.chr))
        disp(n)
        c = S.export.chr(n); 
        %%%%%%%%%%%%%%%%%%%%%%%%%
        M = S.results.M(n,:); 
        M_pr = S.results.M_pr(n,:); 
        draw_list = draw_store_pts(draw_list,M,"*","k",25);
        %draw_list = draw_store_pts(draw_list,M_pr,"*","#FF8243",20);
        if (c == filter)
            switch c
                case "1"
                    disp("CASE 1")
                    V1s = S.export.data(n,S.rng.V1s_rng);
                    V2s = S.export.data(n,S.rng.V2s_rng);
                    E1b = S.export.data(n,S.rng.E1b_rng);
                    E0b = 0.511;
                    betab = acos(1-(E1b*X.mec2/(E0b*(E0b-E1b))));    % disp(rad2deg(beta))

                    %pts = [V1p;V1p + 250*axe_csr];  % !!!
                    %p = plot3(pts(:,1), pts(:,2), pts(:,3),Color="k",LineStyle="-",LineWidth=1);
                    z_f = 0;
                    draw_list = cone(draw_list,X,P,V1s,V2s,betab,z_f,'b');
                    test = "";
                case "2"
                    disp("CASE 2")
                    V1p = S.export.data(n,S.rng.V1p_rng);
                    V2p = S.export.data(n,S.rng.V2p_rng);
                    E1a = S.export.data(n,S.rng.E1a_rng);
                    E0a = 1.157;
                    betaa = acos(1-(E1a*X.mec2/(E0a*(E0a-E1a))));    % disp(rad2deg(beta))
                    z_f = 0;
                    draw_list = cone(draw_list,X,P,V1p,V2p,betaa,z_f,'r');
                    t_csr =  200;
                case "3"
                    disp("CASE 3")
                    V1 = S.export.data(n,S.rng.V1_rng);
                    V2 = S.export.data(n,S.rng.V2_rng);
                    [draw_list]=lor(draw_list,X,P,V1,V2,"b");

                    tez = ""
                case "4"
                    disp("CASE 4")
                    V1s = S.export.data(n,S.rng.V1s_rng);
                    V2s = S.export.data(n,S.rng.V2s_rng);
                    E1b = S.export.data(n,S.rng.E1b_rng);
                    E0b = 0.511;
                    betab = acos(1-(E1b*X.mec2/(E0b*(E0b-E1b))));    % disp(rad2deg(beta))
                   z_f = 0;
                    draw_list = cone(draw_list,X,P,V1s,V2s,betab,z_f,'b');
                    V1p = S.export.data(n,S.rng.V1p_rng);
                    V2p = S.export.data(n,S.rng.V2p_rng);
                    E1a = S.export.data(n,S.rng.E1a_rng);
                    E0a = 1.157;
                    betaa = acos(1-(E1a*X.mec2/(E0a*(E0a-E1a))));    % disp(rad2deg(beta))
                    z_f = 0;
                    draw_list = cone(draw_list,X,P,V1p,V2p,betaa,z_f,'r');
                    t_csr =  200;


                case "5"
                    disp("CASE 5")
                    V1 = S.export.data(n,S.rng.V1_rng);
                    V2 = S.export.data(n,S.rng.V2_rng);
                    [draw_list]=lor(draw_list,X,P,V1,V2,"b");
                    V1p = S.export.data(n,S.rng.V1p_rng);
                    V2p = S.export.data(n,S.rng.V2p_rng);
                    E1a = S.export.data(n,S.rng.E1a_rng);
                    E0a = 1.157;
                    betaa = acos(1-(E1a*X.mec2/(E0a*(E0a-E1a))));    % disp(rad2deg(beta))
                    z_f = 0;
                    draw_list = cone(draw_list,X,P,V1p,V2p,betaa,z_f,'r');
                    test = "";
                    %                 axe_lor = (V2-V1)/norm(V2-V1,2);
                    %                 axe_csr = (V1p-V2p)/norm(V1p-V2p,2);
                    %                 beat(X,V1,axe_lor,V1p,axe_csr,betaa)

            end
            fprintf("N = %d \t C = %s\n",n,c)
            pause()
            test = ""; 
        end

        

        erase_all_elts(draw_list); 
    
    end

    
    

    test = ""; 
end


function [] = draw_unique_event(X,P,S,N,ext)
    save_fig = false; 
    if (nargin > 4)
        save_fig = true; 
    end
    close all; 
    z_f = 0;
    draw_xemis(X,P,true); 
    set(gca,'color','none');
    plot3(X.O(1),X.O(2),X.O(3),'kx'); 
        plotcube(P.T,X.O-P.T/2,0.05,[0,0,0])
    draw_list = []; 
    % 
    [V0f,V0b,V1,V2,V0p,V1p,V2p,V0s,V1s,V2s,E1a,E2a] = reset_interaction_point();
    %liste_3g = find(S.export.chr=="1");
     c = S.export.chr(N); 
        %%%%%%%%%%%%%%%%%%%%%%%%%
        M = S.results.M(N,:); 
        M_pr = S.results.M_pr(N,:); 
        draw_list = draw_store_pts(draw_list,M,"*","k",25);
        %draw_list = draw_store_pts(draw_list,M_pr,"*","#FF8243",20);

        switch c
            case "1"
                class_name = S.Classif(str2num(c)+1,1);
                V1s = S.export.data(N,S.rng.V1s_rng);
                V2s = S.export.data(N,S.rng.V2s_rng);
                E1b = S.export.data(N,S.rng.E1b_rng);
                E0b = 0.511;
                betab = acos(1-(E1b*X.mec2/(E0b*(E0b-E1b))));    % disp(rad2deg(beta))
                
                %pts = [V1p;V1p + 250*axe_csr];  % !!! 
                %p = plot3(pts(:,1), pts(:,2), pts(:,3),Color="k",LineStyle="-",LineWidth=1);
                z_f = 0; 
                draw_list = cone(draw_list,X,P,V1s,V2s,betab,z_f,'b'); 
                tesqt = ""; 
            case "2"
                class_name = S.Classif(3,1);
                V1p = S.export.data(N,S.rng.V1p_rng);
                V2p = S.export.data(N,S.rng.V2p_rng);
                E1a = S.export.data(N,S.rng.E1a_rng);
                E0a = 1.157;
                betaa = acos(1-(E1a*X.mec2/(E0a*(E0a-E1a))));    % disp(rad2deg(beta))
                z_f = 0; 
                draw_list = cone(draw_list,X,P,V1p,V2p,betaa,z_f,'r'); 
                 t_csr =  200;
            case "3"
                class_name = S.Classif(4,1);
                V1 = S.export.data(N,S.rng.V1_rng);
                V2 = S.export.data(N,S.rng.V2_rng);
                [draw_list]=lor(draw_list,X,P,V1,V2,"b");
                test = "";
            case "4"
                class_name = S.Classif(5,1);
                V1s = S.export.data(N,S.rng.V1s_rng);
                V2s = S.export.data(N,S.rng.V2s_rng);
                E1b = S.export.data(N,S.rng.E1b_rng);
                E0b = 0.511;
                betab = acos(1-(E1b*X.mec2/(E0b*(E0b-E1b))));    % disp(rad2deg(beta))
                z_f = 0; 
                draw_list = cone(draw_list,X,P,V1s,V2s,betab,z_f,'b'); 
                V1p = S.export.data(N,S.rng.V1p_rng);
                V2p = S.export.data(N,S.rng.V2p_rng);
                E1a = S.export.data(N,S.rng.E1a_rng);
                E0a = 1.157;
                betaa = acos(1-(E1a*X.mec2/(E0a*(E0a-E1a))));    % disp(rad2deg(beta))
                z_f = 0; 
                draw_list = cone(draw_list,X,P,V1p,V2p,betaa,z_f,'r'); 
                 t_csr =  200;

                
            case "5"
                class_name = S.Classif(6,1);
                V1 = S.export.data(N,S.rng.V1_rng);
                V2 = S.export.data(N,S.rng.V2_rng);
                [draw_list]=lor(draw_list,X,P,V1,V2,"b");
                                V1p = S.export.data(N,S.rng.V1p_rng);
                V2p = S.export.data(N,S.rng.V2p_rng);
                E1a = S.export.data(N,S.rng.E1a_rng);
                E0a = 1.157;
                betaa = acos(1-(E1a*X.mec2/(E0a*(E0a-E1a))));    % disp(rad2deg(beta))
                z_f = 0; 
                draw_list = cone(draw_list,X,P,V1p,V2p,betaa,z_f,'r'); 
                test = "";
%                 axe_lor = (V2-V1)/norm(V2-V1,2); 
%                 axe_csr = (V1p-V2p)/norm(V1p-V2p,2);
%                 beat(X,V1,axe_lor,V1p,axe_csr,betaa)

        end

%        erase_all_elts(draw_list); 
        
      
        
        if (save_fig)
            name_fig = strcat(class_name,"_N",num2str(N)); 
            if (ext ~= "eps")   
                saveas(gcf,name_fig,ext)
            else
                fprintf("Dans la console :\n\tprint -depsc %s\n",name_fig)
                test = ""; 
            end
        end
end


function [draw_list] = lor(draw_list,X,P,V1,V2,col_lor)
    draw_list = draw_store_pts(draw_list,V1,'*',col_lor,15) ;
    draw_list = draw_store_pts(draw_list,V2,'*',col_lor,15) ;
    pts = [V1;V2];
    [draw_list] = draw_store_lin(draw_list,V1,V2,col_lor,"-",2);
end



function [draw_list] = cone(draw_list,X,P,V1p,V2p,beta,z_f,col_cone)
    N = [0 0 1];
    Q = [0 0 z_f];
    t_axe =  100;
    t_csr =  175;

    axe_csr = sign(tan(beta))*(V1p-V2p)/norm(V1p-V2p,2);
%     if (dot(N,axe_csr)~=0)
%         t_p = -dot(N,(V1p-Q))/(dot(N,axe_csr));
%     else
%         t_p = inf;     % Le rayon et le plan sont parallèles
%     end
%     if (t_p>0), t_axe = t_p; end     
    draw_list = draw_store_pts(draw_list,V1p,'*',col_cone,15) ;
    draw_list = draw_store_pts(draw_list,V2p,'*',col_cone,15) ;

%     pts = [V1p;V1p + t_axe*axe_csr];  % !!!
%     p = plot3(pts(:,1), pts(:,2), pts(:,3),Color="k",LineStyle="-",LineWidth=1);
    
    [draw_list] = draw_store_lin(draw_list,V1p,(V1p + t_axe*axe_csr),col_cone,"-",0.015);
    [draw_list] = draw_store_lin(draw_list,V1p,V2p,col_cone,"-",0.015);
%     C = get_point(V1p,axe_csr,t_axe); 
%     draw_pts(C,".","r",5);
%    disp(C)
    for (o = 0:5:360)
        m = get_generatrix_unit_vector(V2p,V1p,beta,deg2rad(o),"0"); % !!!
       
%         if (dot(N,m)~=0)
%             t_p = -dot(N,(V1p-Q))/(dot(N,m));
%         else
%             t_p = inf;     % Le rayon et le plan sont parallèles
%         end
%         if (t_p>0), t_csr = t_p; end 

        I_csr = get_point(V1p,m,t_csr);
%         I_LIST = [I_LIST; get_point(V1p,m,t_p)]; 
        [draw_list] = draw_store_lin(draw_list,V1p,I_csr,col_cone,"-",0.015);
    end

end


function [] = beat(X,O,D,C,V,theta)
    t1 = 0; 
    t2 = 0; 
    CO = O-C; 
    fprintf("theta = %g (deg)\ncos(theta) = %g\nsign(tan(theta))=%g\n",rad2deg(theta),cos(theta),sign(tan(theta))) 
    fprintf("D.V =%g\nCO.V=%g\nD.CO=%g\nCO.CO=%g\n",dot(D,V),dot(CO,V),dot(D,CO),dot(CO,CO));
    
    a = (dot(D,V))^2 - cos(theta)^2;
    b = 2 * ( dot(D,V)*dot(CO,V) - dot(D,CO) * cos(theta)^2);
    c = (dot(CO,V))^2 - dot(CO,CO)*cos(theta)^2;
    delta = b^2-4*a*c;
    if (delta >0 )
       t1 = (-b-sqrt(delta))/(2*a) ;
       t2 = (-b+sqrt(delta))/(2*a) ;
       % But wait! We don’t have one cone but two, so we have to reject solutions that intersect with the shadow cone.
       % if theta < 90 => dot(P-C,V)>0
       fprintf("sign(tan(theta))*dot(get_point(O,D,t1)-C,V) > 0 ? %d \n",sign(tan(theta))*dot(get_point(O,D,t1)-C,V) > 0)
       if (sign(tan(theta))*dot(get_point(O,D,t1)-C,V) > 0 && is_in_FOV(X,get_point(O,D,t1),"X1") ) 
            X1 = O + t1*D;
            draw_pts(X1,"^","k",25);
       else 
           t1 = 0;
       end
       fprintf("sign(tan(theta))*dot(get_point(O,D,t2)-C,V) > 0 ? %d \n",sign(tan(theta))*dot(get_point(O,D,t2)-C,V) > 0)
       if (sign(tan(theta))*dot(get_point(O,D,t2)-C,V) > 0 && is_in_FOV(X,get_point(O,D,t2),"X2")) 
            X2 = O + t2*D;
            draw_pts(X2,"v","k",25);
       else 
           t2 = 0;
       end 
       

    elseif (delta == 0)
        t1 = -b/(2*a); 
        if (sign(tan(theta))*dot(get_point(O,D,t1)-C,V) > 0 && is_in_FOV(X,get_point(O,D,t1),"X1"))
            X1 = O + t1*D;
            draw_pts(X1,"^","k",25);
        end
    else
        
    end
    
    test ="";
end


function [ret] = is_in_FOV(X,V,name)
    verb=true;
    ret = false; 
    msg1 = ""; msg2 = ""; 
    if (nargin<3 || (nargin == 3 && name == ""))
        name = "";  verb = false;
    end
    x = V(1); y = V(2); z = V(3);
    if (abs(z)<X.Ax/2 && x^2+y^2 < X.Rin^2)
        ret = true; msg1 = "Ok"; 
    else 
        if (x^2+y^2 > X.Rin^2)
            msg1 = "x^2+y^2 > X.Rin^2 ";
        end
        if (abs(z)>X.Ax/2)
            msg2 = "abs(z)>X.Ax/2 ";
        end
    end
    if (verb)
        fprintf("%s in FOV ? %s %s \n",name,msg1,msg2)
    end

end 



function [f] = lcl_draw_xemis(X,P,ANIM)
        f = figure; 
        hold on;      box on;  % grid on; 
        % Simulateur en 3D 
        xlim([-X.Rin-10,+X.Rin+10]);        %xlabel('x');
        ylim([-X.Rin-10,+X.Rin+10]);         %ylabel('y');
        zlim([-ceil(X.Ax/2)-10,+ceil(X.Ax/2)+10]);  %zlabel('z');
        plot3(X.O(1),X.O(2),X.O(3),'kx'); 
        plotcube(P.T,X.O-P.T/2,0.005,[0,0,1])
        % Bords des detecteurs
        th = 0:pi/50:2*pi;
        % Bord intérieur en 3D
        xunit = X.Rin * cos(th) + X.O(1);    yunit = X.Rin * sin(th) + X.O(2); zunit = ones(1,length(xunit)); 
        plot3(xunit, yunit,0*zunit,'k'); axis equal;
        plot3(xunit, yunit,(-X.Ax/2)*zunit,'k'); axis equal;
        plot3(xunit, yunit,(+X.Ax/2)*zunit,'k'); axis equal;
        % Bord exterieur en 3D
        xunit = X.Rout * cos(th) + X.O(1);    yunit = X.Rout * sin(th) + X.O(2); zunit = ones(1,length(xunit));
        plot3(xunit, yunit,0*zunit,'k'); axis equal;
        plot3(xunit, yunit,(-X.Ax/2)*zunit,'k'); axis equal;
        plot3(xunit, yunit,(+X.Ax/2)*zunit,'k'); axis equal;
        
%             xline(0,"k--");
%             yline(0,"k--");
      set(gca,'color','none')
end


function [] = draw_step_by_step(X,P,S,N)
    close all; 
        z_f = 0;
    draw_list = [];
    sketch_list = [];

    M = S.results.M(N,:);
    M_pr = S.results.M_pr(N,:);
    Dir.D1 = S.results.D1(N,:);
    Dir.D2 = S.results.D2(N,:);
    E1a = S.export.data(N,S.rng.E1a_rng);
    E0a = 1.157;
    betaa = acos(1-(E1a*X.mec2/(E0a*(E0a-E1a))));
    csr_gen_dir_p = S.results.csr_gen_dir(N,:);
    [t1_min_p,~] = get_intersect_coord_Rin(X,M,Dir.D1);
    [t1_max_p] = get_intersect_coord_Rout(X,M,Dir.D1,t1_min_p);
    %%%
     lcl_draw_xemis(X,P,true); 
    %%%
    lcl_draw_xemis(X,P,true); 
    draw_list = draw_store_pts(draw_list,M,'x','k',25);
    %
    lcl_draw_xemis(X,P,true); 
    draw_list = draw_store_pts(draw_list,M,'x','k',25);
    draw_list = draw_store_pts(draw_list,M_pr,'*','b',25);
    % 
    lcl_draw_xemis(X,P,true); 
    draw_list = draw_store_pts(draw_list,M,'x','k',25);
    draw_list = draw_store_pts(draw_list,M_pr,'*','b',25);
    i1 = get_point(M,Dir.D1,t1_max_p);
    sketch_list = draw_store_lin(sketch_list,M,i1,'#808080',"--",2);
    E0 = 0.511;
    [t_min_f,t_min_b] = get_intersect_coord_Rin(X,M_pr,Dir.D1);
    [succ_lor_f,V0f,V1,E1b,t_V1_f,t_min_f,t_max_f] = create_half_lor(X,M_pr,Dir.D2,t_min_f,E0,"");
    [succ_lor_b,V0b,V2,E1b,t_V1_b,t_min_b,t_max_b] = create_half_lor(X,M_pr,Dir.D2,t_min_b,E0,"");
    i1_f = get_point(M_pr,Dir.D2,t_max_f);
    i1_b = get_point(M_pr,Dir.D2,t_max_b);
    V1p = S.export.data(N,S.rng.V1p_rng);
    t2_hat = get_intersect_coord_csr(X,V1p,csr_gen_dir_p);
    i2 = get_point(V1p,csr_gen_dir_p,t2_hat);
    %sketch_list = draw_store_lin(sketch_list,V1p,i2,'#808080',"--",1);
    sketch_list = draw_store_lin(sketch_list,M,i1_f,'#808080',"--",2);
    sketch_list = draw_store_lin(sketch_list,M,i1_b,'#808080',"--",2);
    %
    lcl_draw_xemis(X,P,true);
    draw_list = draw_store_pts(draw_list,M,'x','k',25);
    draw_list = draw_store_pts(draw_list,M_pr,'x','b',25);
    sketch_list = draw_store_lin(sketch_list,M,i1,'#808080',"--",2);
    %sketch_list = draw_store_lin(sketch_list,V1p,i2,'#808080',"--",2);
    sketch_list = draw_store_lin(sketch_list,M,i1_f,'#808080',"--",2);
    sketch_list = draw_store_lin(sketch_list,M,i1_b,'#808080',"--",2);

    V1 = S.export.data(N,S.rng.V1_rng);
    V2 = S.export.data(N,S.rng.V2_rng);

    draw_list = draw_store_pts(draw_list,V1,'*','b',25) ;
    draw_list = draw_store_pts(draw_list,V2,'x','b',25) ;

    lcl_draw_xemis(X,P,true);
    draw_list = draw_store_pts(draw_list,M,'x','k',25);
    draw_list = draw_store_pts(draw_list,M_pr,'x','b',25);
    sketch_list = draw_store_lin(sketch_list,M,i1,'#808080',"--",2);
    sketch_list = draw_store_lin(sketch_list,V1p,i2,'#808080',"--",2);
    sketch_list = draw_store_lin(sketch_list,M,i1_f,'#808080',"--",2);
    sketch_list = draw_store_lin(sketch_list,M,i1_b,'#808080',"--",2);  
    V1 = S.export.data(N,S.rng.V1_rng);
    V2 = S.export.data(N,S.rng.V2_rng);
    draw_list = draw_store_pts(draw_list,V1,'*','b',25) ;
    draw_list = draw_store_pts(draw_list,V2,'*','b',25) ;
    V1p = S.export.data(N,S.rng.V1p_rng);
    V2p = S.export.data(N,S.rng.V2p_rng);
    draw_list = draw_store_pts(draw_list,V1p,'*','r',25) ;
    draw_list = draw_store_pts(draw_list,V2p,'*','r',25) ;

    lcl_draw_xemis(X,P,true);
    [draw_list]=lor(draw_list,X,P,V1,V2,"b");
    draw_list = cone(draw_list,X,P,V1p,V2p,betaa,z_f,'r');


    test =""; 
end