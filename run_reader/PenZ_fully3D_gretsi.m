clear all
close all
set(0,'DefaultFigureWindowStyle','docked') 
% ---
% ULYSSE 
% P_safe = [
% 0.511759	0.1540339	0.2493787	0.001043	0.0830540	0.000732	;
% 0.473418	0.1665013	0.2473910	0.006134	0.1011316	0.005425	;
% 0.436452	0.1734139	0.2425608	0.015859	0.1162251	0.015489	;
% 0.399996	0.1756207	0.2354432	0.030019	0.1278987	0.031022	;
% 0.364459	0.1735689	0.2263420	0.047805	0.1362430	0.051582	;
% 0.330984	0.1681461	0.2160742	0.067501	0.1416418	0.075653	;
% 0.300769	0.1606666	0.2057022	0.087160	0.1445457	0.101156	;
% 0.274778	0.1523794	0.1959864	0.105182	0.1458373	0.125837	;
% 0.253682	0.1444771	0.1875446	0.120524	0.1461353	0.147637	;
% 0.237689	0.1375647	0.1810298	0.132569	0.1460925	0.165055	;
% 0.226988	0.1323756	0.1766290	0.141032	0.1459067	0.177069	;
% 0.221648	0.1294915	0.1745173	0.145491	0.1456998	0.183152	;
% 0.221674	0.1294967	0.1744863	0.145486	0.1457285	0.183128	;
% 0.227015	0.1323762	0.1766662	0.141048	0.1458618	0.177032	;
% 0.237687	0.1375611	0.1810697	0.132587	0.1460553	0.165040	;
% 0.253641	0.1444399	0.1876034	0.120506	0.1461561	0.147653	;
% 0.274770	0.1523989	0.1959653	0.105223	0.1458205	0.125822	;
% 0.300742	0.1606437	0.2057235	0.087179	0.1445621	0.101150	;
% 0.330968	0.1681662	0.2160881	0.067502	0.1416213	0.075654	;
% 0.364464	0.1735678	0.2263197	0.047813	0.1362387	0.051597	;
% 0.400021	0.1756050	0.2354353	0.030028	0.1278871	0.031023	;
% 0.436447	0.1734202	0.2425995	0.015862	0.1161870	0.015485	;
% 0.473414	0.1665184	0.2473764	0.006133	0.1011400	0.005418	;
% 0.511789	0.1540497	0.2493663	0.001046	0.0830166	0.000732	
% 
% ];
folder_to_save_fig ="FIG_EPS";
if (~isfolder("FIG_EPS")); mkdir(folder_to_save_fig); end 
WITH_LEG = false; 
export_folder = "DATA_PARSED"; 
name = "ulysse"; 
V_max = 6; 
tok = 'wipr'; 
suffix = "_LOR0_COR0"; 


load(strcat(export_folder,"/hector_parsed_",name,"_"+tok+"_v1-v",num2str(V_max),"",suffix)); 
P0G =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,1); 
P1G511 =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,2); 
P1G1157 =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,3);
P2GLOR =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,4);
P2GMIX =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,5);
P3G =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,6);

figure; 
    hold on; box on; %grid on;     
    xlim([-120,+120])
    ylim([0,max(P0G)+0.005])
    %plot(P0G)
    h = 240;
    xticks(-(h)/2:40:+(h)/2);
    c = -115:10:+115;
    c = linspace(-120,+120,24); 
    c = unique(P.Rc(:,3)) + P.V(3)/2; 
    xticks(c);
    plot(c,P0G,'DisplayName','${0\gamma}^{}_{}$','Color',COLOR_LIB(1,:),'LineWidth',2,'LineStyle','-')
    plot(c,P1G511,'DisplayName','${1\gamma}^{511}_{\textrm{COR}}$','LineWidth',2,'LineStyle','-','Color',COLOR_LIB(2,:))
    plot(c,P1G1157,'DisplayName','${1\gamma}^{1157}_{\textrm{COR}}$','LineWidth',2,'LineStyle','-','Color',COLOR_LIB(3,:))
    plot(c,P2GLOR,'DisplayName','${2\gamma}^{}_{\textrm{LOR}}$','LineWidth',2,'LineStyle','-','Color',COLOR_LIB(4,:))
    plot(c,P2GMIX,'DisplayName','${2\gamma}^{}_{\textrm{COR}}$','LineWidth',2,'LineStyle','-','Color',COLOR_LIB(5,:))
    plot(c,P3G,'DisplayName','${3\gamma}^{}_{}$','LineWidth',2,'LineStyle','-','Color',COLOR_LIB(6,:))
    if (WITH_LEG),  legend('Location','north','Interpreter','latex','FontSize',12,'NumColumns',5); legend boxoff  ;  end 
     

    xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16)
    %xlabel("Axiale position $z$ (mm)",'Interpreter','latex','FontSize',16)
    % 
    ylabel("Sensibilit\'e moyenne (\%)",'Interpreter','latex','FontSize',16)
    %ylabel("Mean sensitivity (\%)",'Interpreter','latex','FontSize',16)
    
    %set(gca,'DataAspectRatioMode','manual')
    disp("GERE L'ASPECT RATIO")
    pbaspect([2 1 1])   % https://fr.mathworks.com/help/matlab/creating_plots/aspect-ratio-for-2-d-axes.html
  
    saveas(gcf,strcat(folder_to_save_fig,"/","ZProf_FR","_",tok,'.eps'),'epsc');
