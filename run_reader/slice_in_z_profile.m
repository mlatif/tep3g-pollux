clear all; 
close all; 
set(0,'DefaultFigureWindowStyle','docked') 

addpath('../methods/'); 


export_folder = "DATA_PARSED"; 
folder_to_save_fig ="FIG_EPS";
if (~isfolder("FIG_EPS")); mkdir(folder_to_save_fig); end 



% MODE 1 : distribution sur les points d'interets -> génération de 3 plots
% MODE 2 : Slice par pas constant 
% -------------------------------------------------------------------------
name = "ulysse"; 
suffix = "_LOR0_COR0"; 
V_max = 6; 
K_max = 6; 
save_fig = true; 
tok = 'wipr'; 
ind_fix = 3;    % ['x','y','z'];  
ind_list = ['x','y']; 


K = 6; 
%%% PARAM MODE 1
crit = "min"; 
%crit = "max";
%crit = "1"; 
%%% PARAM MODE 2
extra_slice_ind = 0; 
%%% PARAM MODE 3
SLICE_LIST = [1,3,5,7,9,11,12];
% -------------------------------------------------------------------------
MODE = 2; 
% -------------------------------------------------------------------------
latex_legend = ["0\gamma","1\gamma_{511}","1\gamma_{1157}","2\gamma_{\textrm{LOR}}","2\gamma_{\textrm{COR}}","3\gamma"];
sub_latex_legend = latex_legend(2:end); 

file_name = strcat(export_folder,"/hector_parsed_",name,"_"+tok+"_v1-v",num2str(V_max),suffix); 
BASE_NAME_FIG = "cassandre_results_ulysse_"+tok; 
COL_LIB = load(file_name).COLOR_LIB; 
P = load(file_name).P;
DET = load(file_name).DETECT_PAR_CLASSE;
EMI = load(file_name).EMISSIO_PAR_VOXEL;
% -------------------------------------------------------------------------
profils = zeros(P.D(3),K_max);
for (i = 1:6)
    profils(:,i) = get_ProfEnZ(P,EMI,DET,i); 
end


switch MODE
    case 1
        sub_mat = profils(:,2:end); 
        max_all_classe = max(sub_mat,[],'all'); 
        min_all_classe = min(sub_mat,[],'all'); 
        max_one_classe = max(profils(:,K),[],'all'); 
        min_one_classe = min(profils(:,K),[],'all'); 
        % Indices pour la coupe demandée: 
        switch crit
            case "max"
                [val_slice,ind_slice] = max(profils(:,K));
            case "min"
                [val_slice,ind_slice] = min(profils(:,K));
            otherwise
                ind_slice=str2double(crit);
                val_slice = profils(ind_slice,K);
                if (ind_slice <= 0 || ind_slice>= P.D(3))
                    error("ind_slice = %d non valide !!",ind_slice);
                end
        end
        ind_voxel_for_slice = find(P.Ic(:,3)==ind_slice);
        c = unique(P.Rc(:,3)) + P.V(3)/2; 
        VIZ_mat = zeros(P.D(1),P.D(2));
        for (j = 1:length(ind_voxel_for_slice))
            vj = ind_voxel_for_slice(j); 
            [x,y,~] = get_axes_voxel_idx(P,j); 
            VIZ_mat(x,y) = DET(vj,K)./EMI(vj);
        end
        % -----------------------------------------------------------------
        F1 = figure;
        hold on; box on; axis square
        xlim([-120,+120])
        c = unique(P.Rc(:,3)) + P.V(3)/2;
        plot(c,profils(:,K),'k--','Marker','x','MarkerFaceColor','r','MarkerEdgeColor','r')
        xticks(c);
        %xlabel("$z_{CV}$",'Interpreter','latex','FontSize',16)
        %title("$s_{j}^{k}$",'Interpreter','latex','FontSize',16)
        xline(c(ind_slice),"b--")
        yline(val_slice,"b--",num2str(val_slice,"%4.2e"))
        NAME_FIG = BASE_NAME_FIG+"_K"+num2str(K)+"_slice_crit"+crit+"_FIG1";
        if (save_fig), saveas(gcf,strcat(folder_to_save_fig,"/",NAME_FIG,'.eps'),'epsc'); end
        % -----------------------------------------------------------------
        F2 = figure;  
            imagesc(VIZ_mat'), axis square; 
            if (K~=1), 
                colormap(F2,bone);
            else , 
                %colormap(F2,flipud(bone)); 
                colormap(F2,bone);
            end
            c = unique(P.Rc(:,3)) + P.V(3)/2;
            %str = sprintf("Slice $%d$, $z_{cv} = %g$",ind_slice,c(ind_slice)); 
            %title(str,'Interpreter','latex','FontSize',16); 
            xlabel("$x$",'Interpreter','latex','FontSize',16); ylabel("$y$",'Interpreter','latex','FontSize',16); 
            colorbar
            caxis([min_one_classe, max_one_classe]);
        NAME_FIG = BASE_NAME_FIG+"_K"+num2str(K)+"_slice_crit"+crit+"_FIG2";
        if (save_fig), saveas(gcf,strcat(folder_to_save_fig,"/",NAME_FIG,'.eps'),'epsc'); end
        % -----------------------------------------------------------------
        F3 = figure;
        imagesc(VIZ_mat'), axis square;
        if (K~=1)
            colormap(F3,"turbo") ;
        else
            % colormap(F3,flipud(turbo));
            colormap(F3,turbo);
        end
        c = unique(P.Rc(:,3)) + P.V(3)/2;
        str = sprintf("Slice $%d$, $z_{cv} = %g$",ind_slice,c(ind_slice));
        %title(str,'Interpreter','latex','FontSize',16);
        xlabel("$x$",'Interpreter','latex','FontSize',16); ylabel("$y$",'Interpreter','latex','FontSize',16);
        colorbar
        caxis([min_all_classe, max_all_classe]);
        NAME_FIG = BASE_NAME_FIG+"_K"+num2str(K)+"_slice_crit"+crit+"_FIG3";
        if (save_fig), saveas(gcf,strcat(folder_to_save_fig,"/",NAME_FIG,'.eps'),'epsc'); end
        



% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
    case 2
        sub_mat = profils(:,2:end); 
        [val_max_slice,ind_max_slice] = max(profils(:,K));
        [val_min_slice,ind_min_slice] = min(profils(:,K));
        F1 = figure;
        hold on; box on; axis square
        xlim([-120,+120])
        c = unique(P.Rc(:,3)) + P.V(3)/2;
        plot(c,profils(:,K),'k--','Marker','x','MarkerFaceColor','k','MarkerEdgeColor','k')
        xticks(c);
        xline(c(ind_max_slice),"b--")
        yline(val_max_slice,"b--",num2str(val_max_slice,"%4.3e"))
        xline(c(ind_min_slice),"r--")
        yline(val_min_slice,"r--",num2str(val_min_slice,"%4.3e"))
        if (extra_slice_ind ~= 0)
            val_slice = profils(extra_slice_ind,K);
            xline(c(extra_slice_ind),"m--")
            yline(val_slice,"m--",num2str(val_slice,"%4.3e"),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top')
        end
        NAME_FIG = BASE_NAME_FIG+"_K"+num2str(K)+"_slice_all_vals";
        if (save_fig), saveas(gcf,strcat(folder_to_save_fig,"/",NAME_FIG,'.eps'),'epsc'); end
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------

    case 3
        c = unique(P.Rc(:,3)) + P.V(3)/2;
        sub_mat = profils(:,2:end); 
        max_all_classe = max(sub_mat,[],'all'); 
        min_all_classe = min(sub_mat,[],'all'); 
        % -----------------------------------------------------------------
        fprintf("min,max K=1 = [%2.4e,%2.4e]\n",min(profils(:,K)), max(profils(:,K))); 
        fprintf("min,max = [%2.4e,%2.4e]\n",min_all_classe,max_all_classe); 
        % -----------------------------------------------------------------
        for (K = 1:K_max)
            for (s = 1:length(SLICE_LIST))
                ind_voxel_for_slice = find(P.Ic(:,3)==SLICE_LIST(s));
                %fprintf("K = %g \t slice n° i = %d~> z_c = %d \n",K,s,c(s))
                VIZ_mat = zeros(P.D(1),P.D(2));
                for (j = 1:length(ind_voxel_for_slice))
                    vj = ind_voxel_for_slice(j); 
                    [x,y,~] = get_axes_voxel_idx(P,j); 
                    VIZ_mat(x,y) = DET(vj,K)./EMI(vj);
                end
                figure;
                imagesc(VIZ_mat'), axis square;
                xlabel("$x$",'Interpreter','latex','FontSize',16); ylabel("$y$",'Interpreter','latex','FontSize',16);
                colorbar; colormap("turbo")
                if (K~=1)
                    caxis([min_all_classe, max_all_classe]);
                else
                    caxis([min(profils(:,K)), max(profils(:,K))]);
                end
                NAME_FIG = BASE_NAME_FIG+"_K"+num2str(K)+"_slice_"+num2str(s);
                if (save_fig), saveas(gcf,strcat(folder_to_save_fig,"/",NAME_FIG,'.eps'),'epsc'); end
                close all; 
            end
            
        end
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
end






% -------------------------------------------------------------------------
% -------------------------------------------------------------------------    

% -------------------------------------------------------------------------    

% -------------------------------------------------------------------------    

% -------------------------------------------------------------------------    

% -------------------------------------------------------------------------    
% -------------------------------------------------------------------------    
% -------------------------------------------------------------------------    
%sgtitle(strcat('$',latex_legend(K),'$'),'Interpreter','latex','FontSize',20)











