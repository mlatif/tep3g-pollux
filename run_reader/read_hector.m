% READ_HECTOR: 
% Méthode dédiée à la lectuée des résultats générés avec hector et obtenir les cartes de sensibilité. 
% Voir la fonction run_hector pour voir comment le tout fonctionne.

clear all
close all 
clc; 
addpath("../methods/")
set(0,'DefaultFigureWindowStyle','docked') 
% --- PATH VERS LES FICHIERS
%path = "achille_simu/";
path = "../ulysse_without_pr_simu/"; name = "ulysse"; PR_active = false;J = 4704; ext_phantom = ""; B = 10; V = 6;
%path = "../ulysse_simu/";  name = "ulysse"; PR_active = true;J = 4704; ext_phantom = ""; B = 10; V = 6;
%{


%}

path = "../hector_myrmidon_simu/"; PR_active = false; J = 1  ;  name = "myrmidon"; ext_phantom = "";  B = 10; V = 6;   % Parce que tu es stupide ...

path = "../hector_bernard_simu/"; PR_active = false; J = 1  ; name = "bernard"; ext_phantom = "";  B = 10; V = 10;

LOCAL = false; 
RUN_ALL = false; 
HIDE_LEG_FIG_PARSING = false; 
WITH_TOTAL = true;
SAVE_FIG = true;
SAVE_PARTIAL_FIG = false; 
NEW_VERSION = true;         % Correction du protocole de parsing 
export_folder = "DATA_PARSED"; 
if (~isfolder(export_folder) && SAVE_FIG); mkdir(export_folder); end 
folder_to_save_fig ="FIG_EPS";
if (~isfolder(folder_to_save_fig) && SAVE_FIG); mkdir(folder_to_save_fig); end 


M = 10000; 

NB_SLICE_en_Z = 24; 

COLOR_LIB = [
    0, 0.4470, 0.7410;          % 0G; 
    0.6350, 0.0780, 0.1840;     % 1G511
    0.9290, 0.6940, 0.1250;     % 1G1157
    0.4940, 0.1840, 0.5560;     % 2GLOR
    0.3010, 0.7450, 0.9330;     % 2GMIX
    0.4660, 0.6740, 0.1880;     % 3G
    0.8500, 0.3250, 0.0980;     % AUTRE
]; 

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------


    clc; 
    % -------------------------------------------------------------------------
    if NEW_VERSION
        folder = (name+"_v"+(1:V)+"_"+B+"/"+name+"_v"+(1:V));
    else
        folder = (name+"_v"+(1:V)+"_"+B+"_"+M+"/"+name+"_v"+(1:V));
    end
    
    test = "";
    if (LOCAL), path = ""; end
    save_name = strcat(path,"hector_parsed_",name,"_v1-v",num2str(V),".mat"); 
    tok = "wopr"; if (PR_active),tok = "wipr"; end 
    lcl_save_name = strcat(export_folder,"/hector_parsed_",name,"_",tok,"_v1-v",num2str(V),".mat"); 
    
    if (~isfile(save_name))
        % [hector_occ_config_count,hector_unit_config_count,N_total_hector,N_exp_hector,P,C,X,Ic] = parse(J,path,folder,LOR_is_N_1G511,COR_is_N_1G511);
        [BASE_NAME,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,TAB_DETECT_PAR_CLASSE,TPS_SIMU,PR_active,is_partial_file,Ic,C,P,X,cpt_emission,ECART_PeZ_COMP_2GMIX,ECART_PeZ_COMP_3G] = parse_hector(J,NB_SLICE_en_Z,path,name,ext_phantom,folder,PR_active,HIDE_LEG_FIG_PARSING,COLOR_LIB,SAVE_FIG,SAVE_PARTIAL_FIG,folder_to_save_fig);
        save(save_name,'BASE_NAME','DETECT_PAR_CLASSE','EMISSIO_PAR_VOXEL','TPS_SIMU','PR_active','is_partial_file','Ic','C','P','X','cpt_emission','ECART_PeZ_COMP_2GMIX','ECART_PeZ_COMP_3G','COLOR_LIB','TAB_DETECT_PAR_CLASSE'); 
        tok = "wopr"; if (C.Pos_rng.active),tok = "wipr"; end 
        lcl_save_name = strcat(export_folder,"/hector_parsed_",name,"_",tok,"_v1-v",num2str(V),".mat"); 
        save(lcl_save_name,'BASE_NAME','DETECT_PAR_CLASSE','EMISSIO_PAR_VOXEL','TPS_SIMU','PR_active','is_partial_file','Ic','C','P','X','cpt_emission','ECART_PeZ_COMP_2GMIX','ECART_PeZ_COMP_3G','COLOR_LIB','TAB_DETECT_PAR_CLASSE'); 
        
    elseif (isfile(save_name))
        load(save_name); 
    else
        error('PARSE MOI UN BON FICHIER ABRUTI %s ',save_name)
    end

    N_div = EMISSIO_PAR_VOXEL; 
        print_phantom_info(P);
    fprintf("[HECTOR]%s\n",save_name);
    fprintf("With PR ? %d \n",PR_active)
    if (P.J>1)
        DPC = sum(DETECT_PAR_CLASSE); 
        EPC = sum(EMISSIO_PAR_VOXEL);
    else
        DPC = DETECT_PAR_CLASSE;
        EPC = EMISSIO_PAR_VOXEL;
    end
    
    PROP = (DPC./EPC)*100;
    fprintf("---\n"); 
    fprintf("Classe\t0G\t1G511\t1G1157\t2GLOR\t2GCOR\t3G\n");
    fprintf("D\t"); fprintf(get_fmt(DPC),DPC); fprintf("\n"); 
    fprintf("P (%%)\t"); fprintf(get_fmt(PROP),PROP); fprintf("\n");
    fprintf("NB Emiss 3G per vox: %e.\n",EMISSIO_PAR_VOXEL(1)); 
    fprintf("NB Emiss 3G total: %e.\n",sum(EPC)); 
    
    fprintf("---\n"); 
    fprintf("DISTRIB - Batch\n");
    mean_tab = 100*V*mean(TAB_DETECT_PAR_CLASSE./EPC,1); 
    med_tab = 100*V*median(TAB_DETECT_PAR_CLASSE./EPC,1);
    std_tab = 100*V*std(TAB_DETECT_PAR_CLASSE./EPC,1); 
    fprintf("Classe\t0G\t1G511\t1G1157\t2GLOR\t2GCOR\t3G\n");
    fprintf("mean\t"); fprintf(get_fmt(mean_tab),mean_tab); fprintf("\n"); 
    fprintf("med\t"); fprintf(get_fmt(med_tab),med_tab); fprintf("\n"); 
    fprintf("std\t"); fprintf(get_fmt(std_tab),std_tab); fprintf("\n");
    fprintf("Temps moyen : %g (s) \n",mean(TPS_SIMU)); 


    fprintf("---\n"); 
    fprintf("Seulement détection (exit 0G)\n");
    if (P.J > 1)
        
    else
        DPC = DETECT_PAR_CLASSE(2:end); 
        EPC = EMISSIO_PAR_VOXEL - DETECT_PAR_CLASSE(1); 
    end
     PROP = (DPC./EPC)*100;

    fprintf("Classe\t1G511\t1G1157\t2GLOR\t2GCOR\t3G\n");
    fprintf("D\t"); fprintf(get_fmt(DPC),DPC); fprintf("\n"); 
    fprintf("P (%%)\t"); fprintf(get_fmt(PROP),PROP); fprintf("\n");
    save(strcat(export_folder,"/P_couple_",upper(name),"_",upper(tok),'.mat'),'BASE_NAME','DETECT_PAR_CLASSE','EMISSIO_PAR_VOXEL','PR_active','Ic','C','P','X','TAB_DETECT_PAR_CLASSE','med_tab','mean_tab','std_tab'); 

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
function [BASE_NAME,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,TAB_DETECT_PAR_CLASSE,TPS_SIMU,PR_active,is_partial_file,Ic,C,P,X,cpt_emission,ECART_PeZ_COMP_2GMIX,ECART_PeZ_COMP_3G] = parse_hector(J,NB_SLICE_en_Z,path,name,ext_phantom,folder,PR_active,HIDE_LEG_FIG_PARSING,CU,SAVE_FIG,SAVE_PARTIAL_FIG,folder_to_save_fig)
    
    P = []; X = []; C = []; 
    Ic = zeros(J,3);  
    DETECT_PAR_CLASSE = zeros(J,6);
    TAB_DETECT_PAR_CLASSE = zeros(length(folder),6);
    EMISSIO_PAR_VOXEL = zeros(J,1); 

    TPS_SIMU = zeros(J,length(folder)); 

    ECART_PeZ_COMP_2GMIX = zeros(length(folder),NB_SLICE_en_Z); 
    ECART_PeZ_COMP_3G = zeros(length(folder),NB_SLICE_en_Z); 
    once_Ic = true; 
    load_once = true; 
    is_partial_file = false; 

    cpt_emission = 0;
   
    latex_legend = ["0\gamma","1\gamma_{511}","1\gamma_{1157}","2\gamma_{\textrm{LOR}}","2\gamma_{\textrm{COR}}","3\gamma"];
    tok_woipr = "WOPR"; 
    if (PR_active)
        tok_woipr = "WIPR"; 
    end
    test = ""; 
    BASE_NAME = tok_woipr; 
    BASE_NAME_FIG = "read_hector_"+name+"_"+tok_woipr; 
    fileID = fopen(strcat(folder_to_save_fig,"/","listing_figure_",BASE_NAME_FIG,".txt"),'w+');
    
    
    fprintf(fileID,"[BASE] %s\n\n",BASE_NAME_FIG); 
    for (i=1:length(folder))
        % LECTURE DU FICHIER
        f = strsplit(folder(i),"/");
        fold = ext_phantom+f(1);    file = f(2);
        fprintf("FOLD = %s \t FILE = %s\n",fold,file);
        name_conf =  path+fold+"/hector_"+file+"_conf.mat";
        load(name_conf);
        fprintf(fileID,"[CONF] %s\n",name_conf); 
        once_cpt_emission = true; 
        % 
        if (load_once), P = load(name_conf).P; C = load(name_conf).C;  load_once = false;  end
        % EXTRACTION DES DONNEES PAR VOXEL 
        for (j=1:P.J)
            [ia,ib,ic] = get_axes_voxel_idx(P,j);
            if (once_Ic), Ic(j,:) = [ia,ib,ic]; once_Ic = false;   end % On le fait qu'une fois 
            tok = "wopr"; if (C.Pos_rng.active),tok = "wipr"; end 
            fprintf("[HECTOR|[%s]%s|%s] Voxel j = %d \t [%d,%d,%d]\n",ext_phantom,file,tok,j,ia,ib,ic); 
            % STR de stockage temporaire
            tmp_detect = zeros(1,6);
            tmp_emissi = 0; 
            tmp_tps_simu = zeros(1,C.hector_N_batch);
%             tmp_aggr_detect = zeros(1,4); 
%             tmp_aggr_emissi = 0; 
%             aggr_0G = 0; aggr_1g511 = 0; aggr_1g1157 = 0; aggr_2glor = 0; 
            % EXTRACTION DES DONNES PAR BATCH POUR UN VOXEL
            for (b = 1:C.hector_N_batch)
                 name_batch = strcat(path,fold,"/hector_",file,"_j",num2str(j),"_b",num2str(b),".mat");
                 if (false), fprintf("%s\n",name_batch); end

                 if (isfile(name_batch))   % sécurité supplémentaire
                    load(name_batch);
                    % -----------------------------------------------------
                    % Approche classique, tranquille, tout doux.
                    detect = h.occ_config_count; 
                    n_exp = h.N_exp; 
                    if (once_cpt_emission), cpt_emission = cpt_emission + n_exp; once_cpt_emission = false;  end
                    if (true)
                    fprintf("Classe\t0G\t1G511\t1G1157\t2GLOR\t2GMIX\t3G\n");
                    fprintf("D\t%d\t%d\t%d\t%d\t%d\t%d\n",detect(1),detect(2),detect(3),detect(4),detect(5),detect(6));
                    fprintf("n_exp = %d |sum(D) = %d\n",n_exp,sum(detect)); 
                    end
                    if (n_exp~=sum(detect)),error("PROBLEME : n_exp ~= sum(detect) \n"); end                    
                    tmp_detect = tmp_detect + detect;
                    tmp_emissi = tmp_emissi + n_exp;
                    tmp_tps_simu(b) = tps;  

                    % -----------------------------------------------------
                    % Approche par aggregation des classes unitaires
                    % [0G   1G511   1G1157  2GLOR   2GMIX   3G]
                    % [1    2       3       4       5       6 ]
                    % 0G
%                     aggr_0G = detect(1);
%                     % 1G511 = 1G511(2) + 2GLOR(4) + 2GMIX(5) + 3G(6)
%                     aggr_1g511 = ...
%                         + 0.5*detect(2) + 0.5*detect(5) ...
%                         + 1*detect(4) + 1*detect(6);                                         
%                     % 1G1157 = 1G115(3) + 2GMIX(5) + 3G(6)
%                     aggr_1g1157 =  ...
%                         1*detect(3) + 1*detect(5) + 1*detect(6);  
%                     % 2LOR = 2GLOR(4) + 3G(6)
%                     aggr_2glor =  ...
%                         1*detect(4) + 1*detect(6);    
%                     n_aggr_exp = aggr_0G+aggr_1g511+aggr_1g1157+aggr_2glor; 
%                     if (false)
%                     fprintf("Aggr\t0G\t1G511\t1G1157\t2GLOR\n");
%                     fprintf("D\t%d\t%d\t%d\t%d\n",aggr_0G,aggr_1g511,aggr_1g1157,aggr_2glor);
%                     fprintf("n_exp = %d |sum(D) = %d\n",n_aggr_exp,sum([aggr_0G,aggr_1g511,aggr_1g1157,aggr_2glor])); 
%                     end
                    %if (n_aggr_exp~=sum([aggr_0G,aggr_1g511,aggr_1g1157,aggr_2glor])),error("PROBLEME : n_aggr_exp ~= sum(detect_aggr) \n"); end                                 
                    % -----------------------------------------------------
                     %tmp_aggr_detect = tmp_aggr_detect + [aggr_0G,aggr_1g511,aggr_1g1157,aggr_2glor]; 
                    %tmp_aggr_emissi = tmp_aggr_emissi + n_aggr_exp; 
                    test = ""; 
                 else
                    is_partial_file = true; 
                 end


            end
            clc;
           fprintf("[HECTOR|[%s]%s|%s] Voxel j = %d \t [%d,%d,%d]\n",ext_phantom,file,tok,j,ia,ib,ic); 
            fprintf("Classe\t0G\t1G511\t1G1157\t2GLOR\t2GMIX\t3G\n");
            fprintf("D\t%d\t%d\t%d\t%d\t%d\t%d\n",tmp_detect(1),tmp_detect(2),tmp_detect(3),tmp_detect(4),tmp_detect(5),tmp_detect(6));
            fprintf("n_exp = %d |sum(D) = %d\n",tmp_emissi,sum(tmp_detect)); 
            %fprintf("Aggr\t0G\t1G511\t1G1157\t2GLOR\n");
            %fprintf("D\t%d\t%d\t%d\t%d\n",tmp_aggr_detect(1),tmp_aggr_detect(2),tmp_aggr_detect(3),tmp_aggr_detect(4));
            %fprintf("n_exp = %d |sum(D) = %d\n",tmp_aggr_emissi,sum(tmp_aggr_detect));

            DETECT_PAR_CLASSE(j,:) = DETECT_PAR_CLASSE(j,:) + tmp_detect; 
            EMISSIO_PAR_VOXEL(j,:) = EMISSIO_PAR_VOXEL(j,:) + tmp_emissi;
            %DETECT_AGGR_PAR_CLASSE(j,:) = DETECT_AGGR_PAR_CLASSE(j,:) + tmp_aggr_detect; 
            %EMISSIO_AGGR_PAR_VOXEL(j,:) = EMISSIO_AGGR_PAR_VOXEL(j,:) + tmp_aggr_emissi;
            
            % On stocke le tout dans un tableau; 
            TAB_DETECT_PAR_CLASSE(i,:) = TAB_DETECT_PAR_CLASSE(i,:) + tmp_detect;  
            
            TPS_SIMU(j,i) = mean(tmp_tps_simu); 


            test = "";
        end
        
        

        once_Ic = false; 
        is_savable_fig = false; 
        if (SAVE_PARTIAL_FIG)
            is_savable_fig = true; 
        else
            is_savable_fig = (i==length(folder));
        end
        
        % -----------------------------------------------------------------
        %save(strcat(BASE_NAME_FIG+'_partialparse'))
        % -----------------------------------------------------------------
        %{
        clf;
        % FIGURE 1 - DONNEES SYNTHESES SUR 4 SUBPLOT
        draw_4subfig_synth(C,P,cpt_emission,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,COR_is_N_1G511,LOR_is_N_1G511,latex_legend,CU)
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_synthese_with_leg";
        if (NAME_FIG~="" && SAVE_FIG && is_savable_fig)
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
            fprintf(fileID,"%s\n",NAME_FIG);
        end
        %}
        % -----------------------------------------------------------------
        if (P.J>1)
            
            clf;
            % FIGURE 2 - DONNEES PARSEE - LECTURE STANDARD
            HIDE_LEG = true;
            draw_std_read(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,latex_legend,CU,HIDE_LEG)
            NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_read_std";
            if (NAME_FIG~="" && SAVE_FIG && is_savable_fig)
                saveas(gcf,strcat(folder_to_save_fig,"/",NAME_FIG,'.eps'),'epsc');
                fprintf(fileID,"%s\n",NAME_FIG);
            end
    
            clf;
            HIDE_LEG = false;
            draw_std_read(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,latex_legend,CU,HIDE_LEG)
            NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_read_std_with_leg";
            if (NAME_FIG~="" && SAVE_FIG && is_savable_fig)
                saveas(gcf,strcat(folder_to_save_fig,"/",NAME_FIG,'.eps'),'epsc');
                fprintf(fileID,"%s\n",NAME_FIG);
            end
        end
        % -----------------------------------------------------------------
        %{
        clf;

        
        % -----------------------------------------------------------------
        clf; 
        % FIGURE 4 - DONNEES PARSEE - COMPARAISON 2GMIX
        HIDE_LEG = true; 
        draw_comp_2GMIX(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_equiv_2GMIX";
        if (NAME_FIG~="" && SAVE_FIG && is_savable_fig )
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
            fprintf(fileID,"%s\n",NAME_FIG);
        end  
        
        clf; 
        HIDE_LEG = false; 
        draw_comp_2GMIX(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_equiv_2GMIX_with_leg";
        if (NAME_FIG~="" && SAVE_FIG && is_savable_fig)
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
            fprintf(fileID,"%s\n",NAME_FIG);
        end    
        
        % -----------------------------------------------------------------
        clf; 
        % FIGURE 5 - DONNEES PARSEE - COMPARAISON 3G
        clf; 
        HIDE_LEG = true; 
        draw_comp_3G(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_equiv_3G";
        if (NAME_FIG~="" && SAVE_FIG && is_savable_fig)
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
            fprintf(fileID,"%s\n",NAME_FIG);
        end  
       
        clf; 
        HIDE_LEG = false; 
        draw_comp_3G(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_equiv_3G_with_leg";
        if (NAME_FIG~="" && SAVE_FIG && is_savable_fig)
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
            fprintf(fileID,"%s\n",NAME_FIG);
        end    
        %}
        % -----------------------------------------------------------------
        if (P.J>1)
        clf; 
        % FIGURE 6 - DONNES PARSEE - LECTURE STANDARD FORMAT PeZ
        clf;
        HIDE_LEG = true;
        draw_std_PeZ_read(C,P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,cpt_emission,latex_legend,CU,HIDE_LEG)
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_read_std_PeZ";
        if (NAME_FIG~="" && SAVE_FIG && is_savable_fig)
            saveas(gcf,strcat(folder_to_save_fig,"/",NAME_FIG,'.eps'),'epsc');
            fprintf(fileID,"%s\n",NAME_FIG);
        end


        clf;
        HIDE_LEG = false;
        draw_std_PeZ_read(C,P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,cpt_emission,latex_legend,CU,HIDE_LEG)
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_read_std_PeZ_with_leg";
        if (NAME_FIG~="" && SAVE_FIG  && is_savable_fig)
            saveas(gcf,strcat(folder_to_save_fig,"/",NAME_FIG,'.eps'),'epsc');
            fprintf(fileID,"%s\n",NAME_FIG);
        end
        end
        % -----------------------------------------------------------------
        %{
        clf; 
        % FIGURE 7 - DONNEES PARSEE - COMPARAISON 2GMIX PeZ
        HIDE_LEG = true; 
        draw_comp_2GMIX_PeZ(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,cpt_emission,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG);
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_equiv_2GMIX_PeZ";
        if (NAME_FIG~="" && SAVE_FIG && is_savable_fig)
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
            fprintf(fileID,"%s\n",NAME_FIG);
        end 
        clf; 
        HIDE_LEG = false; 
        draw_comp_2GMIX_PeZ(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,cpt_emission,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG);
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_equiv_2GMIX_PeZ_with_leg";     
        if (NAME_FIG~="" && SAVE_FIG  && is_savable_fig)
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
            fprintf(fileID,"%s\n",NAME_FIG); 
        end
        %}
        %
        % -----------------------------------------------------------------
        %{
        clf; 
        % FIGURE 8 - DONNEES PARSEE - COMPARAISON 3G PeZ
        HIDE_LEG = true;
        draw_comp_3G_PeZ(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,cpt_emission,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG);
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_equiv_3G_PeZ";
        % Avec ou sans légende  
        if (NAME_FIG~="" && SAVE_FIG   && is_savable_fig )
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
            fprintf(fileID,"%s\n",NAME_FIG);
        end 

        clf; 
        HIDE_LEG = false;
        draw_comp_3G_PeZ(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,cpt_emission,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG);
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_equiv_3G_PeZ_with_leg";
        % Avec ou sans légende  
        if (NAME_FIG~="" && SAVE_FIG && is_savable_fig )
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
            fprintf(fileID,"%s\n",NAME_FIG);

        end 

        %}
        
        %{


        test = ""; 
        fprintf(fileID,"\n\n"); 
        %}

        %{
        ECART_PeZ_COMP_2GMIX(i,:) = ecart_abs_2GMIX; 
        fprintf(fileID,"2GMIX PeZ - ECART POUR CETTE CONFIG (%d) :\n",i); 
        for (o = 1:length(ecart_abs_2GMIX))
            fprintf(fileID,"%g\t",ecart_abs_2GMIX(o)); 
        end
        fprintf(fileID,"\n")

        ECART_PeZ_COMP_3G(i,:) = ecart_abs_3G; 
        fprintf(fileID,"3G PeZ - ECART POUR CETTE CONFIG (%d) :\n",i); 
        for (o = 1:length(ecart_abs_3G))
            fprintf(fileID,"%g\t",ecart_abs_3G(o)); 
        end
        fprintf(fileID,"\n")
            
        %}

    end


    %save(strcat(BASE_NAME_FIG+'_completeparse'))
    fclose(fileID)
    test = ""; 
end





function [] = draw_4subfig_synth(C,P,cpt_emission,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,COR_is_N_1G511,LOR_is_N_1G511,latex_legend,CU)

    sgtitle(strcat(num2str(cpt_emission*C.hector_N_batch,"%.2e")," emissions"),'Interpreter','latex','FontSize',13)
        subplot(2,2,1);
            hold on; set(gca,'YScale','log'); 
            xlim([0,P.J])
            plot(1:P.J,DETECT_PAR_CLASSE(1:P.J,1)./EMISSIO_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(1),'$'),'Color',CU(1,:)); % Inutile mais bon, c'est un détrompeur pour voir si on s'est pas planté
            plot(1:P.J,DETECT_PAR_CLASSE(1:P.J,2)./EMISSIO_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(2),'$'),'Color',CU(2,:));
            plot(1:P.J,DETECT_PAR_CLASSE(1:P.J,3)./EMISSIO_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(3),'$'),'Color',CU(3,:));
            plot(1:P.J,DETECT_PAR_CLASSE(1:P.J,4)./EMISSIO_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(4),'$'),'Color',CU(4,:));
            plot(1:P.J,DETECT_PAR_CLASSE(1:P.J,5)./EMISSIO_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(5),'$'),'Color',CU(5,:));
            plot(1:P.J,DETECT_PAR_CLASSE(1:P.J,6)./EMISSIO_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(6),'$'),'Color',CU(6,:));
            title("Lecture standard",'Interpreter','latex')
            xlabel(strcat("$j\in \{1,...,",num2str(P.J,'%d'),"\}$"),'Interpreter','latex','FontSize',12); 
            %ylabel('Detection $\div$ Emission','Interpreter','latex','FontSize',12)
            legend('Location','southoutside','NumColumns',4,'Interpreter','latex','FontSize',12); legend boxoff;
       subplot(2,2,2);
            hold on; set(gca,'YScale','log'); 
            xlim([0,P.J])
            plot(1:P.J,DETECT_AGGR_PAR_CLASSE(1:P.J,1)./EMISSIO_AGGR_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(1),'$'),'Color',CU(1,:)); % Inutile mais bon, c'est un détrompeur pour voir si on s'est pas planté
            plot(1:P.J,DETECT_AGGR_PAR_CLASSE(1:P.J,2)./EMISSIO_AGGR_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(2),'$ AGGR'),'Color',CU(2,:));
            plot(1:P.J,DETECT_AGGR_PAR_CLASSE(1:P.J,3)./EMISSIO_AGGR_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(3),'$ AGGR'),'Color',CU(3,:));
            plot(1:P.J,DETECT_AGGR_PAR_CLASSE(1:P.J,4)./EMISSIO_AGGR_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(4),'$ AGGR'),'Color',CU(4,:));
            title("Aggregation $\textrm{COR}="+num2str(COR_is_N_1G511,"%g")+"\times 1\gamma_{511}$, $\textrm{LOR}="+num2str(LOR_is_N_1G511,'%g')+"\times 1\gamma_{511}$",'Interpreter','latex') 
            xlabel(strcat("$j\in \{1,...,",num2str(P.J,'%d'),"\}$"),'Interpreter','latex','FontSize',12); 
            %%ylabel('Detection $\div$ Emission','Interpreter','latex','FontSize',12)
            legend('Location','southoutside','NumColumns',2,'Interpreter','latex','FontSize',12); legend boxoff;
        subplot(2,2,3);
            hold on; set(gca,'YScale','log'); 
            xlim([0,P.J])
            %
             COMBI_2GMIX = (DETECT_AGGR_PAR_CLASSE(1:P.J,2)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)).*(DETECT_AGGR_PAR_CLASSE(1:P.J,3)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)); 
            plot(1:P.J,DETECT_PAR_CLASSE(1:P.J,5)./EMISSIO_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(5),'$'),'Color',CU(5,:));
            plot(1:P.J,COMBI_2GMIX,'DisplayName',strcat('$',latex_legend(2),'\times',latex_legend(3),'$ AGGR'),'Color',CU(7,:));
            title(strcat("$",latex_legend(2),"\textrm{aggr} \times ",latex_legend(3)," \textrm{aggr} \approx ",latex_legend(5)," $ ?"),'Interpreter','latex')
            xlabel(strcat("$j\in \{1,...,",num2str(P.J,'%d'),"\}$"),'Interpreter','latex','FontSize',12); 
            %ylabel('Detection $\div$ Emission','Interpreter','latex','FontSize',12)
            legend('Location','southoutside','NumColumns',2,'Interpreter','latex','FontSize',12); legend boxoff;
        subplot(2,2,4);
            hold on; set(gca,'YScale','log'); 
            xlim([0,P.J])
            %
            COMBI_3G = (DETECT_AGGR_PAR_CLASSE(1:P.J,3)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)).*(DETECT_AGGR_PAR_CLASSE(1:P.J,4)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)); 
            plot(1:P.J,DETECT_PAR_CLASSE(1:P.J,6)./EMISSIO_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(6),'$'),'Color',CU(6,:));
            plot(1:P.J,COMBI_3G,'DisplayName',strcat('$',latex_legend(3),'\times',latex_legend(4),'$ AGGR'),'Color',CU(7,:));
            title(strcat("$",latex_legend(3),"\textrm{aggr} \times ",latex_legend(4)," \textrm{aggr} \approx ",latex_legend(6)," $ ?"),'Interpreter','latex')
            xlabel(strcat("$j\in \{1,...,",num2str(P.J,'%d'),"\}$"),'Interpreter','latex','FontSize',12); 
            %ylabel('Detection $\div$ Emission','Interpreter','latex','FontSize',12)
            legend('Location','southoutside','NumColumns',2,'Interpreter','latex','FontSize',12); legend boxoff;

end

function [] = draw_std_read(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,latex_legend,CU,HIDE_LEG)
            hold on; set(gca,'YScale','log'); 
            xlim([0,P.J])
            plot(1:P.J,DETECT_PAR_CLASSE(1:P.J,1)./EMISSIO_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(1),'$'),'Color',CU(1,:)); % Inutile mais bon, c'est un détrompeur pour voir si on s'est pas planté
            plot(1:P.J,DETECT_PAR_CLASSE(1:P.J,2)./EMISSIO_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(2),'$'),'Color',CU(2,:));
            plot(1:P.J,DETECT_PAR_CLASSE(1:P.J,3)./EMISSIO_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(3),'$'),'Color',CU(3,:));
            plot(1:P.J,DETECT_PAR_CLASSE(1:P.J,4)./EMISSIO_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(4),'$'),'Color',CU(4,:));
            plot(1:P.J,DETECT_PAR_CLASSE(1:P.J,5)./EMISSIO_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(5),'$'),'Color',CU(5,:));
            plot(1:P.J,DETECT_PAR_CLASSE(1:P.J,6)./EMISSIO_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(6),'$'),'Color',CU(6,:));
        if (~HIDE_LEG)
            title("Lecture standard",'Interpreter','latex')
            xlabel(strcat("$j\in \{1,...,",num2str(P.J,'%d'),"\}$"),'Interpreter','latex','FontSize',12); 
            %ylabel('Detection $\div$ Emission','Interpreter','latex','FontSize',12); 
            legend('Location','southoutside','NumColumns',4,'Interpreter','latex','FontSize',12); legend boxoff;
        end
end


function [] = draw_comp_2GMIX(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
    hold on; set(gca,'YScale','log'); 
        xlim([0,P.J])
        %
        COMBI_2GMIX = (DETECT_AGGR_PAR_CLASSE(1:P.J,2)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)).*(DETECT_AGGR_PAR_CLASSE(1:P.J,3)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)); 
        plot(1:P.J,DETECT_PAR_CLASSE(1:P.J,5)./EMISSIO_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(5),'$'),'Color',CU(5,:));
        plot(1:P.J,COMBI_2GMIX,'DisplayName',strcat('$',latex_legend(2),'\times',latex_legend(3),'$ AGGR'),'Color','k','LineStyle','--');%CU(7,:));

    if (~HIDE_LEG)
            title(strcat("$",latex_legend(2),"\textrm{aggr} \times ",latex_legend(3)," \textrm{aggr} \approx ",latex_legend(5)," $ ?"),'Interpreter','latex')
            xlabel(strcat("$j\in \{1,...,",num2str(P.J,'%d'),"\}$"),'Interpreter','latex','FontSize',12);
            %ylabel('Detection $\div$ Emission','Interpreter','latex','FontSize',12)
            legend('Location','southoutside','NumColumns',2,'Interpreter','latex','FontSize',12); legend boxoff;
    end

end

function [] = draw_comp_3G(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
        hold on; set(gca,'YScale','log'); 
            xlim([0,P.J])
            %
            COMBI_3G = (DETECT_AGGR_PAR_CLASSE(1:P.J,3)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)).*(DETECT_AGGR_PAR_CLASSE(1:P.J,4)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)); 
            plot(1:P.J,DETECT_PAR_CLASSE(1:P.J,6)./EMISSIO_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(6),'$'),'Color',CU(6,:));
            plot(1:P.J,COMBI_3G,'DisplayName',strcat('$',latex_legend(3),'\times',latex_legend(4),'$ AGGR'),'Color','k','LineStyle','--');%CU(7,:));

    if (~HIDE_LEG)
            title(strcat("$",latex_legend(3),"\textrm{aggr} \times ",latex_legend(4)," \textrm{aggr} \approx ",latex_legend(6)," $ ?"),'Interpreter','latex')
            xlabel(strcat("$j\in \{1,...,",num2str(P.J,'%d'),"\}$"),'Interpreter','latex','FontSize',12);
            %ylabel('Detection $\div$ Emission','Interpreter','latex','FontSize',12)
            legend('Location','southoutside','NumColumns',2,'Interpreter','latex','FontSize',12); legend boxoff;
    end
end

function [] = draw_std_PeZ_read(C,P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,cpt_emission,latex_legend,CU,HIDE_LEG)
        hold on; %set(gca,'YScale','log');
        xlim([-120,+120])
        h = 240; 
        xticks(-(h)/2:40:+(h)/2);
        xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16);
        %ylabel("Sensibilit\'e $\mathbf{s}^{k}$",'Interpreter','latex','FontSize',16);
        c = linspace(-120,+120,P.D(3));
        c = unique(P.Rc(:,3)) + P.V(3)/2; 
        xticks(c);

        P_1G511 =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,2); 
        P_1G1157 =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,3);
        P_2GLOR =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,4);
        P_2GMIX =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,5);
        P_3G =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,6);
        p1 = plot(c,1*P_1G511,'DisplayName', strcat('$',latex_legend(2),'$'),'LineWidth',2,'LineStyle','-','Color',CU(2,:));
        p2 = plot(c,1*P_1G1157,'DisplayName',strcat('$',latex_legend(3),'$'),'LineWidth',2,'LineStyle','-','Color',CU(3,:));
        p3 = plot(c,1*P_2GLOR,'DisplayName',strcat('$',latex_legend(4),'$'),'LineWidth',2,'LineStyle','-','Color',CU(4,:)) ;
        p4 = plot(c,1*P_2GMIX,'DisplayName',strcat('$',latex_legend(5),'$'),'LineWidth',2,'LineStyle','-','Color',CU(5,:)) ;
        p5 = plot(c,1*P_3G,'DisplayName',strcat('$',latex_legend(6),'$'),'LineWidth',2,'LineStyle','-','Color',CU(6,:)) ;
        %
        yline(1*max(P_1G511),'--','Color',CU(2,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',15); % ,num2str(1*max(P_unit_1G511),'%.2e')
        text(120+1, max(P_1G511), num2str(1*max(P_1G511),'%.2e'), 'FontSize', 15, 'Color', CU(2,:));
        yline(1*max(P_1G1157),'--',num2str(1*max(P_1G1157),'%.2e'),'Color',CU(3,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',15);
        yline(1*max(P_2GLOR),'--',num2str(1*max(P_2GLOR),'%.2e'),'Color',CU(4,:),'LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',15);
        yline(1*max(P_2GMIX),'--','Color',CU(5,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',15); % ,num2str(1*max(P_unit_1G511),'%.2e')
        text(120+1, max(P_2GMIX), num2str(1*max(P_2GMIX),'%.2e'), 'FontSize', 15, 'Color', CU(5,:));
        yline(1*max(P_3G),'--',num2str(1*max(P_3G),'%.2e'),'Color',CU(6,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',15);
        if (~HIDE_LEG)
            legend([p1,p2,p3,p4,p5],'Location','southoutside','Interpreter','latex','FontSize',11,'NumColumns',3); legend boxoff  
            title(strcat(num2str(cpt_emission*C.hector_N_batch,"%.2e")," detections"),'Interpreter','latex','FontSize',13)
        end
end




function [] = draw_comp_2GMIX_PeZ(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,cpt_emission,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
        COMBI_2GMIX = (DETECT_AGGR_PAR_CLASSE(1:P.J,2)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)).*(DETECT_AGGR_PAR_CLASSE(1:P.J,3)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)); 
        P_combi_2GMIX =   get_ProfEnZ(P,EMISSIO_AGGR_PAR_VOXEL,COMBI_2GMIX,1);
        %P_aggr_1G511 =   get_ProfEnZ(P,EMISSIO_AGGR_PAR_VOXEL,DETECT_PAR_CLASSE,2); 
        %P_aggr_1G1157 =   get_ProfEnZ(P,EMISSIO_AGGR_PAR_VOXEL,DETECT_PAR_CLASSE,3);
        P_read_2GMIX =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,5);
        clf; 
        hold on; set(gca,'YScale','log');
        xlim([-120,+120])
        h = 240; 
        xticks(-(h)/2:40:+(h)/2);
        xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16);
        %ylabel("Sensibilit\'e $\mathbf{s}^{k}$",'Interpreter','latex','FontSize',16);
        c = linspace(-120,+120,P.D(3));
        
        c = unique(P.Rc(:,3)) + P.V(3)/2; 
        xticks(c);

        %p1 = plot(c,1*P_aggr_1G511,'DisplayName', strcat('$',latex_legend(2),'$'),'LineWidth',2,'LineStyle','-','Color',CU(2,:));
        %p2 = plot(c,1*P_aggr_1G1157,'DisplayName',strcat('$',latex_legend(3),'$'),'LineWidth',2,'LineStyle','-','Color',CU(3,:));
        p3 = plot(c,1*P_read_2GMIX,'DisplayName', strcat('$',latex_legend(5),'$'),'LineWidth',2,'LineStyle','-','Color',CU(5,:));
        p4 = plot(c,1*P_combi_2GMIX,'DisplayName',strcat('$',latex_legend(2),'\times',latex_legend(3),'$'),'LineWidth',2,'LineStyle','--','Color','k');%,CU(5,:));
        
        yline(1*max(P_read_2GMIX),'--','Color',CU(5,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',15); % ,num2str(1*max(P_unit_1G511),'%.2e')
        text(120+1, max(P_read_2GMIX), num2str(1*max(P_read_2GMIX),'%.2e'), 'FontSize', 15, 'Color', CU(5,:));
        yline(1*max(P_combi_2GMIX),'--',num2str(1*max(P_combi_2GMIX),'%.2e'),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',15,'Color','k');%,'Color',CU(5,:));
        
        if (~HIDE_LEG)
            title(strcat(num2str(cpt_emission*C.hector_N_batch,"%.2e")," detections"),'Interpreter','latex','FontSize',13)
            %legend([p1,p2,p3,p4],'Location','southoutside','Interpreter','latex','FontSize',11,'NumColumns',4); legend boxoff  
            legend([p3,p4],'Location','southoutside','Interpreter','latex','FontSize',11,'NumColumns',4); legend boxoff  
        end
         
        test = ""; 
end

function [] = draw_comp_3G_PeZ(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,cpt_emission,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
    COMBI_3G = (DETECT_AGGR_PAR_CLASSE(1:P.J,3)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)).*(DETECT_AGGR_PAR_CLASSE(1:P.J,4)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)); 
    P_combi_3G =   get_ProfEnZ(P,EMISSIO_AGGR_PAR_VOXEL,COMBI_3G,1);
    %P_aggr_1G1157 =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,3);
    %P_aggr_2GLOR =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,4);
    P_read_3G =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,6);
    clf; 
    hold on; set(gca,'YScale','log');
    xlim([-120,+120])
    h = 240; 
    xticks(-(h)/2:40:+(h)/2);
    xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16);
    %ylabel("Sensibilit\'e $\mathbf{s}^{k}$",'Interpreter','latex','FontSize',16);
    c = linspace(-120,+120,P.D(3));
    

    c = unique(P.Rc(:,3)) + P.V(3)/2; 
    xticks(c);

    %p1 = plot(c,1*P_aggr_1G1157,'DisplayName',strcat('$',latex_legend(3),'$'),'LineWidth',2,'LineStyle','-','Color',CU(3,:));
    %p2 = plot(c,1*P_aggr_2GLOR,'DisplayName',strcat('$',latex_legend(4),'$'),'LineWidth',2,'LineStyle','-','Color',CU(4,:)) ;
    p3 = plot(c,1*P_read_3G,'DisplayName',strcat('$',latex_legend(6),'$'),'LineWidth',2,'LineStyle','-','Color',CU(6,:)) ;
    p4 = plot(c,1*P_combi_3G,'DisplayName',strcat('$',latex_legend(3),'\times',latex_legend(4),'$'),'LineWidth',2,'LineStyle','--','Color','k'); %,CU(6,:));
    
    yline(1*max(P_read_3G),'--','Color',CU(6,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',15); % ,num2str(1*max(P_unit_1G511),'%.2e')
    text(120+1, max(P_read_3G), num2str(1*max(P_read_3G),'%.2e'), 'FontSize', 15, 'Color', CU(6,:));
    %yline(1*max(P_combi_3G),'--',num2str(1*max(P_combi_3G),'%.2e'),'Color',CU(6,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12);
    yline(1*max(P_combi_3G),'--',num2str(1*max(P_combi_3G),'%.2e'),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',15,'Color','k');%,'Color',CU(5,:));

    if (~HIDE_LEG)
        title(strcat(num2str(cpt_emission*C.hector_N_batch,"%.2e")," detections"),'Interpreter','latex','FontSize',13)
        % legend([p1,p2,p3,p4],'Location','southoutside','Interpreter','latex','FontSize',11,'NumColumns',4); legend boxoff  
        legend([p3,p4],'Location','southoutside','Interpreter','latex','FontSize',11,'NumColumns',4); legend boxoff 
    end
end


% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------


    % -------------------------------------------------------------------------
%     sensi_map_nrm = zeros(J,6);   % Normalisé sur le nombre total de de mesures
%     sensi_map_0G = zeros(J,1);
%     sensi_map_tep = zeros(J,1);
%     sensi_map_cc1157 = zeros(J,1);
%     sensi_map_cc511 = zeros(J,1);
%     sensi_map_unit_cc1157 = zeros(J,1);
%     sensi_map_unit_cc511 = zeros(J,1);
%     sensi_map_unit_tep = zeros(J,1); 
%     sensi_map_oth = zeros(J,6); 
%     

    %     for (j=1:J)
%         sensi_map_nrm(j,:) = DETECT_PAR_CLASSE(j,:)./N_div(j);
%         % Excluant les 0G
%         sensi_map_oth(j) = sum(DETECT_PAR_CLASSE(j,[2:6]))./N_div(j);
%         % --- 
% 
%         % Cas de la CC 511=> #(1G511) + #(2GMIX) 
%         sensi_map_unit_cc511(j) = sum(DETECT_AGGR_PAR_CLASSE(j,2))./N_div_aggr(j);
%         % Cas de la CC 1157 => #(1G1157) + #(2GMIX) +#(3G) 
%         sensi_map_unit_cc1157(j) = sum(DETECT_AGGR_PAR_CLASSE(j,3))./N_div_aggr(j);
%         % Cas de la TEP => #(2GLOR) + #(3G)=#(2GLOR + .)
%         sensi_map_unit_tep(j) = sum(DETECT_AGGR_PAR_CLASSE(j,4))./N_div_aggr(j);
%         % ---    
%         test = "";
%     end
%     % -------------------------------------------------------------------------
%     for (k=1:length(C.Classif))
%         fileID = fopen(strcat(path,"hector_sensi_norm_",C.Classif(k,1),".i"),'w','l');
%         for (i=1:P.J)
%             d = sensi_map_nrm(i,k); 
%             fwrite(fileID,d,"float32");
%         end
%         fclose(fileID);
%         s = sensi_map_nrm(:,k);
%         save(strcat(path,"hector_sensi_norm_",C.Classif(k,1)),'s')
%     end
% 
% 
% 
%     % ---
%     fileID = fopen(strcat(path,"hector_sensi_0G.i"),'w','l');
%         for (j=1:P.J)
%             d = sensi_map_0G(j); 
%             fwrite(fileID,d,"float32");
%         end
%     fclose(fileID); 
%     % ---
%     % ---
%     fileID = fopen(strcat(path,"hector_sensi_othG.i"),'w','l');
%         for (j=1:P.J)
%             d = sensi_map_oth(j); 
%             fwrite(fileID,d,"float32");
%         end
%     fclose(fileID); 
    
    % ---
    %{
    fileID = fopen(strcat(path,"hector_sensi_tep.i"),'w','l');
        for (j=1:P.J)
            d = sensi_map_tep(j); 
            fwrite(fileID,d,"float32");
        end
    fclose(fileID); 
    % ---
    fileID = fopen(strcat(path,"hector_sensi_cc511.i"),'w','l');
        for (j=1:P.J)
            d = sensi_map_cc511(j); 
            fwrite(fileID,d,"float32");
        end
    fclose(fileID);
    % ---
    fileID = fopen(strcat(path,"hector_sensi_cc1157.i"),'w','l');
        for (j=1:P.J)
            d = sensi_map_cc1157(j); 
            fwrite(fileID,d,"float32");
        end
    fclose(fileID);
    %}

%     fileID = fopen(strcat(path,"hector_sensi_map_tep_unit.i"),'w','l');
%         for (j=1:P.J)
%             d = sensi_map_unit_tep(j); 
%             fwrite(fileID,d,"float32");
%         end
%     fclose(fileID); 
%     % ---
%     fileID = fopen(strcat(path,"hector_sensi_map_cc511_unit.i"),'w','l');
%         for (j=1:P.J)
%             d = sensi_map_unit_cc511(j); 
%             fwrite(fileID,d,"float32");
%         end
%     fclose(fileID);
%     % ---
%     fileID = fopen(strcat(path,"hector_sensi_map_cc1157_unit.i"),'w','l');
%         for (j=1:P.J)
%             d = sensi_map_unit_cc1157(j); 
%             fwrite(fileID,d,"float32");
%         end
%     fclose(fileID);
    
    
    
    % fprintf("S\tClassification des évènements détectés (en %%) \n");
    % %N_exp = sum(occ_config_count(:,2:end))
    % for (i=2:length(C.Classif))
    %     prop = sum(occ_config_count(i),1);
    %     fprintf("\t\t%s : %g \n",C.Classif(i),prop/N_exp*100);
    % end


%     tok_eq = "="; if (LOR_is_N_1G511~=2), tok_eq = " !="; end  
%     fprintf("2GLOR %s 2 x 1G511  with LOR_is_N_1G511 = %d\n",tok_eq, LOR_is_N_1G511); 
%     fprintf("1G511 et 2GCOR with COR_is_N_1G511 = %g \n ", COR_is_N_1G511)


    %{
    fprintf("AGGR\n");
    if (P.J>1)
        DPC = sum(DETECT_AGGR_PAR_CLASSE); 
        EPC = sum(EMISSIO_AGGR_PAR_VOXEL);
    else
        DPC = DETECT_AGGR_PAR_CLASSE;
        EPC = EMISSIO_AGGR_PAR_VOXEL;
    end
    PROP = (DPC./EPC)*100;

    fprintf("Classe\t0G\t1G511\t1G1157\t2GLOR\n");
    fprintf("D\t"); fprintf(get_fmt(DPC),DPC); fprintf("\n"); 
    fprintf("P (%%)\t"); fprintf(get_fmt(PROP),PROP); fprintf("\n");
    %}
% 
    



    



%{


%



sensi_map_abs = zeros(P.J,6);   % Collectées telles quelles, sans normalisation

for (j=1:P.J)
    [a,b,c] = get_axes_voxel_idx(P,j);
    [r_a,r_b,r_c] = get_axes_voxel_coord(P,a,b,c);
    
    
    % ---------------------------------------------------------------------
    j_nexp = 0; 
    j_occ_config_count = zeros(1,6);
    for (b = 1:C.hector_N_batch)
        % hector_achille_v1_j9_b9.mat
        name = strcat(folder,"hector_",file,"_j",num2str(j),"_b",num2str(b),".mat");
        load(name);
        j_occ_config_count = j_occ_config_count + h.occ_config_count; 
        j_nexp = j_nexp + h.N_exp; 
        clear('name','h');
    end
    % 
    sensi_map_abs(j,:) = j_occ_config_count;
    
    occ_config_count = occ_config_count + j_occ_config_count; 
    N_exp = N_exp + j_nexp;
    test = "";
end
% -------------------------------------------------------------------------
%}
%{
slice = -1; 
for (d=1:P.D(3))
    is_in_slice = find(Ic(:,3) == d)
    slice_sensi = sensi_map_abs(is_in_slice,:)
    N_fixe = sum(slice_sensi,'all')
    slice_mean = sum(sensi_map_abs(is_in_slice,:))./ sum(slice_sensi,'all')
    slice_std = std(sum(sensi_map_abs(is_in_slice,:)),'includenan')

    t = ""; 
end
%}
%{
for (k=1:length(C.Classif))
    fileID = fopen(strcat("hector_sensi_norm_",C.Classif(k,1),".i"),'w','l');
    for (i=1:P.J)
        d = sensi_map_nrm(i,k); 
        fwrite(fileID,d,"float32");
    end
    fclose(fileID);
%     fileID = fopen(strcat("hector_sensi_abs_",C.Classif(k,1),".i"),'w','l');
%     for (i=1:P.J)
%         d = sensi_map_abs(i,k); 
%         fwrite(fileID,d,"float32");
%     end
%     fclose(fileID);
end

%}

% -------------------------------------------------------------------------


%{


for (j=1:P.J)
   
    
    sensi_map(j,:) = h.occ_config_count./h.N_exp; sum

    
    N_exp = N_exp + h.N_exp;
    
    test = "";
    clear('name','h');
end




fprintf("F\t%s\n",F.file);
fprintf("P\tNombre total de Voxel : J := %d x %d x %d = %d \n",P.D(1),P.D(2),P.D(3),P.J);
fprintf("P\tTaille d'un voxel %d x %d x %d (mm)\n",P.V(1),P.V(2),P.V(3));
fprintf("P\tTaille de l'image %d x %d x %d (mm)\n",P.T(1),P.T(2),P.T(3));
fprintf("P\tActivité totale : %g\n",sum(P.activity,'all'));

fprintf("S\tpositron range : %d (mu = %g, sigma = %g )\n",C.Pos_rng.active,C.Pos_rng.mu,C.Pos_rng.sigma)
fprintf("S\tNombre de tirages demandés : %d\n",C.hector_N);
fprintf("S\tNombre de tirages réalisés : %d\n",N_exp);



fprintf("S\tClassification des évènements détectés (en %%) \n");
for (i=1:length(C.Classif))
    prop = occ_config_count(i);
    fprintf("\t\t%s : %g \n",C.Classif(i),prop/N_exp*100);
end



%%

i = 1;
for (i=1:6)
    figure;
        hold on; box on; grid on; 
        %set(gca,'Yscale','log')
        plot(sensi_map(:,i),"DisplayName",C.Classif(i))
        title(strcat(strrep(file,"_"," "),":  $s_{j}^{k}$ $k = ",C.Classif(i,3),"$"),"Interpreter","latex",'FontSize',16)
end
%%
    show_cur_class = false;
    class_list = [1:6];
    data = occ_config_count;
    lab = C.Classif(:,3);
    th_class_idx = (1:length(data)-1);
    %
    sub_data = data(class_list);
    sub_lab = lab(class_list);
    if (show_cur_class)
        sub_cur_class_idx = find(class_list==cur_class);
    end
    

     
   



    figure; 
        hold on; %box on;
        idx = categorical(1:length(sub_data));
        b = bar(idx,sub_data);
        b.FaceColor = 'flat';
        if (show_cur_class)
            b.CData(sub_cur_class_idx,:) = [.5 0 .5];
        end
        set(gca,'xticklabel',sub_lab,'FontSize',12);
        ylim([0,max(occ_config_count)+10])
        %
        xtips2 = b(1).XEndPoints;
        ytips2 = b(1).YEndPoints;
        labels2 = string(b(1).YData);
        text(xtips2,ytips2,labels2,'HorizontalAlignment','center','VerticalAlignment','bottom','Interpreter','latex','FontSize',12);

        
%}



%{
fprintf("F\t%s\n",F.file);
fprintf("P\tNombre total de Voxel : J := %d x %d x %d = %d \n",P.D(1),P.D(2),P.D(3),P.J);
fprintf("P\tTaille d'un voxel %d x %d x %d (mm)\n",P.V(1),P.V(2),P.V(3));
fprintf("P\tTaille de l'image %d x %d x %d (mm)\n",P.T(1),P.T(2),P.T(3));
fprintf("P\tActivité totale : %g\n",sum(P.activity,'all'));

fprintf("S\tpositron range : %d (mu = %g, sigma = %g )\n",C.Pos_rng.active,C.Pos_rng.mu,C.Pos_rng.sigma)
fprintf("S\tNombre de tirages demandés : %d\n",C.hector_N);
fprintf("S\tNombre de tirages réalisés : %d\n",N_exp);



fprintf("S\tClassification des évènements détectés (en %%) \n");
for (i=1:length(C.Classif))
    prop = occ_config_count(i);
    fprintf("\t\t%s : %g \n",C.Classif(i),prop/N_exp*100);
end



%%

i = 1;
for (i=1:6)
    figure;
        hold on; box on; grid on; 
        %set(gca,'Yscale','log')
        plot(sensi_map(:,i),"DisplayName",C.Classif(i))
        title(strcat(strrep(file,"_"," "),":  $s_{j}^{k}$ $k = ",C.Classif(i,3),"$"),"Interpreter","latex",'FontSize',16)
end
%%
    show_cur_class = false;
    class_list = [1:6];
    data = occ_config_count;
    lab = C.Classif(:,3);
    th_class_idx = (1:length(data)-1);
    %
    sub_data = data(class_list);
    sub_lab = lab(class_list);
    if (show_cur_class)
        sub_cur_class_idx = find(class_list==cur_class);
    end
    

     
   



    figure; 
        hold on; %box on;
        idx = categorical(1:length(sub_data));
        b = bar(idx,sub_data);
        b.FaceColor = 'flat';
        if (show_cur_class)
            b.CData(sub_cur_class_idx,:) = [.5 0 .5];
        end
        set(gca,'xticklabel',sub_lab,'FontSize',12);
        ylim([0,max(occ_config_count)+10])
        %
        xtips2 = b(1).XEndPoints;
        ytips2 = b(1).YEndPoints;
        labels2 = string(b(1).YData);
        text(xtips2,ytips2,labels2,'HorizontalAlignment','center','VerticalAlignment','bottom','Interpreter','latex','FontSize',12);

     
%}