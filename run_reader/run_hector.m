% RUN_HECTOR - SENSIBILITE
% " HECTOOOOOOR !! "
% => Génère des simulation indépendantes sur chaque voxel pour déterminer des cartes de sensibilités.
% ATTENTION: utilise la fonction parpool pour faire du calcul parallèle, ajuster le nombre de worker en fonction de la machine.
% INPUT
%    file: nom du phantom de simulation (doit etre stocké dans /my_phantom)
%    REMARQUE: le file doit respecter la convention suivante eg. achille_vx avec x=1 ou plus ...
%    sv_folder: répertoire de sauvegarde de la simulation 
%    N_batch: nombre de répetition de la simulation
%    N: nombre d'émissions par voxel
%    RNG: la seed
%    write_conf(T,F): générer le fichier de conf (ou non), désactivé si il existe déjà
%    [OPTION] j_start, j_stop: permet de spécifier les indices des départ et de fin des voxels sur lesquels la simulation est lancées.
% OUTPUT
%    dans le répertoire sv_folder/
%    hector_[file]_conf.mat - le fichier de configuration permettant de récupérer les informations générales de la simulation.
%    hector_[nom]_j[x]_b[y] où x est le numéro du voxel et y le numéro du batch généré
% REMARQUE 
%    En respectant scrupuleusement ces notations, le fichier peut etre lu
%    avec la fonction read_hector; 
% eg de conf: 
% run_hector("ulysse_v1","/home/user/Bureau/THESE/POLLUX/ulysse_simu/ulysse_v1_10_10000",10,50000,100823,true,true)
% run_hector("ulysse_v2","/home/user/Bureau/THESE/POLLUX/ulysse_simu/ulysse_v2_10_10000",10,50000,110823,true,true)
% run_hector("ulysse_v3","/home/user/Bureau/THESE/POLLUX/ulysse_simu/ulysse_v3_10_10000",10,50000,120823,true,true)
% run_hector("ulysse_v4","/home/user/Bureau/THESE/POLLUX/ulysse_simu/ulysse_v4_10_10000",10,50000,130823,true,true)
% run_hector("ulysse_v5","/home/user/Bureau/THESE/POLLUX/ulysse_simu/ulysse_v5_10_10000",10,50000,140823,true,true)
% run_hector("ulysse_v6","/home/user/Bureau/THESE/POLLUX/ulysse_simu/ulysse_v6_10_10000",10,50000,150823,true,true)

function [] = run_hector(file,sv_folder,N_batch,N,RNG,activ_PR,write_conf,j_start,j_stop,Emi_loc)
    clc; 
    addpath("../methods/");
    addpath("../my_phantoms/")
    if (nargin <6)
        error("REVOIR LE PASSAGE DE PARAMETRE (%d)\n\trun_hector(file,sv_folder,N_batch,N,RNG,activ_PR,write_conf,[j_start,j_stop])\n",nargin)
    end
    % Clairement pour gérer le CV
    Emi_pts = ""; 
    if (nargin<10), Emi_pts = "IN"; 
    else, Emi_pts = Emi_loc;    % Clairement pour fixer le CV
    end
    % ---------------------------------------------------------------------
    F.folder = "../my_phantoms/";
    F.save_folder = strcat(sv_folder,"/");
    F.file = file;
    % ---------------------------------------------------------------------
    if (~isfolder(F.save_folder))
        mkdir(F.save_folder);
    end
    if (exist(strcat(F.save_folder,"hector_",F.file,"_conf.mat"))~=0)
        write_conf = false;
    end
    % Sauvegarde de la structure config pour les simulations
    if (write_conf)
        rng(RNG);
        cs_file = "../data/nistdata_1_1200_kev_rng.txt";
        data = load(strcat(F.folder,F.file,".txt"));
        neg_rayleigh = true;                                % Est-ce qu'on veut négliger l'effet Rayleigh
        [X,P] = init_simulation(data,cs_file,neg_rayleigh);
        % Un tableau de P.J Lignes et N_batch colonnes contenant les seed pour chacune des P.J*N_batch simulations;
        % => Récupérer la seed pour le b-ième lot du j-ième voxel revient à prendre la valeur seed_lut(j,b)
        seed_lut = randi([1000,99999],P.J,N_batch);
        % -----------------------------------------------------------------
        C.method = "HECTOR";
        C.hector_N = N;
        C.hector_N_batch = N_batch;
        C.rng = RNG;
        C.save_folder = strcat(F.save_folder,F.file);
        C.Classif = ["0G",0,"0\gamma"; "1G511",1,"1\gamma - 511"; "1G1157",1,"1\gamma - 1157"; "2GLOR",2,"2\gamma - LOR"; "2GMIX",2,"2\gamma- Mix"; "3G",3,"3\gamma"];
        C.Pos_rng.mu = 2.4;
        C.Pos_rng.sigma = 2;
        C.Pos_rng.active = activ_PR;
        C.Emi_pts = Emi_pts; 
        C.seed_lut = seed_lut;
        % -----------------------------------------------------------------
        save(strcat(F.save_folder,"hector_",F.file,"_conf"),'F','X','P','C');
        fprintf("CONF FILE GENERATED : %s \n",strcat(F.save_folder,"hector_",F.file,"_conf"))
    else
        warning("Un fichier de conf existe pour cette expé")
        % Simulations sur le PC nomade
        load(strcat(F.save_folder,"hector_",F.file,"_conf"));
    end
    fprintf("POSITRON RANGE ? %g\n",C.Pos_rng.active); 
    fprintf("EMISSION LOCAT ? %s\n",C.Emi_pts);
    % ---------------------------------------------------------------------
    print_phantom_info(P,F);
    % ---------------------------------------------------------------------
    if (nargin < 9), j_stop = P.J;  end
    if (nargin < 8),  j_start = 1; end
    % ---------------------------------------------------------------------

%{

%}
    if (P.J ~= 1)
        delete(gcp('nocreate'));
        pool = parpool('local',12,'IdleTimeout',1440);
        parfor(b_id=1:C.hector_N_batch)
            for (j=j_start:j_stop)
                seed = C.seed_lut(j,b_id);
                hector(F,C,X,P,j,b_id,seed);
            end
        end
    else
        b_id = 1;
        for(b_id=1:C.hector_N_batch)
            for (j=j_start:j_stop)
                seed = C.seed_lut(j,b_id);
                hector(F,C,X,P,j,b_id,seed);
            end
        end
    end
    test ="";
end


function [] = hector(F,C,X,P,j,b_id,seed)
    %fprintf("HECTOR - j = %d \t b_id = %d \t seed = %d \n",j,b_id,seed)
    % Tentative de gain de temps safe - On execute la simulation pour (j,b,seed) ssi elle n'existe pas ... 
    % Cela suppose de faire gaffe au j_start, j_stop quand on travaille avec plusieurs simulation en parallèle 
    cur_simu = strcat(F.save_folder(1),"hector_",F.file,"_j",num2str(j),"_b",num2str(b_id),".mat");
    if (~exist(cur_simu))
        rng(seed)
        [a,b,c] = get_axes_voxel_idx(P,j);
        [r_a,r_b,r_c] = get_axes_voxel_coord(P,a,b,c);
        terminal = false;
        cpt_detection = 0;
        % ---------------------------------------------------------------------
        [h] = sub_hector_init_struct(C,j,false);
        n = 0;
        tps = 0;
        t_start = tic();
        %{
    while (~terminal)
        n = n + 1;
        % -------------------------------------------------------------
        [M,Dir] = sub_hector_simulation_emission(P,r_a,r_b,r_c);
        % -------------------------------------------------------------
        [h,is_detected,evt_label] = sub_hector_simulation(X,C,h,n,j,M,Dir);
        cpt_detection = cpt_detection + is_detected;
        %
        % -------------------------------------------------------------
        % FAUX 
        % if (cpt_detection == C.hector_N), terminal = true; end
        if (n == C.hector_N), terminal = true; end 

    end
        %}
        for (simu_iter=1:C.hector_N)
            n = n + 1;
            [M,Dir] = sub_hector_simulation_emission(P,C,r_a,r_b,r_c);
            %         fprintf("M = "); fprintf(get_fmt(M),M); fprintf("\n");
            %         fprintf("D1 = "); fprintf(get_fmt(Dir.D1),Dir.D1); fprintf("\n");
            %         fprintf("D2 = "); fprintf(get_fmt(Dir.D2),Dir.D2); fprintf("\n");
            %         fprintf("D_pr = "); fprintf(get_fmt(Dir.D_pr),Dir.D_pr); fprintf("\n");
            [h,is_detected,evt_label] = sub_hector_simulation(X,C,h,j,M,Dir);
            %         fprintf("h = "); fprintf(get_fmt(h.occ_config_count),h.occ_config_count); fprintf("\n");
            h.cpt_detection = h.cpt_detection + is_detected;
            h.cpt_non_detection = h.occ_config_count(1);

            %sum(h.occ_config_count)
            test = "";
            %fprintf("Voxel j = %d/%d \t tirage n = %d \t %d/%d \t event = %s \n",j,P.J,n,h.cpt_detection,C.hector_N,evt_label);
        end


        tps = toc(t_start);
        if (~isempty(F.save_folder) )
            %fprintf("HECTOR[NS:j=%d|b=%d]\t#(tirages)=%d[%]\tTps=%g\n",j,b_id,n,tps)
            for (i = 1:length(F.save_folder))
                short_cut = F.save_folder(i);
                fprintf("HECTOR[NS:j=%d|b=%d|%s]\t#(tirages)=%d\tTps=%g\n",j,b_id,short_cut,n,tps)
                save_name = strcat(short_cut,"hector_",F.file,"_j",num2str(j),"_b",num2str(b_id));
                save(save_name,'h',"j","b_id",'tps','seed');
            end
        end

    end
end




function [h] = sub_hector_init_struct(C,j,empty_struct)
    if (empty_struct)
        h.N_exp = [];
        h.occ_config_count = [];
        h.cpt_detection = 0; 
        h.cpt_non_detection = 0;
        h.cpt_rm_kn = [];
    else
        h.N_exp = 0;
        h.cpt_detection = 0; 
        h.cpt_non_detection = 0;
        h.occ_config_count = zeros(1,6);
        h.occ_not_detected = 0;
        h.cpt_rm_kn = zeros(C.hector_N,2);
        h.j = j;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [M,Dir] = sub_hector_simulation_emission(P,C,r_a,r_b,r_c)
%{
    % Tirer un point d'émission dans le voxel
    d =  (randi([0, 4294967295],1,3) / 4294967296.0).*P.V;
    M = [r_a + d(1),r_b + d(2), r_c + d(3)];
%}
    switch C.Emi_pts
        case "IN"
            % Tirer un point d'émission dans le voxel.
            d =  (randi([0, 4294967295],1,3) / 4294967296.0).*P.V; % Je veux inclure 0 comme borne possible de mon tirage
            M = [r_a + d(1),r_b + d(2), r_c + d(3)];
        case "CV"
            % Tirer un point depuis le centre du voxel.
            M = [r_a,r_b,r_c] + P.V./2;
    end

    % -------------------------------------------------------------
    D1 = -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
    Dir.D1 = D1/norm(D1,2);
    D2 = -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
    Dir.D2 = D2/norm(D2,2);
    % -------------------------------------------------------------
    if (C.Pos_rng.active)
        D_pr =  -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
        Dir.D_pr = D_pr/norm(D_pr,2);
    else
        Dir.D_pr = zeros(1,3);
    end
    test = ""; 
end




function [h,is_detected,event_type] = sub_hector_simulation(X,C,h,j,M,Dir)
    [~,~,V1,V2,~,V1p,V2p,~,V1s,V2s,E1a,E2a] = reset_interaction_point();
    [E0,E0_prime] = init_photon_energy_count("3");
    % ---------------------------------------------------------------------
    is_detected = 1;    event_type = ""; 
    
    C.verbose = false; 

    succ_csr_p = false;
    t1_min_p_f = inf; t1_min_p_b = inf;   
    t1_min_p = inf;  t1_max_p = inf; t_V1p = inf; 
    V0p = inf(3,1); V1p = inf(3,1); V_unit_dir = inf(3,1);

    succ_half_lor_f = false; succ_half_lor_b = false; 
    t_min_f = inf; t_min_b = inf;
    V0f =inf(3,1); V0b = inf(3,1);  
    t_V1_f = inf; t_max_f = inf; 
    t_V1_b = inf; t_max_b = inf;


    t2_min = inf ; t2_max =inf;

    succ_csr_f = false;
    succ_csr_b = false;
    
    % ---------------------------------------------------------------------
    % SIMU 1G1157
    [t1_min_p_f,t1_min_p_b] = get_intersect_coord_Rin(X,M,Dir.D1);
    % PATCH CHOIX DE t1_min_p;
    T_min_p = [t1_min_p_f,t1_min_p_b];
    scal_vec = zeros(size(T_min_p));
    V_unit_dir = M + Dir.D1;
    MV_nrm_unit_dir = (V_unit_dir-M)/norm(V_unit_dir-M,2);
    % Pour la première composante t retournée par get_intersect_coord_Rin
    if (~isinf(t1_min_p_f))
        V_t1_min_p_f = M + t1_min_p_f*Dir.D1;
        MV_nrm_min_p_f = (V_t1_min_p_f - M)/norm(V_t1_min_p_f - M,2);
        scal_vec(1) = dot(MV_nrm_unit_dir,MV_nrm_min_p_f);
    end
    % Pour la seconde composante t retournée par get_intersect_coord_Rin
    if (~isinf(t1_min_p_b))
        V_t1_min_p_b = M + t1_min_p_b*Dir.D1;
        MV_nrm_min_p_b = (V_t1_min_p_b - M)/norm(V_t1_min_p_b - M,2);
        scal_vec(2) = dot(MV_nrm_unit_dir,MV_nrm_min_p_b);
    end
    [~,idx_t_min] = max(scal_vec);
    t1_min_p = T_min_p(idx_t_min);
    if (C.verbose)
        ast_idx = ["",""];
        ast_idx(idx_t_min) = "(*)";
        fprintf("[INIT_CSR_1157]\t t1_min_p_f = %g %s \t t1_min_p_b = %g %s\n",t1_min_p_f,ast_idx(1),t1_min_p_b,ast_idx(2));
    end
    % 
    if (~isinf(t1_min_p))
        % V0p: entrée dans le FOD, V1p : premier point d'interaction  
        V0p = get_point(M,Dir.D1,t1_min_p);
        [t1_max_p] = get_intersect_coord_Rout(X,M,Dir.D1,t1_min_p);
        [t_V1p] = distance_xenon_attenuation(X,E0_prime,Dir.D1,t1_max_p);
        V1p = get_point(V0p,Dir.D1,t_V1p);
        msg_1 = ""; if (C.verbose), msg_1 = "[CSR init 1157 V1p]"; end
        if (is_in_FOD(X,V1p,msg_1))
            msg_2 = "" ; if (C.verbose), msg_2 = "1157"; end
            [succ_csr_p,V0p,V1p,V2p,E1a,E2a,~,~] = create_csr_generatrix(X,M,Dir.D1,t1_min_p,t_V1p,E0_prime,msg_2);            
        else
            succ_csr_p = false;
            t_V1p = inf;
            V1p = inf(3,1);
        end
    end
    if (C.verbose)
        fprintf("[POST_CSR_1157] \tsucc_csr_p = %d \n",succ_csr_p);
        fprintf("[SYNTH|V] naii(V1) = %d\t naii(V2) = %d\n",naii(V1p),naii(V2p));
        fprintf("[SYNTH|E] E0 = %g\t E1b = %g\t E2b = %g \n",E0_prime,E1a,E2a);
    end

    
     % SIMU 2GLOR 
     [M,R_pr] = positron_range(M,Dir.D_pr,C.Pos_rng);
     [t_min_f,t_min_b] = get_intersect_coord_Rin(X,M,Dir.D2);  % b : back, f : forward
     %if (C.verbose), fprintf("\n[INIT_LOR] \tt_min_f = %g \t t_min_b = %g \n",t_min_f,t_min_b); end
     % PARTIE 1 - Tenter la LOR
     % => D'abord vers l'avant t>0 (f);
     if (~isinf(t_min_f))
         msg_3 = ""; if (C.verbose), msg_3 = "[LOR-V1-f]"; end
         [succ_half_lor_f,V0f,V1,E1b,t_V1_f,t_min_f,t_max_f] = create_half_lor(X,M,Dir.D2,t_min_f,E0,msg_3);
     end
     % => Puis vers l'arrière t<0 (b)
     if (~isinf(t_min_b))
         msg_4 = ""; if (C.verbose), msg_4 = "[LOR-V2-b]"; end
         [succ_half_lor_b,V0b,V2,E1b,t_V1_b,t_min_b,t_max_b] = create_half_lor(X,M,Dir.D2,t_min_b,E0,msg_4);
     end
     %if (C.verbose),fprintf("[POST_LOR] \tsucc_lor_f = %d \tsucc_lor_b = %d \n",succ_half_lor_f,succ_half_lor_b); end

     tok_dir_511 = "";
     if (~succ_half_lor_f || ~succ_half_lor_b), V1 = inf(3,1); V2 = inf(3,1);  end

     % PARTIE 2 - Si pas de LOR (succ_f=false ou succ_b = false)
     %          - On ré-init les points
     %          - On tente de créer une CSR dans le sens du rayon possible.

     if(succ_half_lor_f && ~succ_half_lor_b)
         % Dans ce cas, on a déjà calculé t_min_f et t_max_f => On a pas besoin de recalculer cette donnée
         msg_5 = ""; %if (C.verbose), msg_5 = "511-f"; end
         [succ_csr_f,V0f,V1s,V2s,E1b,E2b,csr_gen_dir_s,trial_rm_kn_f] = create_csr_generatrix(X,M,Dir.D2,t_min_f,t_V1_f,E0,msg_5);
         if (C.verbose), fprintf("[POST_CSR_F] \tsucc_csr_f = %d \n",succ_csr_f);end
         tok_dir_511 = "f";
     elseif(~succ_half_lor_f && succ_half_lor_b)
         % Dans ce cas, on a déjà calculé t_min_b et t_max_b => On a pas besoin de recalculer cette donnée
         msg_6 = ""; if (C.verbose), msg_6 = "511-b"; end
         [succ_csr_b,V0b,V1s,V2s,E1b,E2b,csr_gen_dir_s,trial_rm_kn_b] = create_csr_generatrix(X,M,Dir.D2, t_min_b,t_V1_b,E0,msg_6);
         if (C.verbose), fprintf("[POST_CSR_B] \tsucc_csr_b = %d \n",succ_csr_b); end
         tok_dir_511 = "b";
     elseif (succ_half_lor_f && succ_half_lor_b)
         if (C.verbose), fprintf("[RES] LOR CREEE \n"); end
         E1b = E0; E2b = E0;
     elseif(~succ_half_lor_f && ~succ_half_lor_b)
         if (C.verbose), fprintf("[RES] ECHEC \n"); end
         E0 = 0; E1b = 0; E2b = 0;
     end
     if (C.verbose)
         fprintf("[SYNTH|V] naii(V1) = %d\t naii(V2) = %d\t naii(V1s) = %d\t naii(V2s) = %d \n",naii(V1),naii(V2),naii(V1s),naii(V2s));
         fprintf("[SYNTH|E] E0 = %g\t E1b = %g\t E2b = %g \n",E0,E1b,E2b);
     end


     n_gamma_511 = length(find([naii(V1),naii(V2)]));
     n_gamma_511_diff = length(find([naii(V1s),naii(V2s)]));
     n_gamma_1157_diff = length(find([naii(V1p),naii(V2p)]));

    [config,inf_msg] = get_type_detected_event(n_gamma_511,n_gamma_511_diff,n_gamma_1157_diff,tok_dir_511);
    
    h.N_exp = h.N_exp + 1;
    h.occ_config_count(config.idx) = h.occ_config_count(config.idx) + 1;

%    disp(config.label)
    if (config.idx==1)
        is_detected = 0;
    end
    test = ""; 
    event_type = config.label; 
end
