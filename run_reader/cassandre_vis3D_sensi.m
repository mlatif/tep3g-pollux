clear all; 
close all; 
set(0,'DefaultFigureWindowStyle','docked'); 

K_max = 6; 
ind_fix = 2;    % ['x','y'];  
type_of_wave_viz = "2D"; 
type_of_wave_viz = "3D"; 
%type_of_wave_viz = "SUM_OF_2D"; 
%type_of_wave_viz = "SUM_OF_3D"; 
K_list = [2,5];                     % Pour les cas de sum of ... prendre la sum des paramètres..
%K_list = [2,4,5];
tok = 'wopr'; 
save_fig = true; 
only_one_colorbar = true; 
axe_leg_font = 12; 

name = "bernard"; 
export_folder = "DATA_PARSED"; 
V_max = 10;


folder_to_save_fig ="FIG_EPS";
if (~isfolder("FIG_EPS") && AFF_FIG); mkdir(folder_to_save_fig); end 


file_name = strcat(export_folder,'/hector_parsed_',name,'_',tok,'_v1-v',num2str(V_max)); 
BASE_NAME_FIG = "cassandre_results_ulysse_"+tok; 
COL_LIB = load(file_name).COLOR_LIB; 
P = load(file_name).P;

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% fixer y, regarder en (x,z)
latex_legend = ["0\gamma","1\gamma_{511}","1\gamma_{1157}","2\gamma_{\textrm{LOR}}","2\gamma_{\textrm{COR}}","3\gamma"];
sub_latex_legend = latex_legend(2:end); 
ind_list = ['x','y']; 
if (~isfile(strcat(export_folder,'/',BASE_NAME_FIG,"_z",ind_list(ind_fix),"_wave_sensi_map.mat")))
    [wave_sensi_map, min_vec, max_vec, min_all_classe, max_all_classe] = compute_wave_sensi_map(P,K_max,ind_list,file_name,BASE_NAME_FIG,ind_fix,latex_legend,sub_latex_legend,export_folder);
else 
    load(strcat(export_folder,'/',BASE_NAME_FIG,"_z",ind_list(ind_fix),"_wave_sensi_map.mat"))
end

switch type_of_wave_viz
    case "2D"        
        for (k = 1:K_max)
            NAME_FIG = BASE_NAME_FIG+"_k"+num2str(k)+"_z"+ind_list(ind_fix);
            %
            figure;set(gca,'color','none')
            imagesc(wave_sensi_map(:,:,k)); 
            if (k~=1)
                colormap("turbo");
            else 
                colormap("bone"); 
            end

            if (only_one_colorbar && (k==1 || k ==K_max) )
                colorbar;
            end
            pbaspect([size(wave_sensi_map, 2), size(wave_sensi_map, 1), 6]);
            shading interp
            xlabel("$"+ind_list(ind_fix)+"$ (ind)",'Interpreter','latex','FontSize',axe_leg_font);
            ylabel("$z$ (ind)",'Interpreter','latex','FontSize',axe_leg_font);
            NAME_FIG = BASE_NAME_FIG+"_k"+num2str(k)+"_z"+ind_list(ind_fix)+"_alone";
            if (save_fig), saveas(gcf,strcat(folder_to_save_fig,"/",NAME_FIG,'.eps'),'epsc'); end
        end
        

        for (k = 1:K_max)
            NAME_FIG = BASE_NAME_FIG+"_k"+num2str(k)+"_z"+ind_list(ind_fix);
            %
            figure;set(gca,'color','none')
            imagesc(wave_sensi_map(:,:,k)); %colorbar; colormap("turbo");
            pbaspect([size(wave_sensi_map, 2), size(wave_sensi_map, 1), 6]);
            shading interp
            if (k~=1)
                min_all_classe = min(min_vec(2:end));
                max_all_classe = max(max_vec(2:end));
                caxis([min_all_classe, max_all_classe]);
            end
            if (k~=1)
                colormap("turbo");
            else 
                colormap("bone"); 
            end
            
            if (only_one_colorbar && (k==1 || k ==K_max) )
                colorbar;
            end


            xlabel("$"+ind_list(ind_fix)+"$ (ind)",'Interpreter','latex','FontSize',axe_leg_font);
            ylabel("$z$ (ind)",'Interpreter','latex','FontSize',axe_leg_font);
            NAME_FIG = BASE_NAME_FIG+"_k"+num2str(k)+"_z"+ind_list(ind_fix)+"_coherent_scale";
            if (save_fig), saveas(gcf,strcat(folder_to_save_fig,"/",NAME_FIG,'.eps'),'epsc'); end
        end
    case "3D"
        for (k = 1:K_max)
            figure;set(gca,'color','none')
            surf(wave_sensi_map(:,:,k)); %colormap("jet");
            %zlabel('Detection $\div$ Emission','Interpreter','latex','FontSize',axe_leg_font)
            zlabel('$\hat{\mathbf{s}}^{k}$','Interpreter','latex','FontSize',axe_leg_font); hZLabel = get(gca,'ZLabel'); set(hZLabel,'rotation',0,'VerticalAlignment','middle')
            %daspect([2, 1, 1]);
            pbaspect([size(wave_sensi_map, 2), size(wave_sensi_map, 1), 10]);
            axis tight;
            shading interp
            
            if (k~=1)
                colormap("turbo");
            else 
                colormap("bone"); 
            end

            if (only_one_colorbar && (k==1 || k ==K_max) )
                colorbar;
            end

            if (k~=1)
                min_all_classe = min(min_vec(2:end));
                max_all_classe = max(max_vec(2:end));
                caxis([min_all_classe, max_all_classe]);
                zlim([0,max_all_classe])
            end
            xlabel("$"+ind_list(ind_fix)+"$ (ind)",'Interpreter','latex','FontSize',axe_leg_font);
            ylabel("$z$ (ind)",'Interpreter','latex','FontSize',axe_leg_font);
            NAME_FIG = BASE_NAME_FIG+"_k"+num2str(k)+"_z"+ind_list(ind_fix)+"_3d_coherent_scale";
            if (save_fig), saveas(gcf,strcat(folder_to_save_fig,"/",NAME_FIG,'.eps'),'epsc'); end
        end
    case "SUM_OF_2D"
        [wave_sensi_sumof_map,min_sumof,max_sumof] =  compute_wave_ofsum_sensi_map(P,K_list,ind_list,file_name,BASE_NAME_FIG,ind_fix,latex_legend,sub_latex_legend);
        figure;set(gca,'color','none')
            imagesc(wave_sensi_sumof_map); colorbar; colormap("jet");
            pbaspect([size(wave_sensi_sumof_map, 2), size(wave_sensi_sumof_map, 1), 6]);
            shading interp
            min_all_classe = min([min_vec(2:end),min_sumof]);
            max_all_classe = max([max_vec(2:end),max_sumof]);
            caxis([min_all_classe, max_all_classe]);
            xlabel("$"+ind_list(ind_fix)+"$ (ind)",'Interpreter','latex','FontSize',axe_leg_font);
            ylabel("$z$ (ind)",'Interpreter','latex','FontSize',axe_leg_font);
            tok_K = sprintf('%d', K_list);
            NAME_FIG = BASE_NAME_FIG+"_z"+ind_list(ind_fix)+"_sumOf"+tok_K+"_2d_coherent_scale";
            if (save_fig), saveas(gcf,strcat(folder_to_save_fig,"/",NAME_FIG,'.eps'),'epsc'); end
    
    case "SUM_OF_3D"
        [wave_sensi_sumof_map,min_sumof,max_sumof] =  compute_wave_ofsum_sensi_map(P,K_list,ind_list,file_name,BASE_NAME_FIG,ind_fix,latex_legend,sub_latex_legend);
        figure;set(gca,'color','none')
            surf(wave_sensi_sumof_map); colormap("jet");
            zlabel('Detection $\div$ Emission','Interpreter','latex','FontSize',axe_leg_font)
            pbaspect([size(wave_sensi_sumof_map, 2), size(wave_sensi_sumof_map, 1), 10]);
            axis tight;
            shading interp
            colorbar; colormap("jet");
            min_all_classe = min([min_vec(2:end),min_sumof]);
            max_all_classe = max([max_vec(2:end),max_sumof]);
            caxis([min_all_classe, max_all_classe]);
            zlim([0,max_all_classe])
                    
            xlabel("$"+ind_list(ind_fix)+"$ (ind)",'Interpreter','latex','FontSize',axe_leg_font);
            ylabel("$z$ (ind)",'Interpreter','latex','FontSize',axe_leg_font);
            tok_K = sprintf('%d', K_list);
            NAME_FIG = BASE_NAME_FIG+"_z"+ind_list(ind_fix)+"_sumOf"+tok_K+"_3d_coherent_scale";
            if (save_fig), saveas(gcf,strcat(folder_to_save_fig,"/",NAME_FIG,'.eps'),'epsc'); end
    otherwise
        
end






    

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------





function [wave_sensi_map, min_vec, max_vec, min_all_classe, max_all_classe] = compute_wave_sensi_map(P,K_max,ind_list,file_name,BASE_NAME_FIG,ind_fix,latex_legend,sub_latex_legend,export_folder)
    wave_sensi_map = zeros(P.D(3),P.D(ind_fix),K_max); 
    min_vec = zeros(1,K_max);
    max_vec = zeros(1,K_max); 
    fileID = fopen(strcat("print_viz_data_",BASE_NAME_FIG,"_z",ind_list(ind_fix),".txt"),'w+');
    for (k = 1:K_max)
        for (zf = 1:P.D(3))
            for (of = 1:P.D(ind_fix))
                %P.Ic(:,1) == xf & P.Ic(:,3) == zf
                D = find(P.Ic(:,ind_fix) == of & P.Ic(:,3) == zf); 
                detec = load(file_name).DETECT_PAR_CLASSE(D,k);
                emiss = load(file_name).EMISSIO_PAR_VOXEL(D);
                %sensi = sum(detec)/sum(emiss);
                wave_sensi_map(zf,of,k) = sum(detec)/sum(emiss);
                
                test = ""; 
            end
        end
        min_vec(k) = min(min(wave_sensi_map(:,:,k)));
        max_vec(k) = max(max(wave_sensi_map(:,:,k))); 
        fprintf(fileID,"%d\t%s\t%e\t%e\n",k,latex_legend(k),min_vec(k),max_vec(k)); 
        test = ""; 
    end
    fprintf(fileID,"\n"); 
    [min_all_classe,i_min_all_classe] = min(min_vec(2:end)); 
    [max_all_classe,i_max_all_classe] = max(max_vec(2:end)); 
    
    fprintf(fileID,"min(2:end) = %e\t[%s]\n",min_all_classe,sub_latex_legend(i_min_all_classe));
    fprintf(fileID,"max(2:end) = %e\t[%s]\n",max_all_classe,sub_latex_legend(i_max_all_classe));
    fclose(fileID)
    save(strcat(export_folder,'/',BASE_NAME_FIG,"_z",ind_list(ind_fix),"_wave_sensi_map"),'wave_sensi_map','min_vec','max_vec','min_all_classe','max_all_classe')
end

function [wave_sensi_map,min_sumof,max_sumof] =  compute_wave_ofsum_sensi_map(P,K_list,ind_list,file_name,BASE_NAME_FIG,ind_fix,latex_legend,sub_latex_legend,export_folder)
    wave_sensi_map = zeros(P.D(3),P.D(ind_fix));
    tok_K = sprintf('%d', K_list);
    fileID = fopen(strcat("print_viz_data_sumof_",tok_K,"_",BASE_NAME_FIG,"_z",ind_list(ind_fix),".txt"),'w+');
    for (ik = 1:length(K_list))
        k = K_list(ik);
        fprintf(fileID,"K = %d \t %s \n",k,latex_legend(k));
        for (zf = 1:P.D(3))
            for (of = 1:P.D(ind_fix))
                %P.Ic(:,1) == xf & P.Ic(:,3) == zf
                D = find(P.Ic(:,ind_fix) == of & P.Ic(:,3) == zf);
                detec = load(file_name).DETECT_PAR_CLASSE(D,k);
                emiss = load(file_name).EMISSIO_PAR_VOXEL(D);
                %sensi = sum(detec)/sum(emiss);
                wave_sensi_map(zf,of) =  wave_sensi_map(zf,of) + sum(detec)/sum(emiss);
    
                test = "";
            end
        end
    end

    min_sumof = min(wave_sensi_map,[],'all');
    max_sumof = max(wave_sensi_map,[],'all');

    fprintf(fileID,"min = %e\n",min_sumof);
    fprintf(fileID,"max = %e\n",max_sumof);
    fclose(fileID);

    save(strcat(export_folder,'/',BASE_NAME_FIG,"_z",ind_list(ind_fix),"_wave_sensi_map_sumof_",tok_K),'wave_sensi_map','min_sumof','max_sumof')

end