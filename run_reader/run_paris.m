function [] = run_paris(file,sv_folder,N_batch,N,RNG,write_conf,j_start,j_stop,Emi_loc)
% function [] = run_paris(file,sv_folder,N_batch,N,RNG,write_conf,j_start,j_stop)
% Paris a du faire un choix entre trois déesses ... et ça a foutu un sacré bordel ...
% Evaluation de la sensiblité sur les classes d'évènements unitaire 
% ATTENTION: utilise la fonction parpool pour faire du calcul parallèle, ajuster le nombre de worker en fonction de la machine.
% INPUT
%    file: nom du phantom de simulation (doit etre stocké dans /my_phantom)
%    REMARQUE: le file doit respecter la convention suivante eg. achille_vx avec x=1 ou plus ...
%    sv_folder: répertoire de sauvegarde de la simulation 
%    N_batch: nombre de répetition de la simulation
%    N: nombre d'émissions par voxel
%    RNG: la seed
%    write_conf(T,F): générer le fichier de conf (ou non), désactivé si il existe déjà
%    [OPTION] j_start, j_stop: permet de spécifier les indices des départ et de fin des voxels sur lesquels la simulation est lancées.
% OUTPUT
%    dans le répertoire sv_folder/
%    paris_[file]_conf.mat - le fichier de configuration permettant de récupérer les informations générales de la simulation.
%    paris_[nom]_j[x]_b[y] où x est le numéro du voxel et y le numéro du batch généré
% REMARQUE 
%    En respectant scrupuleusement ces notations, le fichier peut etre lu
%    avec la fonction read_paris; 
% REMARQUE
%    Par défaut, le positron range est désactivé pour une simulation Paris
% eg de conf: 
% run_paris("achille_v1","/home/user/Bureau/THESE/POLLUX/achille_simu/achille_5_10000_v1",5,10000,220120,true)
% run_paris("achille_v2","/home/user/Bureau/THESE/POLLUX/achille_simu/achille_5_10000_v2",5,10000,220121,true)
% run_paris("achille_v3","/home/user/Bureau/THESE/POLLUX/achille_simu/achille_5_10000_v3",5,10000,220122,true)
% run_paris("achille_v4","/home/user/Bureau/THESE/POLLUX/achille_simu/achille_5_10000_v4",5,10000,220123,true)
    clc; 
    addpath("../methods/");
    addpath("../my_phantoms/");
    Emi_pts = ""; 
    if (nargin<9), Emi_pts = "IN"; 
    else, Emi_pts = Emi_loc;    % Clairement pour fixer le CV
    end
    % ---------------------------------------------------------------------
    F.folder = "../my_phantoms/";
    F.save_folder = strcat(sv_folder,"/");
    F.file = file;
    % ---------------------------------------------------------------------
    if (~isfolder(F.save_folder))
        mkdir(F.save_folder);
    end
    % ---------------------------------------------------------------------
    if (exist(strcat(F.save_folder,"paris_",F.file,"_conf.mat"))~=0)
        write_conf = false;
    end
    % ------------------------------run_paris("ulysse_v1","/home/user/Bureau/THESE/POLLUX/unit_classe_ulysse_simu/uc_ulysse_v1_10_10000",10,50000,100823,true)---------------------------------------
    paris_classif = ["1G511","1G1157","2GLOR"]; 
    % ---------------------------------------------------------------------
    if (write_conf)
        rng(RNG);
        cs_file = "../data/nistdata_1_1200_kev_rng.txt";
        data = load(strcat(F.folder,F.file,".txt"));
        neg_rayleigh = true;                                % Est-ce qu'on veut négliger l'effet Rayleigh
        [X,P] = init_simulation(data,cs_file,neg_rayleigh);
        % Un tableau de P.J Lignes et N_batch colonnes contenant les seed pour chacune des P.J*N_batch simulations;
        % => Récupérer la seed pour le b-ième lot du j-ième voxel revient à prendre la valeur seed_lut(j,b)
        seed_lut = randi([1000,99999],P.J,N_batch,length(paris_classif));
        % -----------------------------------------------------------------
        C.method = "PARIS";
        C.paris_N = N;
        C.paris_N_batch = N_batch;
        C.rng = RNG;
        C.save_folder = strcat(F.save_folder,F.file);
        C.Classif = ["0G",0,"0\gamma"; "1G511",1,"1\gamma - 511"; "1G1157",1,"1\gamma - 1157"; "2GLOR",2,"2\gamma - LOR"; "2GMIX",2,"2\gamma- Mix"; "3G",3,"3\gamma"];
        C.paris_classif = paris_classif; 
        C.Pos_rng.mu = 2.4;
        C.Pos_rng.sigma = 2;
        C.Pos_rng.active = false;
        C.Emi_pts = Emi_pts; 
        C.seed_lut = seed_lut;
        % -----------------------------------------------------------------
        save(strcat(F.save_folder,"paris_",F.file,"_conf"),'F','X','P','C');
        fprintf("GENERATION \t %s \n",strcat(F.save_folder,"paris_",F.file,"_conf"))
    else
        % Simulations sur le PC nomade
        load(strcat(F.save_folder,"paris_",F.file,"_conf"));
    end
    % ---------------------------------------------------------------------
    print_phantom_info(P,F);
    if (nargin < 8), j_stop = P.J;  end
    if (nargin < 7),  j_start = 1; end
    % ---------------------------------------------------------------------
    %{   
    %}  
    if (P.J>1) 
        delete(gcp('nocreate'));
        pool = parpool('local',12,'IdleTimeout',1440);
        parfor(b_id=1:C.paris_N_batch) 
            for (j=j_start:j_stop)        
                seed = squeeze(C.seed_lut(j,b_id,:));
                paris(F,C,X,P,j,b_id,seed);
            end
        end
    else
        for(b_id=1:C.paris_N_batch)
            %
            for (j=j_start:j_stop)
                seed = squeeze(C.seed_lut(j,b_id,:));
                paris(F,C,X,P,j,b_id,seed);
            end
            %break
        end
    end
end






function [] =  paris(F,C,X,P,j,b_id,seed)
    [h] = sub_paris_init_struct(C,j,false);
    %fprintf("PARIS - j = %d \t b_id = %d\n",j,b_id)
    %  
    [a,b,c] = get_axes_voxel_idx(P,j);
    [r_a,r_b,r_c] = get_axes_voxel_coord(P,a,b,c);
    cpt_detection = 0;
    tps = 0;
    t_start = tic();

    for (n = 1:C.paris_N)
        %fprintf("n = %d \t seed = %g,%g,%g\n", n ,lcl_seed(1),lcl_seed(2),lcl_seed(3))
        tmp_detect = zeros(1,6); 
        tmp_no_detec = zeros(1,6); 
        K2_is_detected = false; 
        K3_is_detected = false; 
        
        for (p = 1:length(C.paris_classif))
            %fprintf("seed(p)+n = %d\n",seed(p)+n)
            [M,Dir] = paris_simulation_emission(P,C,r_a,r_b,r_c,seed(p)+n);
            %fprintf("P = %d \n",p)
            % 
            switch p
                case 1
                    [is_detected] = paris_CSR(X,0.511,M,Dir);
                    idx_to_fill = 2;
                case 2
                    [is_detected] = paris_CSR(X,1.157,M,Dir);
                    idx_to_fill = 3;
                    K2_is_detected = is_detected;
                case 3
                    [is_detected] = paris_LOR(X,M,Dir);
                    idx_to_fill = 4;
                    K3_is_detected = is_detected;
%                 case 4
%                     [K2_is_detected] = paris_CSR(X,1.157,M,Dir);
%                     [K3_is_detected] = paris_LOR(X,M,Dir);
%                     idx_to_fill = 5;
%                     is_detected = K2_is_detected && K3_is_detected;
            end
            cpt_detection = cpt_detection + is_detected;
            tmp_detect(idx_to_fill) = is_detected; 
            if (~is_detected), 
                tmp_detect(1) = tmp_detect(1) + 1; 
                tmp_no_detec(idx_to_fill) = tmp_no_detec(idx_to_fill) + 1; 
            end 
            %fprintf("\t p = %d (%s) \t\t seed = %d ~> is_det = %d\n",p, C.paris_classif(p),seed(p)+n,is_detected);
        end
%         
        if (K2_is_detected && K3_is_detected)
            h.K2_K3_simult_detect = h.K2_K3_simult_detect + 1; 
        end

        h.occ_config_count = h.occ_config_count + tmp_detect;
        h.non_detect_count =  h.non_detect_count + tmp_no_detec; 
        h.N_exp = h.N_exp + 1;  % Normalement 3xN_batch

        test = ""; 
    end
    tps = toc(t_start);


    
    if (~isempty(F.save_folder) )
        for (i = 1:length(F.save_folder))
            short_cut = F.save_folder(i);
            fprintf("PARIS[NS:j=%d|b=%d|%s]\t#(tirages)=%d\tTps=%g\n",j,b_id,short_cut,n,tps)
            save_name = strcat(short_cut,"paris_",F.file,"_j",num2str(j),"_b",num2str(b_id));
            save(save_name,'h',"j","b_id",'tps','seed');
        end
    end
    
    test = "";
    
end










function [S] = sub_paris_init_struct(C,j,empty_struct)
    if (empty_struct)
        S.N_exp = [];
        S.occ_config_count = [];
        S.non_detect_count = []; 
        S.cpt_rm_kn = [];
    else
        S.N_exp = 0;
        S.occ_config_count = zeros(1,6);
        S.non_detect_count = zeros(1,6); 
        S.K2_K3_simult_detect = 0;
        S.j = j;
    end
end

function [M,Dir] = paris_simulation_emission(P,C,r_a,r_b,r_c,seed)
            rng(seed)
%{
    % Tirer un point d'émission dans le voxel
    d =  (randi([0, 4294967295],1,3) / 4294967296.0).*P.V;
    M = [r_a + d(1),r_b + d(2), r_c + d(3)];
%}
    switch C.Emi_pts
        case "IN"
            % Tirer un point d'émission dans le voxel.
            d =  (randi([0, 4294967295],1,3) / 4294967296.0).*P.V; % Je veux inclure 0 comme borne possible de mon tirage
            M = [r_a + d(1),r_b + d(2), r_c + d(3)];
        case "CV"
            % Tirer un point depuis le centre du voxel.
            M = [r_a,r_b,r_c] + P.V./2;
    end
    % -------------------------------------------------------------
    D1 = -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
    Dir.D1 = D1/norm(D1,2);
    D2 = -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
    Dir.D2 = D2/norm(D2,2);
end

function [is_detected] = paris_CSR(X,E0,M,Dir)
    is_detected = false; 
    [t1_min_p_f,t1_min_p_b] = get_intersect_coord_Rin(X,M,Dir.D1);
    % PATCH CHOIX DE t1_min_p;
    T_min_p = [t1_min_p_f,t1_min_p_b];
    scal_vec = zeros(size(T_min_p));
    V_unit_dir = M + Dir.D1;
    MV_nrm_unit_dir = (V_unit_dir-M)/norm(V_unit_dir-M,2);
    % Pour la première composante t retournée par get_intersect_coord_Rin
    if (~isinf(t1_min_p_f))
        V_t1_min_p_f = M + t1_min_p_f*Dir.D1;
        MV_nrm_min_p_f = (V_t1_min_p_f - M)/norm(V_t1_min_p_f - M,2);
        scal_vec(1) = dot(MV_nrm_unit_dir,MV_nrm_min_p_f);
    end
    % Pour la seconde composante t retournée par get_intersect_coord_Rin
    if (~isinf(t1_min_p_b))
        V_t1_min_p_b = M + t1_min_p_b*Dir.D1;
        MV_nrm_min_p_b = (V_t1_min_p_b - M)/norm(V_t1_min_p_b - M,2);
        scal_vec(2) = dot(MV_nrm_unit_dir,MV_nrm_min_p_b);
    end
    [~,idx_t_min] = max(scal_vec);
    t1_min_p = T_min_p(idx_t_min);
    %{
    ast_idx = ["",""];
    ast_idx(idx_t_min) = "(*)";
    fprintf("[INIT_CSR_%g]\t t1_min_p_f = %g %s \t t1_min_p_b = %g %s\n",E0,t1_min_p_f,ast_idx(1),t1_min_p_b,ast_idx(2));
    %}

    if (~isinf(t1_min_p))
        V0p = get_point(M,Dir.D1,t1_min_p);
        [t1_max_p] = get_intersect_coord_Rout(X,M,Dir.D1,t1_min_p);
        [t_V1p] = distance_xenon_attenuation(X,E0,Dir.D1,t1_max_p);
        V1p = get_point(V0p,Dir.D1,t_V1p);
        if (is_in_FOD(X,V1p))
            [is_detected,~,~,~,~,~,~,~] = create_csr_generatrix(X,M,Dir.D1,t1_min_p,t_V1p,E0);
        end
    end

    test = ""; 
end

function [is_detected] = paris_LOR(X,M,Dir)
    E0 = 0.511; 
    succ_lor_f=false;
    succ_lor_b = false;
    % >>>>> C'est bien D2 et pas D1 
    [t_min_f,t_min_b] = get_intersect_coord_Rin(X,M,Dir.D2);
    % <<<<< 
    if (~isinf(t_min_f))
        [succ_lor_f,~,~,~,~,~,~] = create_half_lor(X,M,Dir.D2,t_min_f,E0);
    end
    if (~isinf(t_min_b))
        [succ_lor_b,~,~,~,~,~,~] = create_half_lor(X,M,Dir.D2,t_min_b,E0);
    end
    is_detected = (succ_lor_f && succ_lor_b); 
end