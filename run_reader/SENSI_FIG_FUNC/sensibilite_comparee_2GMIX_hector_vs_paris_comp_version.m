function [] = sensibilite_comparee_2GMIX_hector_vs_paris_comp_version(CU,P2GMIX,COMBI_2GMIX_PARIS,COMBI_2GMIX_HECTOR_V1,COMBI_2GMIX_HECTOR_V2,max_y)

figure; 
    hold on; box on; %grid on;     
    xlim([-120,+120])
    %ylim([0,1*max_y+15])
    set(gca,'YScale','log');
    %ylim([0,1])
    ylim([0,max_y+1000*max_y])
    h = 240; 
    xticks(-(h)/2:40:+(h)/2);
    c = -115:10:+115;
    c = linspace(-120,+120,24);
    xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16)
    %ylabel("Sensibilit\'e moyenne (\%)",'Interpreter','latex','FontSize',16)
    ylabel("Sensibilit\'e $\mathbf{s}_{k}$",'Interpreter','latex','FontSize',16)
    legend boxoff
    % Données Hector pures
    p1 = plot(c,1*P2GMIX,'DisplayName','${2\gamma}^{}_{\textrm{COR}}$ (hector)','LineWidth',2,'LineStyle','-','Color','k');
    p0 = scatter(0,0,'w.','DisplayName','');
    p2 = plot(c,1*COMBI_2GMIX_PARIS,'DisplayName','${1\gamma}^{511}_{\textrm{COR}} \times {1\gamma}^{1157}_{\textrm{COR}}$ (paris)','LineWidth',2,'LineStyle','-','Color',CU(1,1));
    p3 = plot(c,1*COMBI_2GMIX_HECTOR_V1,'DisplayName','${1\gamma}^{511}_{\textrm{COR}} \times {1\gamma}^{1157}_{\textrm{COR}}$ (hector) $N=0$','LineWidth',2,'LineStyle','--','Color',CU(2,1));
    p4 = plot(c,1*COMBI_2GMIX_HECTOR_V2,'DisplayName','${1\gamma}^{511}_{\textrm{COR}} \times {1\gamma}^{1157}_{\textrm{COR}}$ (hector) $N=1$','LineWidth',2,'LineStyle','--','Color','#A2142F');
    %
    yline(1*max(P2GMIX),'--',num2str(1*max(P2GMIX),'%.2e'),'Color','k','LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',12);
    yline(1*max(COMBI_2GMIX_PARIS),'--',num2str(1*max(COMBI_2GMIX_PARIS),'%.2e'),'Color',CU(1,1),'LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',12);
    yline(1*max(COMBI_2GMIX_HECTOR_V1),'--',num2str(1*max(COMBI_2GMIX_HECTOR_V1),'%.2e'),'Color',CU(2,1),'LabelHorizontalAlignment','right','LabelVerticalAlignment','top','FontSize',12,'Color','#7E2F8E');
    yline(1*max(COMBI_2GMIX_HECTOR_V2),'--',num2str(1*max(COMBI_2GMIX_HECTOR_V2),'%.2e'),'Color','#A2142F','LabelHorizontalAlignment','right','LabelVerticalAlignment','top','FontSize',12,'Color','#A2142F');
    %
    %legend([p1,p2,p3,p4],'Location','northeast','Interpreter','latex','FontSize',11,'NumColumns',2)
    legend([p1,p2,p3,p4],'Location','northoutside','Interpreter','latex','FontSize',11,'NumColumns',2)
end
