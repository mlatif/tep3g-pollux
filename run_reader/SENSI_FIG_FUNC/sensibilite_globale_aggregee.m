function [] = sensibilite_globale_aggregee(CU,P_aggr_wxpr_1G511,P_aggr_wxpr_1G1157,P_aggr_wxpr_2GLOR,my_title,max_y)
figure; 
    hold on; box on; %grid on;     
    xlim([-120,+120])
    %ylim([0,1*max_y+10])
     set(gca,'YScale','log');
    %ylim([0,1])
    ylim([0,max_y+1000*max_y])
    h = 240; 
    xticks(-(h)/2:40:+(h)/2);
    xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16)
    %ylabel("Sensibilit\'e moyenne (\%)",'Interpreter','latex','FontSize',16)
    ylabel("Sensibilit\'e $\mathbf{s}_{k}$",'Interpreter','latex','FontSize',16)
    legend boxoff
    %plot(P0G)
    c = -115:10:+115;
    c = linspace(-120,+120,24);
    p1 = plot(c,1*P_aggr_wxpr_1G511,'DisplayName',strcat(CU(1,2),' aggr'),'LineWidth',2,'LineStyle','-','Color',CU(1,1));
 
    p2 = plot(c,1*P_aggr_wxpr_1G1157,'DisplayName',strcat(CU(2,2),' aggr'),'LineWidth',2,'LineStyle','-','Color',CU(2,1));
    p3 = plot(c,1*P_aggr_wxpr_2GLOR,'DisplayName',strcat(CU(3,2),' aggr'),'LineWidth',2,'LineStyle','-','Color',CU(3,1));  
    
    %title(my_title,'Interpreter','latex','FontSize',16)
    
    yline(1*max(P_aggr_wxpr_1G511),'--',num2str(1*max(P_aggr_wxpr_1G511),'%.2e'),'Color',CU(1,1),'LabelHorizontalAlignment','left','LabelVerticalAlignment','bottom','FontSize',12);
    yline(1*max(P_aggr_wxpr_1G1157),'--',num2str(1*max(P_aggr_wxpr_1G1157),'%.2e'),'Color',CU(2,1),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12);
    yline(1*max(P_aggr_wxpr_2GLOR),'--',num2str(1*max(P_aggr_wxpr_2GLOR),'%.2e'),'Color',CU(3,1),'LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',12);
    %legend([p1,p2,p3],'Location','northeast','Interpreter','latex','FontSize',11,'NumColumns',3)
    legend([p1,p2,p3],'Location','northoutside','Interpreter','latex','FontSize',11,'NumColumns',3)
end