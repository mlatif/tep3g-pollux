function [] = sensibilite_unitaire_paris_vs_hector(CU,P_unit,P_aggr_wopr,P_aggr_wipr,idx,max_y)
    figure; 
        hold on; box on; %grid on;     
        xlim([-120,+120])
        %ylim([0,1*max(max_y)+10])
         set(gca,'YScale','log');
        %ylim([0,1])
        ylim([0,max_y+1000*max_y])
        h = 240; 
        xticks(-(h)/2:40:+(h)/2);
        xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16)
        %ylabel("Sensibilit\'e moyenne (\%)",'Interpreter','latex','FontSize',16)
        ylabel("Sensibilit\'e $\mathbf{s}_{k}$",'Interpreter','latex','FontSize',16)
        legend boxoff
        c = -115:10:+115;
        c = linspace(-120,+120,24); 
        p1 = plot(c,1*P_unit,'DisplayName', strcat(CU(idx,2),' unit'),'LineWidth',2,'LineStyle','-','Color','#0072BD');
        p2 = plot(c,1*P_aggr_wopr,'DisplayName',strcat(CU(idx,2),' aggr woPR'),'LineWidth',2,'LineStyle','--','Color',	'#D95319');
        p3 = plot(c,1*P_aggr_wipr,'DisplayName',strcat(CU(idx,2),' aggr wiPR'),'LineWidth',2,'LineStyle',':','Color',			'#7E2F8E');
        yline(1*max(P_unit),'--',num2str(1*max(P_unit),'%.2e'),'Color',CU(1,1),'LabelHorizontalAlignment','left','LabelVerticalAlignment','bottom','FontSize',12,'Color',	'#0072BD');
        yline(1*max(P_aggr_wopr),'--',num2str(1*max(P_aggr_wopr),'%.2e'),'Color',CU(2,1),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12,'Color',	'#D95319');
        yline(1*max(P_aggr_wipr),'--',num2str(1*max(P_aggr_wipr),'%.2e'),'Color',CU(3,1),'LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',12,'Color',			'#7E2F8E');
        %legend([p1,p2,p3],'Location','northeast','Interpreter','latex','FontSize',11,'NumColumns',3)
        legend([p1,p2,p3],'Location','northoutside','Interpreter','latex','FontSize',11,'NumColumns',3)
end