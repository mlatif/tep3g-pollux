function [] = sensibilite_unitaire_paris_vs_hector_comp_version(CU,P_unit,P_wo_V1,P_wi_V1,P_wo_V2,P_wi_V2,idx,max_y)
    figure; 
        hold on; box on; %grid on;     
        xlim([-120,+120])
        %ylim([0,1*max(max_y_comp_vers)+15])
         set(gca,'YScale','log');
        %ylim([0,1])
        ylim([0,max_y+1000*max_y])
        h = 240; 
        xticks(-(h)/2:40:+(h)/2);
        xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16)
        %ylabel("Sensibilit\'e moyenne (\%)",'Interpreter','latex','FontSize',16)
        ylabel("Sensibilit\'e $\mathbf{s}_{k}$",'Interpreter','latex','FontSize',16)
        legend boxoff
        c = -115:10:+115;
        c = linspace(-120,+120,24); 
        % ---
        p0 = scatter(0,0,'w','DisplayName','');
        p1 = plot(c,1*P_unit,'DisplayName', strcat(CU(idx,2),' unit'),'LineWidth',2,'LineStyle','-','Color','k');
        % V1
        p2 = plot(c,1*P_wo_V1,'DisplayName',strcat(CU(idx,2),' aggr no PR $N=0$'),'LineWidth',2,'LineStyle','-','Color',		'#0072BD');
        p3 = plot(c,1*P_wi_V1,'DisplayName',strcat(CU(idx,2),' aggr wi PR $N=0$'),'LineWidth',2,'LineStyle','--','Color', 	'#A2142F');
        % V2
        p4 = plot(c,1*P_wo_V2,'DisplayName',strcat(CU(idx,2),' aggr no PR $N=1$'),'LineWidth',2,'LineStyle','-','Color',	'#D95319');
        p5 = plot(c,1*P_wi_V2,'DisplayName',strcat(CU(idx,2),' aggr wi PR $N=1$'),'LineWidth',2,'LineStyle','--','Color',	'#77AC30');
        % ---
        
        % ---
        yline(1*max(P_unit),'--',num2str(1*max(P_unit),'%.2e'),'Color',CU(1,1),'LabelHorizontalAlignment','center','LabelVerticalAlignment','bottom','FontSize',12,'Color',	'k');
        % V1
        yline(1*max(P_wo_V1),'--',num2str(1*max(P_wo_V1),'%.2e'),'Color',CU(2,1),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12,'Color', '#0072BD');
        yline(1*max(P_wi_V1),'--',num2str(1*max(P_wi_V1),'%.2e'),'Color',CU(3,1),'LabelHorizontalAlignment','left','LabelVerticalAlignment','bottom','FontSize',12,'Color',	'#A2142F');
        % V2
        yline(1*max(P_wo_V2),'--',num2str(1*max(P_wo_V2),'%.2e'),'Color',CU(2,1),'LabelHorizontalAlignment','right','LabelVerticalAlignment','top','FontSize',12,'Color','#D95319');
        yline(1*max(P_wi_V2),'--',num2str(1*max(P_wi_V2),'%.2e'),'Color',CU(3,1),'LabelHorizontalAlignment','right','LabelVerticalAlignment','bottom','FontSize',12,'Color','#77AC30');
        
        %legend([p1,p0,p2,p3,p4,p5],'Location','northeast','Interpreter','latex','FontSize',11,'NumColumns',3)
        legend([p1,p0,p2,p3,p4,p5],'Location','northoutside','Interpreter','latex','FontSize',11,'NumColumns',3)
end