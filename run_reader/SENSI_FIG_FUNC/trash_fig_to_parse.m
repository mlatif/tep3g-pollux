clear all; 
close all; 
load('read_hector_results_PR0_LOR0_COR1_partialparse.mat')
% -------------------------------------------------------------------------


       % Sauvegarde de la forme résultat - synthèse
    clf; 
       HIDE_LEG = true; 
       draw_agr_read(C,P,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)   
       NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_read_aggr";
       if (NAME_FIG~="" && SAVE_FIG)
           saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
       end
       clf;
       HIDE_LEG = false; 
       draw_agr_read(C,P,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)   
       NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_read_aggr";
       if (NAME_FIG~="" && SAVE_FIG)
           saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
       end




        clf; 
        HIDE_LEG = true; 
        draw_comp_2GMIX(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_equiv_2GMIX";
        if (NAME_FIG~="" && SAVE_FIG)
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
        end    



        clf; 
        HIDE_LEG = true; 
        draw_comp_3G(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_equiv_2GMIX";
        if (NAME_FIG~="" && SAVE_FIG)
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
        end    
        clf; 
        HIDE_LEG = false; 
        draw_comp_3G(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_equiv_2GMIX_with_leg";
        if (NAME_FIG~="" && SAVE_FIG)
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
        end    

%             if (false)
%             title(strcat("$",latex_legend(3),"\textrm{aggr} \times ",latex_legend(4)," \textrm{aggr} \approx ",latex_legend(6)," $ ?"),'Interpreter','latex')
%             xlabel(strcat("$j\in \{1,...,",num2str(P.J,'%d'),"\}$"),'Interpreter','latex','FontSize',12); 
%             ylabel('Detection $\div$ Emission','Interpreter','latex','FontSize',12)
%             legend('Location','southoutside','NumColumns',2,'Interpreter','latex','FontSize',12); legend boxoff;
%             end
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_equiv_3G";
        if (NAME_FIG~="" && SAVE_FIG)
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
        end 



        clf; 
        HIDE_LEG = true; 
        draw_std_PeZ_read(C,P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,cpt_emission,latex_legend,CU,HIDE_LEG)
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_PeZ";
        % Avec ou sans légende  
        if (NAME_FIG~="" && SAVE_FIG)
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
        end 
        clf; 
        HIDE_LEG = false; 
      draw_std_PeZ_read(C,P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,cpt_emission,latex_legend,CU,HIDE_LEG)
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_PeZ_with_leg";
        % Avec ou sans légende        
        if (NAME_FIG~="" && SAVE_FIG)
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
        end 


        clf; 
        HIDE_LEG = true; 
        [ecart_abs_2GMIX] =draw_comp_2GMIX_PeZ(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,cpt_emission,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_equiv_2GMIX_PeZ";
        % Avec ou sans légende  
        if (NAME_FIG~="" && SAVE_FIG)
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
        end 
        clf; 
        HIDE_LEG = false; 
        [ecart_abs_2GMIX] = draw_comp_2GMIX_PeZ(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,cpt_emission,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_equiv_2GMIX_PeZ_with_leg";
        % Avec ou sans légende        
        if (NAME_FIG~="" && SAVE_FIG)
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
        end 

        clf; 
        HIDE_LEG = true;
        [ecart_abs_3G] = draw_comp_3G_PeZ(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,cpt_emission,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_equiv_3G_PeZ";
        % Avec ou sans légende  
        if (NAME_FIG~="" && SAVE_FIG)
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
        end 
        clf; 
        HIDE_LEG = false;
        [ecart_abs_3G] = draw_comp_3G_PeZ(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,cpt_emission,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
        NAME_FIG = BASE_NAME_FIG+"_f"+num2str(i,'%d')+"_equiv_3G_PeZ_with_leg";
        % Avec ou sans légende  
        if (NAME_FIG~="" && SAVE_FIG)
            saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
        end 


 

function [ecart_abs] = draw_comp_3G_PeZ(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,cpt_emission,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
    COMBI_3G = (DETECT_AGGR_PAR_CLASSE(1:P.J,3)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)).*(DETECT_AGGR_PAR_CLASSE(1:P.J,4)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)); 
    P_combi_3G =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,COMBI_3G,1);
    P_aggr_1G1157 =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,3);
    P_aggr_2GLOR =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,4);
    P_aggr_3G =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,6);
    clf; 
    hold on; set(gca,'YScale','log');
    xlim([-120,+120])
    h = 240; 
    xticks(-(h)/2:40:+(h)/2);
    xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16);
    ylabel("Sensibilit\'e $\mathbf{s}^{k}$",'Interpreter','latex','FontSize',16);
    c = linspace(-120,+120,P.D(3));
    
    p1 = plot(c,1*P_aggr_1G1157,'DisplayName',strcat('$',latex_legend(3),'$'),'LineWidth',2,'LineStyle','-','Color',CU(3,:));
    p2 = plot(c,1*P_aggr_2GLOR,'DisplayName',strcat('$',latex_legend(4),'$'),'LineWidth',2,'LineStyle','-','Color',CU(4,:)) ;
    p3 = plot(c,1*P_aggr_3G,'DisplayName',strcat('$',latex_legend(6),'$'),'LineWidth',2,'LineStyle','-','Color',CU(6,:)) ;
    p4 = plot(c,1*P_combi_3G,'DisplayName',strcat('$',latex_legend(3),'\times',latex_legend(4),'$'),'LineWidth',2,'LineStyle','--','Color','k'); %,CU(6,:));
    
    yline(1*max(P_aggr_3G),'--','Color',CU(6,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12); % ,num2str(1*max(P_unit_1G511),'%.2e')
    text(120+1, max(P_aggr_3G), num2str(1*max(P_aggr_3G),'%.2e'), 'FontSize', 12, 'Color', CU(6,:));
    %yline(1*max(P_combi_3G),'--',num2str(1*max(P_combi_3G),'%.2e'),'Color',CU(6,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12);
    yline(1*max(P_combi_3G),'--',num2str(1*max(P_combi_3G),'%.2e'),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12,'Color','k');%,'Color',CU(5,:));

    if (~HIDE_LEG)
        title(strcat(num2str(cpt_emission*C.hector_N_batch,"%.2e")," detections"),'Interpreter','latex','FontSize',13)
        legend([p1,p2,p3,p4],'Location','northoutside','Interpreter','latex','FontSize',11,'NumColumns',4); legend boxoff  
    end
    
     ecart_abs = abs(P_combi_3G - P_aggr_3G); 

end


function [ecart_abs] = draw_comp_2GMIX_PeZ(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,cpt_emission,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
        COMBI_2GMIX = (DETECT_AGGR_PAR_CLASSE(1:P.J,2)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)).*(DETECT_AGGR_PAR_CLASSE(1:P.J,3)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)); 
        P_combi_2GMIX =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,COMBI_2GMIX,1);
        P_aggr_1G511 =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,2); 
        P_aggr_1G1157 =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,3);
        P_aggr_2GMIX =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,5);
        clf; 
        hold on; set(gca,'YScale','log');
        xlim([-120,+120])
        h = 240; 
        xticks(-(h)/2:40:+(h)/2);
        xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16);
        ylabel("Sensibilit\'e $\mathbf{s}^{k}$",'Interpreter','latex','FontSize',16);
        c = linspace(-120,+120,P.D(3));
        
        p1 = plot(c,1*P_aggr_1G511,'DisplayName', strcat('$',latex_legend(2),'$'),'LineWidth',2,'LineStyle','-','Color',CU(2,:));
        p2 = plot(c,1*P_aggr_1G1157,'DisplayName',strcat('$',latex_legend(3),'$'),'LineWidth',2,'LineStyle','-','Color',CU(3,:));
        p3 = plot(c,1*P_aggr_2GMIX,'DisplayName', strcat('$',latex_legend(5),'$'),'LineWidth',2,'LineStyle','-','Color',CU(5,:));
        p4 = plot(c,1*P_combi_2GMIX,'DisplayName',strcat('$',latex_legend(2),'\times',latex_legend(3),'$'),'LineWidth',2,'LineStyle','--','Color','k');%,CU(5,:));
        
        yline(1*max(P_aggr_2GMIX),'--','Color',CU(5,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12); % ,num2str(1*max(P_unit_1G511),'%.2e')
        text(120+1, max(P_aggr_2GMIX), num2str(1*max(P_aggr_2GMIX),'%.2e'), 'FontSize', 12, 'Color', CU(5,:));
        yline(1*max(P_combi_2GMIX),'--',num2str(1*max(P_combi_2GMIX),'%.2e'),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12,'Color','k');%,'Color',CU(5,:));
        
        if (~HIDE_LEG)
            title(strcat(num2str(cpt_emission*C.hector_N_batch,"%.2e")," detections"),'Interpreter','latex','FontSize',13)
            legend([p1,p2,p3,p4],'Location','northoutside','Interpreter','latex','FontSize',11,'NumColumns',4); legend boxoff  
        end
        ecart_abs = abs(P_combi_2GMIX - P_aggr_2GMIX); 
        test = ""; 
end





function [] = draw_std_PeZ_read(C,P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,cpt_emission,latex_legend,CU,HIDE_LEG)
        hold on; set(gca,'YScale','log');
        xlim([-120,+120])
        h = 240; 
        xticks(-(h)/2:40:+(h)/2);
        xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16);
        ylabel("Sensibilit\'e $\mathbf{s}^{k}$",'Interpreter','latex','FontSize',16);
        c = linspace(-120,+120,P.D(3));
        P_aggr_1G511 =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,2); 
        P_aggr_1G1157 =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,3);
        P_aggr_2GLOR =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,4);
        P_aggr_2GMIX =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,5);
        P_aggr_3G =   get_ProfEnZ(P,EMISSIO_PAR_VOXEL,DETECT_PAR_CLASSE,6);
        p1 = plot(c,1*P_aggr_1G511,'DisplayName', strcat('$',latex_legend(2),'$'),'LineWidth',2,'LineStyle','-','Color',CU(2,:));
        p2 = plot(c,1*P_aggr_1G1157,'DisplayName',strcat('$',latex_legend(3),'$'),'LineWidth',2,'LineStyle','-','Color',CU(3,:));
        p3 = plot(c,1*P_aggr_2GLOR,'DisplayName',strcat('$',latex_legend(4),'$'),'LineWidth',2,'LineStyle','-','Color',CU(4,:)) ;
        p4 = plot(c,1*P_aggr_2GMIX,'DisplayName',strcat('$',latex_legend(5),'$'),'LineWidth',2,'LineStyle','-','Color',CU(5,:)) ;
        p5 = plot(c,1*P_aggr_3G,'DisplayName',strcat('$',latex_legend(6),'$'),'LineWidth',2,'LineStyle','-','Color',CU(6,:)) ;
        %
        yline(1*max(P_aggr_1G511),'--','Color',CU(2,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12); % ,num2str(1*max(P_unit_1G511),'%.2e')
        text(120+1, max(P_aggr_1G511), num2str(1*max(P_aggr_1G511),'%.2e'), 'FontSize', 12, 'Color', CU(2,:));
        yline(1*max(P_aggr_1G1157),'--',num2str(1*max(P_aggr_1G1157),'%.2e'),'Color',CU(3,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12);
        yline(1*max(P_aggr_2GLOR),'--',num2str(1*max(P_aggr_2GLOR),'%.2e'),'Color',CU(4,:),'LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',12);
        yline(1*max(P_aggr_2GMIX),'--','Color',CU(5,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12); % ,num2str(1*max(P_unit_1G511),'%.2e')
        text(120+1, max(P_aggr_2GMIX), num2str(1*max(P_aggr_2GMIX),'%.2e'), 'FontSize', 12, 'Color', CU(5,:));
        yline(1*max(P_aggr_3G),'--',num2str(1*max(P_aggr_3G),'%.2e'),'Color',CU(6,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12);
        if (~HIDE_LEG)
            legend([p1,p2,p3,p4,p5],'Location','northoutside','Interpreter','latex','FontSize',11,'NumColumns',3); legend boxoff  
            title(strcat(num2str(cpt_emission*C.hector_N_batch,"%.2e")," detections"),'Interpreter','latex','FontSize',13)
        end
end



function [] = draw_comp_3G(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
        hold on; set(gca,'YScale','log'); 
            xlim([0,P.J])
            %
            COMBI_3G = (DETECT_AGGR_PAR_CLASSE(1:P.J,3)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)).*(DETECT_AGGR_PAR_CLASSE(1:P.J,4)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)); 
            plot(1:P.J,DETECT_PAR_CLASSE(1:P.J,6)./EMISSIO_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(6),'$'),'Color',CU(6,:));
            plot(1:P.J,COMBI_3G,'DisplayName',strcat('$',latex_legend(3),'\times',latex_legend(4),'$ AGGR'),'Color','k','LineStyle','--');%CU(7,:));

    if (~HIDE_LEG)
            title(strcat("$",latex_legend(3),"\textrm{aggr} \times ",latex_legend(4)," \textrm{aggr} \approx ",latex_legend(6)," $ ?"),'Interpreter','latex')
            xlabel(strcat("$j\in \{1,...,",num2str(P.J,'%d'),"\}$"),'Interpreter','latex','FontSize',12);
            ylabel('Detection $\div$ Emission','Interpreter','latex','FontSize',12)
            legend('Location','southoutside','NumColumns',2,'Interpreter','latex','FontSize',12); legend boxoff;
    end
end


function [] = draw_comp_2GMIX(C,P,DETECT_PAR_CLASSE,EMISSIO_PAR_VOXEL,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)
    hold on; set(gca,'YScale','log'); 
        xlim([0,P.J])
        %
        COMBI_2GMIX = (DETECT_AGGR_PAR_CLASSE(1:P.J,2)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)).*(DETECT_AGGR_PAR_CLASSE(1:P.J,3)./EMISSIO_AGGR_PAR_VOXEL(1:P.J)); 
        plot(1:P.J,DETECT_PAR_CLASSE(1:P.J,5)./EMISSIO_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(5),'$'),'Color',CU(5,:));
        plot(1:P.J,COMBI_2GMIX,'DisplayName',strcat('$',latex_legend(2),'\times',latex_legend(3),'$ AGGR'),'Color',CU(7,:));

    if (~HIDE_LEG)
            title(strcat("$",latex_legend(2),"\textrm{aggr} \times ",latex_legend(3)," \textrm{aggr} \approx ",latex_legend(5)," $ ?"),'Interpreter','latex')
            xlabel(strcat("$j\in \{1,...,",num2str(P.J,'%d'),"\}$"),'Interpreter','latex','FontSize',12);
            ylabel('Detection $\div$ Emission','Interpreter','latex','FontSize',12)
            legend('Location','southoutside','NumColumns',2,'Interpreter','latex','FontSize',12); legend boxoff;
    end


end


        % Sauvegardes indiividuelles 
function [] = draw_agr_read(C,P,DETECT_AGGR_PAR_CLASSE,EMISSIO_AGGR_PAR_VOXEL,latex_legend,CU,COR_is_N_1G511,LOR_is_N_1G511,HIDE_LEG)       
        hold on; set(gca,'YScale','log'); 
            xlim([0,P.J])
            plot(1:P.J,DETECT_AGGR_PAR_CLASSE(1:P.J,1)./EMISSIO_AGGR_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(1),'$'),'Color',CU(1,:)); % Inutile mais bon, c'est un détrompeur pour voir si on s'est pas planté
            plot(1:P.J,DETECT_AGGR_PAR_CLASSE(1:P.J,2)./EMISSIO_AGGR_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(2),'$ AGGR'),'Color',CU(2,:));
            plot(1:P.J,DETECT_AGGR_PAR_CLASSE(1:P.J,3)./EMISSIO_AGGR_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(3),'$ AGGR'),'Color',CU(3,:));
            plot(1:P.J,DETECT_AGGR_PAR_CLASSE(1:P.J,4)./EMISSIO_AGGR_PAR_VOXEL(1:P.J),'DisplayName',strcat('$',latex_legend(4),'$ AGGR'),'Color',CU(4,:));
if (~HIDE_LEG)
            title("Aggregation $\textrm{COR}="+num2str(COR_is_N_1G511,"%g")+"\times 1\gamma_{511}$, $\textrm{LOR}="+num2str(LOR_is_N_1G511,'%g')+"\times 1\gamma_{511}$",'Interpreter','latex') 
            xlabel(strcat("$j\in \{1,...,",num2str(P.J,'%d'),"\}$"),'Interpreter','latex','FontSize',12); 
            ylabel('Detection $\div$ Emission','Interpreter','latex','FontSize',12)
end
end


