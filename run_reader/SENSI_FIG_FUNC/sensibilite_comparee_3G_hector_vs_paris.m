function [] = sensibilite_comparee_3G_hector_vs_paris(CU,P3G,COMBI_3G_PARIS,COMBI_3G_HECTOR_WOPR,max_y)
figure
    hold on; box on; %grid on;     
    xlim([-120,+120])
    %ylim([0,1*max_y+10])
     set(gca,'YScale','log');
    %ylim([0,1])
    ylim([0,max_y+1000*max_y])
    h = 240; 
    xticks(-(h)/2:40:+(h)/2);
    c = -115:10:+115;
    c = linspace(-120,+120,24);
    xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16)
    %ylabel("Sensibilit\'e moyenne (\%)",'Interpreter','latex','FontSize',16)
    ylabel("Sensibilit\'e $\mathbf{s}_{k}$",'Interpreter','latex','FontSize',16)
    legend boxoff
    % Données Hector pures
    p1 = plot(c,1*P3G,'DisplayName',strcat('${3\gamma}^{}_{\textrm{}}$',' (hector)'),'LineWidth',2,'LineStyle','-','Color','k');
    p0 = scatter(0,0,'w.','DisplayName','');
    % Données Paris/Hector aggrégées
    p2 = plot(c,1*COMBI_3G_PARIS,'DisplayName','${2\gamma}^{}_{\textrm{LOR}} \times {1\gamma}^{1157}_{\textrm{COR}}$ (paris)','LineWidth',2,'LineStyle','-','Color',CU(1,1));
    p3 = plot(c,1*COMBI_3G_HECTOR_WOPR,'DisplayName','${2\gamma}^{}_{\textrm{LOR}} \times {1\gamma}^{1157}_{\textrm{COR}}$ (aggr)','LineWidth',2,'LineStyle','--','Color',CU(2,1));
    yline(1*max(P3G),'--',num2str(1*max(P3G),'%.2e'),'Color','k','LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12);
    yline(1*max(COMBI_3G_PARIS),'--',num2str(1*max(COMBI_3G_PARIS),'%.2e'),'Color',CU(1,1),'LabelHorizontalAlignment','right','LabelVerticalAlignment','top','FontSize',12);
    yline(1*max(COMBI_3G_HECTOR_WOPR),'--',num2str(1*max(COMBI_3G_HECTOR_WOPR),'%.2e'),'Color',CU(2,1),'LabelHorizontalAlignment','left','LabelVerticalAlignment','bottom','FontSize',12,'Color',CU(2,1));    
    %legend([p1,p0,p2,p3],'Location','northeast','Interpreter','latex','FontSize',11,'NumColumns',2)
    legend([p1,p0,p2,p3],'Location','northoutside','Interpreter','latex','FontSize',11,'NumColumns',2)
end
