function [P_norm_wi_pr,P_norm_wo_pr,P_aggr_wi_pr,P_aggr_wo_pr,P_unit] = just_import(V)
% ---
% Ulysse - Génération des données avec Hector (sensi_norm_...) par classe
load('MATFILE_FROM_IMAGEJ/SENSIBILITE_NORM_HECTOR.mat'); 
% P_aggr_wi_pr;P_aggr_wo_pr;  Ulysse - comptage des données par classe unitiare avec hector (aggrgée) Avec et Sans PR
% P_unit;  Ulysse - génération des données avec Paris
load(strcat('MATFILE_FROM_IMAGEJ/SENSILIBITE_PARIS_HECTOR_Ph_ULYSSE_V',num2str(V),'.mat')); 
end
