function [] = sensibilite_unitaire_paris_with_coherent_scale(CU,P_unit_1G511,P_unit_1G1157,P_unit_2GLOR,max_y)
figure; 
    hold on; box on; %grid on;     
    xlim([-120,+120])
    %ylim([0,1*max_y+15])
     set(gca,'YScale','log');
    ylim([0,max_y+1000*max_y])
    h = 240; 
    xticks(-(h)/2:40:+(h)/2);
    xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16)
    %ylabel("Sensibilit\'e moyenne (\%)",'Interpreter','latex','FontSize',16)
    ylabel("Sensibilit\'e $\mathbf{s}_{k}$",'Interpreter','latex','FontSize',16)
    %legend('Location','northeast','Interpreter','latex','FontSize',12)%,'NumColumns',5)

    legend boxoff
    
    %title("Sensibilite Ulysse (Paris)",'Interpreter','latex')
    
    %plot(P0G)
    c = -115:10:+115;
    c = linspace(-120,+120,24);
    p1 = plot(c,1*P_unit_1G511,'DisplayName', strcat(CU(1,2),' unit'),'LineWidth',2,'LineStyle','-','Color',CU(1,1));
    p2 = plot(c,1*P_unit_1G1157,'DisplayName',strcat(CU(2,2),' unit'),'LineWidth',2,'LineStyle','-','Color',CU(2,1));
    p3 = plot(c,1*P_unit_2GLOR,'DisplayName',strcat(CU(3,2),' unit'),'LineWidth',2,'LineStyle','-','Color',CU(3,1)) ; 
    
    yline(1*max(P_unit_1G511),'--',num2str(1*max(P_unit_1G511),'%.2e'),'Color',CU(1,1),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12);
    yline(1*max(P_unit_1G1157),'--',num2str(1*max(P_unit_1G1157),'%.2e'),'Color',CU(2,1),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12);
    yline(1*max(P_unit_2GLOR),'--',num2str(1*max(P_unit_2GLOR),'%.2e'),'Color',CU(3,1),'LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',12);

    legend([p1,p2,p3],'Location','northoutside','Interpreter','latex','FontSize',11,'NumColumns',3)
end
