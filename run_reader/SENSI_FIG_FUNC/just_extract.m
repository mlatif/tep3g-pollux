function [P_hector_norm,P_hector_aggr,P_paris_unit] = just_extract(P_norm_wi_pr,P_norm_wo_pr,P_aggr_pr,P_aggr_wo_pr,P_unit)
    % HECTOR - SENSIBILITES GLOBALES 
    P_hector_norm = []; 
    P_hector_norm.wipr_c0G = P_norm_wi_pr(:,1); 
    P_hector_norm.wipr_c1G511 = P_norm_wi_pr(:,2);
    P_hector_norm.wipr_c1G1557 = P_norm_wi_pr(:,3);
    P_hector_norm.wipr_c2GLOR = P_norm_wi_pr(:,4); 
    P_hector_norm.wipr_c2GMIX = P_norm_wi_pr(:,5); 
    P_hector_norm.wipr_c3G = P_norm_wi_pr(:,6); 
    %
    P_hector_norm.wopr_c0G = P_norm_wo_pr(:,1); 
    P_hector_norm.wopr_c1G511 = P_norm_wo_pr(:,2);
    P_hector_norm.wopr_c1G1557 = P_norm_wo_pr(:,3);
    P_hector_norm.wopr_c2GLOR = P_norm_wo_pr(:,4); 
    P_hector_norm.wopr_c2GMIX = P_norm_wo_pr(:,5); 
    P_hector_norm.wopr_c3G = P_norm_wo_pr(:,6); 
    % ---
    % HECTOR - AGGR - SENSIBLITE UNITAIRES AVEC PR
    P_hector_aggr = []; 
    P_hector_aggr.wipr_1G511 = P_aggr_pr (:,1); 
    P_hector_aggr.wipr_1G1157 = P_aggr_pr (:,2); 
    P_hector_aggr.wipr_2GLOR = P_aggr_pr (:,3); 
    % ---
    % HECTOR - AGGR - SENSIBLITE UNITAIRES SANS PR
    P_hector_aggr.wopr_1G511 = P_aggr_wo_pr (:,1); 
    P_hector_aggr.wopr_1G1157 = P_aggr_wo_pr (:,2);
    P_hector_aggr.wopr_2GLOR = P_aggr_wo_pr (:,3);
    % ---
    % PARIS - SENSIBLITE UNITAIRES SANS PR
    P_paris_unit = []; 
    P_paris_unit.c1G511 = P_unit(:,1); 
    P_paris_unit.c1G1157 = P_unit(:,2); 
    P_paris_unit.c2GLOR = P_unit(:,3); 
end