function [] = sensibilite_globale_aggregee_comp_version(CU,P_1G511_V1,P_1G511_V2,P_1G1157_V1,P_1G1157_V2,P_2GLOR_V1,P_2GLOR_V2,my_title,max_y)
figure; 
    hold on; box on; %grid on;     
    xlim([-120,+120])
    %ylim([0,1*max_y+15])
     set(gca,'YScale','log');
    %ylim([0,1])
    ylim([0,max_y+1000*max_y])
    h = 240; 
    xticks(-(h)/2:40:+(h)/2);
    xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16)
    %ylabel("Sensibilit\'e moyenne (\%)",'Interpreter','latex','FontSize',16)
    ylabel("Sensibilit\'e $\mathbf{s}_{k}$",'Interpreter','latex','FontSize',16)
    legend boxoff
    %plot(P0G)
    c = -115:10:+115;
    c = linspace(-120,+120,24);
    p1 = plot(c,1*P_1G511_V1,'DisplayName',strcat(CU(1,2),' aggr $N=0$'),'LineWidth',2,'LineStyle','-','Color',CU(1,1));
    p2 = plot(c,1*P_1G511_V2,'DisplayName',strcat(CU(1,2),' aggr $N=1$'),'LineWidth',2,'LineStyle','--','Color',CU(1,1));
    p3 = plot(c,1*P_1G1157_V1,'DisplayName',strcat(CU(2,2),' aggr $N=0$'),'LineWidth',2,'LineStyle','-','Color',CU(2,1));
    p4 = plot(c,1*P_1G1157_V2,'DisplayName',strcat(CU(2,2),' aggr $N=1$'),'LineWidth',2,'LineStyle','--','Color',CU(2,1));
    p5 = plot(c,1*P_2GLOR_V1,'DisplayName',strcat(CU(3,2),' aggr $N=0$'),'LineWidth',2,'LineStyle','-','Color',CU(3,1));
    p6 = plot(c,1*P_2GLOR_V2,'DisplayName',strcat(CU(3,2),' aggr $N=1$'),'LineWidth',2,'LineStyle','--','Color',CU(3,1));  
    
    %title(my_title,'Interpreter','latex','FontSize',16)
    
    yline(1*max(P_1G511_V1),'--',num2str(1*max(P_1G511_V1),'%.2e'),'Color',CU(1,1),'LabelHorizontalAlignment','right','LabelVerticalAlignment','bottom','FontSize',12);
    if (P_1G511_V2 ~= P_1G511_V1), yline(1*max(P_1G511_V2),'--',num2str(1*max(P_1G511_V2),'%.2e'),'Color',CU(1,1),'LabelHorizontalAlignment','right','LabelVerticalAlignment','top','FontSize',12); end
    yline(1*max(P_1G1157_V1),'--',num2str(1*max(P_1G1157_V1),'%.2e'),'Color',CU(2,1),'LabelHorizontalAlignment','left','LabelVerticalAlignment','bottom','FontSize',12);
    if (P_1G1157_V2 ~= P_1G1157_V1), yline(1*max(P_1G1157_V2),'--',num2str(1*max(P_1G1157_V2),'%.2e'),'Color',CU(2,1),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12); end
    yline(1*max(P_2GLOR_V1),'--',num2str(1*max(P_2GLOR_V1),'%.2e'),'Color',CU(3,1),'LabelHorizontalAlignment','left','LabelVerticalAlignment','bottom','FontSize',12);
    if (P_2GLOR_V2~=P_2GLOR_V1), yline(1*max(P_2GLOR_V2),'--',num2str(1*max(P_2GLOR_V1),'%.2e'),'Color',CU(3,1),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12); end
    %legend([p1,p2,p3,p4,p5,p6],'Location','northeast','Interpreter','latex','FontSize',11,'NumColumns',3)
    legend([p1,p2,p3,p4,p5,p6],'Location','northoutside','Interpreter','latex','FontSize',11,'NumColumns',3)
end
