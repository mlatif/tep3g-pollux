function [] = sensibilite_comparee_2GMIX_hector_vs_paris(CU,P2GMIX,COMBI_2GMIX_PARIS,COMBI_2GMIX_HECTOR_WOPR,max_y)

figure; 
    hold on; box on; %grid on;     
    xlim([-120,+120])
    %ylim([0,1])
    ylim([0,max_y+1000*max_y])
    set(gca,'YScale','log');
    h = 240; 
    xticks(-(h)/2:40:+(h)/2);
    c = -115:10:+115;
    c = linspace(-120,+120,24);
    xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16)
    %ylabel("Sensibilit\'e moyenne (\%)",'Interpreter','latex','FontSize',16)
    ylabel("Sensibilit\'e $\mathbf{s}_{k}$",'Interpreter','latex','FontSize',16)
    legend('Location','north','Interpreter','latex','FontSize',12,'NumColumns',2)
    legend boxoff
    % Données Hector pures
    p1 = plot(c,1*P2GMIX,'DisplayName','${2\gamma}^{}_{\textrm{COR}}$ (hector)','LineWidth',2,'LineStyle','-','Color','k');
    p0 = scatter(0,0,'w.','DisplayName','');
    p2 = plot(c,1*COMBI_2GMIX_PARIS,'DisplayName','${1\gamma}^{511}_{\textrm{COR}} \times {1\gamma}^{1157}_{\textrm{COR}}$ (paris)','LineWidth',2,'LineStyle','-','Color',CU(1,1));
    p3 = plot(c,1*COMBI_2GMIX_HECTOR_WOPR,'DisplayName','${1\gamma}^{511}_{\textrm{COR}} \times {1\gamma}^{1157}_{\textrm{COR}}$ (aggr)','LineWidth',2,'LineStyle','--','Color',CU(2,1));
    %
    yline(1*max(P2GMIX),'--',num2str(1*max(P2GMIX),'%.2e'),'Color','k','LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12);
    yline(1*max(COMBI_2GMIX_PARIS),'--',num2str(1*max(COMBI_2GMIX_PARIS),'%.2e'),'Color',CU(1,1),'LabelHorizontalAlignment','right','LabelVerticalAlignment','top','FontSize',12);
    yline(1*max(COMBI_2GMIX_HECTOR_WOPR),'--',num2str(1*max(COMBI_2GMIX_HECTOR_WOPR),'%.2e'),'Color',CU(2,1),'LabelHorizontalAlignment','center','LabelVerticalAlignment','bottom','FontSize',12,'Color','#7E2F8E');
    %
    %legend([p1,p0,p2,p3],'Location','northeast','Interpreter','latex','FontSize',11,'NumColumns',3)
    legend([p1,p0,p2,p3],'Location','northoutside','Interpreter','latex','FontSize',11,'NumColumns',3)
end
