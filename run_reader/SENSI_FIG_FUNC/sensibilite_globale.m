function [] = sensibilite_globale(P0G,P1G511,P1G1557,P2GLOR,P2GMIX,P3G,max_y)
figure; 
    hold on; box on; %grid on;     
    xlim([-120,+120])
    %ylim([0,max(1*P0G)+0.005])
     set(gca,'YScale','log');
    %ylim([0,1])
    %ylim([0,max_y])
    h = 240; 
    xticks(-(h)/2:40:+(h)/2);
    xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16)
    %ylabel("Sensibilit\'e moyenne (\%)",'Interpreter','latex','FontSize',16)
    ylabel("Sensibilit\'e $\mathbf{s}_{k}$",'Interpreter','latex','FontSize',16)
    %legend('Location','north','Interpreter','latex','FontSize',12,'NumColumns',5)
    legend('Location','northoutside','Interpreter','latex','FontSize',12,'NumColumns',5)
    legend boxoff  
      c = -115:10:+115;
    c = linspace(-120,+120,24); 
    plot(c,1*P0G,'DisplayName','${0\gamma}^{}_{}$','Color','k','LineWidth',2,'LineStyle','-')
    plot(c,1*P1G511,'DisplayName','${1\gamma}^{511}_{\textrm{COR}}$','LineWidth',2,'LineStyle','-')
    plot(c,1*P1G1557,'DisplayName','${1\gamma}^{1157}_{\textrm{COR}}$','LineWidth',2,'LineStyle','-')
    plot(c,1*P2GLOR,'DisplayName','${2\gamma}^{}_{\textrm{LOR}}$','LineWidth',2,'LineStyle','-')
    plot(c,1*P2GMIX,'DisplayName','${2\gamma}^{}_{\textrm{COR}}$','LineWidth',2,'LineStyle','-')
    plot(c,1*P3G,'DisplayName','${3\gamma}^{}_{}$','LineWidth',2,'LineStyle','-')
    title("Sensibilité Ulysse (Hector)")
    disp("GERE L'ASPECT RATIO")
    pbaspect([2 1 1])   % https://fr.mathworks.com/help/matlab/creating_plots/aspect-ratio-for-2-d-axes.html
end     
