% READ_HECTOR: 
% Méthode dédiée à la lectuée des résultats générés avec paris et obtenir les cartes de sensibilité unitaire. 
% Voir la fonction run_paris pour voir comment le tout fonctionne.
clear all
close all 
clc; 
addpath("../methods/")
set(0,'DefaultFigureWindowStyle','docked') 
% -------------------------------------------------------------------------
path = "../unit_classe_ulysse_simu/";
name_exp = "uc_ulysse";
name_pht = "ulysse";
J = 4704; 
M = 10000; 
B = 10; 
V = 6;          
% 


path = "../paris_myrmidon_simu/";
name_exp = "myrmidon";
name_pht = "myrmidon";
J = 1;
M = 10000; 
B = 10; 
V = 6;          
%{
%}

path = "../paris_bernard_simu/";
name_exp = "bernard";
name_pht = "bernard";
J = 1;
M = 10000; 
B = 10; 
V = 10;          


% -------------------------------------------------------------------------
AFF_FIG = false; 
HIDE_LEG_FIG_PARSING = true; 

WITH_TOTAL = true;


export_folder = "DATA_PARSED"; 
if (~isfolder(export_folder) && SAVE_FIG); mkdir(export_folder); end 
folder_to_save_fig ="FIG_EPS";
if (~isfolder("FIG_EPS") && AFF_FIG); mkdir(folder_to_save_fig); end 




COLOR_LIB = [
    0, 0.4470, 0.7410;          % 0G; 
    0.6350, 0.0780, 0.1840;     % 1G511
    0.9290, 0.6940, 0.1250;     % 1G1157
    0.4940, 0.1840, 0.5560;     % 2GLOR
    0.3010, 0.7450, 0.9330;     % 2GMIX
    0.4660, 0.6740, 0.1880;     % 3G
    0.8500, 0.3250, 0.0980;     % AUTRE
]; 


%folder = (name_exp+"_v"+(1:V)+"_"+B+"_"+M+"/"+name_pht+"_v"+(1:V));
folder = (name_exp+"_v"+(1:V)+"_"+B+"/"+name_pht+"_v"+(1:V));
% -------------------------------------------------------------------------
save_name = strcat(path,"paris_parsed_",name_exp,"_v1-v",num2str(V)); 
if (~exist(strcat(save_name,".mat")) )%|| (exist(strcat(save_name,"_PART.mat"))~=0 && allow_part))
    %[paris_occ_config_count,N_total_paris,N_exp_paris,P,C,Ic,is_partial_file]= parse(J,path,folder);
    [DETECT_PAR_CLASSE,NO_DET_PAR_CLASSE,EMISSI_PAR_CLASSE,TAB_DETECT_PAR_CLASSE,TAB_K2_K3_simult_detect,is_partial_file,Ic,C,P,cpt_detec] = parse_paris(J,name_pht,path,folder,HIDE_LEG_FIG_PARSING,COLOR_LIB,AFF_FIG,folder_to_save_fig); 
    if (is_partial_file), fprintf("WARNING - PARTIAL FORLDER \n"); save_name=save_name+"_part"; end
    save(strcat(save_name,".mat"),'DETECT_PAR_CLASSE','NO_DET_PAR_CLASSE','EMISSI_PAR_CLASSE','is_partial_file','Ic','C','P','cpt_detec','TAB_DETECT_PAR_CLASSE','TAB_K2_K3_simult_detect'); 
    % Fichier local 
    save_name_lcl = strcat(export_folder,"/paris_parsed_",name_exp,"_v1-v",num2str(V)); 
    save(strcat(save_name_lcl,".mat"),'DETECT_PAR_CLASSE','NO_DET_PAR_CLASSE','EMISSI_PAR_CLASSE','is_partial_file','Ic','C','P','cpt_detec','TAB_DETECT_PAR_CLASSE','TAB_K2_K3_simult_detect'); 
else
    load(save_name); 
end
% -------------------------------------------------------------------------
if (P.J>1)
    DPC = sum(DETECT_PAR_CLASSE);
    NDPC = sum(NO_DET_PAR_CLASSE);
    EPC = sum(EMISSI_PAR_CLASSE);
    PROP = (DPC./EPC)*100;
else
    DPC = DETECT_PAR_CLASSE;
    NDPC = NO_DET_PAR_CLASSE;
    EPC = EMISSI_PAR_CLASSE;
    PROP = (DPC./EPC)*100;
end
fprintf("[PARIS|%s]\n",name_pht);
fprintf("---\n");
fprintf("Classe\t1G511*\t1G1157*\t2GLOR*\n");
fprintf("D\t%d\t%d\t%d\n",DPC(2),DPC(3),DPC(4));
fprintf("ND\t%d\t%d\t%d\n",NDPC(2),NDPC(3),NDPC(4));
fprintf("E\t%d\t%d\t%d\n",EPC(2),EPC(3),EPC(4));
fprintf("P\t%.2f\t%.2f\t%.2f\n",PROP(2),PROP(3),PROP(4));

fprintf("---\n");
fprintf("DISTRIB - Batch\n");
mean_tab = 100*V*mean(TAB_DETECT_PAR_CLASSE(:,2:end)./EPC(2:end),1);
med_tab = 100*V*median(TAB_DETECT_PAR_CLASSE(:,2:end)./EPC(2:end),1);
std_tab = 100*V*std(TAB_DETECT_PAR_CLASSE(:,2:end)./EPC(2:end),1);
fprintf("Classe\t1G511\t1G1157\t2GLOR\n");
fprintf("mean\t"); fprintf(get_fmt(mean_tab),mean_tab); fprintf("\n");
fprintf("med\t"); fprintf(get_fmt(med_tab),med_tab); fprintf("\n");
fprintf("std\t"); fprintf(get_fmt(std_tab),std_tab); fprintf("\n");
if (~all(TAB_K2_K3_simult_detect==0))
fprintf("---\n");
prop_k2k3_simult = (sum(TAB_K2_K3_simult_detect)/EPC(4))*100; 
fprintf("DETECTION SIMUL K2 K3\n");
fprintf("Classe\t1G511*\t1G1157*\t2GLOR*\t1G1157*&2GLOR*\n");
fprintf("D\t%d\t%d\t%d\t%d\n",DPC(2),DPC(3),DPC(4), sum(TAB_K2_K3_simult_detect));
fprintf("ND\t%d\t%d\t%d\n",NDPC(2),NDPC(3),NDPC(4));
fprintf("E\t%d\t%d\t%d\t%d\n",EPC(2),EPC(3),EPC(4),EPC(4));
fprintf("P\t%.2f\t%.2f\t%.2f\t%.2f\n",PROP(2),PROP(3),PROP(4),prop_k2k3_simult);
end

%{

J = P.J; 
sensi_map_0G = zeros(P.J,1);
sensi_map_unit_cc1157 = zeros(P.J,1);
sensi_map_unit_cc511 = zeros(P.J,1);
sensi_map_unit_tep = zeros(P.J,1); 

N_div = EMISSI_PAR_CLASSE;
for (j = 1:P.J)
    sensi_map_0G = DETECT_PAR_CLASSE(j,1)/N_div(j,1);   %
    sensi_map_unit_cc511(j) = DETECT_PAR_CLASSE(j,2)/N_div(j,2); % 
    sensi_map_unit_cc1157(j) = DETECT_PAR_CLASSE(j,3)/N_div(j,3); % 
    sensi_map_unit_tep(j) = DETECT_PAR_CLASSE(j,4)/N_div(j,4); % 
end
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
fileID = fopen(strcat(path,"sensi_0G.i"),'w','l');
    for (j=1:P.J)
        d = sensi_map_unit_tep(j); 
        fwrite(fileID,d,"float32");
    end
fclose(fileID); 
% ---
fileID = fopen(strcat(path,"sensi_tep_unit.i"),'w','l');
    for (j=1:P.J)
        d = sensi_map_unit_tep(j); 
        fwrite(fileID,d,"float32");
    end
fclose(fileID); 
% ---
fileID = fopen(strcat(path,"sensi_cc511_unit.i"),'w','l');
    for (j=1:P.J)
        d = sensi_map_unit_cc511(j); 
        fwrite(fileID,d,"float32");
    end
fclose(fileID);
% ---
fileID = fopen(strcat(path,"sensi_cc1157_unit.i"),'w','l');
    for (j=1:P.J)
        d = sensi_map_unit_cc1157(j); 
        fwrite(fileID,d,"float32");
    end
fclose(fileID);


N_exp = sum(EMISSI_PAR_CLASSE);  
Prop = sum(DETECT_PAR_CLASSE); 
PERC = (Prop./N_exp)*100;




fprintf("[PARIS|%s]\n",name_pht); 

fprintf("---\n"); 
fprintf("sum(ND(2:end)) = %d\n",sum(NDPC(2:end))); 
fprintf("S\tClassification des évènements détectés (en %%) \n");
for (i=2:4)%length(C.Classif))
    (DPC(i)/EPC(i))*100
    fprintf("\t\t%s : %g \n",C.Classif(i),PERC(i));
end             
%}

%{
fprintf("Nombre d'émissions réalisées pour avoir %d détections : %d \n",cpt_detec*B,sum(EMISSI_PAR_CLASSE,'all'));
fprintf("S\tClassification des évènements détectés (en %%) \n");
for (i=1:4)%length(C.Classif))
    fprintf("\t\t%s : %g \n",C.Classif(i),PERC(i));
end
%}

% -------------------------------------------------------------------------
P_unit = zeros(P.D(3),3); 
cpt = 0; 
for classe = 2:4
    Det = DETECT_PAR_CLASSE; 
    Emi = EMISSI_PAR_CLASSE; 
    cpt = cpt+1; 
    P_unit(:,cpt) = get_ProfEnZ(P,Emi,Det,classe) ; 
end

save(strcat(export_folder,"/P_margin_",upper(name_exp),".mat"),'P_unit','DETECT_PAR_CLASSE','NO_DET_PAR_CLASSE','EMISSI_PAR_CLASSE','is_partial_file','Ic','C','P','COLOR_LIB'); 





function [DETECT_PAR_CLASSE,NO_DET_PAR_CLASSE,EMISSI_PAR_CLASSE,TAB_DETECT_PAR_CLASSE,TAB_K2_K3_simult_detect,is_partial_file,Ic,C,P,cpt_detec] = parse_paris(J,name_pht,path,folder,HIDE_LEG_FIG_PARSING,CU,AFF_FIG,folder_to_save_fig)
    % coordonnées entières de positionnement des voxels
    Ic = zeros(J,3);    
    once_Ic = true; 
    once_cpt_detec = true; 
    fig = figure(1); 
    DETECT_PAR_CLASSE = zeros(J,4); 
    NO_DET_PAR_CLASSE = zeros(J,4); 
    EMISSI_PAR_CLASSE = zeros(J,4);
    TAB_K2_K3_simult_detect = zeros(length(folder),1);
    TAB_DETECT_PAR_CLASSE = zeros(length(folder),4);
    latex_legend = ["0\gamma","1\gamma_{511}","1\gamma_{1157}","2\gamma_{\textrm{LOR}}"];
    is_partial_file = false; 
    cpt_detec = 0;


    for (i=1:length(folder))    
        % LECTURE DU FICHIER
        f = strsplit(folder(i),"/");
        fold = f(1);    file = f(2);
        fprintf("FOLD = %s \t FILE = %s\n",fold,file); 
        name_conf = strcat(path,fold,"/paris_",file,"_conf.mat");
        load(name_conf);
        once_cpt_detec = true; 
        % EXTRACTION DES DONNEES PAR VOXEL
        tmp_k2_k3 = zeros(1,1); 
        glob_tmp_detect = zeros(1,4); 
        glob_tmp_k2k3sim = 0; 
        for (j=1:P.J)
            [a,b,c] = get_axes_voxel_idx(P,j);
            if (once_Ic), Ic(j,:) = [a,b,c];once_Ic = false;   end % On le fait qu'une fois 
            fprintf("[PARIS|%s]Voxel j = %d \t [%d,%d,%d]\n",file,j,a,b,c); 
            % STR de stockage temporaire
            tmp_detect = zeros(1,4); 
            tmp_no_det = zeros(1,4); 
            tmp_emissi = zeros(1,4); 
          
            % EXTRACTION DES DONNES PAR BATCH POUR UN VOXEL
            for (batch = 1:C.paris_N_batch)
               
                name_batch = strcat(path,fold,"/paris_",file,"_j",num2str(j),"_b",num2str(batch),".mat");
                if (false), fprintf("%s\n",name_batch); end 
                if (isfile(name_batch))   % sécurité supplémentaire
                    load(name_batch);
                    detect = h.occ_config_count(1:4);
                    no_det = h.non_detect_count(1:4);
                    emissi = detect + no_det; 
                    if (once_cpt_detec), cpt_detec = cpt_detec + emissi(end); once_cpt_detec = false;  end
                    if (detect ~= sum(no_det)), error("PROBLEME : detect ~= sum(non_detect) \n"); end
                    if (false)
                        fprintf("Classe\t0G\t1G511\t1G1157\t2GLOR\n");
                        fprintf("---\n"); 
                        fprintf("D\t%d\t%d\t%d\t%d\n",detect(1),detect(2),detect(3),detect(4));
                        fprintf("ND\t%d\t%d\t%d\t%d\n",no_det(1),no_det(2),no_det(3),no_det(4));
                        fprintf("---\n"); 
                        fprintf("E\t%d\t%d\t%d\t%d\n",emissi(1),emissi(2),emissi(3),emissi(4));
                    end
                    % -----------------------------------------------------
                    tmp_detect = tmp_detect + detect; 
                    tmp_no_det = tmp_no_det + no_det;
                    tmp_emissi = tmp_emissi + emissi;
                    if (isfield(h,'K2_K3_simult_detect'))
                        tmp_k2_k3 = tmp_k2_k3 + h.K2_K3_simult_detect; 
                    end
                    % -----------------------------------------------------

                    %sum(detect)
                    %sum(non_detect)


                    test = ""; 
                else
                    is_partial_file = true; 
                end

            end
            clc;
            fprintf("[PARIS|%s]Voxel j = %d \t [%d,%d,%d]\n",file,j,a,b,c); 
            fprintf("Classe\t0G\t1G511*\t1G1157*\t2GLOR*\n");
            fprintf("---\n"); 
            fprintf("D\t%d\t%d\t%d\t%d\n",tmp_detect(1),tmp_detect(2),tmp_detect(3),tmp_detect(4));
            fprintf("ND\t%d\t%d\t%d\t%d\n",tmp_no_det(1),tmp_no_det(2),tmp_no_det(3),tmp_no_det(4));
            fprintf("---\n"); 
            fprintf("E\t%d\t%d\t%d\t%d\n",tmp_emissi(1),tmp_emissi(2),tmp_emissi(3),tmp_emissi(4));
            fprintf("sum(ND(2:end)) = %d\n",sum(tmp_no_det(2:end))); 

            DETECT_PAR_CLASSE(j,:) = DETECT_PAR_CLASSE(j,:) + tmp_detect; 
            NO_DET_PAR_CLASSE(j,:) = NO_DET_PAR_CLASSE(j,:) + tmp_no_det;
            EMISSI_PAR_CLASSE(j,:) = EMISSI_PAR_CLASSE(j,:) + tmp_emissi;
            glob_tmp_detect =  glob_tmp_detect + tmp_detect; 
            glob_tmp_k2k3sim = glob_tmp_k2k3sim + tmp_k2_k3;

            
        end

        TAB_DETECT_PAR_CLASSE(i,:) = glob_tmp_detect; 
        if (isfield(h,'K2_K3_simult_detect'))
            TAB_K2_K3_simult_detect(i) = glob_tmp_k2k3sim;
            test = "";
        end
        once_Ic = false;
        if (AFF_FIG)
            % -----------------------------------------------------------------
            ax = gca; % current axes
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Génération de la distribution à afficher
            clf;
            hold on; set(gca,'YScale','log');
            xlim([0,P.J])
            % plot(1:j,DETECT_PAR_CLASSE(1:j,:)./EMISSI_PAR_CLASSE(1:j,:))
            plot(1:j,DETECT_PAR_CLASSE(1:j,1)./EMISSI_PAR_CLASSE(1:j,1),'--','DisplayName',strcat('$',latex_legend(1),'$')); % Inutile mais bon, c'est un détrompeur pour voir si on s'est pas planté
            plot(1:j,DETECT_PAR_CLASSE(1:j,2)./EMISSI_PAR_CLASSE(1:j,2),'DisplayName',strcat('$',latex_legend(2),'$'));
            plot(1:j,DETECT_PAR_CLASSE(1:j,3)./EMISSI_PAR_CLASSE(1:j,3),'DisplayName',strcat('$',latex_legend(3),'$'));
            plot(1:j,DETECT_PAR_CLASSE(1:j,4)./EMISSI_PAR_CLASSE(1:j,4),'DisplayName',strcat('$',latex_legend(4),'$'));

            xlabel(strcat("$j\in \{1,...,",num2str(J,'%d'),"\}$"),'Interpreter','latex','FontSize',12);
            %ylabel('Detection $\div$ Emission','Interpreter','latex','FontSize',12)
            if (~HIDE_LEG_FIG_PARSING)
                legend('Location','southoutside','NumColumns',4,'Interpreter','latex','FontSize',12); legend boxoff;
                title(strcat(num2str(cpt_detec*C.paris_N_batch,"%.2e")," detections"),'Interpreter','latex','FontSize',13)
            end
            % Avec ou sans légende
            tok_leg = ""; if (~HIDE_LEG_FIG_PARSING), tok_leg = "_with_leg"; end
            NAME = "read_paris_results";
            if (NAME~="")
                saveas(gcf,strcat(folder_to_save_fig,"/",NAME,"_f",num2str(i,'%d'),tok_leg,'.eps'),'epsc');
            end
            % Avec légende et titre figure pour analyse
            legend('Location','southoutside','NumColumns',4,'Interpreter','latex','FontSize',12); legend boxoff;
            title(strcat(num2str(cpt_detec*C.paris_N_batch,"%.2e")," detections"),'Interpreter','latex','FontSize',13)
            tok_leg = "_with_leg";
            NAME = "read_paris_results";
            if (NAME~="")
                saveas(gcf,strcat(folder_to_save_fig,"/",NAME,"_f",num2str(i,'%d'),tok_leg,'.eps'),'epsc');
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Génération des profils en Z à afficher
            P_unit_1G511 = get_ProfEnZ(P,EMISSI_PAR_CLASSE,DETECT_PAR_CLASSE,2);
            P_unit_1G1157 = get_ProfEnZ(P,EMISSI_PAR_CLASSE,DETECT_PAR_CLASSE,3);
            P_unit_2GLOR = get_ProfEnZ(P,EMISSI_PAR_CLASSE,DETECT_PAR_CLASSE,4);
            %
            clf;
            hold on; %set(gca,'YScale','log');
            xlim([-120,+120])
            h = 240;
            %xticks(-(h)/2:40:+(h)/2));
            xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16);
            %ylabel("Sensibilit\'e $\mathbf{s}^{k}$",'Interpreter','latex','FontSize',16);
            c = linspace(-120,+120,P.D(3));
            c = unique(P.Rc(:,3)) + P.V(3)/2;
            xticks(c);
            p1 = plot(c,1*P_unit_1G511,'DisplayName', strcat('$',latex_legend(2),'$ unit'),'LineWidth',2,'LineStyle','-','Color',CU(2,:),'Marker','.');
            p2 = plot(c,1*P_unit_1G1157,'DisplayName',strcat('$',latex_legend(3),'$ unit'),'LineWidth',2,'LineStyle','-','Color',CU(3,:),'Marker','.');
            p3 = plot(c,1*P_unit_2GLOR,'DisplayName',strcat('$',latex_legend(4),'$ unit'),'LineWidth',2,'LineStyle','-','Color',CU(4,:),'Marker','.') ;
            yline(1*max(P_unit_1G511),'--','Color',CU(2,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12); % ,num2str(1*max(P_unit_1G511),'%.2e')
            text(120+1, max(P_unit_1G511), num2str(1*max(P_unit_1G511),'%.2e'), 'FontSize', 12, 'Color', CU(2,:));
            yline(1*max(P_unit_1G1157),'--',num2str(1*max(P_unit_1G1157),'%.2e'),'Color',CU(3,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',12);
            yline(1*max(P_unit_2GLOR),'--',num2str(1*max(P_unit_2GLOR),'%.2e'),'Color',CU(4,:),'LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',12);
            if (~HIDE_LEG_FIG_PARSING)
                legend([p1,p2,p3],'Location','northoutside','Interpreter','latex','FontSize',11,'NumColumns',3); legend boxoff
            end
            % Avec ou sans légende
            tok_leg = ""; if (~HIDE_LEG_FIG_PARSING), tok_leg = "_with_leg"; end
            NAME = "read_paris_PeZ";
            if (NAME~="")
                saveas(gcf,strcat(folder_to_save_fig,"/",NAME,"_f",num2str(i,'%d'),tok_leg,'.eps'),'epsc');
            end
            % Avec légende et titre figure pour analyse
            title(strcat(num2str(cpt_detec*C.paris_N_batch,"%.2e")," detections"),'Interpreter','latex','FontSize',13)
            legend([p1,p2,p3],'Location','northoutside','Interpreter','latex','FontSize',11,'NumColumns',3); legend boxoff
            tok_leg = "_with_leg";
            NAME = "read_paris_PeZ";
            if (NAME~="")
                saveas(gcf,strcat(folder_to_save_fig,"/",NAME,"_f",num2str(i,'%d'),tok_leg,'.eps'),'epsc');
            end

            test = "";
        end
    end
    

    test = "";
end






function [paris_occ_config_count,N_total_paris,N_exp_paris,N_per_class,P,C,Ic,is_partial_file] = parse(J,path,folder)
    paris_occ_config_count = zeros(J,6);
    paris_all_trials_count = zeros(J,6); 
    Ic = zeros(J,3);
    %
    N_total_paris = 0;
    %
    N_exp_paris = 0;
    %
    N_per_class.c0G = 0; 
    N_per_class.c1G511 = 0;
    N_per_class.c1G1157 = 0;
    N_per_class.c2GLOR = 0;
    %
    for (i=1:length(folder))
        f = strsplit(folder(i),"/");
        fold = f(1);
        file = f(2);
        % ---------------------------------------------------------------------
        name = strcat(path,fold,"/paris_",file,"_conf.mat");

        load(name);
        %load(strcat(path,fold,"/hector_",file,"_conf.mat"));
         
        N_exp_paris = N_exp_paris + C.paris_N_batch * C.paris_N; 
        
        for (j=1:P.J)
            [a,b,c] = get_axes_voxel_idx(P,j);
            [r_a,r_b,r_c] = get_axes_voxel_coord(P,a,b,c);
            Ic(j,:) = [a,b,c];
            j_nexp = 0;
            j_occ_config_count = zeros(1,6);
            j_all_trials_count = zeros(1,6); 
            fprintf("[PARIS|%s]Voxel j = %d \t [%d,%d,%d] \t [%g,%g,%g] \n",file,j,a,b,c,r_a,r_b,r_c)
            for (b = 1:C.paris_N_batch)
                % hector_achille_v1_j9_b9.mat
                name = strcat(path,fold,"/paris_",file,"_j",num2str(j),"_b",num2str(b),".mat");
                if (isfile(name))
                    load(name);
                    % LE COMPTAGE DE PARIS SE FAIT SUR LES DONNEES DETECTEES ET LES NON DETECTEES
                    N_total_paris = N_total_paris + sum(h.occ_config_count) + sum(h.non_detect_count);
                    %fprintf("Batch %d \t N(émissions) = %d \n",b,sum(h.occ_config_count))
                    fprintf("Détection : %d \t Non détection : %d \n",sum(h.occ_config_count),sum(h.non_detect_count))
                    % ON COMPTE SEPAREMENT LES DONNEES PAR CLASSE POUR NORMALISER CORRECTEMENT
                    N_per_class.c0G = N_per_class.c0G + h.occ_config_count(1); 
                    N_per_class.c1G511 = N_per_class.c1G511 + h.occ_config_count(2);
                    N_per_class.c1G1157 = N_per_class.c1G1157 + h.occ_config_count(3);
                    N_per_class.c2GLOR = N_per_class.c2GLOR + h.occ_config_count(4);

                    j_occ_config_count = j_occ_config_count + h.occ_config_count;
                    j_all_trials_count = j_all_trials_count + h.occ_config_count + h.non_detect_count; 
                    j_nexp = j_nexp + h.N_exp;
                    clear('h')
                else
                    is_partial_file = true; 
                end
                clear('name');
            end
            paris_occ_config_count(j,:) = paris_occ_config_count(j,:) + j_occ_config_count;
            paris_all_trials_count(j,:) = paris_all_trials_count(j,:) + j_all_trials_count; 
            %N_exp(j) = N_exp(j) + j_nexp;
            break; 
            test = ""; 
        end

    break ;



        test ="";
    end

    test = "";
end