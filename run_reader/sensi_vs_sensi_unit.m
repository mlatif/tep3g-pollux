clear all
close all
set(0,'DefaultFigureWindowStyle','docked') ; 
addpath("SENSI_FIG_FUNC/");
clc; 
% -------------------------------------------------------------------------
% VERSION 
%       1) LOR_is_N_1G511 = 0 && COR_is_N_1G511 = 2 (ie 1)
%       2) LOR_is_N_1G511 = 1 && COR_is_N_1G511 = 2 (ie 1)
%       3) LOR_is_N_1G511 = 2 && COR_is_N_1G511 = 1 (ie 0.5)
SAVE_FIG = true; 
VERSION=3; 
% -------------------------------------------------------------------------
NAME = "";
[P_norm_wi_pr,P_norm_wo_pr,P_aggr_wi_pr,P_aggr_wo_pr,P_unit] = just_import(VERSION);
[P_hector_norm,P_hector_aggr,P_paris_unit] = just_extract(P_norm_wi_pr,P_norm_wo_pr,P_aggr_wi_pr,P_aggr_wo_pr,P_unit);
% -------------------------------------------------------------------------
if (VERSION==3)
    P_paris_unit.c1G511 = P_paris_unit.c1G511./2;
    P_hector_aggr.wopr_1G511 = (P_hector_aggr.wopr_1G511)./2;
    P_hector_aggr.wipr_1G511 = (P_hector_aggr.wipr_1G511)./2;
end


% Plot simples
CU = ["#0072BD", "${1\gamma}^{511}_{\textrm{COR}}$"; "#7E2F8E", "${1\gamma}^{1157}_{\textrm{COR}}$"; "#A2142F", "${2\gamma}^{}_{\textrm{LOR}}$"]; 
max_y_all_cat = max([max(P_paris_unit.c1G511),max(P_hector_aggr.wopr_1G511),max(P_hector_aggr.wipr_1G511), ...
    max(P_paris_unit.c1G1157),max(P_hector_aggr.wopr_1G1157),max(P_hector_aggr.wipr_1G511), ...
    max(P_paris_unit.c2GLOR),max(P_hector_aggr.wopr_2GLOR),max(P_hector_aggr.wipr_2GLOR)]);
% Comparaison de version 
VERSION_1 = 2; 
VERSION_2 = 3;
[P_norm_wi_pr_1,P_norm_wo_pr_1,P_aggr_wi_pr_1,P_aggr_wo_pr_1,P_unit_1] = just_import(VERSION_1);
[P_hector_norm_1,P_hector_aggr_1,~] = just_extract(P_norm_wi_pr_1,P_norm_wo_pr_1,P_aggr_wi_pr_1,P_aggr_wo_pr_1,P_unit_1);
% -----
if (VERSION_1==3)
    P_paris_unit.c1G511 = P_paris_unit.c1G511./2;
    P_hector_aggr.wopr_1G511 = (P_hector_aggr.wopr_1G511)./2;
    P_hector_aggr.wipr_1G511 = (P_hector_aggr.wipr_1G511)./2;
end
max_y_all_cat_1 = max([max(P_paris_unit.c1G511),max(P_hector_aggr_1.wopr_1G511),max(P_hector_aggr_1.wipr_1G511), ...
    max(P_paris_unit.c1G1157),max(P_hector_aggr_1.wopr_1G1157),max(P_hector_aggr_1.wipr_1G511), ...
    max(P_paris_unit.c2GLOR),max(P_hector_aggr_1.wopr_2GLOR),max(P_hector_aggr_1.wipr_2GLOR)]);
[P_norm_wi_pr_2,P_norm_wo_pr_2,P_aggr_wi_pr_2,P_aggr_wo_pr_2,P_unit_2] = just_import(VERSION_2);
[P_hector_norm_2,P_hector_aggr_2,~] = just_extract(P_norm_wi_pr_2,P_norm_wo_pr_2,P_aggr_wi_pr_2,P_aggr_wo_pr_2,P_unit_2);
% -----
if (VERSION_2==3)
    P_paris_unit.c1G511 = P_paris_unit.c1G511./2;
    P_hector_aggr.wopr_1G511 = (P_hector_aggr.wopr_1G511)./2;
    P_hector_aggr.wipr_1G511 = (P_hector_aggr.wipr_1G511)./2;
end
max_y_all_cat_2 = max([max(P_paris_unit.c1G511),max(P_hector_aggr_2.wopr_1G511),max(P_hector_aggr_2.wipr_1G511), ...
    max(P_paris_unit.c1G1157),max(P_hector_aggr_2.wopr_1G1157),max(P_hector_aggr_2.wipr_1G511), ...
    max(P_paris_unit.c2GLOR),max(P_hector_aggr_2.wopr_2GLOR),max(P_hector_aggr_2.wipr_2GLOR)]);
max_y_comp_vers = max([max_y_all_cat_1,max_y_all_cat_2]);


% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
if (true)
    fprintf("VERSION =  %d\n",VERSION);
    fprintf("Figures:\n\t" + ...
     "UNITAIRES\n\t" + ...
     "1) [H] Sensibilité globale \n\t" + ...
     "2) [P] Sensibilité des briques unitaires \n\t" + ...
     "3) [P&H] Sensibilité des briques unitaires VS aggregées - 1G511 \n\t" + ...
     "4) [P&H] Sensibilité des briques unitaires VS aggregées - 1G1157 \n\t" + ...
     "5) [P&H] Sensibilité des briques unitaires VS aggregées - 2GLOR \n\t" + ...
     "6) [H] Sensibilité des briques aggregées sans PR + maximum atteints \n\t" + ...
     "7) [H] Sensibilité des briques aggregées avec PR + maximum atteints \n\t" + ...
     "8) [P&H] Sensibilité comparée combi 3G  unitaire/aggregées sans PR + maximum atteints \n\t" + ...
     "9) [P&H] Sensibilité comparée combi 3G  unitaire/aggregées avec PR + maximum atteints \n\t" + ...
     "10) [P&H] Sensibilité comparée combi 2GMIX  unitaire/aggregées sans PR + maximum atteints \n\t" + ...
     "11) [P&H] Sensibilité comparée combi 2GMIX  unitaire/aggregées avec PR + maximum atteints \n\t" + ...
     "12) [P] Sensibilité des briques unitaires pour comparaison avec 6),7) \n\t" + ...
     "COMPARAISON VERSIONS d'acquisition V%d vs V%d\n\t" + ...
     "13) [P&H-COMP] Sensibilité des briques unitaires VS aggregées - 1G511 \n\t" + ...
     "14) [P&H-COMP] Sensibilité des briques unitaires VS aggregées - 1G1157 \n\t" + ...
     "15) [P&H-COMP] Sensibilité des briques unitaires VS aggregées - 2GLOR \n\t" + ...
     "16) [H-COMP] Sensibilité des briques aggregées sans PR + maximum atteints \n\t" + ...
     "17) [H-COMP] Sensibilité des briques aggregées avec PR + maximum atteints \n\t" + ...
     "18) [P&H-COMP] Sensibilité comparée combi 3G  unitaire/aggregées sans PR + maximum atteints \n\t" + ...
     "19) [P&H-COMP] Sensibilité comparée combi 3G  unitaire/aggregées avec PR + maximum atteints \n\t" + ...
     "20) [P&H-COMP] Sensibilité comparée combi 2GMIX  unitaire/aggregées sans PR + maximum atteints \n\t" + ...
     "21) [P&H-COMP] Sensibilité comparée combi 2GMIX  unitaire/aggregées avec PR + maximum atteints \n\t" + ...
     "22) [P-COMP] Sensibilité des briques unitaires pour comparaison avec 16),17) \n\t" + ...
     "\n...\n",VERSION_1,VERSION_2); 
%         "1) Distribution angle beta\n\t" + ...
%         "2) Distribution distance entre les intersections (2 inters)\n\t" + ...
%         "3) Distribution distance à l'émission (1 inter)\n\t" + ...
%         "4) Distribution distance à l'émission (2 inters - [min(:,1),max(:,2)])\n\t" + ...
%         "5) Distribution des angles simulés ([511,1157])\n\t" + ...
%         "6) Distribution distance PR (Valide ? = %g)\n\t" + ...
%         "7) Distribution bivariée distance entre 2 inters x angle beta associé \n\t" + ...
%         "8) Expliquer les résultats de distance entre intersection avec le PR ? \n\t" + ...
%         "\n",pos_rng.active)
    
    % -------------------------------------------------------------------------

    
    choix = input("Choix de figure = "); 
    switch choix
        case 1
            max_y = max(max([P_hector_norm.wipr_c0G,P_hector_norm.wipr_c1G511,P_hector_norm.wipr_c1G1557,P_hector_norm.wipr_c2GLOR,P_hector_norm.wipr_c2GMIX,P_hector_norm.wipr_c3G])); 
            sensibilite_globale(P_hector_norm.wipr_c0G,P_hector_norm.wipr_c1G511,P_hector_norm.wipr_c1G1557,P_hector_norm.wipr_c2GLOR,P_hector_norm.wipr_c2GMIX,P_hector_norm.wipr_c3G,max_y); 
            NAME = ""; 
            fprintf("NAME = %s (V%d)\n",NAME,VERSION);
        case 2
            max_y =max([max(P_paris_unit.c1G511),max(P_paris_unit.c1G1157),max(P_paris_unit.c2GLOR)]); 
            sensibilite_unitaire_paris(CU,P_paris_unit.c1G511,P_paris_unit.c1G1157,P_paris_unit.c2GLOR,max_y)
            NAME = "ulysse_paris_unit_brique";
            fprintf("NAME = %s (V%d)\n",NAME,VERSION);
        case 3
            max_y = max_y_all_cat;
            sensibilite_unitaire_paris_vs_hector(CU,P_paris_unit.c1G511,P_hector_aggr.wopr_1G511,P_hector_aggr.wipr_1G511,1,max_y)
            NAME = "brique_unitaire_paris_vs_hector_1G511";
            fprintf("NAME = %s (V%d)\n",NAME,VERSION); NAME = strcat(NAME,"_V",num2str(VERSION));
        case 4 
            max_y = max_y_all_cat;
            sensibilite_unitaire_paris_vs_hector(CU,P_paris_unit.c1G1157,P_hector_aggr.wopr_1G1157,P_hector_aggr.wipr_1G511,2,max_y)
            NAME = "brique_unitaire_paris_vs_hector_1G1557";
            fprintf("NAME = %s (V%d)\n",NAME,VERSION); NAME = strcat(NAME,"_V",num2str(VERSION));
        case 5
            max_y = max_y_all_cat;
            sensibilite_unitaire_paris_vs_hector(CU,P_paris_unit.c2GLOR,P_hector_aggr.wopr_2GLOR,P_hector_aggr.wipr_2GLOR,3,max_y)
            NAME = "brique_unitaire_paris_vs_hector_2GLOR";
            fprintf("NAME = %s (V%d)\n",NAME,VERSION); NAME = strcat(NAME,"_V",num2str(VERSION));
        case 6 
            my_title = "Sensibilit\'e Ulysse wo PR (Hector)";
            max_y = max_y_all_cat;
            sensibilite_globale_aggregee(CU,P_hector_aggr.wopr_1G511,P_hector_aggr.wopr_1G1157,P_hector_aggr.wopr_2GLOR,my_title,max_y)
            NAME = "sensibilite_ulysse_hector_without_pr";
            fprintf("NAME = %s (V%d)\n",NAME,VERSION); NAME = strcat(NAME,"_V",num2str(VERSION));
        case 7 
            my_title = "Sensibilit\'e Ulysse wi PR (Hector)";
            max_y = max_y_all_cat;
            sensibilite_globale_aggregee(CU,P_hector_aggr.wipr_1G511,P_hector_aggr.wipr_1G511,P_hector_aggr.wipr_2GLOR,my_title,max_y)
            NAME = "sensibilite_ulysse_hector_with_pr";
            fprintf("NAME = %s (V%d)\n",NAME,VERSION); NAME = strcat(NAME,"_V",num2str(VERSION));
        case 8
            COMBI_3G_PARIS = P_paris_unit.c2GLOR.*P_paris_unit.c1G1157;   
            % Par données aggregées sans positron range 
            COMBI_3G_HECTOR_WOPR = P_hector_aggr.wopr_2GLOR.*P_hector_aggr.wopr_1G1157;   
            max_y = max([max(P_hector_norm.wopr_c3G),max(COMBI_3G_PARIS),max(COMBI_3G_HECTOR_WOPR)]); 
            sensibilite_comparee_3G_hector_vs_paris(CU,P_hector_norm.wopr_c3G,COMBI_3G_PARIS,COMBI_3G_HECTOR_WOPR,max_y)
            NAME = "sensibilite_ulysse_paris_vs_hector_wo_pr_3g";
            fprintf("NAME = %s (V%d)\n",NAME,VERSION); NAME = strcat(NAME,"_V",num2str(VERSION));
        case 9
            COMBI_3G_PARIS = P_paris_unit.c2GLOR.*P_paris_unit.c1G1157;    
            COMBI_3G_HECTOR_WIPR = P_hector_aggr.wipr_2GLOR.*P_hector_aggr.wipr_1G511;    
            max_y = max([max(P_hector_norm.wipr_c3G),max(COMBI_3G_PARIS),max(COMBI_3G_HECTOR_WIPR)]); 
            sensibilite_comparee_3G_hector_vs_paris(CU,P_hector_norm.wipr_c3G,COMBI_3G_PARIS,COMBI_3G_HECTOR_WIPR,max_y)
            NAME = "sensibilite_ulysse_paris_vs_hector_wi_pr_3g";
            fprintf("NAME = %s (V%d)\n",NAME,VERSION); NAME = strcat(NAME,"_V",num2str(VERSION));
        case 10
            COMBI_2GMIX_PARIS = P_paris_unit.c1G511.*P_paris_unit.c1G1157;   
            COMBI_2GMIX_HECTOR_WOPR = P_hector_aggr.wopr_1G511.*P_hector_aggr.wopr_1G1157;    
            max_y = max([max(P_hector_norm.wopr_c2GMIX),max(COMBI_2GMIX_PARIS),max(COMBI_2GMIX_HECTOR_WOPR)]); 
            sensibilite_comparee_2GMIX_hector_vs_paris(CU,P_hector_norm.wopr_c2GMIX,COMBI_2GMIX_PARIS,COMBI_2GMIX_HECTOR_WOPR,max_y)
            NAME = "sensibilite_ulysse_paris_vs_hector_wo_pr_2gmix";
            fprintf("NAME = %s (V%d)\n",NAME,VERSION); NAME = strcat(NAME,"_V",num2str(VERSION));
        case 11
            COMBI_2GMIX_PARIS = P_paris_unit.c1G511.*P_paris_unit.c1G1157;   
            COMBI_2GMIX_HECTOR_WIPR = P_hector_aggr.wipr_1G511.*P_hector_aggr.wipr_1G511;   
            max_y = max([max(P_hector_norm.wipr_c2GMIX),max(COMBI_2GMIX_PARIS),max(COMBI_2GMIX_HECTOR_WIPR)]); 
            sensibilite_comparee_2GMIX_hector_vs_paris(CU,P_hector_norm.wipr_c2GMIX,COMBI_2GMIX_PARIS,COMBI_2GMIX_HECTOR_WIPR,max_y)
            NAME = "sensibilite_ulysse_paris_vs_hector_wi_pr_2gmix";
            fprintf("NAME = %s (V%d)\n",NAME,VERSION); NAME = strcat(NAME,"_V",num2str(VERSION));
        case 12
            max_y = max_y_all_cat;
            sensibilite_unitaire_paris_with_coherent_scale(CU,P_paris_unit.c1G511,P_paris_unit.c1G1157,P_paris_unit.c2GLOR,max_y)
            NAME = "ulysse_paris_unit_brique_with_coherent_scale";
            fprintf("NAME = %s (V%d)\n",NAME,VERSION); NAME = strcat(NAME,"_V",num2str(VERSION));
        case 13
            idx = 1; 
            max_y=max_y_comp_vers;
            P_unit = P_paris_unit.c1G511; 
            P_wo_V1 = P_hector_aggr_1.wopr_1G511; 
            P_wi_V1 = P_hector_aggr_1.wipr_1G511;
            P_wo_V2 = P_hector_aggr_2.wopr_1G511; 
            P_wi_V2 = P_hector_aggr_2.wipr_1G511;
            sensibilite_unitaire_paris_vs_hector_comp_version(CU,P_unit,P_wo_V1,P_wi_V1,P_wo_V2,P_wi_V2,idx,max_y)
            NAME = "brique_unitaire_paris_vs_hector_1G511_COMP";
            fprintf("NAME = %s (V%d)\n",NAME,VERSION); NAME = strcat(NAME,"_V",num2str(VERSION_1),"_V",num2str(VERSION_2));
        case 14
            idx = 2; 
            max_y=max_y_comp_vers;
            P_unit = P_paris_unit.c1G1157; 
            P_wo_V1 = P_hector_aggr_1.wopr_1G1157; 
            P_wi_V1 = P_hector_aggr_1.wipr_1G1157;
            P_wo_V2 = P_hector_aggr_2.wopr_1G1157; 
            P_wi_V2 = P_hector_aggr_2.wipr_1G1157;
            sensibilite_unitaire_paris_vs_hector_comp_version(CU,P_unit,P_wo_V1,P_wi_V1,P_wo_V2,P_wi_V2,idx,max_y)
            NAME = "brique_unitaire_paris_vs_hector_1G1157_COMP";
            fprintf("NAME = %s (V%d)\n",NAME,VERSION); NAME = strcat(NAME,"_V",num2str(VERSION_1),"_V",num2str(VERSION_2));            
        case 15
            idx = 3; 
            max_y=max_y_comp_vers;
            P_unit = P_paris_unit.c2GLOR; 
            P_wo_V1 = P_hector_aggr_1.wopr_2GLOR; 
            P_wi_V1 = P_hector_aggr_1.wipr_2GLOR;
            P_wo_V2 = P_hector_aggr_2.wopr_2GLOR; 
            P_wi_V2 = P_hector_aggr_2.wipr_2GLOR;
            sensibilite_unitaire_paris_vs_hector_comp_version(CU,P_unit,P_wo_V1,P_wi_V1,P_wo_V2,P_wi_V2,idx,max_y)
            NAME = "brique_unitaire_paris_vs_hector_2GLOR_COMP";
            fprintf("NAME = %s (COMP)\n",NAME); NAME = strcat(NAME,"_V",num2str(VERSION_1),"_V",num2str(VERSION_2));
        case 16
            max_y=max_y_comp_vers;
            P_1G511_V1 = P_hector_aggr_1.wopr_1G511; 
            P_1G511_V2 = P_hector_aggr_2.wopr_1G511; 
            P_1G1157_V1 = P_hector_aggr_1.wopr_1G1157; 
            P_1G1157_V2 = P_hector_aggr_2.wopr_1G1157; 
            P_2GLOR_V1 = P_hector_aggr_1.wopr_2GLOR; 
            P_2GLOR_V2 = P_hector_aggr_2.wopr_2GLOR; 
            my_title = "Sensibilit\'e Ulysse wo PR (Hector)";
            sensibilite_globale_aggregee_comp_version(CU,P_1G511_V1,P_1G511_V2,P_1G1157_V1,P_1G1157_V2,P_2GLOR_V1,P_2GLOR_V2,my_title,max_y)
            NAME = "sensibilite_ulysse_hector_without_pr_COMP";
            fprintf("NAME = %s (COMP)\n",NAME); NAME = strcat(NAME,"_V",num2str(VERSION_1),"_V",num2str(VERSION_2));
        case 17
            max_y=max_y_comp_vers;
            P_1G511_V1 = P_hector_aggr_1.wipr_1G511; 
            P_1G511_V2 = P_hector_aggr_2.wipr_1G511; 
            P_1G1157_V1 = P_hector_aggr_1.wipr_1G1157; 
            P_1G1157_V2 = P_hector_aggr_2.wipr_1G1157; 
            P_2GLOR_V1 = P_hector_aggr_1.wipr_2GLOR; 
            P_2GLOR_V2 = P_hector_aggr_2.wipr_2GLOR; 
            my_title = "Sensibilit\'e Ulysse wi PR (Hector)";
            sensibilite_globale_aggregee_comp_version(CU,P_1G511_V1,P_1G511_V2,P_1G1157_V1,P_1G1157_V2,P_2GLOR_V1,P_2GLOR_V2,my_title,max_y)
            NAME = "sensibilite_ulysse_hector_with_pr";
            fprintf("NAME = %s (COMP)\n",NAME); NAME = strcat(NAME,"_V",num2str(VERSION_1),"_V",num2str(VERSION_2));
        case 18
            P2GMIX = P_hector_norm.wopr_c2GMIX;
            COMBI_2GMIX_PARIS = P_paris_unit.c1G511.*P_paris_unit.c1G1157;   
            % 
            P_1G511_V1 = P_hector_aggr_1.wopr_1G511; 
            P_1G1157_V1 = P_hector_aggr_1.wopr_1G1157; 
            P_1G511_V2 = P_hector_aggr_2.wopr_1G511; 
            P_1G1157_V2 = P_hector_aggr_2.wopr_1G1157; 
            %
            COMBI_2GMIX_HECTOR_V1 = P_1G511_V1.*P_1G1157_V1;   
            COMBI_2GMIX_HECTOR_V2 = P_1G511_V2.*P_1G1157_V2;   
            max_y = max([max(P_hector_norm.wopr_c2GMIX),max(COMBI_2GMIX_PARIS),max(COMBI_2GMIX_HECTOR_V1),max(COMBI_2GMIX_HECTOR_V2)]); 
            sensibilite_comparee_2GMIX_hector_vs_paris_comp_version(CU,P2GMIX,COMBI_2GMIX_PARIS,COMBI_2GMIX_HECTOR_V1,COMBI_2GMIX_HECTOR_V2,max_y)
            NAME = "sensibilite_ulysse_paris_vs_hector_wo_pr_2gmix_COMP";
            fprintf("NAME = %s (COMP)\n",NAME); NAME = strcat(NAME,"_V",num2str(VERSION_1),"_V",num2str(VERSION_2));            
        case 19
            P2GMIX = P_hector_norm.wipr_c2GMIX;
            COMBI_2GMIX_PARIS = P_paris_unit.c1G511.*P_paris_unit.c1G1157;   
            % 
            P_1G511_V1 = P_hector_aggr_1.wipr_1G511; 
            P_1G1157_V1 = P_hector_aggr_1.wipr_1G1157; 
            P_1G511_V2 = P_hector_aggr_2.wipr_1G511; 
            P_1G1157_V2 = P_hector_aggr_2.wipr_1G1157; 
            %
            COMBI_2GMIX_HECTOR_V1 = P_1G511_V1.*P_1G1157_V1;   
            COMBI_2GMIX_HECTOR_V2 = P_1G511_V2.*P_1G1157_V2;   
            max_y = max([max(P_hector_norm.wipr_c2GMIX),max(COMBI_2GMIX_PARIS),max(COMBI_2GMIX_HECTOR_V1),max(COMBI_2GMIX_HECTOR_V2)]); 
            sensibilite_comparee_2GMIX_hector_vs_paris_comp_version(CU,P2GMIX,COMBI_2GMIX_PARIS,COMBI_2GMIX_HECTOR_V1,COMBI_2GMIX_HECTOR_V2,max_y)
            NAME = "sensibilite_ulysse_paris_vs_hector_wi_pr_2gmix_COMP";
            fprintf("NAME = %s (COMP)\n",NAME); NAME = strcat(NAME,"_V",num2str(VERSION_1),"_V",num2str(VERSION_2));
        case 20
            P3G = P_hector_norm.wopr_c3G ;
            COMBI_3G_PARIS = P_paris_unit.c2GLOR.*P_paris_unit.c1G1157;   
            P_2GLOR_V1 = P_hector_aggr_1.wopr_2GLOR; 
            P_1G1157_V1 = P_hector_aggr_1.wopr_1G1157; 
            P_2GLOR_V2 = P_hector_aggr_2.wopr_2GLOR; 
            P_1G1157_V2 = P_hector_aggr_2.wopr_1G1157; 
            %
            COMBI_3G_HECTOR_V1 = P_2GLOR_V1.*P_1G1157_V1;   
            COMBI_3G_HECTOR_V2 = P_2GLOR_V2.*P_1G1157_V2;   
            max_y = max([max(P_hector_norm.wopr_c3G),max(COMBI_3G_PARIS),max(COMBI_3G_HECTOR_V1),max(COMBI_3G_HECTOR_V2)]); 
            sensibilite_comparee_3G_hector_vs_paris_comp_version(CU,P3G,COMBI_3G_PARIS,COMBI_3G_HECTOR_V1,COMBI_3G_HECTOR_V2,max_y)
            NAME = "sensibilite_ulysse_paris_vs_hector_wo_pr_3g_COMP";
            fprintf("NAME = %s (COMP)\n",NAME); NAME = strcat(NAME,"_V",num2str(VERSION_1),"_V",num2str(VERSION_2));
        case 21
            P3G = P_hector_norm.wipr_c3G;
            COMBI_3G_PARIS = P_paris_unit.c2GLOR.*P_paris_unit.c1G1157;   
            P_2GLOR_V1 = P_hector_aggr_1.wipr_2GLOR; 
            P_1G1157_V1 = P_hector_aggr_1.wipr_1G1157; 
            P_2GLOR_V2 = P_hector_aggr_2.wipr_2GLOR; 
            P_1G1157_V2 = P_hector_aggr_2.wipr_1G1157; 
            %
            COMBI_3G_HECTOR_V1 = P_2GLOR_V1.*P_1G1157_V1;   
            COMBI_3G_HECTOR_V2 = P_2GLOR_V2.*P_1G1157_V2;   
            max_y = max([max(P_hector_norm.wipr_c3G),max(COMBI_3G_PARIS),max(COMBI_3G_HECTOR_V1),max(COMBI_3G_HECTOR_V2)]); 
            sensibilite_comparee_3G_hector_vs_paris_comp_version(CU,P3G,COMBI_3G_PARIS,COMBI_3G_HECTOR_V1,COMBI_3G_HECTOR_V2,max_y)
            NAME = "sensibilite_ulysse_paris_vs_hector_wi_pr_3g_COMP";
            fprintf("NAME = %s (COMP)\n",NAME); NAME = strcat(NAME,"_V",num2str(VERSION_1),"_V",num2str(VERSION_2));
        case 22
            max_y = max_y_comp_vers; 
            sensibilite_unitaire_paris_with_coherent_scale_comp_version(CU,P_paris_unit.c1G511,P_paris_unit.c1G1157,P_paris_unit.c2GLOR,max_y)
            NAME = "ulysse_paris_unit_brique_with_coherent_scale_comp_version";
            fprintf("NAME = %s (V%d)\n",NAME,VERSION); NAME = strcat(NAME,"_VC");
        
        otherwise
    end
end


%{
            COMBI_2GMIX_PARIS = (P_paris_unit.c1G511./2).*P_paris_unit.c1G1157;   
            COMBI_2GMIX_HECTOR_WOPR = (P_hector_aggr.wopr_1G511).*P_hector_aggr.wopr_1G1157;    
            max_y = max([max(P_hector_norm.wipr_c2GMIX),max(COMBI_2GMIX_PARIS),max(COMBI_2GMIX_HECTOR_WOPR)]); 
            sensibilite_comparee_2GMIX_hector_vs_paris(CU,P_hector_norm.wipr_c2GMIX,COMBI_2GMIX_PARIS,COMBI_2GMIX_HECTOR_WOPR,max_y)
            NAME = "sensibilite_ulysse_paris_vs_hector_wo_pr_2gmix";
            fprintf("NAME = %s (V%d)\n",NAME,VERSION); NAME = strcat(NAME,"_V",num2str(VERSION));

 max_y = max_y_all_cat;
            sensibilite_unitaire_paris_vs_hector(CU,P_paris_unit.c1G511,(P_hector_aggr.wopr_1G511)./2,(P_hector_aggr.wipr_1G511)./2,1,max_y)
            NAME = "brique_unitaire_paris_vs_hector_1G511";
            fprintf("NAME = %s (V%d)\n",NAME,VERSION); NAME = strcat(NAME,"_V",num2str(VERSION));
%}
fprintf("DECOMMENTE MODELE SIMON \n")
% -------------------

% -------------------

% -------------------

% -------------------

% -------------------

% -------------------

% -------------------

% -------------------

% -------------------

% -------------------



%{



    p2 = plot(c,1*COMBI_2GMIX_PARIS,'DisplayName','${1\gamma}^{511}_{\textrm{COR}} \times {1\gamma}^{1157}_{\textrm{COR}}$ (paris)','LineWidth',2,'LineStyle','-','Color',CU(1,1));
    p3 = plot(c,1*COMBI_2GMIX_HECTOR_V1,'DisplayName','${1\gamma}^{511}_{\textrm{COR}} \times {1\gamma}^{1157}_{\textrm{COR}}$ (hector) $N=0$','LineWidth',2,'LineStyle','--','Color',CU(2,1));
    p4 = plot(c,1*COMBI_2GMIX_HECTOR_V2,'DisplayName','${1\gamma}^{511}_{\textrm{COR}} \times {1\gamma}^{1157}_{\textrm{COR}}$ (hector) $N=1$','LineWidth',2,'LineStyle','--','Color','#A2142F');
    %
    yline(1*max(P2GMIX),'--',num2str(1*max(P2GMIX),'%.2e'),'Color','k','LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',12);
    yline(1*max(COMBI_2GMIX_PARIS),'--',num2str(1*max(COMBI_2GMIX_PARIS),'%.2e'),'Color',CU(1,1),'LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',12);
    yline(1*max(COMBI_2GMIX_HECTOR_V1),'--',num2str(1*max(COMBI_2GMIX_HECTOR_V1),'%.2e'),'Color',CU(2,1),'LabelHorizontalAlignment','right','LabelVerticalAlignment','top','FontSize',12,'Color','#7E2F8E');
    yline(1*max(COMBI_2GMIX_HECTOR_V2),'--',num2str(1*max(COMBI_2GMIX_HECTOR_V2),'%.2e'),'Color','#A2142F','LabelHorizontalAlignment','right','LabelVerticalAlignment','top','FontSize',12,'Color','#A2142F');
    %
    legend([p1,p2,p3,p4],'Location','northeast','Interpreter','latex','FontSize',11,'NumColumns',2)
%}

%  X 3 NAME = "brique_unitaire_paris_vs_hector_1G511";
%  X 12 NAME = "ulysse_paris_unit_brique_with_coherent_scale"; pour les nouvelles valeurs 
%  X 6 NAME = "sensibilite_ulysse_hector_without_pr";
%  X 7 NAME = "sensibilite_ulysse_hector_with_pr";
%  X 10 NAME = "sensibilite_ulysse_paris_vs_hector_wo_pr_2gmix";
%  X 11 NAME = "sensibilite_ulysse_paris_vs_hector_wi_pr_2gmix";







%{
  p1 = plot(c,1*P_paris_unit.c1G511,'DisplayName', strcat(CU(1,2),' unit'),'LineWidth',2,'LineStyle','-','Color',CU(1,1));
    p2 = plot(c,1*P_paris_unit.c1G1157,'DisplayName',strcat(CU(2,2),' unit'),'LineWidth',2,'LineStyle','-','Color',CU(2,1));
    p3 = plot(c,1*P_paris_unit.c2GLOR,'DisplayName',strcat(CU(3,2),' unit'),'LineWidth',2,'LineStyle','-','Color',CU(3,1)) ; 

%}


    
fprintf(">> print -depsc ... \n")
fprintf(">> print -depsc %s \n",NAME)
if (NAME~="" && SAVE_FIG)
    saveas(gcf,strcat(NAME,'.eps'),'epsc');
end









