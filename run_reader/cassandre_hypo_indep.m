clear all; 
close all; 
set(0,'DefaultFigureWindowStyle','docked') 
clc; 
% -------------------------------------------------------------------------
tok = 'wopr'; 
withLeg = false; 
%name_couple = "ulysse"; 
%name_margin = "uc_ulysse"; 

name_couple = "bernard"; 
name_margin = "bernard"; 

file_name_couple = strcat("DATA_PARSED/P_couple_",upper(name_couple),"_",upper(tok),'.mat'); 
file_name_margin = strcat("DATA_PARSED/P_margin_",upper(name_margin),'.mat'); 
COL_LIB = load(file_name_margin).COLOR_LIB; 

% ---
tok_withLeg = "withleg"; if (~withLeg), tok_withLeg = ""; end
latex_legend = ["0\gamma","1\gamma_{511}","1\gamma_{1157}","2\gamma_{\textrm{LOR}}","2\gamma_{\textrm{COR}}","3\gamma"];

P = load(file_name_margin).P; 


folder_to_save_fig ="FIG_EPS";
if (~isfolder("FIG_EPS") && AFF_FIG); mkdir(folder_to_save_fig); end 
BASE_NAME_FIG = folder_to_save_fig+"/cassandre_results_ulysse_indep_classe_"+tok; 

% -------------------------------------------------------------------------
% Données de PARIS - MARGINALE
% EXTRACTION DES DONNEES MARGINALES POUR LES TESTS
M_1G511_detec = load(file_name_margin).DETECT_PAR_CLASSE(:,2);
M_1G511_emiss = load(file_name_margin).EMISSI_PAR_CLASSE(:,2);
M_1G1157_detec = load(file_name_margin).DETECT_PAR_CLASSE(:,3);
M_1G1157_emiss = load(file_name_margin).EMISSI_PAR_CLASSE(:,3);
M_2GLOR_detec = load(file_name_margin).DETECT_PAR_CLASSE(:,4);
M_2GLOR_emiss = load(file_name_margin).EMISSI_PAR_CLASSE(:,4);
% --
M_1G511_sensi = M_1G511_detec./M_1G511_emiss; 
M_1G1157_sensi = M_1G1157_detec./M_1G1157_emiss; 
M_2GLOR_sensi = M_2GLOR_detec./M_2GLOR_emiss; 
% -------------------------------------------------------------------------
% Données de HECTOR - COUPLE
C_1G511_detec = load(file_name_couple).DETECT_PAR_CLASSE(:,2); 
C_1G511_emiss = load(file_name_couple).EMISSIO_PAR_VOXEL; 
C_1G1157_detec = load(file_name_couple).DETECT_PAR_CLASSE(:,3); 
C_1G1157_emiss = load(file_name_couple).EMISSIO_PAR_VOXEL; 
C_2GLOR_detec = load(file_name_couple).DETECT_PAR_CLASSE(:,4); 
C_2GLOR_emiss = load(file_name_couple).EMISSIO_PAR_VOXEL; 
C_2GCOR_detec = load(file_name_couple).DETECT_PAR_CLASSE(:,5); 
C_2GCOR_emiss = load(file_name_couple).EMISSIO_PAR_VOXEL; 
C_3G_detec = load(file_name_couple).DETECT_PAR_CLASSE(:,6); 
C_3G_emiss = load(file_name_couple).EMISSIO_PAR_VOXEL; 
% -------------------------------------------------------------------------
% PeZ des données paris
s_M_1G511 = get_ProfEnZ(P,M_1G511_emiss,M_1G511_detec,1); 
s_M_1G1157 = get_ProfEnZ(P,M_1G1157_emiss,M_1G1157_detec,1); 
s_M_2GLOR = get_ProfEnZ(P,M_2GLOR_emiss,M_2GLOR_detec,1); 
% PeZ des du couple (hector).
s_C_1G551 = get_ProfEnZ(P,C_1G511_emiss,C_1G511_detec,1); 
s_C_1G1157 = get_ProfEnZ(P,C_1G1157_emiss,C_1G1157_detec,1); 
s_C_2GLOR = get_ProfEnZ(P,C_2GLOR_emiss,C_2GLOR_detec,1); 
s_C_2GCOR = get_ProfEnZ(P,C_2GCOR_emiss,C_2GCOR_detec,1); 
s_C_3G = get_ProfEnZ(P,C_3G_emiss,C_3G_detec,1); 
% Expression des marginales à partir des données du couple 
s_MC_1G511 = 0.5.*s_C_1G551 + 1.*s_C_2GLOR + 0.5.*s_C_2GCOR + 1.*s_C_3G;
s_MC_1G1157 = 1.*s_C_1G1157 + 1.*s_C_2GCOR + 1.*s_C_3G; 
s_MC_2GLOR = 1.*s_C_2GLOR + 1.*s_C_3G; 

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% CAS 2 : 3G =?= 1G1157 x 2GLOR
% Pour chaque voxel 
figure; 
    hold on; box on; set(gca,'YScale','log');
    xlim([0,P.J])
    xlabel(strcat("$j\in \{1,...,",num2str(P.J,'%d'),"\}$"),'Interpreter','latex','FontSize',16); 
    ylabel('Detection $\div$ Emission','Interpreter','latex','FontSize',16); 
    if (withLeg), legend('Location','southoutside','NumColumns',4,'Interpreter','latex','FontSize',12); legend boxoff; end
    C_3G_sensi = C_3G_detec./C_3G_emiss; 
    Combi_M_3G_sensi = (M_1G1157_detec./M_1G1157_emiss).*(M_2GLOR_detec./M_2GLOR_emiss); 
    Combi_MC_3G_sensi = ...
        (1.*(C_1G1157_detec./C_1G1157_emiss) + 1.*(C_3G_detec./C_3G_emiss)) ...
        .* (1.*(C_2GLOR_detec./C_2GLOR_emiss) + 1.*(C_3G_detec./C_3G_emiss)) ...
        ; 

    plot(C_3G_sensi,'DisplayName',strcat('$',latex_legend(6),'^{C}$'),'Color',COL_LIB(6,:));
    plot(Combi_M_3G_sensi,'k--','DisplayName',strcat('$',latex_legend(6),'^{M}$'));
    plot(Combi_MC_3G_sensi,'--','DisplayName',strcat('$',latex_legend(6),'^{MC}$'),'Color',COL_LIB(7,:));
    
    NAME_FIG = BASE_NAME_FIG+"_3G_pervox_"+tok_withLeg;
    saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
    
    % 
% Pour chaque position axiale $z$    
figure; 
    hold on; box on; %set(gca,'YScale','log');
    xlim([-120,+120])
    %ylim([0,1])
%     h = 240; 
%     xticks(-(h)/2:40:+(h)/2);
%     c = linspace(-120,+120,P.D(3));
    c = unique(P.Rc(:,3)) + P.V(3)/2;
    xticks(c);
    xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16);
    ylabel(strcat("Sensibilit\'e $\mathbf{s}^{",latex_legend(6),"}$"),'Interpreter','latex','FontSize',16);

    %C_3G_sensi = C_3G_detec./C_3G_emiss;
    Prof_C_3G = s_C_3G; %get_ProfEnZ(P,C_3G_emiss,C_3G_detec,1); 
    p1 = plot(c,1*Prof_C_3G,'DisplayName',strcat('$',latex_legend(6),'^{C}$'),'LineWidth',2,'LineStyle','--','Color',COL_LIB(6,:)) ;
    %Combi_3G_sensi = (M_1G1157_detec./M_1G1157_emiss).*(M_2GLOR_detec./M_2GLOR_emiss); 
    %part_detect = M_1G1157_detec.*M_2GLOR_detec; 
    %part_emissi = M_1G1157_emiss.*M_2GLOR_emiss; 
    Prof_M_3G = s_M_2GLOR.*s_M_1G1157; %get_ProfEnZ(P,part_emissi,part_detect,1);
    p2 = plot(c,1*Prof_M_3G,'k--','DisplayName',strcat('$',latex_legend(6),'^{M}$'),'LineWidth',2) ;
    % Combi_MC_3G_sensi = (1.*(C_1G1157_detec./C_1G1157_emiss) .* 1.*(C_3G_detec./C_3G_emiss)) .* (1.*(C_2GLOR_detec./C_2GLOR_emiss) .* 1.*(C_3G_detec./C_3G_emiss)) ; 
    % part_detect = (C_1G1157_detec + C_3G_detec) .* (C_2GLOR_detec + C_3G_detec); 
    % part_emissi = (C_1G1157_emiss + C_3G_emiss) .* (C_2GLOR_emiss + C_3G_emiss); 
    Prof_MC_3G = s_MC_2GLOR.*s_MC_1G1157; %get_ProfEnZ(P,part_emissi,part_detect,1);
    p3 = plot(c,1*Prof_MC_3G,':','DisplayName',strcat('$',latex_legend(6),'^{MC}$'),'LineWidth',2,'Color',COL_LIB(7,:)) ;    

    yline(1*max(Prof_C_3G),':',num2str(1*max(Prof_C_3G),'%.2e'),'Color',COL_LIB(6,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')
    yline(1*min(Prof_C_3G),':',num2str(1*min(Prof_C_3G),'%.2e'),'Color',COL_LIB(6,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')
    yline(1*max(Prof_M_3G),':',num2str(1*max(Prof_M_3G),'%.2e'),'Color','k','LabelHorizontalAlignment','right','LabelVerticalAlignment','top','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')
    yline(1*min(Prof_M_3G),':',num2str(1*min(Prof_M_3G),'%.2e'),'Color','k','LabelHorizontalAlignment','right','LabelVerticalAlignment','top','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')
    yline(1*max(Prof_MC_3G),':',num2str(1*max(Prof_MC_3G),'%.2e'),'Color',COL_LIB(7,:),'LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')

    if (withLeg),legend([p1,p2,p3],'Location','southoutside','NumColumns',4,'Interpreter','latex','FontSize',12); legend boxoff; end

    NAME_FIG = BASE_NAME_FIG+"_3G_perz_"+tok_withLeg;
    saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');


    % Ecart - position axiale $z$
    ecart_C_M = abs(Prof_C_3G-Prof_M_3G); 
    ecart_C_MC = abs(Prof_C_3G-Prof_MC_3G); 

figure; 

    hold on; box on; set(gca,'YScale','log');
    xlim([-120,+120])
    ylim([0,0.1])
%     h = 240; 
%     xticks(-(h)/2:40:+(h)/2);
%     c = linspace(-120,+120,P.D(3));
    c = unique(P.Rc(:,3)) + P.V(3)/2;
    xticks(c);
    xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16);
    %ylabel(strcat("Ecarts absolus $\mathbf{s}^{",latex_legend(6),"}$"),'Interpreter','latex','FontSize',16);
    
    p1 = plot(c,1*ecart_C_M,'DisplayName',strcat('$|s^{',latex_legend(6),'^{C}}-s^{',latex_legend(6),'^{M}}|$'),'LineWidth',2,'LineStyle','--','Color','k') ;
    yline(1*max(ecart_C_M),'--',num2str(1*max(ecart_C_M),'%.2e'),'Color','k','LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')
    yline(1*min(ecart_C_M),'--',num2str(1*min(ecart_C_M),'%.2e'),'Color','k','LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')

    p2 = plot(c,1*ecart_C_MC,'DisplayName',strcat('$|s^{',latex_legend(6),'^{C}}-s^{',latex_legend(6),'^{CM}}|$'),'LineWidth',2,'LineStyle','-','Color',COL_LIB(7,:)) ;
    yline(1*max(ecart_C_MC),':',num2str(1*max(ecart_C_MC),'%.2e'),'Color',COL_LIB(7,:),'LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')
    yline(1*min(ecart_C_MC),':',num2str(1*min(ecart_C_MC),'%.2e'),'Color',COL_LIB(7,:),'LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')

    if (withLeg), legend([p1,p2],'Location','southoutside','NumColumns',4,'Interpreter','latex','FontSize',12); legend boxoff; end

    NAME_FIG = BASE_NAME_FIG+"_3G_ecc_perz_"+tok_withLeg;
    saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');

% 
% % CAS 1 : 2GCOR =?= 1G511 x 1G1157
% % Pour chaque voxel
% figure; 
%     hold on; box on; set(gca,'YScale','log');
%     xlim([0,P.J])
%     xlabel(strcat("$j\in \{1,...,",num2str(P.J,'%d'),"\}$"),'Interpreter','latex','FontSize',16); 
%     ylabel('Detection $\div$ Emission','Interpreter','latex','FontSize',16); 
%     if (withLeg), legend('Location','southoutside','NumColumns',4,'Interpreter','latex','FontSize',12); legend boxoff; end
%     
%     C_2GCOR_sensi = C_2GCOR_detec./C_2GCOR_emiss; 
%     plot(C_2GCOR_sensi,'DisplayName',strcat('$',latex_legend(5),'^{C}$'),'Color',COL_LIB(5,:));
%     Combi_M_2GCOR_sensi = (M_1G511_detec./M_1G511_emiss).*(M_1G1157_detec./M_1G1157_emiss);
%     plot(Combi_M_2GCOR_sensi,'k--','DisplayName',strcat('$',latex_legend(5),'^{M}$'));
%     
%     f1 = (.5.*(C_1G511_detec./C_1G511_emiss)) + (1.*(C_2GLOR_detec./C_2GLOR_emiss)) + (.5 .*(C_2GCOR_detec./C_2GCOR_emiss)) + (1.*(C_3G_detec./C_3G_emiss)); 
%     f2 = (1.*(C_1G1157_detec./C_1G1157_emiss) + 1.*(C_2GCOR_detec./C_2GCOR_emiss) + 1.* (C_3G_detec./C_3G_emiss)); 
%     Combi_MC_2GCOR_sensi = f1.*f2; 
% 
%     plot(Combi_MC_2GCOR_sensi,'--','DisplayName',strcat('$',latex_legend(5),'^{MC}$'),'Color',COL_LIB(7,:));
%     
%     NAME_FIG = BASE_NAME_FIG+"_2GCOR_pervox_"+tok_withLeg;
%     saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
% 
% % Pour chaque position axiale $z$    
% figure; 
%     hold on; box on; set(gca,'YScale','log');
%     xlim([-120,+120])
%     ylim([0,1])
%     h = 240; 
%     xticks(-(h)/2:40:+(h)/2);
%     c = linspace(-120,+120,P.D(3));
%     xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16);
%     ylabel(strcat("Sensibilit\'e $\mathbf{s}^{",latex_legend(5),"}$"),'Interpreter','latex','FontSize',16); 
%     % 
%     Prof_C_2GCOR = s_C_2GCOR; 
%     p1 = plot(c,1*Prof_C_2GCOR,'DisplayName',strcat('$',latex_legend(5),'^{C}$'),'LineWidth',2,'LineStyle','-','Color',COL_LIB(5,:)) ;
%     %
%     Prof_M_2GCOR = s_M_1G511.*s_M_1G1157;
%     p2 = plot(c,1*Prof_M_2GCOR,'k--','DisplayName',strcat('$',latex_legend(5),'^{M}$'),'LineWidth',2) ;
%     % 
%     Prof_MC_2GCOR = s_MC_1G511.*s_MC_1G1157;
%     p3 = plot(c,1*Prof_MC_2GCOR,'--','DisplayName',strcat('$',latex_legend(5),'^{MC}$'),'LineWidth',2,'Color',COL_LIB(7,:)) ;
% 
% 
%     yline(1*max(Prof_C_2GCOR),':',num2str(1*max(Prof_C_2GCOR),'%.2e'),'Color',COL_LIB(5,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')
%     yline(1*min(Prof_C_2GCOR),':',num2str(1*min(Prof_C_2GCOR),'%.2e'),'Color',COL_LIB(5,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','bottom','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')
%     yline(1*max(Prof_M_2GCOR),':',num2str(1*max(Prof_M_2GCOR),'%.2e'),'Color','k','LabelHorizontalAlignment','right','LabelVerticalAlignment','top','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')
%     yline(1*min(Prof_M_2GCOR),':',num2str(1*min(Prof_M_2GCOR),'%.2e'),'Color','k','LabelHorizontalAlignment','right','LabelVerticalAlignment','bottom','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')
%     yline(1*max(Prof_MC_2GCOR),':',num2str(1*max(Prof_MC_2GCOR),'%.2e'),'Color',COL_LIB(7,:),'LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')
% 
% 
%     if (withLeg), legend([p1,p2,p3],'Location','southoutside','NumColumns',4,'Interpreter','latex','FontSize',12); legend boxoff; end
%     
%     NAME_FIG = BASE_NAME_FIG+"_2GCOR_perz_"+tok_withLeg;
%     saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');
% % Ecart - position axiale $z$
% ecart_C_M = abs(Prof_C_2GCOR-Prof_M_2GCOR); 
% ecart_C_MC = abs(Prof_C_2GCOR-Prof_MC_2GCOR); 
% 
% figure; 
% 
%     hold on; box on; set(gca,'YScale','log');
%     xlim([-120,+120])
%     ylim([0,1])
%     h = 240; 
%     xticks(-(h)/2:40:+(h)/2);
%     c = linspace(-120,+120,P.D(3));
%     xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16);
%     %ylabel(strcat("Ecarts absolus $\mathbf{s}^{",latex_legend(6),"}$"),'Interpreter','latex','FontSize',16);
%     
%     p1 = plot(c,1*ecart_C_M,'DisplayName',strcat('$|s^{',latex_legend(5),'^{C}}-s^{',latex_legend(5),'^{M}}|$'),'LineWidth',2,'LineStyle','--','Color','k') ;
%     yline(1*max(ecart_C_M),'--',num2str(1*max(ecart_C_M),'%.2e'),'Color','k','LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')
%     yline(1*min(ecart_C_M),'--',num2str(1*min(ecart_C_M),'%.2e'),'Color','k','LabelHorizontalAlignment','left','LabelVerticalAlignment','bottom','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')
% 
%     p2 = plot(c,1*ecart_C_MC,'DisplayName',strcat('$|s^{',latex_legend(5),'^{C}}-s^{',latex_legend(5),'^{CM}}|$'),'LineWidth',2,'LineStyle','-','Color',COL_LIB(7,:)) ;
%     yline(1*max(ecart_C_MC),':',num2str(1*max(ecart_C_MC),'%.2e'),'Color',COL_LIB(7,:),'LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')
%     yline(1*min(ecart_C_MC),':',num2str(1*min(ecart_C_MC),'%.2e'),'Color',COL_LIB(7,:),'LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')
% 
%     if (withLeg), legend([p1,p2],'Location','southoutside','NumColumns',4,'Interpreter','latex','FontSize',12); legend boxoff; end
% 
%     NAME_FIG = BASE_NAME_FIG+"_2GCOR_ecc_perz_"+tok_withLeg;
%     saveas(gcf,strcat(NAME_FIG,'.eps'),'epsc');


    %{    

  
     


    






% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% 
    
    
    
    

   


    yline(1*max(Prof_C_2GCOR),'--',num2str(1*max(Prof_C_2GCOR),'%.2e'),'Color',COL_LIB(5,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')
    yline(1*min(Prof_C_2GCOR),'--',num2str(1*min(Prof_C_2GCOR),'%.2e'),'Color',COL_LIB(5,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','bottom','FontSize',13); % ,num2str(1*max(P_unit_1G511),'%.2e')


    legend([p1,p2,p3],'Location','southoutside','NumColumns',4,'Interpreter','latex','FontSize',12); legend boxoff;

%}
%{
figure; 
    hold on; box on; set(gca,'YScale','log');
    xlim([-120,+120])
    h = 240; 
    xticks(-(h)/2:40:+(h)/2);
    c = linspace(-120,+120,P.D(3));
    xlabel("Position axiale $z$ (mm)",'Interpreter','latex','FontSize',16);    
%}

%{

M_2GMIX_sensi = M_1G511_sensi.*M_1G1157_sensi; 
M_3G_sensi = M_2GLOR_sensi.*M_1G1157_sensi; 


C_1G511_sensi = C_1G511_detec./C_1G511_emiss; 

C_1G1157_sensi = C_1G1157_detec./C_1G1157_emiss; 
C_2GLOR_sensi = C_2GLOR_detec./C_2GLOR_emiss; 
C_2GCOR_sensi = C_2GCOR_detec./C_2GCOR_emiss; 



Combi_1G1557 = 1*C_1G1157_sensi+1*C_2GCOR_sensi+1*C_3G_sensi;
Combi_1G511 = 0.5*C_1G511_sensi+1*C_2GLOR_sensi+0.5*C_2GCOR_sensi+1*C_3G_sensi; 
Combi_2GMIX = Combi_1G511.*Combi_1G1557; 
Combi_3G = C_2GCOR_sensi.*C_3G_sensi; 

Ecart_2GMIX = abs(Combi_2GMIX-M_2GMIX_sensi); 
Ecart_3G = abs(Combi_3G-M_3G_sensi); 












%}


%{





end
%}

%{
figure; 
    subplot(1,2,1)
    hold on; 
    legend('Location','southoutside')
    plot(M_1G511_sensi,'b','DisplayName','M 1G511')
    plot(M_1G1157_sensi,'r','DisplayName','M 1G1157')
    plot(M_2GMIX_sensi,'g','DisplayName','M 1G511 x U 1G1157')

    subplot(1,2,2)
    hold on; 
    legend('Location','southoutside')
    plot(C_1G511_sensi,'Color',COL_LIB(2,:),'DisplayName','C 1G551')
    plot(C_1G1157_sensi,'Color',COL_LIB(3,:),'DisplayName','C 1G1557')
    plot(C_2GLOR_sensi,'Color',COL_LIB(4,:),'DisplayName','C 2GLOR')
    plot(C_2GCOR_sensi,'Color',COL_LIB(5,:),'DisplayName','C 2GCOR')
    plot(C_3G_sensi,'Color',COL_LIB(6,:),'DisplayName','C 3G')


P.J = length(Combi_2GMIX); 
%}
%{
figure; 
    hold on; set(gca,'Yscale','log')
    xlim([0,length(Combi_2GMIX)])
    p1 = plot(Combi_2GMIX,'-','Color',COL_LIB(5,:),'DisplayName','C 1G511 x 1G1157');
    p2 = plot(M_2GMIX_sensi,'k--','DisplayName','M 1G511 x 1G1157'); 
    yline(1*max(Combi_2GMIX),'--',num2str(1*max(Combi_2GMIX),'%.2e'),'Color', COL_LIB(5,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',15);
    yline(1*max(M_2GMIX_sensi),'--',num2str(1*max(M_2GMIX_sensi),'%.2e'),'Color',	'k','LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',15)
    yline(1*min(Combi_2GMIX),'--',num2str(1*min(Combi_2GMIX),'%.2e'),'Color', COL_LIB(5,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',15);
    yline(1*min(M_2GMIX_sensi),'--',num2str(1*min(M_2GMIX_sensi),'%.2e'),'Color',	'k','LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',15)
    
    legend([p1,p2],'Location','southoutside')
    xlabel(strcat("$j\in \{1,...,",num2str(P.J,'%d'),"\}$"),'Interpreter','latex','FontSize',12);

figure; 
    hold on; set(gca,'Yscale','log')
    xlim([0,length(Ecart_2GMIX)])
    p1 = plot(Ecart_2GMIX,'Color',COL_LIB(5,:),'DisplayName','Ecart 2GCOR'); 
    yline(1*max(Ecart_2GMIX),'--',num2str(1*max(Ecart_2GMIX),'%.2e'),'Color', 'k','LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',15);
    yline(1*min(Ecart_2GMIX),'--',num2str(1*min(Ecart_2GMIX),'%.2e'),'Color',	'k','LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',15)
    legend([p1],'Location','southoutside')
    xlabel(strcat("$j\in \{1,...,",num2str(P.J,'%d'),"\}$"),'Interpreter','latex','FontSize',12);

figure; 
    hold on; set(gca,'Yscale','log')
    legend('Location','southoutside')
    xlim([0,length(Ecart_2GMIX)])
    p1 = plot(Combi_3G,'-','Color',COL_LIB(6,:),'DisplayName','C 2GLOR x 1G1157'); 
    p2 = plot(M_3G_sensi,'--','Color','k','DisplayName','M 2GLOR x 1G1157');
    yline(1*max(Combi_3G),'--',num2str(1*max(Combi_3G),'%.2e'),'Color', COL_LIB(6,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',15);
    yline(1*max(M_3G_sensi),'--',num2str(1*max(M_3G_sensi),'%.2e'),'Color',	'k','LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',15)
    yline(1*min(Combi_3G),'--',num2str(1*min(Combi_3G),'%.2e'),'Color', COL_LIB(6,:),'LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',15);
    yline(1*min(M_3G_sensi),'--',num2str(1*min(M_3G_sensi),'%.2e'),'Color',	'k','LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',15)

    legend([p1,p2],'Location','southoutside')
    xlabel(strcat("$j\in \{1,...,",num2str(P.J,'%d'),"\}$"),'Interpreter','latex','FontSize',12);

figure; 
    hold on; set(gca,'Yscale','log')
    legend('Location','southoutside')
    xlim([0,length(Ecart_2GMIX)])
    p1 = plot(Ecart_3G,'Color',COL_LIB(6,:),'DisplayName','Ecart 3G');
    yline(1*max(Ecart_3G),'--',num2str(1*max(Ecart_3G),'%.2e'),'Color', 'k','LabelHorizontalAlignment','left','LabelVerticalAlignment','top','FontSize',15);
    yline(1*min(Ecart_3G),'--',num2str(1*min(Ecart_3G),'%.2e'),'Color',	'k','LabelHorizontalAlignment','center','LabelVerticalAlignment','top','FontSize',15)
    legend([p1],'Location','southoutside')
    xlabel(strcat("$j\in \{1,...,",num2str(P.J,'%d'),"\}$"),'Interpreter','latex','FontSize',12);


%}