function [] = run_pollux(file,sv_folder,RNG,activ_PR,j_start,j_stop)
    clc; 
    addpath("methods/");
    addpath("my_phantoms/")
    F.folder = "my_phantoms/";
    F.save_folder = strcat(sv_folder,"/");
    F.file = file;
    % ---------------------------------------------------------------------
    if (nargin <4)
        error("REVOIR LE PASSAGE DE PARAMETRE (%d)\n\trun_pollux(file,sv_folder,RNG,activ_PR,[j_start,j_stop])\n",nargin)
    end
    % ---------------------------------------------------------------------
    Q = 10000;
    write_conf = true;
    if (exist(strcat(F.save_folder,"pollux_",F.file,"_config.mat"))==0)
        rng(RNG);
        B = 1000;  
        seed_lut = randi([1000,9999],B,1); 
        % ---------------------------------------------------------------------
        cs_file = "data/nistdata_1_1200_kev_rng.txt";
        data = load(strcat(F.folder,F.file,".txt"));
        neg_rayleigh = true;                                % Est-ce qu'on veut négliger l'effet Rayleigh
        [X,P] = init_simulation(data,cs_file,neg_rayleigh);
        % ---------------------------------------------------------------------
        C.Classif = ["0G",0,"0\gamma"; "1G511",1,"1\gamma - 511"; "1G1157",1,"1\gamma - 1157"; "2GLOR",2,"2\gamma - LOR"; "2GMIX",2,"2\gamma- Mix"; "3G",3,"3\gamma"];
        C.Pos_rng.mu = 0.4;                      % Positron range (exprimé en mm)
        C.Pos_rng.sigma = 2;                     % => Deux paramètres pour le moment N(mu,sigma^2), on verra après
        C.Pos_rng.active = activ_PR;
        C.pollux_case = "3";
        C.preset_type = "count";
        C.verbose = false; C.draw = false; C.rem_pts = false; C.wait_time = 0; C.create_anim = false; C.N_max =B; % dessine le résultat de la simulation (faux si N > N_max ou preset_time)
        C.pollux_N = Q;
        % ---------------------------------------------------------------------
        save_name = strcat(F.save_folder,"pollux_",F.file,"_config");
        save(save_name,"X","P","F","C","RNG",'B','Q','seed_lut');
        fprintf("CONF FILE GENERATED : %s \n",save_name)
    else
        disp("LOAD")
        save_name = strcat(F.save_folder,"pollux_",F.file,"_config");
        load(save_name); 
    end
    print_phantom_info(P,F);
    fprintf("POSITRON RANGE ? %g\n",C.Pos_rng.active)
    % ---------------------------------------------------------------------
    if (nargin < 6), j_stop = B;  end
    if (nargin < 5),  j_start = 1; end

    delete(gcp('nocreate'));
    pool = parpool('local',12,'IdleTimeout',144000);
    parfor (b=j_start:j_stop)
        lcl_pollux(F,C,X,P,b,seed_lut(b));
    end






end



function [] = lcl_pollux(F,C,X,P,b,seed)
    C.method = "POLLUX"; 
    C.N = C.pollux_N;
    verb_lim = C.N_max;
    C.rng = seed; 
    rng(seed)
    %   Remarque : tout ce qui est relatif au photon 1.157 est marqué par un "prime"
    %   Pour la CSR : V1p, V2p, E1a, E2a
    %   Pour la LOR : V1,V2 si LOR détectée
    %                 V1, (V1s,V2s,E1b,E2b) si DEMI-LOR avec V1 détecté
    %                 V2, (V1s,V2s,E1b,E2b) si DEMI-LOR avec V2 detecté
    % MANTRA : P = E + t * V  
    % => On peut tout calculer à partir de t (puisque l'on connait E et V)
    % => On ne s'embête plus à maintenir simultanément des retour de point et de coefficient t
    % ---------------------------------------------------------------------
    [S] = pollux_init_struct(C,P);     
    [pure_evt_idx] = get_pure_event_idx(C);
    % ---------------------------------------------------------------------
    % ---------------------------------------------------------------------
    terminal = false;
    n = 0;
    t_start = tic(); 
    while (~terminal)
        n = n + 1;        

        % ---
        [n_gamma_511,n_gamma_1157] = reset_detection();
        [V0f,V0b,V1,V2,V0p,V1p,V2p,V0s,V1s,V2s,E1a,E2a] = reset_interaction_point();
        [E0,E0_prime,P_gamma_511,P_gamma_1157] = init_photon_energy_count(C.pollux_case);
        % ---
        [S,M,Dir,info] = pollux_simulation_emission(P,C,S,n);        
        switch C.pollux_case
            % ---------------------------------------------------------------------
            % ---------------------------------------------------------------------
            case "3"

                t1_min_p = inf; t1_max_p = inf; t_V1p = inf; t2_min_p = inf; t2_max_p = inf; t_V2p = inf;
                 t_max_f = inf; t_max_b = inf; 
                t2_min = inf ; t2_max =inf; 
                t_V1_f = inf; t_V1_b = inf; 
                csr_gen_dir_s = inf(3,1);
                csr_gen_dir_p = inf(3,1);
                csr_list = [];
                
                succ_lor_f = false;
                succ_lor_b = false; 
                succ_csr_f = false; 
                succ_csr_b = false;
            
                nb_intersection = 0; 
                t_inter_1 = 0; t_inter_2 = 0;
                tab_inter_status = zeros(1,7); 

                % 
                M_orig = M;     % on vient écrasé par la suite en appliquant le positron range
                % >>>>>
                succ_csr_p = false;
                % SIMULATION
                [t1_min_p,~] = get_intersect_coord_Rin(X,M,Dir.D1);
                if (~isinf(t1_min_p))
                    % Point d'entrée dans la zone active
                    V0p = get_point(M,Dir.D1,t1_min_p);
                    % Calcul du premier point d'intraction
                    [t1_max_p] = get_intersect_coord_Rout(X,M,Dir.D1,t1_min_p);
                    [t_V1p] = distance_xenon_attenuation(X,E0_prime,Dir.D1,t1_max_p);
                    V1p = get_point(V0p,Dir.D1,t_V1p);
                    msg_1 = ""; if (C.verbose), msg_1 = "[CSR init 1157 V1p]"; end 
                    if (is_in_FOD(X,V1p,msg_1))
                        msg_2 = "" ; if (C.verbose), msg_2 = "1157"; end 
                        [succ_csr_p,V0p,V1p,V2p,E1a,E2a,csr_gen_dir_p,trial_rm_kn_p] = create_csr_generatrix(X,M,Dir.D1,t1_min_p,t_V1p,E0_prime,msg_2);
                        S.results.cpt_rm_kn(n,1) = trial_rm_kn_p;
                    else
                        succ_csr_p = false;
                        t_V1p = inf;
                        V1p = inf(3,1);
                    end
                end
                if (C.verbose)
                    fprintf("[POST_CSR_1157] \tsucc_csr_b = %d \n",succ_csr_p);
                    fprintf("[SYNTH|V] naii(V1) = %d\t naii(V2) = %d\n",naii(V1p),naii(V2p));
                    fprintf("[SYNTH|E] E0 = %g\t E1b = %g\t E2b = %g \n",E0_prime,E1a,E2a);
                end
                %%%
                [M,R_pr] = positron_range(M,Dir.D_pr,S.Conf.Pos_rng);
                % 
                [t_min_f,t_min_b] = get_intersect_coord_Rin(X,M,Dir.D2);  % b : back, f : forward
                if (C.verbose), fprintf("[INIT - %d] \tt_min_f = %g \t t_min_b = %g \n",n,t_min_f,t_min_b); end
                % ---------------------------------------------------------
                % ---------------------------------------------------------
                % PARTIE 1 - Tenter la LOR
                % => D'abord vers l'avant t>0 (f); 
                if (~isinf(t_min_f)) 
                    msg_3 = ""; if (C.verbose), msg_3 = "[LOR-V1-f]"; end 
                    [succ_lor_f,V0f,V1,E1b,t_V1_f,t_min_f,t_max_f] = create_half_lor(X,M,Dir.D2,t_min_f,E0,msg_3);
                end
                % => Puis vers l'arrière t<0 (b)
                if (~isinf(t_min_b))
                    msg_4 = ""; if (C.verbose), msg_4 = "[LOR-V2-b]"; end
                    [succ_lor_b,V0b,V2,E1b,t_V1_b,t_min_b,t_max_b] = create_half_lor(X,M,Dir.D2,t_min_b,E0,msg_4); 
                end
                if (C.verbose),fprintf("[POST_LOR] \tsucc_lor_f = %d \tsucc_lor_b = %d \n",succ_lor_f,succ_lor_b); end
                % ---------------------------------------------------------
                % ---------------------------------------------------------
                if (~succ_lor_f || ~succ_lor_b), V1 = inf(3,1); V2 = inf(3,1);  end
                % PARTIE 2 - Si pas de LOR (succ_f=false ou succ_b = false)
                %          - On ré-init les points 
                %          - On tente de créer une CSR dans le sens du rayon possible.
                
                if(succ_lor_f && ~succ_lor_b)
                    % Dans ce cas, on a déjà calculé t_min_f et t_max_f => On a pas besoin de recalculer cette donnée 
                    msg_5 = ""; if (C.verbose), msg_5 = "511-f"; end
                    [succ_csr_f,V0f,V1s,V2s,E1b,E2b,csr_gen_dir_s,trial_rm_kn_f] = create_csr_generatrix(X,M,Dir.D2,t_min_f,t_V1_f,E0,msg_5);
                    S.results.cpt_rm_kn(n,2) = trial_rm_kn_f;
                    if (C.verbose), fprintf("[POST_CSR_F] \tsucc_csr_f = %d \n",succ_csr_f);end
                elseif(~succ_lor_f && succ_lor_b)
                    % Dans ce cas, on a déjà calculé t_min_b et t_max_b => On a pas besoin de recalculer cette donnée 
                    msg_6 = ""; if (C.verbose), msg_6 = "511-b"; end
                    [succ_csr_b,V0b,V1s,V2s,E1b,E2b,csr_gen_dir_s,trial_rm_kn_b] = create_csr_generatrix(X,M,Dir.D2, t_min_b,t_V1_b,E0,msg_6);
                    S.results.cpt_rm_kn(n,2) = trial_rm_kn_b;
                    if (C.verbose), fprintf("[POST_CSR_B] \tsucc_csr_b = %d \n",succ_csr_b); end
                elseif (succ_lor_f && succ_lor_b)
                    if (C.verbose), fprintf("[RES] LOR CREEE \n"); end
                    E1b = E0; E2b = E0; 
                elseif(~succ_lor_f && ~succ_lor_b)
                    if (C.verbose), fprintf("[RES] ECHEC \n"); end
                    E0 = 0; E1b = 0; E2b = 0; 
                end     
                if (C.verbose)
                    fprintf("[SYNTH|V] naii(V1) = %d\t naii(V2) = %d\t naii(V1s) = %d\t naii(V2s) = %d \n",naii(V1),naii(V2),naii(V1s),naii(V2s));
                    fprintf("[SYNTH|E] E0 = %g\t E1b = %g\t E2b = %g \n",E0,E1b,E2b);
                end
                n_gamma_511 = length(find([naii(V1),naii(V2)]));
                n_gamma_511_diff = length(find([naii(V1s),naii(V2s)]));
                n_gamma_1157_diff = length(find([naii(V1p),naii(V2p)])); 

                % <<<<< 
                [config,inf_msg] = get_type_detected_event(n_gamma_511,n_gamma_511_diff,n_gamma_1157_diff);
                if (config.idx == 6)
                    axe_lor = (V2-V1)/norm(V2-V1,2); 
                    axe_csr = (V1p-V2p)/norm(V1p-V2p,2); 
                    apex = V1p; 
                    beta =  acos(1-(E1a*X.mec2/(1.157*(1.157-E1a))));
                    [t_inter_1,t_inter_2,tab_inter_status] = intersection_lor_cor(X,V1,axe_lor,apex,axe_csr,beta,C.verbose);
                    nb_intersection = (t_inter_1~=0)+(t_inter_2~=0); 
                end

                % STOCKAGE
                
                S.export.chr(n)= config.chr;
                if (succ_csr_p)
                    S.export.data(n,S.rng.V1p_rng) = V1p;
                    S.export.data(n,S.rng.E1a_rng) = E1a;
                    S.export.data(n,S.rng.V2p_rng) = V2p;
                    S.export.data(n,S.rng.E2a_rng) = E2a;
                end
                if (succ_lor_f && succ_lor_b)
                    S.export.data(n,S.rng.V1_rng) = V1;
                    S.export.data(n,S.rng.V2_rng) = V2;
                    S.export.data(n,S.rng.E1b_rng) = E1b;
                    S.export.data(n,S.rng.E2b_rng) = E2b;
                elseif (succ_csr_f || succ_csr_b)
                    S.export.data(n,S.rng.V1s_rng) = V1s;
                    S.export.data(n,S.rng.V2s_rng) = V2s;
                    S.export.data(n,S.rng.E1b_rng) = E1b;
                    S.export.data(n,S.rng.E2b_rng) = E2b;
                else
                    % Nothing to do my Lord
                end

                S.results.nb_intersection(n) = nb_intersection; 
                % Nature des intersections: [sgn(delta),t1,t1::in_fov,t1::sign(beta),t2,t2::in_fov,t2::sign(beta)]                
                S.results.inter_status(n,:) = tab_inter_status;   
                if (t_inter_1~=0)
                    S.export.intersection_points(n,S.rng.inter_I1_rng) = get_point(V1,axe_lor,t_inter_1);
                end
                if (t_inter_2~=0)
                    S.export.intersection_points(n,S.rng.inter_I2_rng) = get_point(V1,axe_lor,t_inter_2);
                end




                test = "";
            % ---------------------------------------------------------------------
            % ---------------------------------------------------------------------
            % ---------------------------------------------------------------------
            % ---------------------------------------------------------------------
        end % switch C.case

        S.results.M_pr(n,:) = M;    % Dans le cas du postrin range, M a été stocké lors de l'appel à la fonction d'émission unifiée (simulation_emission.m)
        S.results.R_pr(n,:) = R_pr;
        % Stockage des données de comptage
        S.results.occ_config_count(config.idx) = S.results.occ_config_count(config.idx) + 1;
        % Stockage dans la structure H-map
        key_list = find(isinf(S.results.occ_config(config.idx,:)));
        S.results.occ_config(config.idx,key_list(1)) = n;

        test="";
        %break;
        if (mod(n,verb_lim) == 0 )
            info.R_pr = R_pr;
            info.n = n;
            info.N_allG =S.results.occ_config_count;
            info.pure_evt_idx = pure_evt_idx;
            info.pts = [S.results.M(n,:);M;Dir.D1;Dir.D2];
            info.inf_msg = inf_msg;
            info.config_idx = config.idx;
            %
            N_focG = info.N_allG(info.pure_evt_idx);
            N_remG = sum(info.N_allG) - N_focG; 
            fprintf("[B=%d] [G=%d] = %d/%d\t #(othG) = %g\t (%g %%)\n",b,seed,N_focG,C.N,N_remG,((N_focG)/C.N)*100);
            clear('N_focG','N_remG')
        end
        



        % ------------------------------------------------------------------
        % Condition d'arr^et de la simulation
        switch C.preset_type
            case "count"
                % prendre dans le range (2:end) car le premier indice du vecteur correspond aux 0-détections.
                %if (sum(S.results.occ_event(2:end)) == C.N), S.terminal=true; break;  end
                if (S.results.occ_config_count(pure_evt_idx) == C.N), S.terminal=true; break;  end
            case "time"
                 disp("A FAIRE - TODO "); S.terminal = false; break;
            otherwise
        end
    % Sécurité 
    [V0f,V0b,V1,V2,V0p,V1p,V2p,V0s,V1s,V2s,E1a,E2a] = reset_interaction_point();
    [n_gamma_511,n_gamma_1157] = reset_detection();
    test ="";
    
    
    end % while (~terminal)
    % on ferme la figure à la fin

    % ------------------------------------------------------------------
    S.results.N_exp = sum(S.results.occ_config_count);
    %{
        Je demande N tirages (preset-count) eg. N=100 ;
        Lors de la simulation, je peux en obtenir en fait M=107 car 7 seront sortis mais je m’en fiche, je veux N.
        Je simule mes N tirages voulus.
        Je stocke les résultats dans une matrice C
            - Est de dim 3 car je travaille avec un fantôme en 3D ;
            - Contient des entiers (+1 à chaque fois qu’un voxel est sélectionné).
        Une fois que j’ai obtenu mes N tirages (je peux en avoir fais M, je m’en fiche, j’en voulais N), je calcul l’activité de MON fantômes  (simulé) tq.
        activité = sum_i,j,k C(i,j,k) 	// i <=> x, j <=> y, k <=> z
        Je calcule alors le facteur de calibration tq.
        ECF = activité/N
    %}
    S.results.calib_factor = sum(S.results.activity,'all')/S.Conf.N;
    % ------------------------------------------------------------------
    t_stop = toc(t_start);
    save_name = strcat(F.save_folder,"pollux_",F.file,"_",C.pollux_case,"G_","B",num2str(b));
    save(save_name,"S",'t_stop');
    fprintf("##########################################################\n")
    print_phantom_info(P,F,S);
    fprintf("##########################################################\n")
    test ="";
end