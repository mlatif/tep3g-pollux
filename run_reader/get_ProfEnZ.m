function [prof_en_z] = get_ProfEnZ(P,Emi,Det,classe)
    prof_en_z = zeros(P.D(3),1);
    for (z = 1:P.D(3))
        vox_list = find(P.Ic(:,3)==z);
        detect_per_z = sum(Det(vox_list,classe));
        if (size(Emi,2)>1)
            emissi_per_z = sum(Emi(vox_list,classe));
        else
            emissi_per_z = sum(Emi(vox_list));
        end
        prof_en_z(z) = detect_per_z/emissi_per_z;
        test = ""; 
    end
end