clc;
clear all;
close all;
set(0,'DefaultFigureWindowStyle','docked')
addpath("../methods/");
addpath("../methods/dessin/");
% ------------------------------------------------------------------------
cs_file = "../data/nistdata_1_1200_kev_rng.txt";
neg_rayleigh = true;
X = orion_init_simulation(cs_file,neg_rayleigh);
C.draw = false;
C.verbose = false;
C.N_max = 1e7; 
C.mode_tirage = "cart"; % "pola"
PR.active = true; 
PR.mu = 2.4;
PR.sigma = 2;
% ------------------------------------------------------------------------
tps_max = 600; % (s)
nb_repet = 1; 
% ------------------------------------------------------------------------
rayon = 35; % (mm)
axe_l = 120; % (mm)
vol_quart = (pi*(rayon^2)*axe_l)/4; 
% ------------------------------------------------------------------------
%{
for (n = 1:nb_repet)
    disp("Start "+num2str(n)+" @ "+string(datetime('now')));
    S = run_orion(X,C,PR,rayon,axe_l,tps_max);
    % Estimation temporelle: 
    est_tps = (S.N_exp/vol_quart)/tps_max;
    disp("Nb part = "+num2str(S.N_exp))
    % ------------------------------------------------------------------------
    % Sauvegarde 
    folder = "SIMU_ORION"; 
    pref = "simu_tps"+num2str(tps_max)+"_ray"+num2str(rayon); 
    reg = fullfile(char(folder), char(pref + "*"));
    nb_exp = numel(dir(reg));
    name = folder+"/"+pref+"_"+num2str(nb_exp+1); 
    disp("SAVE "+name+" @ "+string(datetime('now')));
    save(name,'rayon','axe_l','vol_quart','tps_max','S','est_tps')
end
%}




N_exp_tab = zeros(nb_repet,1); 
event_class = zeros(nb_repet,6);
for (n = 1:nb_repet)
    folder = "SIMU_ORION"; 
    pref = "simu_tps"+num2str(tps_max)+"_ray"+num2str(rayon); 
    name = folder+"/"+pref+"_"+num2str(n);
    fprintf("LOAD %s\n",name)
    % = load(name).S.N_exp; 
    S = load(name).S; 
    N_exp_tab(n) = S.N_exp; 
    for (s = 1:6)
        event_class(n,s) = sum(S.chr==string(s-1));
    end
    test = ""; 
    clc; 
end

figure; 
    hold on; 
    plot(1:nb_repet,N_exp_tab); 
    yline(mean(N_exp_tab))
    title("Nombres d'émissions générées sur "+num2str(tps_max)+ " sec");

figure; 
    hold on; 
    plot(1:nb_repet,N_exp_tab./tps_max);
    yline(mean(N_exp_tab./tps_max));
    title("Nombres d'émissions générées par seconde")
    
Classif = unique(S.chr)'; 
prop = sum(event_class,1)./sum(event_class,'all')*100; 
fprintf('%s\t',Classif); fprintf("\n")
fmt = repmat('%.2f\t', 1, length(prop));
fprintf(fmt,prop); fprintf(" %%\n");

N_batch = 10; 
N_emiss = 1000; % Objectif  
d = 1;           % dimension d'un coté voxel 
objectif = N_emiss/(d^3);  %[nb_part/mm^3]
fprintf("Volume : %g (mm^3)\n",vol_quart); 
mean_time_part = mean(N_exp_tab./tps_max); 
fprintf("mean(Nb emiss per sec) = %g \n",mean_time_part)
capacity = mean_time_part/vol_quart; %[nb_part/mm^3/s]
% Chaque seconde, on est capable de générer E particule  par mm^3 par
% seconde
fprintf("mean(Nb emiss per sec) / vol total  [nb_emis/mm^3/s] = %g \n",capacity)
e = (capacity*(d^3));
fprintf("nb emiss per sec pour vox (%d)^3 : e = %g\n",d,e)
% Time to reach the objective
T = objectif/capacity; 
fprintf("Time to reach objective : T = %g (s) => T = %g (h) \n",T,(T/3600))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% t = tic();
% run_orion_preset_count(X,C,PR,rayon,axe_l,1000)
% stop = toc(t); 
% disp(stop)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% Réaffectation des points par symétrie sur l'ensemble du FOV
V = 5*[1,1,1];
orion_draw_xemis(X,rayon); view(3)
[G] = orion_create_grid(X,rayon,V);


%{
% Permutation des coordonnées par symétries
perm_sign = [ 1,  1,  1; -1,  1,  1; 1, -1,  1; 1,  1, -1; -1, -1,  1; -1,  1, -1; 1, -1, -1; -1, -1, -1];
Nb_q = size(perm_sign,1);    % Nombre de quadrants pour réaffecter les points  

% ATTENTION QUAND TU FERAS LA DIVISION POUR LES SENSI ÀBIEN PRENDRE EN
% COMPTE 8*N_exp;  

for (n = 1:1)
    P = S.data(n,:);
    c = str2double(S.chr(n)); 
    fprintf("P = (%g,%g,%g) \t c = %d \n",P(1),P(2),P(3),c)
    % ---------------------------------------------------------------------
    sym_P = perm_sign.*P; 
    for (s = 1:Nb_q)
        Ps = sym_P(s,:);
        %draw_pts(Ps,'x','r',5);
        ix = floor((Ps(1)+.5*G.T(1))/G.V(1))+1;
        iy = floor((Ps(2)+.5*G.T(2))/G.V(2))+1;
        iz = floor((Ps(3)+.5*G.T(3))/G.V(3))+1;
        % ATTENTION - En castor, les indices des voxels démarrent à 0 !!!
        vox_j = find(G.Ic(:,1)==ix & G.Ic(:,2)==iy & G.Ic(:,3)==iz);
        
        [R] = get_corresponding_point(G,vox_j);
        is_inside = (R(1) <= Ps(1) && Ps(1) <= R(1) + G.V(1)) && (R(2) <= Ps(2) && Ps(2) <= R(2) + G.V(2)) && (R(3) <= Ps(3) && Ps(3) <= R(3) + G.V(3));
        if (~is_inside)
            disp(n+"-"+s)
            break
        end
        fprintf("Voxel = %d \n",vox_j)
        %plotcube(G.V,[R(1),R(2),R(3)],0.005,[1,0,0]);
    end

            
end
%}
% générer directement interfile 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ------------------------------------------------------------------------
% ------------------------------------------------------------------------
% ------------------------------------------------------------------------
function [S] = run_orion(X,C,PR,rayon,axe_l,tps_max)
    it = 0;
    tps_exp = 0;
    % Initialisation de la structure de retour; 
    [S] = orion_init_struct(C);
    % 
    while (tps_exp<tps_max)
        it = it + 1;
        if (it > S.cur_N_limit)
            [S] = orion_update_struct(S,C);
        end

        t = tic();
        % ---------------------------------------------------------------------
        %%%%%%%%%%%%%%%%%%%%% TIRAGES D'UNE EMISSION %%%%%%%%%%%%%%%%%%%%%%%%%%
        % Simulation d'un point d'émission dans la région étudiée
        if (strcmp(C.mode_tirage,"cart"))
            ok = false;
            tz = axe_l*rand();
            tx = 0; ty = 0; 
            while (~ok)
                tx = rayon*rand(); 
                ty = rayon*rand();
                ok = sqrt(tx^2 + ty^2) <= rayon; 
            end
            M = [tx,ty,tz]; 
        elseif (strcmp(C.mode_tirage,"pola"))
            t_rho = rayon*sqrt(rand()); 
            t_theta = (pi/2)*rand(); 
            t_z = axe_l*rand(); 
            M = [t_rho*cos(t_theta),t_rho*sin(t_theta),t_z];
        end
        %pts_list = draw_store_pts(pts_list,M,'.','r',0.5);
        % Simulation des vecteurs directionnels pour chaque photons
        D1 = -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
        Dir.D1 = D1/norm(D1,2);
        D2 = -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
        Dir.D2 = D2/norm(D2,2);
        D_pr =  -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
        Dir.D_pr = D_pr/norm(D_pr,2);
        % ---------------------------------------------------------------------
        %%%%%%%%%%%%%%%%%%%%% SIMULATION D'UNE EMISSION %%%%%%%%%%%%%%%%%%%%%%%
        [E0,E0_prime,~,~] = init_photon_energy_count("3");
        % >>>>>>>>>>>>>>>>>>>>> Photon à 1.157 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        [t1_min_p_f,t1_min_p_b] = get_intersect_coord_Rin(X,M,Dir.D1);
        T_min_p = [t1_min_p_f,t1_min_p_b];
        scal_vec = zeros(size(T_min_p));
        V_unit_dir = M + Dir.D1;
        MV_nrm_unit_dir = (V_unit_dir-M)/norm(V_unit_dir-M,2);
        if (~isinf(t1_min_p_f))
            V_t1_min_p_f = M + t1_min_p_f*Dir.D1;
            MV_nrm_min_p_f = (V_t1_min_p_f - M)/norm(V_t1_min_p_f - M,2);
            scal_vec(1) = dot(MV_nrm_unit_dir,MV_nrm_min_p_f);
        end
        if (~isinf(t1_min_p_b))
            V_t1_min_p_b = M + t1_min_p_b*Dir.D1;
            MV_nrm_min_p_b = (V_t1_min_p_b - M)/norm(V_t1_min_p_b - M,2);
            scal_vec(2) = dot(MV_nrm_unit_dir,MV_nrm_min_p_b);
            if (C.draw), pts_list = draw_store_pts(pts_list,V_t1_min_p_b,'v','c'); end
        end
        [~,idx_t_min] = max(scal_vec);
        t1_min_p = T_min_p(idx_t_min);
        track_1157 = orion_init_tracker();
        if (~isinf(t1_min_p))
            [track_1157] = photon_tracker(X,C,track_1157,E0_prime,M,Dir.D1,t1_min_p);
        end
        % >>>>>>>>>>>>>>>>>>>>> Photon à 0.511 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        [M_pr,~] = positron_range(M,Dir.D_pr,PR);
        [t_min_f,t_min_b] = get_intersect_coord_Rin(X,M_pr,Dir.D2);  % b : back, f : forward
        % forward
        track_511_f = orion_init_tracker();
        if (~isinf(t_min_f))
            [track_511_f] = photon_tracker(X,C,track_511_f,E0,M_pr,Dir.D2,t_min_f);
        end
        % backward
        [track_511_b] = orion_init_tracker();
        if (~isinf(t_min_b))
            [track_511_b] = photon_tracker(X,C,track_511_b,E0,M_pr,Dir.D2,t_min_b);
        end
        % ---------------------------------------------------------------------
        %%%%%%%%%%%%%%%%%%%%% TRAITEMENT DE LA DETECTION %%%%%%%%%%%%%%%%%%%%%%
        [config] = orion_process_event(track_1157,track_511_f,track_511_b);
        S.chr(it,:) = config.chr; 
        S.data(it,:) = M; 
        S.N_exp = S.N_exp + 1 ; 
        % ---------------------------------------------------------------------
        tps = toc(t);
        tps_exp = tps_exp + tps;
        %
        test = "";
    end
    [S] = orion_plane_struct(S);
end

function [S] = run_orion_preset_count(X,C,PR,rayon,axe_l,N_max)
    it = 0;
    tps_exp = 0;
    % Initialisation de la structure de retour; 
    [S] = orion_init_struct(C);
    % 
    while (it<N_max)
        it = it + 1;
        if (it > S.cur_N_limit)
            [S] = orion_update_struct(S,C);
        end

        t = tic();
        % ---------------------------------------------------------------------
        %%%%%%%%%%%%%%%%%%%%% TIRAGES D'UNE EMISSION %%%%%%%%%%%%%%%%%%%%%%%%%%
        % Simulation d'un point d'émission dans la région étudiée
        r = rayon*rand();
        ang = pi/2*rand();
        M = [r*cos(ang),r*sin(ang),axe_l*rand()];
        %pts_list = draw_store_pts(pts_list,M,'.','r',0.5);
        % Simulation des vecteurs directionnels pour chaque photons
        D1 = -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
        Dir.D1 = D1/norm(D1,2);
        D2 = -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
        Dir.D2 = D2/norm(D2,2);
        D_pr =  -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
        Dir.D_pr = D_pr/norm(D_pr,2);
        % ---------------------------------------------------------------------
        %%%%%%%%%%%%%%%%%%%%% SIMULATION D'UNE EMISSION %%%%%%%%%%%%%%%%%%%%%%%
        [E0,E0_prime,~,~] = init_photon_energy_count("3");
        % >>>>>>>>>>>>>>>>>>>>> Photon à 1.157 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        [t1_min_p_f,t1_min_p_b] = get_intersect_coord_Rin(X,M,Dir.D1);
        T_min_p = [t1_min_p_f,t1_min_p_b];
        scal_vec = zeros(size(T_min_p));
        V_unit_dir = M + Dir.D1;
        MV_nrm_unit_dir = (V_unit_dir-M)/norm(V_unit_dir-M,2);
        if (~isinf(t1_min_p_f))
            V_t1_min_p_f = M + t1_min_p_f*Dir.D1;
            MV_nrm_min_p_f = (V_t1_min_p_f - M)/norm(V_t1_min_p_f - M,2);
            scal_vec(1) = dot(MV_nrm_unit_dir,MV_nrm_min_p_f);
        end
        if (~isinf(t1_min_p_b))
            V_t1_min_p_b = M + t1_min_p_b*Dir.D1;
            MV_nrm_min_p_b = (V_t1_min_p_b - M)/norm(V_t1_min_p_b - M,2);
            scal_vec(2) = dot(MV_nrm_unit_dir,MV_nrm_min_p_b);
            if (C.draw), pts_list = draw_store_pts(pts_list,V_t1_min_p_b,'v','c'); end
        end
        [~,idx_t_min] = max(scal_vec);
        t1_min_p = T_min_p(idx_t_min);
        track_1157 = orion_init_tracker();
        if (~isinf(t1_min_p))
            [track_1157] = photon_tracker(X,C,track_1157,E0_prime,M,Dir.D1,t1_min_p);
        end
        % >>>>>>>>>>>>>>>>>>>>> Photon à 0.511 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        [M_pr,~] = positron_range(M,Dir.D_pr,PR);
        [t_min_f,t_min_b] = get_intersect_coord_Rin(X,M_pr,Dir.D2);  % b : back, f : forward
        % forward
        track_511_f = orion_init_tracker();
        if (~isinf(t_min_f))
            [track_511_f] = photon_tracker(X,C,track_511_f,E0,M_pr,Dir.D2,t_min_f);
        end
        % backward
        [track_511_b] = orion_init_tracker();
        if (~isinf(t_min_b))
            [track_511_b] = photon_tracker(X,C,track_511_b,E0,M_pr,Dir.D2,t_min_b);
        end
        % ---------------------------------------------------------------------
        %%%%%%%%%%%%%%%%%%%%% TRAITEMENT DE LA DETECTION %%%%%%%%%%%%%%%%%%%%%%
        [config] = orion_process_event(track_1157,track_511_f,track_511_b);
        S.chr(it,:) = config.chr; 
        S.data(it,:) = M; 
        S.N_exp = S.N_exp + 1 ; 
        % ---------------------------------------------------------------------
        tps = toc(t);
        tps_exp = tps_exp + tps;
        %
        test = "";
    end
    [S] = orion_plane_struct(S);
end


function [G] = orion_create_grid(X,rayon,V)
    C = 2*rayon;
    % ---------------------------------------------------------------------
    G.V = V;
    G.D = [C/V(1),C/V(2),X.Ax/V(3)];
    G.T = G.D .* G.V;
    G.J = prod(G.D);
    % ---------------------------------------------------------------------
    % Coordonnées : Réelles (Rc), Indicielles (Ic)
    G.Rc = zeros(G.J,3);
    G.Ic = zeros(G.J,3);
    for j=1:G.J
        [a,b,c] = get_axes_voxel_idx(G,j);
        [r_a,r_b,r_c] = get_axes_voxel_coord(G,a,b,c);
        G.Rc(j,:) = [r_a,r_b,r_c];
        G.Ic(j,:) = [a,b,c];
        %draw_pts(G.Rc(j,:),'.','r',1);
    end
    % ---------------------------------------------------------------------

end






function [X] = orion_init_simulation(cs_file,neg_rayleigh)
X.O = [0 0 0];          % centre du repère
X.Rin = 75;             % Rayon intérieur LXe actif (mm)
X.Rout = 190;           % Rayon extérieur LXe actif (mm)
X.Rfov = 50;            % Rayon extérieur FOV (mm)
X.Ax = 240;             % Longueur axiale (mm);
X.L = X.Rin*cos(pi/4);  % demi longueur atteignable
% X.nVpL = 0;             % nb Voxel par demi longueur atteignable
% Dans l'ordre de lecture [Energy, Rayleigh, Compton, Photo Electrique, Att totale with Compton]
% Remarque : Si tu tentes de vérifier que la somme des trois effets est égale à l'attenuation totale, tu risques d'^etre déçu ... (perte de précision dans les données importées);
X.rho = 3.057;  % densité volumique de masse [g.cm^-3]
%nist_data = importdata(cs_file,"\t").data(:,1:4);
parsed_data = importdata(cs_file,"\t");
nist_data = parsed_data(:,1:4);
X.cs_data.Energy = nist_data(:,1);
nist_data = nist_data(:,2:end);                     % coefficient d'atténuation massique [cm^2.g^-1]
X.cs_data.mu = X.rho.*nist_data./10;                % ATTENTION : là tu es en [cm^-1], et toute tes données sont en [mm] -> il faut diviser par 10
% coefficient d'atténuation linéaire [cm^-1]
%X.cs_data.mu = [X.cs_data.mu, sum(X.cs_data.mu,2)];
%
X.cs_data.is_considered = ones(3,1);
if (neg_rayleigh), X.cs_data.is_considered(1) = 0; end
%
X.mec2 = 0.51099895; % (MeV) masse d'un électron au repos en MeV - https://en.wikipedia.org/wiki/Electron_mass#cite_note-physconst-mec2MeV-3
test ="";
end

function [track] = orion_init_tracker()
track.N = 0;
track.inter_pts = [];
track.dep_energ = [];
track.effect_id = [];
end

function [S] = orion_init_struct(C)
    S.N_exp = 0; 
    S.cur_N_limit = C.N_max;
    S.chr = strings(C.N_max,1);          % TODO Voir pour potentiel pb à la conversion
    S.data = zeros(C.N_max,3,'double');
end

function [S] = orion_update_struct(S,C)
    Es1 = strings(C.N_max,1); 
    Ez3 = zeros(C.N_max,3,'double'); 
    %
    S.cur_N_limit = S.cur_N_limit + C.N_max; 
    S.chr = [S.chr;Es1]; 
    S.data = [S.data;Ez3]; 
    test = "";
end

function [S] = orion_plane_struct(S)
    bound = S.N_exp + 1;
    S.chr(bound:end,:) = []; 
    S.data(bound:end,:) = []; 
    test = ""; 
end

function [config] = orion_process_event(T1157,T511f,T511b)
% Rappel: Rayleigh_id = 1; Photo_id = 2; Compton_id = 3;
% INCOMPATIBLE AVEC UN AJOUT FUTUR DE L'EFFET RAYLEIGH (CALCUL DE LA SOMME)

LOR = (T511f.N >= 1 && T511b.N >= 1);
G1157 = (T1157.N>=2 ); % && sum(T1157.effect_id(1:2))>=5
G511f = (~LOR && T511f.N>=2 ); % && sum(T511f.effect_id(1:2))>=5
G511b = (~LOR && T511b.N>=2 ); % && sum(T511b.effect_id(1:2))>=5

if (LOR)
    % Config initiale
    config.idx=4;
    config.chr='3';
    config.label="2GLOR";
    if (G1157)
        % Update de la config et des données associées
        config.idx=6;
        config.chr='5';
        config.label="3G";
    end
    % ---------------------------------------------------------------------
elseif (G511f)
    % Config initiale
    config.idx=2;
    config.chr='1';
    config.label="1G511_f";
    if (G1157)
        % Update de la config et des données associées
        config.idx=5;
        config.chr='4';
        config.label="2GMIX_f";
    end
    % ---------------------------------------------------------------------
elseif (G511b)
    % Config initiale
    config.idx=2;
    config.chr='1';
    config.label="1G511_b";
    if (G1157)
        % Update de la config et des données associées
        config.idx=5;
        config.chr='4';
        config.label="2GMIX_b";
    end
    % ---------------------------------------------------------------------
elseif (G1157)
    config.idx=3;
    config.chr='2';
    config.label = "1G1157";
    % -------------------------------------------------------------
else
    config.idx=1;
    config.chr='0';
    config.label = "0G";
end
% % ---------------------------------------------------------------------
% % ---------------------------------------------------------------------
% % ---------------------------------------------------------------------
% S.export.chr(n)= config.chr;
% % Stockage des données de comptage
% S.results.occ_config_count(config.idx) = S.results.occ_config_count(config.idx) + 1;
% % ---------------------------------------------------------------------
% if (config.idx>1)
%     S.nb_detection = S.nb_detection + 1;
% end
end


function [f] = orion_draw_xemis(X,rayon)
f = figure;
hold on;      box on;   %  grid on;
set(gca,'color','none')
% Simulateur en 3D
xlim([-X.Rin-10,+X.Rin+10]);                    xlabel('$x$','Interpreter','latex','FontSize',15);
ylim([-X.Rin-10,+X.Rin+10]);                    ylabel('$y$','Interpreter','latex','FontSize',15);
zlim([-ceil(X.Ax/2)-10,+ceil(X.Ax/2)+10]);      zlabel('$z$','Interpreter','latex','FontSize',15); hZLabel = get(gca,'ZLabel'); set(hZLabel,'rotation',0,'VerticalAlignment','middle')
plot3(X.O(1),X.O(2),X.O(3),'kx');

% Bords des detecteurs
th = 0:pi/50:2*pi;
% Bords des simulations (cylindre de rayon "rayon")
xunit = rayon * cos(th) + X.O(1);    yunit = rayon * sin(th) + X.O(2); zunit = ones(1,length(xunit));
plot3(xunit, yunit,0*zunit,'b'); axis equal;
plot3(xunit, yunit,(-X.Ax/2)*zunit,'b'); axis equal;
plot3(xunit, yunit,(+X.Ax/2)*zunit,'b'); axis equal;
% % Bords des simulations (cylindre de rayon "rayon")
T = [2*rayon,2*rayon,X.Ax]; 
plotcube(T,X.O-T/2,0.25,[.7 .7 .7])

% Bord intérieur en 3D
xunit = X.Rin * cos(th) + X.O(1);    yunit = X.Rin * sin(th) + X.O(2); zunit = ones(1,length(xunit));
plot3(xunit, yunit,0*zunit,'k'); axis equal;
plot3(xunit, yunit,(-X.Ax/2)*zunit,'k'); axis equal;
plot3(xunit, yunit,(+X.Ax/2)*zunit,'k'); axis equal;
% Bord exterieur en 3D
xunit = X.Rout * cos(th) + X.O(1);    yunit = X.Rout * sin(th) + X.O(2); zunit = ones(1,length(xunit));
plot3(xunit, yunit,0*zunit,'k'); axis equal;
plot3(xunit, yunit,(-X.Ax/2)*zunit,'k'); axis equal;
plot3(xunit, yunit,(+X.Ax/2)*zunit,'k'); axis equal;
% Limite FOV en 3D
xunit = X.Rfov * cos(th) + X.O(1);    yunit = X.Rfov * sin(th) + X.O(2); zunit = ones(1,length(xunit));
plot3(xunit, yunit,0*zunit,'k'); axis equal;
plot3(xunit, yunit,(-X.Ax/2)*zunit,'k--'); axis equal;
plot3(xunit, yunit,(+X.Ax/2)*zunit,'k--'); axis equal;
xline(0,"k--");
yline(0,"k--");
end
%{%}

