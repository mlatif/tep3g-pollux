% READ_POLLUX_SIMU
% METHODE PERMETTANT LA LECTURE DES FICHIERS RETOURNES PAR RUN_POLLUX

clear all;
close all;
addpath("../")
addpath("../methods/")
addpath("../methods/dessin/")
set(0,'DefaultFigureWindowStyle','docked') 
%path_name = "cortex_inter_pr35_short"; 
path_name = "cortex_inter_wopr_short"; 
path_name = "cortex_inter_short"; 
name = "cortex";
%path = strcat(name,"_simu/"); 
path = strcat("../",path_name,"_simu/"); 
load(strcat(path,"pollux_",name,"_config.mat"))
% 
K = 6;  % Nombre de classes 
N = 0;  % Nombre de simulations
M = 0; 
activity = zeros(P.D(1),P.D(2),P.D(3));
lambda = zeros(P.J,1);
occ_config_count = zeros(1,K); 
inter_config_count = zeros(1,3); 
pos_rng = []; once = true; 
compute_inter = false;
% PATCH Question SIMON: 
deux_int_distrib_beta_rad = []; 
deux_int_distrib_distance = []; 
une_int_distrib_distance_ori_inter = []; 
deux_int_distrib_distance_ori_inter = []; 
%
distribution_angulaire_compton_511 = [];
distribution_angulaire_compton_1157 = []; 
%
distribution_positron_range = []; 

lcl_archive_name = strcat(path_name,"_F_",name,"_parsed.mat"); 
if (isfile(lcl_archive_name))
    load(lcl_archive_name);
else 
    for (b=1:B)
        file = strcat(path,"pollux_",name,"_3G_B",num2str(b),".mat");
        if (exist(file)~=0)
            fprintf("%d|%s\n",K,file);
            load(file,'S');
            O = S.results.occ_config;
            V = S.results.vox_idx;
            if (once), pos_rng = S.Conf.Pos_rng; once = false; end
            occ_config_count = occ_config_count + S.results.occ_config_count;
            % ---
            if (isfield(S.results,'nb_intersection'))
                L = find(S.export.chr=="5");
                for (i=0:2)
                    inter_config_count(i+1) = inter_config_count(i+1)  + sum(S.results.nb_intersection(L)==i);
                    %fprintf("\t\t %d Inter(s) = %d (%g %%) \n",i,nb_inter_tab(i+1),;
                end
                compute_inter = true;
                % Question supplémentaires simon
                [dbr,dip,doi1i,doi2i]= analyse_intersections(X,S);

                deux_int_distrib_beta_rad = [deux_int_distrib_beta_rad; dbr];
                deux_int_distrib_distance = [deux_int_distrib_distance; dip];
                une_int_distrib_distance_ori_inter = [une_int_distrib_distance_ori_inter; doi1i];
                deux_int_distrib_distance_ori_inter = [deux_int_distrib_distance_ori_inter;doi2i];
                
                % Distribution angulaire Compton 
                [angles] = distribution_angulaire_CC(X,S);
                distribution_angulaire_compton_511 = [distribution_angulaire_compton_511;angles(~isinf(angles(:,1)),1) ];
                distribution_angulaire_compton_1157 = [distribution_angulaire_compton_1157;angles(~isinf(angles(:,2)),2) ];
                test = "";

                if (pos_rng.active)
                    dpr = distribution_pr(X,S);
                    distribution_positron_range = [distribution_positron_range; dpr]; 
                    test = ""; 
                end

            else

            end
            % ---
            N = N + length(V);
            % ---
            row = O(K,find(~isinf(O(K,:))));
            M = M + length(row);
            for (r=row)
                lambda(V(r)) = lambda(V(r)) + 1;
                [i,j,k] = get_axes_voxel_idx(P,V(r));
                activity(i,j,k)= activity(i,j,k)+1;
                test ="";
            end
            %         clf; hold on;
            %         plot(1:P.J,lambda)
            %         pause(0.0001)
        end
        test ="";
        %break
    end
    clear('S'); 
    save(lcl_archive_name,'lambda','activity','N','K','M','occ_config_count','inter_config_count','pos_rng','compute_inter','deux_int_distrib_beta_rad', 'deux_int_distrib_distance' ,'une_int_distrib_distance_ori_inter','deux_int_distrib_distance_ori_inter','distribution_angulaire_compton_511','distribution_angulaire_compton_1157','distribution_positron_range')
    % PATCH Question SIMON:
end


fprintf("N = %d \t K = %d \t M = %d\n",N,K,M); 
save(strcat(path,C.Classif(K),"_read"),'lambda','activity','N','K','M','occ_config_count','pos_rng','compute_inter','deux_int_distrib_beta_rad', 'deux_int_distrib_distance' ,'une_int_distrib_distance_ori_inter','deux_int_distrib_distance_ori_inter','distribution_angulaire_compton_511','distribution_angulaire_compton_1157','distribution_positron_range' );

% Exclusion des cas 0G.
Np = sum(occ_config_count(2:end));
PERC_ALOP = (1*Np)/N; 
PERC_CONFIG = (occ_config_count/N)*100;
PERC_CONFIG_ALOP = (occ_config_count(2:end)/Np)*100;
if (compute_inter)
    PERC_INTERS = (100.*inter_config_count)./M; 
end



fprintf("run_pollux(""%s"",""%s"",SEED,1,%d) avec SEED = %d;\n",name,path,M/Q,RNG)
if (pos_rng.active)
    fprintf("Positron range ~ N(%g,%g). \n",pos_rng.mu,pos_rng.sigma^2)
else
    fprintf("Positron range desactivé. \n")
end

fprintf("Nombre mesures 3G demandées : M =  %d\n",M);
fprintf("Nombre mesures réalisées : N = %d \n",N); 
fprintf("Nombre mesures réalisées (au moins 1 photon): N' = %d \n",Np); 
fprintf("=> %g %% de détections >= 1 photon \n",PERC_ALOP)
%
fprintf("Avec N = %d \n",N); 
disp(C.Classif(:,1)'); 
disp(PERC_CONFIG)
fprintf("Avec N' = %d \n",Np); 
disp(C.Classif(2:end,1)'); 
disp(PERC_CONFIG_ALOP)
fprintf("Intersection LOR/COR \n"); 
if (compute_inter)
    disp((0:2))
    disp(PERC_INTERS)
end


if (true)

    fprintf("Figures:\n\t" + ...
        "1) Distribution angle beta\n\t" + ...
        "2) Distribution distance entre les intersections (2 inters)\n\t" + ...
        "3) Distribution distance à l'émission (1 inter)\n\t" + ...
        "4) Distribution distance à l'émission (2 inters - [min(:,1),max(:,2)])\n\t" + ...
        "5) Distribution des angles simulés ([511,1157])\n\t" + ...
        "6) Distribution distance PR (Valide ? = %g)\n\t" + ...
        "7) Distribution bivariée distance entre 2 inters x angle beta associé \n\t" + ...
        "8) Expliquer les résultats de distance entre intersection avec le PR ? \n\t" + ...
        "\n",pos_rng.active)
    
    % -------------------------------------------------------------------------
    choix = input("Choix de figure = "); 
    switch choix
        case 1
             distrib_beta(deux_int_distrib_beta_rad,pos_rng);
        case 2
            distrib_dist_double_inter(deux_int_distrib_distance,pos_rng); 
        case 3
            distrib_dist_orig_une_inter(une_int_distrib_distance_ori_inter,pos_rng);
        case 4
            distrib_dist_orig_double_inter(deux_int_distrib_distance_ori_inter,pos_rng);
        case 5
            distribution_angulaire_compton(distribution_angulaire_compton_511,distribution_angulaire_compton_1157); 
        case 6
            distribution_distance_pr(distribution_positron_range,pos_rng); 
        case 7
            %OLD_distribution_biv_dist_angle(deux_int_distrib_beta_rad,deux_int_distrib_distance,pos_rng); 
            distribution_biv_dist_angle(deux_int_distrib_beta_rad,deux_int_distrib_distance,pos_rng)
        case 8
            tentative_explication_dist_inter_with_pr();
        otherwise

    end
end
fprintf(">> print -depsc ... \n");






fileID = fopen(strcat("",C.Classif(K),".i"),'w','l');
    for (j=1:P.J)
        d = lambda(j); 
        fwrite(fileID,d,"float32");
    end
fclose(fileID); 







function [] = distrib_beta(deux_int_distrib_beta_rad,pos_rng)
    fprintf("[Q1]Distrib beta [%d obs]\nMean = %g\t Med = %g \t Var = %g \n",length(deux_int_distrib_beta_rad),mean(rad2deg(deux_int_distrib_beta_rad)),median(rad2deg(deux_int_distrib_beta_rad)),var(rad2deg(deux_int_distrib_beta_rad)))
    figure; 
    [n_elem,cent] = hist(rad2deg(deux_int_distrib_beta_rad),180);
    h = cent(2) - cent(1);        % qui est le même que c(3)-c(2) ... <=> milieu d'une classe définie par hist 
    hauteurs = n_elem/(length(deux_int_distrib_beta_rad)*h);
    col = 'b'; if (pos_rng.active), col = 'r'; end 
    bar(cent,hauteurs,'FaceColor',col);
    xlabel("$\beta \in [0,\pi]$ (deg)",'Interpreter','latex'); 
    ylabel("")
    % ---------------------------------------------------------------------
    % Cas diffusion
    idx_rng = (1:floor(length(cent)/2)); 
    sub_cent = cent(idx_rng); 
    sub_haut = hauteurs(idx_rng);
    [~,i] = max(sub_haut);
    x1 = xline(sub_cent(i),'k:',num2str(sub_cent(i),'%.2f'),'LabelOrientation','horizontal',LabelVerticalAlignment='top',FontSize=15,Interpreter='latex',LineWidth=2);
    idx_rng = (floor(length(cent)/2):length(cent));
    sub_cent = cent(idx_rng); 
    sub_haut = hauteurs(idx_rng);
    [~,i] = max(sub_haut);
    x2 = xline(sub_cent(i),'k:',num2str(sub_cent(i),'%.2f'),'LabelOrientation','horizontal',LabelVerticalAlignment='top',LabelHorizontalAlignment='left',FontSize=15,Interpreter='latex',LineWidth=2);
    test = ""; 
end

function [] = distrib_dist_double_inter(deux_int_distrib_distance,pos_rng)
    figure; 
        [n_elem,cent] = hist(deux_int_distrib_distance,100);
        h = cent(2) - cent(1);        % qui est le même que c(3)-c(2) ... <=> milieu d'une classe définie par hist 
        hauteurs = n_elem/(length(deux_int_distrib_distance)*h);
        col = 'b'; if (pos_rng.active), col = 'r'; end 
        bar(cent,hauteurs,'FaceColor',col);
        xlabel("Distance inter intersections (mm)",'Interpreter','latex'); 
        ylabel("")
    fprintf("[Q2]Distrib distance [%d obs]\nMean = %g\t Med = %g \t Var = %g\n",length(deux_int_distrib_distance),mean(deux_int_distrib_distance),median(deux_int_distrib_distance),var(deux_int_distrib_distance))
end

function [] = distrib_dist_orig_une_inter(une_int_distrib_distance_ori_inter,pos_rng)
    figure; 
        [n_elem,cent] = hist(une_int_distrib_distance_ori_inter,75);
        h = cent(2) - cent(1);        % qui est le même que c(3)-c(2) ... <=> milieu d'une classe définie par hist 
        hauteurs = n_elem/(length(une_int_distrib_distance_ori_inter)*h);
        col = 'b'; if (pos_rng.active), col = 'r'; end 
        bar(cent,hauteurs,'FaceColor',col);
        xlabel("Distance decay/inter1 (mm) - Card($\mathcal{C}\cap\mathcal{L}$)=1",'Interpreter','latex'); 
        ylabel("")
        xlim([-1,50]);
    fprintf("[Q3]Distrib distance Decay/Int1 [%d obs]\nMean = %g\t Med = %g \t Var = %g\n",length(une_int_distrib_distance_ori_inter),mean(une_int_distrib_distance_ori_inter),median(une_int_distrib_distance_ori_inter),var(une_int_distrib_distance_ori_inter))
end

function [] = distrib_dist_orig_double_inter(deux_int_distrib_distance_ori_inter,pos_rng)
    figure; 
    hold on; 
    [n_elem,cent] = hist(deux_int_distrib_distance_ori_inter(:,1)',75);
    h = cent(2) - cent(1);        % qui est le même que c(3)-c(2) ... <=> milieu d'une classe définie par hist 
    hauteurs = n_elem/(size(deux_int_distrib_distance_ori_inter,1)*h);
    col = 'b'; if (pos_rng.active), col = 'r'; end 
    plot(cent,hauteurs,'Color',col,'Marker','.','DisplayName','Inter | dist min');
    [n_elem,cent] = hist(deux_int_distrib_distance_ori_inter(:,2)',75);
    h = cent(2) - cent(1);        % qui est le même que c(3)-c(2) ... <=> milieu d'une classe définie par hist 
    hauteurs = n_elem/(size(deux_int_distrib_distance_ori_inter,1)*h);
    plot(cent,hauteurs,'Color','magenta','Marker','.','DisplayName','Inter | dist max');
    if(~pos_rng.active ),set(gca,'YScale','log'); end % C'est tellement petit qu'on voit pas
    legend('Location','best')
    fprintf("[Q4]Distrib distance Decay/Int1,Int2 [%d obs]\nMIN: Mean = %g\t Med = %g \t Var = %g\nMAX: Mean = %g\t Med = %g \t Var = %g\n",length(deux_int_distrib_distance_ori_inter),mean(deux_int_distrib_distance_ori_inter(:,1)),median(deux_int_distrib_distance_ori_inter(:,1)),var(deux_int_distrib_distance_ori_inter(:,1)),mean(deux_int_distrib_distance_ori_inter(:,2)),median(deux_int_distrib_distance_ori_inter(:,2)),var(deux_int_distrib_distance_ori_inter(:,2)))
end 

function [] = distribution_angulaire_compton(distribution_angulaire_compton_511,distribution_angulaire_compton_1157)
  
        
        fprintf("[Q5]Distrib angle Compton [nb,mean,med,std]\n");
        
        idx_D = distribution_angulaire_compton_511 < (pi/2);
        idx_rD = ~idx_D;
        mu_511_D = mean(distribution_angulaire_compton_511(idx_D)); 
        mu_511_rD = mean(distribution_angulaire_compton_511(idx_rD));
        %
        eta_511_D = median(distribution_angulaire_compton_511(idx_D)); 
        eta_511_rD = median(distribution_angulaire_compton_511(idx_rD));
        % 
        std_511_D = std(distribution_angulaire_compton_511(idx_D)); 
        std_511_rD = std(distribution_angulaire_compton_511(idx_rD));
        fprintf("\t511-D:\t%d\t%g\t%g\t%g\t\n",sum(idx_D),rad2deg(mu_511_D),rad2deg(eta_511_D),rad2deg(std_511_D)); 
        fprintf("\t511-rD:\t%d\t%g\t%g\t%g\t\n",sum(idx_rD),rad2deg(mu_511_rD),rad2deg(eta_511_rD),rad2deg(std_511_rD)); 
        
        idx_D = distribution_angulaire_compton_1157 < (pi/2);
        idx_rD = ~idx_D;
        mu_1157_D = mean(distribution_angulaire_compton_1157(idx_D)); 
        mu_1157_rD = mean(distribution_angulaire_compton_1157(idx_rD));
        %
        eta_1157_D = median(distribution_angulaire_compton_1157(idx_D)); 
        eta_1157_rD = median(distribution_angulaire_compton_1157(idx_rD));
        % 
        std_1157_D = std(distribution_angulaire_compton_1157(idx_D)); 
        std_1157_rD = std(distribution_angulaire_compton_1157(idx_rD));
        fprintf("\t1157-D:\t%d\t%g\t%g\t%g\t\n",sum(idx_D),rad2deg(mu_1157_D),rad2deg(eta_1157_D),rad2deg(std_1157_D)); 
        fprintf("\t1157-rD:\t%d\t%g\t%g\t%g\t\n",sum(idx_rD),rad2deg(mu_1157_rD),rad2deg(eta_1157_rD),rad2deg(std_1157_rD)); 

        figure; 
            hold on; box on; legend box off; 
            x0 = xline(90,'--','LabelOrientation','horizontal',LineWidth=0.5);
            % 511
            [n_elem_511,cent_511] = hist(rad2deg(distribution_angulaire_compton_511),180);
            h_511 = cent_511(2) - cent_511(1);        % qui est le même que c(3)-c(2) ... <=> milieu d'une classe définie par hist 
            hauteurs_511 = n_elem_511/(length(distribution_angulaire_compton_511)*h_511);
            p511 = plot(cent_511,hauteurs_511,'Color','b','Marker','.','DisplayName','$\gamma = 511$ keV');
            x1 = xline(rad2deg(eta_511_D),'b:',num2str(rad2deg(eta_511_D),'%.2f'),'LabelOrientation','horizontal',LabelVerticalAlignment='top',LabelHorizontalAlignment='right',FontSize=15,Interpreter='latex',LineWidth=2);
            x2 = xline(rad2deg(eta_511_rD),'b:',num2str(rad2deg(eta_511_rD),'%.2f'),'LabelOrientation','horizontal',LabelVerticalAlignment='top',LabelHorizontalAlignment='right',FontSize=15,Interpreter='latex',LineWidth=2);
            % --
            [n_elem_1157,cent_1157] = hist(rad2deg(distribution_angulaire_compton_1157),180);
            h = cent_1157(2) - cent_1157(1);        % qui est le même que c(3)-c(2) ... <=> milieu d'une classe définie par hist 
            hauteurs_1157 = n_elem_1157/(length(distribution_angulaire_compton_1157)*h);
            p1157 = plot(cent_1157,hauteurs_1157,'Color','r','Marker','.','DisplayName','$\gamma = 1157$ keV');
        
            x1 = xline(rad2deg(eta_1157_D),'r:',num2str(rad2deg(eta_1157_D),'%.2f'),'LabelOrientation','horizontal',LabelVerticalAlignment='top',LabelHorizontalAlignment='left',FontSize=15,Interpreter='latex',LineWidth=2);
            x2 = xline(rad2deg(eta_1157_rD),'r:',num2str(rad2deg(eta_1157_rD),'%.2f'),'LabelOrientation','horizontal',LabelVerticalAlignment='top',LabelHorizontalAlignment='left',FontSize=15,Interpreter='latex',LineWidth=2);
            
            legend([p511,p1157],'Location','northoutside','Interpreter','latex','NumColumns',2,FontSize=15); legend boxoff; 
            xlabel("$\beta \in [0,\pi]$",'Interpreter','latex',FontSize=15);
            

        test = ""; 
end

%{
function [] = distrib_beta(deux_int_distrib_beta_rad,pos_rng)
    fprintf("[Q1]Distrib beta [%d obs]\nMean = %g\t Med = %g \t Var = %g \n",length(deux_int_distrib_beta_rad),mean(rad2deg(deux_int_distrib_beta_rad)),median(rad2deg(deux_int_distrib_beta_rad)),var(rad2deg(deux_int_distrib_beta_rad)))
    figure; 
    [n_elem,cent] = hist(rad2deg(deux_int_distrib_beta_rad),180);
    h = cent(2) - cent(1);        % qui est le même que c(3)-c(2) ... <=> milieu d'une classe définie par hist 
    hauteurs = n_elem/(length(deux_int_distrib_beta_rad)*h);
    col = 'b'; if (pos_rng.active), col = 'r'; end 
    bar(cent,hauteurs,'FaceColor',col);
    xlabel("$\beta \in [0,\pi]$ (deg)",'Interpreter','latex'); 
    ylabel("")
    % ---------------------------------------------------------------------
   
    
    [~,i] = max(sub_haut);
    
   
    [~,i] = max(sub_haut);
    x2 = xline(sub_cent(i),'k:',num2str(sub_cent(i),'%.2f'),'LabelOrientation','horizontal',LabelVerticalAlignment='top',LabelHorizontalAlignment='left',FontSize=15,Interpreter='latex',LineWidth=2);
    test = ""; 
end

%}


function [] = distribution_distance_pr(distribution_positron_range,pos_rng)
    if (pos_rng.active)
        figure; 
        [n_elem,cent] = hist(distribution_positron_range,75);
        h = cent(2) - cent(1);        % qui est le même que c(3)-c(2) ... <=> milieu d'une classe définie par hist 
        hauteurs = n_elem/(length(distribution_positron_range)*h);
        col = 'b'; if (pos_rng.active), col = 'r'; end 
        bar(cent,hauteurs,'FaceColor',col);
        xlabel("Distance PR (mm)",'Interpreter','latex'); 
        ylabel("")
        xlim([-5,15]);
        fprintf("[Q6]Distrib PR [%d obs]\nMean = %g\t Med = %g \t Var = %g\n",length(distribution_positron_range),mean(distribution_positron_range),median(distribution_positron_range),var(distribution_positron_range))
    else
        fprintf("Tu es stupide mon pauvre")
    end
end

function [] = OLD_distribution_biv_dist_angle(deux_int_distrib_beta_rad,deux_int_distrib_distance,pos_rng)
figure; 
    hist_angle = hist(rad2deg(deux_int_distrib_beta_rad),180);
    hist_distance = hist(deux_int_distrib_distance,180);
    min_distance = min(deux_int_distrib_distance); 
    max_distance = max(deux_int_distrib_distance); 
    distances = linspace(min_distance, max_distance, length(hist_distance));
    angles = linspace(0, 180, length(hist_angle));    
    [angles_matrix, distances_matrix] = meshgrid(angles, distances);
    intensities_matrix = hist_angle' * hist_distance;
    imagesc(angles, distances, intensities_matrix);
    map = [1 1 1 
    1 0 0];
    % Couleur de départ (blanc)
    color_start = [255, 255, 255]; % RVB : blanc
    % Couleur d'arrêt (rouge)
    color_end = [0, 0, 255]; 
    if (pos_rng.active), color_end = [255, 0, 0]; end 
    %color_end = [255, 0, 0]; % RVB : rouge
    % Nombre de couleurs intermédiaires à générer
    num_colors = 100; % Vous pouvez ajuster le nombre de couleurs selon vos besoins
    % Générer la colormap en interpolant entre les deux couleurs
    custom_colormap = zeros(num_colors, 3); % Initialiser la colormap
    % Utiliser linspace pour interpoler entre les deux couleurs
    for i = 1:3
        custom_colormap(:, i) = linspace(color_start(i), color_end(i), num_colors) / 255; % Diviser par 255 pour normaliser à l'intervalle [0, 1]
    end

    colormap(custom_colormap); % Choisissez la colormap souhaitée
    colorbar; % Affiche l'échelle de couleur
    xlabel('$\beta$ (deg)','Interpreter','latex');
    ylabel('Distance entre intersections (mm)');
    title("$\textrm{Card}(\mathcal{L}\cap\mathcal{C})=2$",'Interpreter','latex');

end

function [] = distribution_biv_dist_angle(deux_int_distrib_beta_rad,deux_int_distrib_distance,pos_rng)
    deux_int_distrib_beta = rad2deg(deux_int_distrib_beta_rad);
    nb_couple = length(deux_int_distrib_distance);
    nb_classe_angl = 180; 
    nb_classe_dist = 125; 
    [n_elem_ang,cent_ang] = hist(deux_int_distrib_beta,nb_classe_angl);
    [n_elem_dst,cent_dst] = hist(deux_int_distrib_distance,nb_classe_dist);
    Mat_Sim = zeros(nb_classe_dist,nb_classe_angl); 
    
    % Genre de distrib conjointe 
    for (n = 1:nb_couple)
        dst = deux_int_distrib_distance(n); 
        [~,i] = min(abs(dst-cent_dst)) ;
        ang = deux_int_distrib_beta(n); 
        [~,j] = min(abs(ang-cent_ang)) ;
        % 
        Mat_Sim(i,j) = Mat_Sim(i,j) + 1; 
    end
    % Test des marginales 
    fprintf("Marg Ang ? = %d\n",all(sum(Mat_Sim,1)==n_elem_ang))
    fprintf("Marg Dst ? = %d\n",all(sum(Mat_Sim,2)'==n_elem_dst))
    maximum = max(max(Mat_Sim));
    [x,y]=find(Mat_Sim==maximum); 
    fprintf("With PR ? %d \n",pos_rng.active);
    fprintf("M = %d \n",nb_couple); 
    fprintf("Max = %g (beta = %g (deg), dist = %g (mm))\n",maximum,cent_ang(x),cent_dst(y))
    
    figure; 
        imagesc(Mat_Sim); colorbar; colormap("jet"); axis equal
        xlabel('$\beta$ (deg)','Interpreter','latex');
        ylabel('Distance entre intersections (mm)','Interpreter','latex');
    
    figure; 
        surf(Mat_Sim); colormap("jet")
        shading interp
        xlabel('$\beta$ (deg)','Interpreter','latex');
        ylabel('$d$ (mm)','Interpreter','latex');
        zlabel('$C(\beta,d)$','Interpreter','latex')

end


function [] = tentative_explication_dist_inter_with_pr()
    dist_btw_inter_wo_pr = load('cortex_inter_wopr_short_F_cortex_parsed.mat').deux_int_distrib_distance;
    dist_btw_inter_wi_pr = load('cortex_inter_short_F_cortex_parsed.mat').deux_int_distrib_distance;
    distribution_positron_range = load('cortex_inter_short_F_cortex_parsed.mat').distribution_positron_range; 
    
    [n_elem_wo_pr,cent_wo_pr] = hist(dist_btw_inter_wo_pr,100); h = cent_wo_pr(2) - cent_wo_pr(1);
    hauteurs_wo_pr = n_elem_wo_pr/(length(dist_btw_inter_wo_pr)*h);
    [n_elem_wi_pr,cent_wi_pr] = hist(dist_btw_inter_wi_pr,100); h = cent_wi_pr(2) - cent_wi_pr(1);
    hauteurs_wi_pr = n_elem_wi_pr/(length(dist_btw_inter_wi_pr)*h);
    [n_elem_noyau,cent_noyau] = hist(distribution_positron_range,100); h = cent_noyau(2) - cent_noyau(1);
    hauteurs_noyau = n_elem_noyau/(length(distribution_positron_range)*h);
    %
    figure; 
        hold on; 
        plot(cent_wo_pr,hauteurs_wo_pr,'Color','b','Marker','.','DisplayName','no PR');
        %plot(cent_wi_pr,hauteurs_wi_pr,'Color','r','Marker','.','DisplayName','PR');
        legend('Location','best')
        xlabel('distance btw Int1,Int2 [mm]')
    figure; 
        hold on; 
        plot(cent_noyau,hauteurs_noyau,'Color','k','Marker','.','DisplayName','PR');
        xlabel('distance PR [mm]')
    
    figure; 
        hold on; 
        plot(cent_wi_pr,hauteurs_wi_pr,'Color','r','Marker','.','DisplayName','PR');
        plot(cent_wi_pr,hauteurs_wo_pr.*hauteurs_noyau,'Color','b','Marker','.','DisplayName','no PR x dist(PR)');
        %test_conv = conv2(hauteurs_wo_pr,hauteurs_noyau,'same');
        %plot(1:length(test_conv),test_conv,'Color','k','Marker','.','DisplayName','PR-conv');
        xlabel('distance btw Int1,Int2 [mm]')
        legend('Location','best')
end



function [i,j,k] = get_axes_voxel_idx(P,v)
    k = ceil(v/(P.D(1)*P.D(2)));
    t = mod(v,P.D(1)*P.D(2)); if (t==0), t = P.D(1)*P.D(2); end
    j = ceil(t/P.D(1));
    i = mod(t,P.D(1)); if (i==0), i = P.D(1); end
end


function [distrib_beta_rad,distance_inter_pts,distrib_distance_ori_inter_1I,distrib_distance_ori_inter_2I] = analyse_intersections(X,S)
    L = find(S.export.chr=="5");
    % QUESTION 1 - Distribution angle beta dans le cas d'une double intersection 
    % QUESTION 2 - Distribution distance points dans le cas d'une double intersection 
    focus_point_idx = find(S.results.nb_intersection == 2);
    distrib_beta_rad = zeros(length(focus_point_idx),1);        % Q1
    distance_inter_pts = zeros(length(focus_point_idx),1);      % Q2

    for (l = 1:length(focus_point_idx))
        n = focus_point_idx(l);
        V1 = S.export.data(n,S.export.rng.V1_rng);
        V2 = S.export.data(n,S.export.rng.V2_rng);
        V1p= S.export.data(n,S.export.rng.V1p_rng);
        V2p= S.export.data(n,S.export.rng.V2p_rng);
        E1a= S.export.data(n,S.export.rng.E1a_rng);
%         axe_lor = (V2-V1)/norm(V2-V1,2); 
%         axe_csr = (V1p-V2p)/norm(V1p-V2p,2); 
%         apex = V1p; 
        %
        distrib_beta_rad(l) = acos(1-(E1a*X.mec2/(1.157*(1.157-E1a)))); 
        % 
        inter_1 = S.export.intersection_points(n,S.rng.inter_I1_rng);
        inter_2 = S.export.intersection_points(n,S.rng.inter_I2_rng); 
        distance_inter_pts(l) = norm(inter_1 - inter_2);
    end
    % QUESTION 3 - Distribution distance à l'émission dans le cas d'une 1-intersection 
    focus_point_idx = find(S.results.nb_intersection == 1);
    distrib_distance_ori_inter_1I = zeros(length(focus_point_idx),1); 
    for (l = 1:length(focus_point_idx))
        n = focus_point_idx(l);
        inter_1 = S.export.intersection_points(n,S.rng.inter_I1_rng);
        %inter_2 = S.export.intersection_points(n,S.rng.inter_I2_rng); 
        M = S.results.M(n,:);
        %M_pr = S.results.M_pr(n,:);
%         if (M~=M_pr)
%             test =""; 
%         end
        distrib_distance_ori_inter_1I(l) = norm(M-inter_1); 
        test = "";  

    end
    % QUESTION 4 - Distribution distance à l'émission dans le cas d'une 2-intersection 
    focus_point_idx = find(S.results.nb_intersection == 2);
    distrib_distance_ori_inter_2I = zeros(length(focus_point_idx),2);
     for (l = 1:length(focus_point_idx))
        n = focus_point_idx(l);
        inter_1 = S.export.intersection_points(n,S.rng.inter_I1_rng);
        inter_2 = S.export.intersection_points(n,S.rng.inter_I2_rng); 
        M = S.results.M(n,:);
        n_MI1 = norm(M-inter_1);
        n_MI2 = norm(M-inter_2); 
        if (n_MI1 <= n_MI2)
            plop = [n_MI1, n_MI2];
        else
            plop = [n_MI2, n_MI1];
        end
        distrib_distance_ori_inter_2I(l,:) = plop; 
        
        %M_pr = S.results.M_pr(n,:);
%         if (M~=M_pr)
%             test =""; 
%         end

        test = "";  

    end

    test  = ""; 
end


function [ANGLE] = distribution_angulaire_CC(X,S)
    % [511,1157]
    ANGLE = inf(length(S.export.chr),2); 
    for (n = 1:length(S.export.chr))
        %disp(S.export.chr(n))
        [~,~,~,~,~,~,E0,E0_prime,E1a,E2a,E1b,E2b] = lcl_load_event(S,n);
        switch S.export.chr(n)
            case '1' % 1G511
                ANGLE(n,1) = acos(1-(E1b*X.mec2/(E0*(E0-E1b)))); 
            case '2' % 1G1157
                ANGLE(n,2) = acos(1-(E1a*X.mec2/(E0_prime*(E0_prime-E1a))));
                test = ""; 
            case '4' %2GMIX
                ANGLE(n,1) = acos(1-(E1b*X.mec2/(E0*(E0-E1b)))); 
                ANGLE(n,2) = acos(1-(E1a*X.mec2/(E0_prime*(E0_prime-E1a)))); 
                test = ""; 
            case '5' % 3G
                ANGLE(n,2) = acos(1-(E1a*X.mec2/(E0_prime*(E0_prime-E1a)))); 
                test = ""; 
            otherwise
                % PAS INTERESSE PAR LA CLASSE 3, SORRY
        end
        clear('E0','E0_prime','E1a','E2a','E1b','E2b');
        test = ""; 
    end

    test = ""; 
end



function [V1,V2,V1p,V2p,V1s,V2s,E0,E0_prime,E1a,E2a,E1b,E2b] = lcl_load_event(S,n)

    S.export.chr(n); 
    V1 = [];% S.export.data(n,S.rng.V1_rng);                                     if (all(V1==0)), V1 = []; end                              
    V2 = [];% S.export.data(n,S.rng.V2_rng);                                     if (all(V2==0)), V2 = []; end  
    V1p =[];% S.export.data(n,S.rng.V1p_rng);                                   if (all(V1p==0)), V1p = []; end
    V2p =[]; %S.export.data(n,S.rng.V2p_rng);                                   if (all(V2p==0)), V2p = []; end  
    V1s =[]; % S.export.data(n,S.rng.V1s_rng);                                   if (all(V1s==0)), V1s = []; end
    V2s =[]; % S.export.data(n,S.rng.V2s_rng);                                   if (all(V2s==0)), V2s = []; end  
    E1a = S.export.data(n,S.rng.E1a_rng);                                   if (E1a==0),E1a = []; end 
    E2a = S.export.data(n,S.rng.E2a_rng);                                   if (E2a==0),E2a = []; end
    E1b = S.export.data(n,S.rng.E1b_rng);                                   if (E1b==0),E1b = []; end
    E2b = S.export.data(n,S.rng.E2b_rng);                                   if (E2b==0),E2b = []; end
    I1 =[]; % S.export.intersection_points(n,S.rng.inter_I1_rng);                if (all(I1==0)), I1 = []; end
    I2 =[]; % S.export.intersection_points(n,S.rng.inter_I2_rng);                if (all(I2==0)), I2 = []; end 
    % 
    [E0,E0_prime,~,~] = init_photon_energy_count("3");
    switch S.export.chr(n)
        case "0"
            E0 = []; E0_prime = []; 
        case "1"
            E0_prime = []; 
        case "2"
            E0 = []; 
        case "3"
            E0_prime = []; 
        case "4"
            % Nothing, besoin des deux
        case "5"
            % Nothing, besoin des deux
    end

test = ""; 
end


function [distri_pr] = distribution_pr(X,S)

    distri_pr = inf(length(S.export.chr),1); 
    for (n=1:length(S.export.chr))
        M = S.results.M(n,:); 
        M_pr = S.results.M_pr(n,:); 
        distri_pr(n) = norm(M-M_pr); 
    end


    
    test = ""; 

end
