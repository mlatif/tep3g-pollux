function [] = Copy_of_run_orion(N)
% RUN_ORION(N)
%   Simulateur Orion pour la génération de carte de sensibilité dans un
%   quart de cyclindre (rayon=35,hauteur=120); 
%   INPUT
%       - N: nombre de répétion à réaliser (chaque répétion ~7h)
    addpath("../methods/");
    % ---------------------------------------------------------------------
    % path général où l'on va stocker les données;  
    PATH = "/home/user/Bureau/THESE/SIMU_ORION/DEBUG_TEST_CC/"; 
    % Génération ou chargement de la config; 
    if (~exist(PATH+"config.mat"))
        [X,C,PR,G,F] = orion_get_config();
        save(strcat(PATH,"config.mat"),'X','C','PR','G','F')
        fprintf("SAVE %s \n",strcat(PATH,"config.mat"))
    else
        load(strcat(PATH,"config.mat"))
        fprintf("LOAD %s \n",strcat(PATH,"config.mat"))
    end
    % ---------------------------------------------------------------------
    for (n=1:N)
        % Génération du répertoire pour la simulation courante  
        reg = fullfile(char(F.path), char(F.folder + "*"));
        nb_folder = numel(dir(reg));
        disp(nb_folder)
        CUR_DIR_FOLDER = F.path+F.folder+num2str(nb_folder+1)+"/"; 
        mkdir(CUR_DIR_FOLDER); 
        fprintf("CREATE %s \n",CUR_DIR_FOLDER)
        % Lancement de la simulation orion en preset_count
        CUR_SEED = C.seed(nb_folder+1); 
        CUR_NB_SIMU = C.N_sim_total;
        %CUR_NB_SIMU = 1000;  % !!!!!!!
        % >>>
        tps_start = tic(); 
        [S] = orion_simu_preset_count(X,C,PR,G,CUR_NB_SIMU,CUR_SEED);
        tps_stop = toc(tps_start); 
        fprintf("ORION: Tps de simulation = %g (s) <=> %g (h)\n",tps_stop,(tps_stop/3600)); 
        % <<<
        CUR_DIR_FOLDER_SAVE_RAW_DATA = strcat(CUR_DIR_FOLDER,F.raw_data);  
        save(strcat(CUR_DIR_FOLDER_SAVE_RAW_DATA,'.mat'),'S','tps_stop','CUR_SEED','-v7.3')
        % Génération de l'interfile 
        orion_generate_interfile_raw_data(C,G,S,CUR_DIR_FOLDER_SAVE_RAW_DATA); 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % [!!!] Trop long considérant CUR_NB_SIMU*8 à affecter dans des voxels ... 
        % Implémentation en .cc des ces fonctions associées. 

        % Génération des cartes de sensibilité
        for (v = 1:length(C.vox_size))
            VOX_SIZE = C.vox_size(v);
            % - Création d'une grille de voxel et affectation des points d'émission
            [SENSI_MAT,GRID] = orion_get_sensitivity_matrix(X,C,G,S,VOX_SIZE); 
            % - Ecriture des fichiers dans le répertoire courant
            CUR_DIR_FOLDER_SAVE_SENSI_MAT = strcat(CUR_DIR_FOLDER,F.sensi_data); 
            orion_generate_sensi_map(C,SENSI_MAT,GRID,CUR_DIR_FOLDER_SAVE_SENSI_MAT);
        end % for (v = 1:length(C.vox_size))         
        %{%}
        test = "";
    end % (n=1:N)
    % ---------------------------------------------------------------------
    
    test = ""; 
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [X,C,PR,G,F] = orion_get_config()
    cs_file = "/home/user/Bureau/THESE/POLLUX/data/nistdata_1_1200_kev_rng.txt";
    neg_rayleigh = true;
    X = orion_init_simulation(cs_file,neg_rayleigh);
    %
    G.rayon = 35; % (mm)
    G.axe_l = 120; % (mm)
    G.vol_quart = (pi*(G.rayon^2)*G.axe_l)/4; 
    % Elements de simulation souhaité
    C.N_class = 6; 
    C.N_sim = 500; % par mm^3
    C.N_sim_total = C.N_sim; %*ceil(G.vol_quart);  
    C.vox_size = [5,1];
    % 
    C.N_max = 1e7;  % Nombre limite de données dans les tableaux avant update de N_max lignes supplémentaires
    C.seed = randi([1000,99999],C.N_max,1);
    C.classif = ["0G","1G511","1G1157","2GLOR","2GMIX","3G"]; 
    C.verb_lim = 10000; % Pour le suivi de la simu dans la console
    C.draw = false; C.verbose = false; 
    %
    PR.active = true; 
    PR.mu = 2.4;
    PR.sigma = 2;
    %  
    F.path = "/home/user/Bureau/THESE/SIMU_ORION/DEBUG_TEST_CC/";
    F.folder = "simu_";
    F.raw_data = "raw_data";    % [hdr,bin] préfixe pour la génération de l'interfile associé au données brutes
    F.sensi_data = "sensi_map_";    % [hdr,img] préfixe pour la génération des cartes de sensibilité
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [X] = orion_init_simulation(cs_file,neg_rayleigh)
    X.O = [0 0 0];          % centre du repère
    X.Rin = 75;             % Rayon intérieur LXe actif (mm)
    X.Rout = 190;           % Rayon extérieur LXe actif (mm)
    X.Rfov = 50;            % Rayon extérieur FOV (mm)
    X.Ax = 240;             % Longueur axiale (mm);
    X.L = X.Rin*cos(pi/4);  % demi longueur atteignable
    % X.nVpL = 0;             % nb Voxel par demi longueur atteignable
    % Dans l'ordre de lecture [Energy, Rayleigh, Compton, Photo Electrique, Att totale with Compton]
    % Remarque : Si tu tentes de vérifier que la somme des trois effets est égale à l'attenuation totale, tu risques d'^etre déçu ... (perte de précision dans les données importées);
    X.rho = 3.057;  % densité volumique de masse [g.cm^-3]
    %nist_data = importdata(cs_file,"\t").data(:,1:4);
    parsed_data = importdata(cs_file,"\t");
    nist_data = parsed_data(:,1:4);
    X.cs_data.Energy = nist_data(:,1);
    nist_data = nist_data(:,2:end);                     % coefficient d'atténuation massique [cm^2.g^-1]
    X.cs_data.mu = X.rho.*nist_data./10;                % ATTENTION : là tu es en [cm^-1], et toute tes données sont en [mm] -> il faut diviser par 10
    % coefficient d'atténuation linéaire [cm^-1]
    %X.cs_data.mu = [X.cs_data.mu, sum(X.cs_data.mu,2)];
    %
    X.cs_data.is_considered = ones(3,1);
    if (neg_rayleigh), X.cs_data.is_considered(1) = 0; end
    %
    X.mec2 = 0.51099895; % (MeV) masse d'un électron au repos en MeV - https://en.wikipedia.org/wiki/Electron_mass#cite_note-physconst-mec2MeV-3
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [S] = orion_simu_preset_count(X,C,PR,G,N_max,seed)
    % Initialisation de la seed pour le compteur courant; 
    rng(seed);
    % Initialisation de la structure de retour; 
    [S] = orion_init_struct(C,seed);
    % Let's Go
    it = 0;
    while (it<N_max)
        it = it + 1;
        if (it > S.cur_N_limit)
            [S] = orion_update_struct(S,C);
        end
        % ---------------------------------------------------------------------
        %%%%%%%%%%%%%%%%%%%%% TIRAGES D'UNE EMISSION %%%%%%%%%%%%%%%%%%%%%%%%%%
        % Simulation d'un point d'émission dans la région étudiée
        r = G.rayon*rand();
        ang = pi/2*rand();
        M = [r*cos(ang),r*sin(ang),G.axe_l*rand()];
        %pts_list = draw_store_pts(pts_list,M,'.','r',0.5);
        % Simulation des vecteurs directionnels pour chaque photons
        D1 = -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
        Dir.D1 = D1/norm(D1,2);
        D2 = -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
        Dir.D2 = D2/norm(D2,2);
        D_pr =  -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
        Dir.D_pr = D_pr/norm(D_pr,2);
        % ---------------------------------------------------------------------
        %%%%%%%%%%%%%%%%%%%%% SIMULATION D'UNE EMISSION %%%%%%%%%%%%%%%%%%%%%%%
        [E0,E0_prime,~,~] = init_photon_energy_count("3");
        % >>>>>>>>>>>>>>>>>>>>> Photon à 1.157 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        [t1_min_p_f,t1_min_p_b] = get_intersect_coord_Rin(X,M,Dir.D1);
        T_min_p = [t1_min_p_f,t1_min_p_b];
        scal_vec = zeros(size(T_min_p));
        V_unit_dir = M + Dir.D1;
        MV_nrm_unit_dir = (V_unit_dir-M)/norm(V_unit_dir-M,2);
        if (~isinf(t1_min_p_f))
            V_t1_min_p_f = M + t1_min_p_f*Dir.D1;
            MV_nrm_min_p_f = (V_t1_min_p_f - M)/norm(V_t1_min_p_f - M,2);
            scal_vec(1) = dot(MV_nrm_unit_dir,MV_nrm_min_p_f);
        end
        if (~isinf(t1_min_p_b))
            V_t1_min_p_b = M + t1_min_p_b*Dir.D1;
            MV_nrm_min_p_b = (V_t1_min_p_b - M)/norm(V_t1_min_p_b - M,2);
            scal_vec(2) = dot(MV_nrm_unit_dir,MV_nrm_min_p_b);
        end
        [~,idx_t_min] = max(scal_vec);
        t1_min_p = T_min_p(idx_t_min);
        track_1157 = orion_init_tracker();
        if (~isinf(t1_min_p))
            [track_1157] = photon_tracker(X,C,track_1157,E0_prime,M,Dir.D1,t1_min_p);
        end
        % >>>>>>>>>>>>>>>>>>>>> Photon à 0.511 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        [M_pr,~] = positron_range(M,Dir.D_pr,PR);
        [t_min_f,t_min_b] = get_intersect_coord_Rin(X,M_pr,Dir.D2);  % b : back, f : forward
        % forward
        track_511_f = orion_init_tracker();
        if (~isinf(t_min_f))
            [track_511_f] = photon_tracker(X,C,track_511_f,E0,M_pr,Dir.D2,t_min_f);
        end
        % backward
        [track_511_b] = orion_init_tracker();
        if (~isinf(t_min_b))
            [track_511_b] = photon_tracker(X,C,track_511_b,E0,M_pr,Dir.D2,t_min_b);
        end
        % ---------------------------------------------------------------------
        %%%%%%%%%%%%%%%%%%%%% TRAITEMENT DE LA DETECTION %%%%%%%%%%%%%%%%%%%%%%
        [config] = orion_process_event(track_1157,track_511_f,track_511_b);
        S.chr(it,:) = config.chr; 
        S.data(it,:) = M; 
        S.N_exp = S.N_exp + 1 ; 
        S.count(config.idx) = S.count(config.idx) + 1; 
        % ---------------------------------------------------------------------
        %%%%%%%%%%%%%%%%%%%%% SUIVI SIMU DANS LA CONSOLE %%%%%%%%%%%%%%%%%%%%%%
        if (mod(it,C.verb_lim) == 0 )
            info.n = it;
            info.N_allG =S.count;
            info.pure_evt_idx = 6;
            orion_print_one_simu(C,info);
        end
        test = "";
    end
    [S] = orion_plane_struct(S);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = orion_print_one_simu(C,info)   
    clc; 
    prop = info.N_allG./sum(info.N_allG)*100; 
    fprintf("it = %d/%d\n",info.n,C.N_sim_total);
    fprintf('%s\t',C.classif'); fprintf("\n")
    fmt = repmat('%.2f\t', 1, length(prop));
    fprintf(fmt,prop); fprintf(" %%\n")
    test = ""; 
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [track] = orion_init_tracker()
    track.N = 0;
    track.inter_pts = [];
    track.dep_energ = [];
    track.effect_id = [];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [S] = orion_init_struct(C,seed)
    S.N_exp = 0; 
    S.cur_N_limit = C.N_max;
    S.chr = strings(C.N_max,1);          % TODO Voir pour potentiel pb à la conversion
    S.data = zeros(C.N_max,3,'double');
    S.count = zeros(1,6); 
    S.seed = seed; 
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [S] = orion_update_struct(S,C)
    Es1 = strings(C.N_max,1); 
    Ez3 = zeros(C.N_max,3,'double'); 
    S.cur_N_limit = S.cur_N_limit + C.N_max; 
    S.chr = [S.chr;Es1]; 
    S.data = [S.data;Ez3]; 
    test = "";
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [S] = orion_plane_struct(S)
    bound = S.N_exp + 1;
    S.chr(bound:end,:) = []; 
    S.data(bound:end,:) = []; 
    test = ""; 
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [config] = orion_process_event(T1157,T511f,T511b)
% Rappel: Rayleigh_id = 1; Photo_id = 2; Compton_id = 3;
% INCOMPATIBLE AVEC UN AJOUT FUTUR DE L'EFFET RAYLEIGH
    LOR = (T511f.N >= 1 && T511b.N >= 1);
    G1157 = (T1157.N>=2 ); % && sum(T1157.effect_id(1:2))>=5
    G511f = (~LOR && T511f.N>=2 ); % && sum(T511f.effect_id(1:2))>=5
    G511b = (~LOR && T511b.N>=2 ); % && sum(T511b.effect_id(1:2))>=5
    
    if (LOR)
        % Config initiale
        config.idx=4;
        config.chr='3';
        config.label="2GLOR";
        if (G1157)
            % Update de la config et des données associées
            config.idx=6;
            config.chr='5';
            config.label="3G";
        end
        % ---------------------------------------------------------------------
    elseif (G511f)
        % Config initiale
        config.idx=2;
        config.chr='1';
        config.label="1G511_f";
        if (G1157)
            % Update de la config et des données associées
            config.idx=5;
            config.chr='4';
            config.label="2GMIX_f";
        end
        % ---------------------------------------------------------------------
    elseif (G511b)
        % Config initiale
        config.idx=2;
        config.chr='1';
        config.label="1G511_b";
        if (G1157)
            % Update de la config et des données associées
            config.idx=5;
            config.chr='4';
            config.label="2GMIX_b";
        end
        % ---------------------------------------------------------------------
    elseif (G1157)
        config.idx=3;
        config.chr='2';
        config.label = "1G1157";
        % -------------------------------------------------------------
    else
        config.idx=1;
        config.chr='0';
        config.label = "0G";
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = orion_generate_interfile_raw_data(C,G,S,CUR_DIR_FOLDER_SAVE_RAW_DATA)
    % Génération du fichier header des données brutes
    ext = ".hdr";
    name = strcat(CUR_DIR_FOLDER_SAVE_RAW_DATA,ext);
    fileID = fopen(name,'w');
    fprintf(fileID,"!INTERFILE  :=\n");
    fprintf(fileID,"name of data file := raw_data.bin\n\n");
    fprintf(fileID,"!GENERAL DATA :=\n");
    fprintf(fileID,"cylinder radius (mm) := %d\n",G.rayon);
    fprintf(fileID,"cylinder length (mm) := %d\n",G.axe_l);
    fprintf(fileID,"emission volume (mm^3) := %g\n\n",G.vol_quart);
    fprintf(fileID,"!SIMULATION DATA :=\n");
    fprintf(fileID,"seed := %d\n",S.seed);
    fprintf(fileID,"number of emissions := %d\n",S.N_exp);
    for (i=1:length(S.count))
    fprintf(fileID,"count class %d := %d\n",(i-1),S.count(i));
    end
    fprintf(fileID,"\n");
    fprintf(fileID,"!DATA FORMAT :=\n");
    fprintf(fileID,"data byte order := LITTLEENDIAN\n");
    fprintf(fileID,"number of dimensions := 4\n");
    fprintf(fileID,"vector label [1] := c\n");
    fprintf(fileID,"vector type [1] := uint16\n");
    fprintf(fileID,"vector label [2] := x\n");
    fprintf(fileID,"vector type [2] := float32\n");
    fprintf(fileID,"vector label [3] := y\n");
    fprintf(fileID,"vector type [3] := float32\n");
    fprintf(fileID,"vector label [4] := z\n");
    fprintf(fileID,"vector type [4] := float32\n\n");
    % EXTRA INFO
    fprintf(fileID,"!COMMENT :=\n");
    str=""; for (c=1:C.N_class), tok = ","; if (c==C.N_class), tok = ";"; end ;str=str+C.classif(c)+"("+num2str(c-1)+")"+tok+" ";end
    fprintf(fileID,"Class indices correspond to:\n"); 
    fprintf(fileID,"%s\n",str);
    fprintf(fileID,"Events are stored in list mode; for each event:\n");
    fprintf(fileID,"- c is the class associated to the event;\n");
    fprintf(fileID,"- (x,y,z) are the Cartesian coordinates of the emission point.\n");
    fprintf(fileID,"!END OF INTERFILE :=\n");
    fclose(fileID);
    fprintf("SAVE %s \n",name);
    % Génération du fichier bin des données brutes
    ext = ".bin";
    name = strcat(CUR_DIR_FOLDER_SAVE_RAW_DATA,ext);
    fileID = fopen(name,'w','l');
    for (i=1:S.N_exp)
        % [c(uint16),x(float32),y(float32),z(float32)]
        c = str2double(S.chr(i));  
        fwrite(fileID,uint16(c),"uint16");
        M = S.data(i,:);
        fwrite(fileID,M,"float32");
    end
    fclose(fileID);
    fprintf("SAVE %s \n",name);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [SENSI_MAT,GRID] = orion_get_sensitivity_matrix(X,C,G,S,V)
    % Création de la grille de voxel de dimension V^3 
    GRID.V = V*ones(1,3);
    GRID.D = [(2*G.rayon)/GRID.V(1),(2*G.rayon)/GRID.V(2),X.Ax/GRID.V(3)];
    GRID.T = GRID.D .* GRID.V;
    GRID.J = prod(GRID.D);
    GRID.Rc = zeros(GRID.J,3);  % Coordonnées : Réelles (Rc)
    GRID.Ic = zeros(GRID.J,3);  % Coordonnées : Indicielles (Ic)
    for j=1:GRID.J
        [a,b,c] = get_axes_voxel_idx(GRID,j);
        [r_a,r_b,r_c] = get_axes_voxel_coord(GRID,a,b,c);
        GRID.Rc(j,:) = [r_a,r_b,r_c];
        GRID.Ic(j,:) = [a,b,c];
    end
    % ---------------------------------------------------------------------
    % Création de la structure qui va contenir les counts réalisés sur les 
    % points d'émission 
    SENSI_MAT = zeros(GRID.J,C.N_class);
    % ---------------------------------------------------------------------
    % Permutation des coordonnées par symétries
    perm_sign = [ 1,  1,  1; -1,  1,  1; 1, -1,  1; 1,  1, -1; -1, -1,  1; -1,  1, -1; 1, -1, -1; -1, -1, -1];
    Nb_q = size(perm_sign,1);    % Nombre de quadrants pour réaffecter les points  
    % Pour le calcul de la sensibilité - facteur de normalisation après affectation au 8 quadrants 
    TOTAL_EMISS_COUNT = Nb_q * S.N_exp;     
    % Parcours des points de S et remplissage de la grille;
    for (n = 1:S.N_exp)
        P = S.data(n,:);
        c = str2double(S.chr(n));
        %fprintf("P = (%g,%g,%g) \t c = %d \n",P(1),P(2),P(3),c)
        sym_P = perm_sign.*P;
        for (s = 1:Nb_q)
            Ps = sym_P(s,:);
            ix = floor((Ps(1)+.5*GRID.T(1))/GRID.V(1))+1;
            iy = floor((Ps(2)+.5*GRID.T(2))/GRID.V(2))+1;
            iz = floor((Ps(3)+.5*GRID.T(3))/GRID.V(3))+1;
            vox_j = find(GRID.Ic(:,1)==ix & GRID.Ic(:,2)==iy & GRID.Ic(:,3)==iz);
            [R] = get_corresponding_point(GRID,vox_j);
            is_inside = (R(1) <= Ps(1) && Ps(1) <= R(1) + GRID.V(1)) ...
                        &&   (R(2) <= Ps(2) && Ps(2) <= R(2) + GRID.V(2)) ...
                        && (R(3) <= Ps(3) && Ps(3) <= R(3) + GRID.V(3));
            if (~is_inside)
                disp(n+"-"+s)
                break
            end
            
            %fprintf("Voxel = %d \n",vox_j)
            SENSI_MAT(vox_j,(c+1)) = SENSI_MAT(vox_j,(c+1)) + 1; 
            
        end % (s = 1:Nb_q)
        test = ""; 
        if (mod(n,10000)==0)
            fprintf("n*Nb_q = %d <=> %g %% \n",n*Nb_q,(n*Nb_q)*100/TOTAL_EMISS_COUNT)
        end
    end %  (n = 1:S.N_exp)
    % Normalisation de la matrice système 
    SENSI_MAT = SENSI_MAT./TOTAL_EMISS_COUNT; 
    fprintf("GENERATE: sensitivity matrix for v = %d (mm)\n",V);
    test = "";
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] =orion_generate_sensi_map(C,SENSI_MAT,GRID,CUR_DIR_FOLDER_SAVE_SENSI_MAT)
    for (c=1:C.N_class)
        save_name = strcat(CUR_DIR_FOLDER_SAVE_SENSI_MAT,"c",num2str(c-1),"_v",num2str(GRID.V(1)));
        % Génération de la carte de sensibilité (img)
        S_vec = SENSI_MAT(:,c); 
        ext = ".img";
        name = strcat(save_name,ext);
        fileID = fopen(name,'w','l');
        for (j=1:GRID.J)
            d = S_vec(j);
            fwrite(fileID,d,"float32");
        end
        fclose(fileID);
        fprintf("SAVE %s \n",name);
        % Génération du header associé à la carte de sensibilité (hdr)
        sc_name = split(save_name,'/'); sc_name = sc_name(end);
        ext = ".hdr";
        name = strcat(save_name,ext);
        fileID = fopen(name,'w');
        fprintf(fileID,"!INTERFILE := \n");
        fprintf(fileID,"!imaging modality := XEMIS\n");
        fprintf(fileID,"!version of keys := CASToRv3.1\n");
        fprintf(fileID,"CASToR version := 3.1\n\n");
        fprintf(fileID,"!GENERAL DATA :=\n");
        fprintf(fileID,"!originating system := XEMIS2\n");
        fprintf(fileID,"!data offset in bytes := 0\n");
        fprintf(fileID,"!name of data file := %s.img\n",sc_name);
        fprintf(fileID,"patient name := %s\n\n",sc_name);
        fprintf(fileID,"!GENERAL IMAGE DATA\n");
        fprintf(fileID,"!type of data := Dynamic\n");
        fprintf(fileID,"!total number of images := %d\n",GRID.D(3));
        fprintf(fileID,"imagedata byte order := LITTLEENDIAN\n");
        fprintf(fileID,"!number of frame groups :=1\n");
        fprintf(fileID,"process status :=\n\n");
        fprintf(fileID,"!STATIC STUDY (General) :=\n");
        fprintf(fileID,"number of dimensions := 3\n");
        fprintf(fileID,"!matrix size [1] := %d\n",GRID.D(1));
        fprintf(fileID,"!matrix size [2] := %d\n",GRID.D(2));
        fprintf(fileID,"!matrix size [3] := %d\n",GRID.D(3));
        fprintf(fileID,"!number format := float\n");
        fprintf(fileID,"!number of bytes per pixel := 4\n");
        fprintf(fileID,"scaling factor (mm/pixel) [1] := %d\n",GRID.V(1));
        fprintf(fileID,"scaling factor (mm/pixel) [2] := %d\n",GRID.V(2));
        fprintf(fileID,"scaling factor (mm/pixel) [3] := %d\n",GRID.V(3));
        fprintf(fileID,"first pixel offset (mm) [1] := 0\n");
        fprintf(fileID,"first pixel offset (mm) [2] := 0\n");
        fprintf(fileID,"first pixel offset (mm) [3] := 0\n");
        fprintf(fileID,"data rescale offset := 0\n");
        fprintf(fileID,"data rescale slope := 1\n");
        fprintf(fileID,"quantification units := 1\n");
        fprintf(fileID,"!number of images in this frame group := 1\n");
        fprintf(fileID,"!image duration (sec) := 1\n");
        fprintf(fileID,"!image start time (sec) := 0\n");
        fprintf(fileID,"pause between frame groups (sec) := 0\n\n");
        fprintf(fileID,"!COMMENT := \n");
        fprintf(fileID,"Sensitivity map for the %s class.\n",C.classif(c));
        fprintf(fileID,"!END OF INTERFILE :=\n");
        fclose(fileID);
        fprintf("SAVE %s \n",name);
    end
    test = ""; 
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





