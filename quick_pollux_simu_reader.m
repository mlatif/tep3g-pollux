clear all; 
close all; 
set(0,'DefaultFigureWindowStyle','docked');  
addpath("methods/")
%{
load("cortex_simu/pollux_cortex_config.mat");
load("cortex_simu/3G_read")
%}
clc; 
path = "res_simu/";
name = "pollux_tracker_cortex_sim_100000_3G_PR"; 
%name = "pollux_cortex_sim_10000_3G"; 
load(path+name); 
NAME_FIG = "HIST_"+name; 



print_phantom_info(P,F,S);

class_list = 1:6; 
cur_class = 6; 

occ_config_count = S.results.occ_config_count(class_list); 

data = occ_config_count/sum(occ_config_count)*100; 
lab = ["0\gamma","1\gamma_{COR}^{511}","1\gamma_{COR}^{1157}","2\gamma_{LOR}","2\gamma_{COR}","3\gamma"];
th_class_idx = (1:length(data)-1);
sub_data = data;
sub_lab = lab(class_list);
sub_cur_class_idx = find(class_list==cur_class);
bar_cur_class_idx = find(class_list~=cur_class);
figure; 
hold on; %box on;
set(gca,'color','none')
idx = categorical(1:length(sub_data));
b = bar(idx,sub_data);
b.FaceColor = 'flat';
b.EdgeColor = 'none';

COLOR_LIB = [
    0.7500, 0.7500, 0.7500;          % 0G;
    0.6350, 0.0780, 0.1840;     % 1G511
    0.9290, 0.6940, 0.1250;     % 1G1157
    0.4940, 0.1840, 0.5560;     % 2GLOR
    0.3010, 0.7450, 0.9330;     % 2GMIX
    0.4660, 0.6740, 0.1880;     % 3G
    0.8500, 0.3250, 0.0980;     % AUTRE
    ];

%        b.CData(sub_cur_class_idx,:) = [0.5 0 0.5];
for (idx=1:6)
    b.CData(idx,:) = COLOR_LIB(idx,:);
end



%         for (i = 1:length(bar_cur_class_idx))
%             b.CData(bar_cur_class_idx(i),:) = [0 0 1];
%         end
set(gca,'xticklabel',sub_lab,'FontSize',15);
%ylim([0,max(data)])

xtips2 = b(1).XEndPoints;
ytips2 = b(1).YEndPoints;
labels2 = string(b(1).YData);
%%%
% Initialisation d'un tableau pour stocker les valeurs formatées
labels2_formatted = cell(size(labels2));

% Parcours de labels2 et formatage des valeurs avec deux chiffres après la virgule
for i = 1:numel(labels2)
    labels2_formatted{i} = sprintf('%.2f', str2double(labels2{i}));
end

% Affichage des valeurs formatées
%%%
ylabel("\% of detections",'Interpreter','latex','FontSize',18)
text(xtips2,ytips2,labels2_formatted,'HorizontalAlignment','center','VerticalAlignment','bottom','Interpreter','latex','FontSize',18);

NAME_FIG_H = NAME_FIG+"1-6";
saveas(gcf,strcat(NAME_FIG_H,'.eps'),'epsc');    


C = S.results.occ_config_count(class_list); 

list = 2:6; 
COLOR_LIB(1,:) = []; 
lab(1) = []; 
occ_config_count = C(:,list);
data = occ_config_count./sum(occ_config_count)*100; 
figure; 
    set(gca,'color','none')
    %set(gca,'TickLabelInterpreter','Latex');
    hold on; %box on;
    idx = categorical(1:length(data));
    b = bar(idx,data);
    b.FaceColor = 'flat';
    b.EdgeColor = 'none';
    for (idx=1:length(list))
        b.CData(idx,:) = COLOR_LIB(idx,:);
    end    
    xtips2 = b(1).XEndPoints;
    ytips2 = b(1).YEndPoints;
    labels2 = string(b(1).YData);
    labels2_formatted = cell(size(labels2));
    for i = 1:numel(labels2)
        labels2_formatted{i} = sprintf('%.2f', str2double(labels2{i}));
    end
    ylabel("\% of detections",'Interpreter','latex','FontSize',18)
    set(gca,'xticklabel',lab,'FontSize',15);
    text(xtips2,ytips2,labels2_formatted,'HorizontalAlignment','center','VerticalAlignment','bottom','Interpreter','latex','FontSize',18);

NAME_FIG_H = NAME_FIG+"2-6";
saveas(gcf,strcat(NAME_FIG_H,'.eps'),'epsc');    

