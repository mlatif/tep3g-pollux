#!/bin/bash
# TODO pour comprendre
#   Aller ici
#   - https://physics.nist.gov/cgi-bin/Xcom/xcom2-t
#   Ouvrir l'inspecteur de fen^etre HTML en parallèle
#   Dans le formulaire :  XCOM: Element Options
#     - Saisir les données (eg. Atomic Symbol = Xe OU Atomic Number = 54)
#     - Saisir une énergie bidon mais valide exprimée en MeV (eg. 0.001)
#     - Décocher "Include Standard Grid"
#     => Cliquer sur Submit Information
#   En parallèle dans l'inspecteur de commande
#     - Aller dans l'onglet NETWORK
#     - Regarder le nom des requ^etes qui ont été lancées
#     - Cliquer sur xcom3_1_t pour inspecter
#     - Faire clic droit puis Copy/Copy as cURL
#   On doit obtenit un truc de ce type
#     curl 'https://physics.nist.gov/cgi-bin/Xcom/xcom3_1-t' \
#       -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
#       -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
#       -H 'Cache-Control: max-age=0' \
#       -H 'Connection: keep-alive' \
#       -H 'Content-Type: application/x-www-form-urlencoded' \
#       -H 'Cookie: _ga=GA1.3.1013593307.1655716964; nmstat=b198eb88-d79d-764a-85c0-999ef69d0c67; _gid=GA1.3.204321771.1671443318; _gid=GA1.2.294224029.1671555720; _ga=GA1.2.1013593307.1655716964; _ga_HEQ0YF2VYL=GS1.1.1671555720.13.1.1671555737.0.0.0; _gat_GSA_ENOR0=1; _gat_GSA_ENOR1=1' \
#       -H 'Origin: https://physics.nist.gov' \
#       -H 'Referer: https://physics.nist.gov/cgi-bin/Xcom/xcom2-t' \
#       -H 'Sec-Fetch-Dest: document' \
#       -H 'Sec-Fetch-Mode: navigate' \
#       -H 'Sec-Fetch-Site: same-origin' \
#       -H 'Sec-Fetch-User: ?1' \
#       -H 'Upgrade-Insecure-Requests: 1' \
#       -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36' \
#       -H 'sec-ch-ua: "Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"' \
#       -H 'sec-ch-ua-mobile: ?0' \
#       -H 'sec-ch-ua-platform: "Linux"' \
#       --data-raw 'ZNum=54&ZSym=&Energies=0.001&OutOpt=PIC' \
#       --compressed
#
# https://askubuntu.com/questions/883120/length-of-string-separated-by-spaces
# https://www.tutorialkart.com/bash-shell-scripting/bash-array-slice/#:~:text=To%20slice%20an%20array%20in,index%2C%20use%20the%20following%20syntax.&text=The%20array%20variable%2C%20which%20we%20would%20like%20to%20slice.&text=The%20starting%20index%20of%20slice.&text=The%20ending%20index%20of%20slice.
# https://stackoverflow.com/questions/54419168/bash-how-to-separate-array-elements-with-tabs-and-print-on-the-same-line
HEADER="PHOTON SCATTERING PHOTO- PAIR PRODUCTION TOTAL ATTENUATION ENERGY COHERENT INCOHER. ELECTRIC IN IN WITH WITHOUT ABSORPTION NUCLEAR ELECTRON COHERENT COHERENT FIELD FIELD SCATT. SCATT. (MeV) (cm2/g) (cm2/g) (cm2/g) (cm2/g) (cm2/g) (cm2/g) (cm2/g) ";
#
MIN=1;
MAX=1200;
D=1000;
#
FILENAME="nistdata_${MIN}_${MAX}_kev_rng.txt"
rm $FILENAME
touch "$FILENAME"
echo -e "MIN=$MIN\tMAX=$MAX"
for i in $(seq $MIN 1 $MAX); do
  ##############################################################################
  VAL=$(echo "scale=3 ; $i/$D" |bc -l);
  if [ $i -lt $D ]; then
      VAL="0"$VAL;
  fi
  E="$VAL"
  ##############################################################################
  DATA="ZNum=54&ZSym=&Energies=$E&OutOpt=PIC";
  #echo -e $DATA
  #echo " --- "
  ##############################################################################
  OUTPUT=$(curl 'https://physics.nist.gov/cgi-bin/Xcom/xcom3_1-t' --data-raw $DATA);
  # echo -e $OUTPUT
  # echo -e "---\nREMOVE PREFIX"
  OUTPUT=${OUTPUT#*"<pre>"}
  # echo -e $OUTPUT
  # echo " ---\nREMOVE SUFFIX"
  OUTPUT=${OUTPUT%"</pre>"*}
  #echo -e $OUTPUT;
  ##############################################################################
  arr_1=($HEADER);
  arr_2=($OUTPUT);
  # echo -e "ARR[HEADER] = ${#arr_1[@]} \t ARR[OTHER] = ${#arr_2[@]}";
  DATA=${arr_2[@]:${#arr_1[@]}:${#arr_2[@]}}
  echo -e "[$i] \t $DATA"
  echo -e "$DATA" | tr " " "\t " >> $FILENAME;
done
