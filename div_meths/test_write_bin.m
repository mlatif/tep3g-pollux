% Source : https://fr.mathworks.com/matlabcentral/answers/1679274-how-to-output-multiple-types-of-binary-files-in-one-file?s_tid=ans_lp_feed_leaf
clear all;
close all;
x = [
 1  7  13
 2  8  14
 3  9  15
 4 10  16
 5 11  17
 6 12  18];
fid = fopen("test.bin", "w+", "ieee-le");
for i=1:size(x, 1)
    % 1(uint32) 7(float32) 13(uint32)
    fwrite(fid, x(i, 1), "uint32");
    fwrite(fid, x(i, 2), "float32");
    fwrite(fid, x(i, 3), "uint32");

    disp(i)

end
fclose(fid);
