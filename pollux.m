function [S] = pollux(C,X,P)
    %   Remarque : tout ce qui est relatif au photon 1.157 est marqué par un "prime"
    %   Pour la CSR : V1p, V2p, E1a, E2a
    %   Pour la LOR : V1,V2 si LOR détectée
    %                 V1, (V1s,V2s,E1b,E2b) si DEMI-LOR avec V1 détecté
    %                 V2, (V1s,V2s,E1b,E2b) si DEMI-LOR avec V2 detecté
    % MANTRA : P = E + t * V  
    % => On peut tout calculer à partir de t (puisque l'on connait E et V)
    % => On ne s'embête plus à maintenir simultanément des retour de point et de coefficient t
    % ---------------------------------------------------------------------
    [S] = pollux_init_struct(C,P);     
    [C,verb_lim,ANIM] = draw_conditions(C);
    [pure_evt_idx] = get_pure_event_idx(C);
    % ---------------------------------------------------------------------
    if (C.draw)
        set(0,'DefaultFigureWindowStyle','docked') 
        draw_xemis(X,P,ANIM);
        if (ANIM), [frames] = manage_anim(C,1);    end
    end
    % ---------------------------------------------------------------------
    terminal = false;
    n = 0;
    while (~terminal)
%        clc; 
        n = n + 1;       
        if (n >= S.cur_N_limit)
            [S] = update_S_struct(S);
        end
        pts_list = [];     % Liste des points à conserver (émission, interaction) 
        sketch_list = [];
        % ---
        [n_gamma_511,n_gamma_1157] = reset_detection();
        [V0f,V0b,V1,V2,V0p,V1p,V2p,V0s,V1s,V2s,E1a,E2a] = reset_interaction_point();
        [E0,E0_prime,P_gamma_511,P_gamma_1157] = init_photon_energy_count(C.pollux_case);
        % ---
        [S,M,Dir,info] = pollux_simulation_emission(P,C,S,n);        
        switch C.pollux_case
            % ---------------------------------------------------------------------
            % ---------------------------------------------------------------------
            
            % ---------------------------------------------------------------------
            % ---------------------------------------------------------------------
            case "1"     
                t1_min_p_f = inf; t1_min_p_b = inf; t1_max_p = inf; t_V1p = inf; t2_min_p = inf; t2_max_p = inf; t_V2p = inf; 
                csr_gen_dir_p = inf(3,1); 
                nrm_MV_min = zeros(1,2);
                R_pr = inf;
                % >>>>> 
                succ_csr_p = false; 
                % SIMULATION
                [t1_min_p_f,t1_min_p_b] = get_intersect_coord_Rin(X,M,Dir.D1);

                % PATCH CHOIX DE t1_min_p; 
                T_min_p = [t1_min_p_f,t1_min_p_b]; 
                scal_vec = zeros(size(T_min_p));
                V_unit_dir = M + Dir.D1;
                MV_nrm_unit_dir = (V_unit_dir-M)/norm(V_unit_dir-M,2);  
                % Pour la première composante t retournée par get_intersect_coord_Rin
                if (~isinf(t1_min_p_f))
                    V_t1_min_p_f = M + t1_min_p_f*Dir.D1;
                    MV_nrm_min_p_f = (V_t1_min_p_f - M)/norm(V_t1_min_p_f - M,2); 
                    scal_vec(1) = dot(MV_nrm_unit_dir,MV_nrm_min_p_f);
                    if (C.draw), sketch_list = draw_store_pts(sketch_list,V_t1_min_p_f,'^','m'); end
                end
                % Pour la seconde composante t retournée par get_intersect_coord_Rin
                if (~isinf(t1_min_p_b))
                    V_t1_min_p_b = M + t1_min_p_b*Dir.D1;
                    MV_nrm_min_p_b = (V_t1_min_p_b - M)/norm(V_t1_min_p_b - M,2); 
                    scal_vec(2) = dot(MV_nrm_unit_dir,MV_nrm_min_p_b);
                    if (C.draw), sketch_list = draw_store_pts(sketch_list,V_t1_min_p_b,'v','c'); end
                end
                [~,idx_t_min] = max(scal_vec); 
                t1_min_p = T_min_p(idx_t_min);
                if (C.verbose)
                    ast_idx = ["",""]; 
                    ast_idx(idx_t_min) = "(*)"; 
                    fprintf("[INIT_CSR_1157]\t t1_min_p_f = %g %s \t t1_min_p_b = %g %s\n",t1_min_p_f,ast_idx(1),t1_min_p_b,ast_idx(2));
                end
                if (~isinf(t1_min_p))
                    % Point d'entrée dans la zone active
                    V0p = get_point(M,Dir.D1,t1_min_p);
                    % Calcul du premier point d'intraction
                    [t1_max_p] = get_intersect_coord_Rout(X,M,Dir.D1,t1_min_p);
                    [t_V1p] = distance_xenon_attenuation(X,E0_prime,Dir.D1,t1_max_p);
                    V1p = get_point(V0p,Dir.D1,t_V1p);
                    msg_1 = ""; if (C.verbose), msg_1 = "[CSR init 1157 V1p]"; end 
                    if (is_in_FOD(X,V1p,msg_1))
                        msg_2 = "" ; if (C.verbose), msg_2 = "1157"; end 
                        [succ_csr_p,V0p,V1p,V2p,E1a,E2a,csr_gen_dir_p,trial_rm_kn_p] = create_csr_generatrix(X,M,Dir.D1,t1_min_p,t_V1p,E0_prime,msg_2);
                        S.results.cpt_rm_kn(n,1) = trial_rm_kn_p;
                    else
                        succ_csr_p = false;
                        t_V1p = inf; 
                        V1p = inf(3,1);
                    end
                end
                %
                if(C.verbose)
                    fprintf("[POST_CSR_1157] \tsucc_csr_b = %d \n",succ_csr_p);
                    fprintf("[SYNTH|V] naii(V1p) = %d\t naii(V2p) = %d\n",naii(V1p),naii(V2p));
                    fprintf("[SYNTH|E] E0 = %g\t E1b = %g\t E2b = %g \n",E0_prime,E1a,E2a); 
                end
                % <<<<< 
                % INFERENCE
                n_gamma_511 = 0;
                n_gamma_511_diff = 0;
                n_gamma_1157_diff = length(find([naii(V1p),naii(V2p)])); 
                [config,inf_msg] = get_type_detected_event(n_gamma_511,n_gamma_511_diff,n_gamma_1157_diff);
                % STOCKAGE
                S.export.chr(n)= config.chr;
                S.export.data(n,S.rng.V1p_rng) = V1p;
                S.export.data(n,S.rng.E1a_rng) = E1a;
                S.export.data(n,S.rng.V2p_rng) = V2p;
                S.export.data(n,S.rng.E2a_rng) = E2a;
                S.nb_detection = S.nb_detection + 1; 
                %
                
                % DESSIN
                if (C.draw)
                    pts_list = draw_store_pts(pts_list,M,'x','r',4);
                    if (succ_csr_p)
                        sketch_list = draw_store_pts(sketch_list,V0p,'.','k');    % Point d'éntrée dans la zone active
                        sketch_list = draw_store_pts(sketch_list, get_point(M,Dir.D1,t1_min_p_b),'.','m');    % Point d'éntrée dans la zone active
                        i1 = get_point(M,Dir.D1,t1_max_p);                 
                        % Axe du c^one
                        sketch_list = draw_store_lin(sketch_list,M,i1,'#808080',"--",1);   
                        t2_hat = get_intersect_coord_csr(X,V1p,csr_gen_dir_p);
                        % génératrice 
                        i2 = get_point(V1p,csr_gen_dir_p,t2_hat);
                        sketch_list = draw_store_lin(sketch_list,V1p,i2,'#808080',"--",1);  
                        % interaction avec diffusion
                        pts_list = draw_store_pts(pts_list,V1p,'*',config.part_color,8);
                        pts_list = draw_store_pts(pts_list,V2p,'*',config.part_color,8);
                    end      
                end

                
                    test="";                
                    
                    %{
                    rho_max = get_rho_max(beta_fixe,t2_max)
                    V2_max = V1p + rho_max*Dir.D1;
                    
                    sketch_list = draw_store_pts(sketch_list,V2_max,'s','b');
                [sketch_list] = draw_store_csr(sketch_list,X,M,V1p,beta_fixe,t_V1p,n);
                    %}
                    
            % ---------------------------------------------------------------------
            % ---------------------------------------------------------------------
            % ---------------------------------------------------------------------
            % ---------------------------------------------------------------------
            case "2"
                
                [M,R_pr] = positron_range(M,Dir.D_pr,S.Conf.Pos_rng);
                % >>>>>
                t_max_f = inf; t_max_b = inf; 
                t2_min = inf ; t2_max =inf; 
                t_V1_f = inf; t_V1_b = inf; 
                csr_gen_dir_s = inf(3,1); 
                csr_list = [];
                
                succ_half_lor_f = false;
                succ_half_lor_b = false; 
                succ_csr_f = false; 
                succ_csr_b = false;
            

                
                % 
                [t_min_f,t_min_b] = get_intersect_coord_Rin(X,M,Dir.D2);  % b : back, f : forward
                if (C.verbose)
                    fprintf("[INIT - %d] \tt_min_f = %g \t t_min_b = %g \n",n,t_min_f,t_min_b);
                end
                % ---------------------------------------------------------
                % ---------------------------------------------------------
                % PARTIE 1 - Tenter la LOR
                % => D'abord vers l'avant t>0 (f); 
                if (~isinf(t_min_f)) 
                    msg_1 = ""; if (C.verbose), msg_1 = "[LOR-V1-f]"; end 
                    [succ_half_lor_f,V0f,V1,E1b,t_V1_f,t_min_f,t_max_f] = create_half_lor(X,M,Dir.D2,t_min_f,E0,msg_1);
                end
                % => Puis vers l'arrière t<0 (b)
                if (~isinf(t_min_b))
                    msg_2 = ""; if (C.verbose), msg_2 = "[LOR-V2-b]"; end
                    [succ_half_lor_b,V0b,V2,E1b,t_V1_b,t_min_b,t_max_b] = create_half_lor(X,M,Dir.D2,t_min_b,E0,msg_2); 
                end
                if (C.verbose)
                    fprintf("[POST_LOR] \tsucc_half_lor_f = %d \tsucc_half_lor_b = %d \n",succ_half_lor_f,succ_half_lor_b);
                end
                % ---------------------------------------------------------
                % ---------------------------------------------------------
                if (~succ_half_lor_f || ~succ_half_lor_b), V1 = inf(3,1); V2 = inf(3,1);  end
                % PARTIE 2 - Si pas de LOR (succ_f=false ou succ_b = false)
                %          - On ré-init les points 
                %          - On tente de créer une CSR dans le sens du rayon possible.

                tok_dir_511 = ""; 
                if(succ_half_lor_f && ~succ_half_lor_b)
                    % Dans ce cas, on a déjà calculé t_min_f et t_max_f => On a pas besoin de recalculer cette donnée 
                    msg_3 = ""; if (C.verbose), msg_3 = "511-f"; end
                    [succ_csr_f,V0f,V1s,V2s,E1b,E2b,csr_gen_dir_s,trial_rm_kn_f] = create_csr_generatrix(X,M,Dir.D2,t_min_f,t_V1_f,E0,msg_3);
                    if (C.verbose),fprintf("[POST_CSR_F] \tsucc_csr_f = %d \n",succ_csr_f); end
                    S.results.cpt_rm_kn(n,2) = trial_rm_kn_f;
                    tok_dir_511 = "f"; 
                elseif(~succ_half_lor_f && succ_half_lor_b)
                    % Dans ce cas, on a déjà calculé t_min_b et t_max_b => On a pas besoin de recalculer cette donnée 
                    msg_4 = ""; if (C.verbose), msg_4 = "511-b"; end
                    [succ_csr_b,V0b,V1s,V2s,E1b,E2b,csr_gen_dir_s,trial_rm_kn_b] = create_csr_generatrix(X,M,Dir.D2, t_min_b,t_V1_b,E0,msg_4);
                    if (C.verbose),fprintf("[POST_CSR_B] \tsucc_csr_b = %d \n",succ_csr_b);end
                    S.results.cpt_rm_kn(n,2) = trial_rm_kn_b;
                    tok_dir_511 = "b"; 
                elseif (succ_half_lor_f && succ_half_lor_b)
                    if (C.verbose),fprintf("[RES] LOR CREEE \n"); end
                    E1b = E0; E2b = E0; 
                elseif(~succ_half_lor_f && ~succ_half_lor_b)
                    if (C.verbose), fprintf("[RES] ECHEC \n"); end
                    E0 = 0; E1b = 0; E2b = 0; 
                end          
                if (C.verbose)
                    fprintf("[SYNTH|V] naii(V1) = %d\t naii(V2) = %d\t naii(V1s) = %d\t naii(V2s) = %d \n",naii(V1),naii(V2),naii(V1s),naii(V2s));
                    fprintf("[SYNTH|E] E0 = %g\t E1b = %g\t E2b = %g \n",E0,E1b,E2b);
                end
                % <<<<<
                n_gamma_511 = length(find([naii(V1),naii(V2)]));
                n_gamma_511_diff = length(find([naii(V1s),naii(V2s)]));
                n_gamma_1157_diff = 0; 

                [config,inf_msg] = get_type_detected_event(n_gamma_511,n_gamma_511_diff,n_gamma_1157_diff,tok_dir_511);
                % STOCKAGE
                S.export.chr(n)= config.chr;
                if (succ_half_lor_f && succ_half_lor_b)
                    S.export.data(n,S.rng.V1_rng) = V1;
                    S.export.data(n,S.rng.V2_rng) = V2;
                    S.export.data(n,S.rng.E1b_rng) = E1b;
                    S.export.data(n,S.rng.E2b_rng) = E2b;
                    S.nb_detection = S.nb_detection + 1;
                elseif (succ_csr_f || succ_csr_b)
                    S.export.data(n,S.rng.V1s_rng) = V1s;
                    S.export.data(n,S.rng.V2s_rng) = V2s;
                    S.export.data(n,S.rng.E1b_rng) = E1b;
                    S.export.data(n,S.rng.E2b_rng) = E2b;
                    S.nb_detection = S.nb_detection + 1;
                else
                    % Nothing to do my Lord
                end
                
                
                % ---------------------------------------------------------
                % ---------------------------------------------------------
                % DESSIN
                if (C.draw)                
                    draw_pts(M,'x','r',4);
                    % Demi-LOR f 
                    if (succ_half_lor_f)
                        sketch_list = draw_store_pts(sketch_list,V0f,'.','k');    % Point d'éntrée dans la zone active (f)
                        i1_f = get_point(M,Dir.D2,t_max_f);
                        sketch_list = draw_store_lin(sketch_list,M,i1_f,'#808080',"--",1);
                    end
                    % Demi-LOR b
                    if (succ_half_lor_b)
                        sketch_list = draw_store_pts(sketch_list,V0b,'.','k');    % Point d'éntrée dans la zone active (b)
                        i1_b = get_point(M,Dir.D2,t_max_b);
                        sketch_list = draw_store_lin(sketch_list,M,i1_b,'#808080',"--",1);
                    end
                    % generatrice Cone - f
                    if (succ_csr_f)
                        t2_hat = get_intersect_coord_csr(X,V1s,csr_gen_dir_s); 
                        i2_f = get_point(V1s,csr_gen_dir_s,t2_hat);
                        sketch_list = draw_store_lin(sketch_list,V1s,i2_f,'#808080',"--",1);
                    end
                    % generatrice Cone - b
                    if (succ_csr_b)
                        t2_hat = get_intersect_coord_csr(X,V1s,csr_gen_dir_s); 
                        i2_b = get_point(V1s,csr_gen_dir_s,t2_hat);
                        sketch_list = draw_store_lin(sketch_list,V1s,i2_b,'#808080',"--",1);
                    end
                    % Point d'interaction sans diffusion 
                    if (succ_half_lor_f && succ_half_lor_b)
%                         sketch_list = draw_store_pts(sketch_list,V1,'x',config.part_color);
%                         sketch_list = draw_store_pts(sketch_list,V2,'x',config.part_color);
                        pts_list = draw_store_pts(pts_list,V1,'*',config.part_color,8);
                        pts_list = draw_store_pts(pts_list,V2,'*',config.part_color,8);
                    end
                    % Point d'interaction si diffusion
                    if (succ_csr_f || succ_csr_b)
%                         sketch_list = draw_store_pts(sketch_list,V1s,'x',config.part_color);
%                         sketch_list = draw_store_pts(sketch_list,V2s,'x',config.part_color);
                        pts_list = draw_store_pts(pts_list,V1s,'*',config.part_color,8);
                        pts_list = draw_store_pts(pts_list,V2s,'*',config.part_color,8);
                    end
                end

                
                test ="";
            % ---------------------------------------------------------------------
            % ---------------------------------------------------------------------
            % ---------------------------------------------------------------------
            % ---------------------------------------------------------------------
            case "3"

                t1_min_p_f = inf; t1_max_p = inf; t_V1p = inf; t2_min_p = inf; t2_max_p = inf; t_V2p = inf;
                 t_max_f = inf; t_max_b = inf; 
                t2_min = inf ; t2_max =inf; 
                t_V1_f = inf; t_V1_b = inf; 
                csr_gen_dir_s = inf(3,1);
                csr_gen_dir_p = inf(3,1);
                csr_list = [];
                
                succ_half_lor_f = false;
                succ_half_lor_b = false; 
                succ_csr_f = false; 
                succ_csr_b = false;
            
                nb_intersection = 0; 
                t_inter_1 = 0; t_inter_2 = 0;
                tab_inter_status = zeros(1,7); 
                % 
                M_orig = M;     % on vient écrasé par la suite en appliquant le positron range
                % >>>>>
                succ_csr_p = false;
                % SIMULATION
                [t1_min_p_f,t1_min_p_b] = get_intersect_coord_Rin(X,M,Dir.D1);
                % PATCH CHOIX DE t1_min_p; 
                T_min_p = [t1_min_p_f,t1_min_p_b]; 
                scal_vec = zeros(size(T_min_p));
                V_unit_dir = M + Dir.D1;
                MV_nrm_unit_dir = (V_unit_dir-M)/norm(V_unit_dir-M,2);  
                % Pour la première composante t retournée par get_intersect_coord_Rin
                if (~isinf(t1_min_p_f))
                    V_t1_min_p_f = M + t1_min_p_f*Dir.D1;
                    MV_nrm_min_p_f = (V_t1_min_p_f - M)/norm(V_t1_min_p_f - M,2); 
                    scal_vec(1) = dot(MV_nrm_unit_dir,MV_nrm_min_p_f);
                    if (C.draw), sketch_list = draw_store_pts(sketch_list,V_t1_min_p_f,'^','m'); end
                end
                % Pour la seconde composante t retournée par get_intersect_coord_Rin
                if (~isinf(t1_min_p_b))
                    V_t1_min_p_b = M + t1_min_p_b*Dir.D1;
                    MV_nrm_min_p_b = (V_t1_min_p_b - M)/norm(V_t1_min_p_b - M,2); 
                    scal_vec(2) = dot(MV_nrm_unit_dir,MV_nrm_min_p_b);
                    if (C.draw), sketch_list = draw_store_pts(sketch_list,V_t1_min_p_b,'v','c'); end
                end
                [~,idx_t_min] = max(scal_vec); 
                t1_min_p = T_min_p(idx_t_min);
                if (C.verbose)
                    ast_idx = ["",""]; 
                    ast_idx(idx_t_min) = "(*)"; 
                    fprintf("[INIT_CSR_1157 - %d]\t t1_min_p_f = %g %s \t t1_min_p_b = %g %s\n",n,t1_min_p_f,ast_idx(1),t1_min_p_b,ast_idx(2));
                end
                if (~isinf(t1_min_p))
                    % Point d'entrée dans la zone active
                    V0p = get_point(M,Dir.D1,t1_min_p);
                    % Calcul du premier point d'intraction
                    [t1_max_p] = get_intersect_coord_Rout(X,M,Dir.D1,t1_min_p);
                    [t_V1p] = distance_xenon_attenuation(X,E0_prime,Dir.D1,t1_max_p);
                    V1p = get_point(V0p,Dir.D1,t_V1p);
                    msg_1 = ""; if (C.verbose), msg_1 = "[CSR init 1157 V1p]"; end 
                    if (is_in_FOD(X,V1p,msg_1))
                        msg_2 = "" ; if (C.verbose), msg_2 = "1157"; end 
                        [succ_csr_p,V0p,V1p,V2p,E1a,E2a,csr_gen_dir_p,trial_rm_kn_p] = create_csr_generatrix(X,M,Dir.D1,t1_min_p,t_V1p,E0_prime,msg_2);
                        S.results.cpt_rm_kn(n,1) = trial_rm_kn_p;
                    else
                        succ_csr_p = false;
                        t_V1p = inf;
                        V1p = inf(3,1);
                    end
                    S.results.csr_gen_dir(n,:) = csr_gen_dir_p; 
                end
                if (C.verbose)
                    fprintf("[POST_CSR_1157] \tsucc_csr_b = %d \n",succ_csr_p);
                    fprintf("[SYNTH|V] naii(V1) = %d\t naii(V2) = %d\n",naii(V1p),naii(V2p));
                    fprintf("[SYNTH|E] E0 = %g\t E1b = %g\t E2b = %g \n",E0_prime,E1a,E2a);
                end
                %%%
                [M,R_pr] = positron_range(M,Dir.D_pr,S.Conf.Pos_rng);
                % 
                [t_min_f,t_min_b] = get_intersect_coord_Rin(X,M,Dir.D2);  % b : back, f : forward
                if (C.verbose), fprintf("\n[INIT_LOR - %d] \tt_min_f = %g \t t_min_b = %g \n",n,t_min_f,t_min_b); end
                % ---------------------------------------------------------
                % ---------------------------------------------------------
                % PARTIE 1 - Tenter la LOR
                % => D'abord vers l'avant t>0 (f); 
                if (~isinf(t_min_f)) 
                    msg_3 = ""; if (C.verbose), msg_3 = "[LOR-V1-f]"; end 
                    [succ_half_lor_f,V0f,V1,E1b,t_V1_f,t_min_f,t_max_f] = create_half_lor(X,M,Dir.D2,t_min_f,E0,msg_3);
                end
                % => Puis vers l'arrière t<0 (b)
                if (~isinf(t_min_b))
                    msg_4 = ""; if (C.verbose), msg_4 = "[LOR-V2-b]"; end
                    [succ_half_lor_b,V0b,V2,E1b,t_V1_b,t_min_b,t_max_b] = create_half_lor(X,M,Dir.D2,t_min_b,E0,msg_4); 
                end
                if (C.verbose),fprintf("[POST_LOR] \tsucc_lor_f = %d \tsucc_lor_b = %d \n",succ_half_lor_f,succ_half_lor_b); end
                % ---------------------------------------------------------
                % ---------------------------------------------------------
                 tok_dir_511 = ""; 
                if (~succ_half_lor_f || ~succ_half_lor_b), V1 = inf(3,1); V2 = inf(3,1);  end
                % PARTIE 2 - Si pas de LOR (succ_f=false ou succ_b = false)
                %          - On ré-init les points 
                %          - On tente de créer une CSR dans le sens du rayon possible.
                
                if(succ_half_lor_f && ~succ_half_lor_b)
                    % Dans ce cas, on a déjà calculé t_min_f et t_max_f => On a pas besoin de recalculer cette donnée 
                    msg_5 = ""; if (C.verbose), msg_5 = "511-f"; end
                    [succ_csr_f,V0f,V1s,V2s,E1b,E2b,csr_gen_dir_s,trial_rm_kn_f] = create_csr_generatrix(X,M,Dir.D2,t_min_f,t_V1_f,E0,msg_5);
                    S.results.cpt_rm_kn(n,2) = trial_rm_kn_f;
                    tok_dir_511 = "f"; 
                    if (C.verbose), fprintf("[POST_CSR_F] \tsucc_csr_f = %d \n",succ_csr_f);end
                elseif(~succ_half_lor_f && succ_half_lor_b)
                    % Dans ce cas, on a déjà calculé t_min_b et t_max_b => On a pas besoin de recalculer cette donnée 
                    msg_6 = ""; if (C.verbose), msg_6 = "511-b"; end
                    [succ_csr_b,V0b,V1s,V2s,E1b,E2b,csr_gen_dir_s,trial_rm_kn_b] = create_csr_generatrix(X,M,Dir.D2, t_min_b,t_V1_b,E0,msg_6);
                    S.results.cpt_rm_kn(n,2) = trial_rm_kn_b;
                    if (C.verbose), fprintf("[POST_CSR_B] \tsucc_csr_b = %d \n",succ_csr_b); end
                    tok_dir_511 = "b"; 
                elseif (succ_half_lor_f && succ_half_lor_b)
                    if (C.verbose), fprintf("[RES] LOR CREEE \n"); end
                    E1b = E0; E2b = E0; 
                elseif(~succ_half_lor_f && ~succ_half_lor_b)
                    if (C.verbose), fprintf("[RES] ECHEC \n"); end
                    E0 = 0; E1b = 0; E2b = 0; 
                end     
                if (C.verbose)
                    fprintf("[SYNTH|V] naii(V1) = %d\t naii(V2) = %d\t naii(V1s) = %d\t naii(V2s) = %d \n",naii(V1),naii(V2),naii(V1s),naii(V2s));
                    fprintf("[SYNTH|E] E0 = %g\t E1b = %g\t E2b = %g \n",E0,E1b,E2b);
                end
                n_gamma_511 = length(find([naii(V1),naii(V2)]));
                n_gamma_511_diff = length(find([naii(V1s),naii(V2s)]));
                n_gamma_1157_diff = length(find([naii(V1p),naii(V2p)])); 

                % <<<<< 
                [config,inf_msg] = get_type_detected_event(n_gamma_511,n_gamma_511_diff,n_gamma_1157_diff,tok_dir_511);
                % 3Gamma
                if (config.idx == 6)
                    axe_lor = (V2-V1)/norm(V2-V1,2); 
                    axe_csr = (V1p-V2p)/norm(V1p-V2p,2); 
                    apex = V1p; 
                    beta =  acos(1-(E1a*X.mec2/(1.157*(1.157-E1a))));
                    [t_inter_1,t_inter_2,tab_inter_status] = intersection_lor_cor(X,V1,axe_lor,apex,axe_csr,beta,C.verbose);
                    nb_intersection = (t_inter_1~=0)+(t_inter_2~=0); 
%                     %
%                     
%                     % <<<< 
%                     
%                     %Les points d'intersection calculées 
%                     if (t_inter_1~=0), X1_inters = V1 + t_inter_1*axe_lor;   pts_list = draw_store_pts(pts_list,X1_inters,'*','g',15);    end
%                     if (t_inter_2~=0), X2_inters = V1 + t_inter_2*axe_lor;   pts_list = draw_store_pts(pts_list,X2_inters,'*','g',15);    end
%                     %La LOR
%                     sketch_list = draw_store_lin(sketch_list,M,get_point(M,Dir.D2,t_max_f),'#808080',"--",1);
%                     sketch_list = draw_store_lin(sketch_list,M,get_point(M,Dir.D2,t_max_b),'#808080',"--",1);
%                     % Le COR dans le sens de l'axe 
%                     t_csr =  100; 
%                     for (o = 0:10:360)
%                         m = get_generatrix_unit_vector(V2p,V1p,beta,deg2rad(o),"0");
%                         sketch_list = draw_store_lin(sketch_list,V1p,get_point(V1p,m,t_csr),"b","-",1);
%                     end
%                     % CONE "DUAL" LORS DE L'ELEVATION AU CARRE
%                     for (o = 0:10:360)
%                         m = get_generatrix_unit_vector(V2p,V1p,beta,deg2rad(o),"0");
%                         sketch_list = draw_store_lin(sketch_list,V1p,get_point(V1p,m,-t_csr),"#CC0000","-",1);
%                     end
%                     
                    % >>>>
                    
                    test = ""; 
                end
                % STOCKAGE
                
                S.export.chr(n)= config.chr;
                if (succ_csr_p)
                    S.export.data(n,S.rng.V1p_rng) = V1p;
                    S.export.data(n,S.rng.E1a_rng) = E1a;
                    S.export.data(n,S.rng.V2p_rng) = V2p;
                    S.export.data(n,S.rng.E2a_rng) = E2a;
                    S.nb_detection = S.nb_detection + 1;
                end
                if (succ_half_lor_f && succ_half_lor_b)
                    S.export.data(n,S.rng.V1_rng) = V1;
                    S.export.data(n,S.rng.V2_rng) = V2;
                    S.export.data(n,S.rng.E1b_rng) = E1b;
                    S.export.data(n,S.rng.E2b_rng) = E2b;
                    S.nb_detection = S.nb_detection + 1;
                elseif (succ_csr_f || succ_csr_b)
                    S.export.data(n,S.rng.V1s_rng) = V1s;
                    S.export.data(n,S.rng.V2s_rng) = V2s;
                    S.export.data(n,S.rng.E1b_rng) = E1b;
                    S.export.data(n,S.rng.E2b_rng) = E2b;
                    S.nb_detection = S.nb_detection + 1;
                else
                    % Nothing to do my Lord
                end
                % Stockage des point d'intersection 
                S.results.nb_intersection(n) = nb_intersection; 
                % Nature des intersections: [sgn(delta),t1,t1::in_fov,t1::sign(beta),t2,t2::in_fov,t2::sign(beta)]
                S.results.inter_status(n,:) = tab_inter_status;    
                if (t_inter_1~=0)
                    S.export.intersection_points(n,S.rng.inter_I1_rng) = get_point(V1,axe_lor,t_inter_1);
                end
                if (t_inter_2~=0)
                    S.export.intersection_points(n,S.rng.inter_I2_rng) = get_point(V1,axe_lor,t_inter_2);
                end


                
                    



                % DESSIN
                if (C.draw)            
                    %draw_pts(M_orig,'x','r',5);
                    pts_list = draw_store_pts(pts_list,M_orig,'x','r',5);
                    if (succ_csr_p)                    
                        sketch_list = draw_store_pts(sketch_list,V0p,'.','k');    % Point d'éntrée dans la zone active
                        i1 = get_point(M,Dir.D1,t1_max_p);                 
                        % Axe du c^one
                        sketch_list = draw_store_lin(sketch_list,M,i1,'#808080',"--",1);   
                        t2_hat = get_intersect_coord_csr(X,V1p,csr_gen_dir_p);
                        % génératrice 
                        i2 = get_point(V1p,csr_gen_dir_p,t2_hat);
                        sketch_list = draw_store_lin(sketch_list,V1p,i2,'#808080',"--",1);  
                        % interaction avec diffusion
                        pts_list = draw_store_pts(pts_list,V1p,'*',config.part_color,8);
                        pts_list = draw_store_pts(pts_list,V2p,'*',config.part_color,8);
                    end    


                    %draw_pts(M,'x','r',4);
                    pts_list = draw_store_pts(pts_list,M,'x','r',4);
                    % Demi-LOR f 
                    if (succ_half_lor_f)
                        sketch_list = draw_store_pts(sketch_list,V0f,'.','k');    % Point d'éntrée dans la zone active (f)
                        i1_f = get_point(M,Dir.D2,t_max_f);
                        sketch_list = draw_store_lin(sketch_list,M,i1_f,'#808080',"--",1);
                    end
                    % Demi-LOR b
                    if (succ_half_lor_b)
                        sketch_list = draw_store_pts(sketch_list,V0b,'.','k');    % Point d'éntrée dans la zone active (b)
                        i1_b = get_point(M,Dir.D2,t_max_b);
                        sketch_list = draw_store_lin(sketch_list,M,i1_b,'#808080',"--",1);
                    end
                    % generatrice Cone - f
                    if (succ_csr_f)
                        t2_hat = get_intersect_coord_csr(X,V1s,csr_gen_dir_s); 
                        i2_f = get_point(V1s,csr_gen_dir_s,t2_hat);
                        sketch_list = draw_store_lin(sketch_list,V1s,i2_f,'#808080',"--",1);
                    end
                    % generatrice Cone - b
                    if (succ_csr_b)
                        t2_hat = get_intersect_coord_csr(X,V1s,csr_gen_dir_s); 
                        i2_b = get_point(V1s,csr_gen_dir_s,t2_hat);
                        sketch_list = draw_store_lin(sketch_list,V1s,i2_b,'#808080',"--",1);
                    end
                    % Point d'interaction sans diffusion 
                    if (succ_half_lor_f && succ_half_lor_b)
%                         sketch_list = draw_store_pts(sketch_list,V1,'x',config.part_color);
%                         sketch_list = draw_store_pts(sketch_list,V2,'x',config.part_color);
                        pts_list = draw_store_pts(pts_list,V1,'*',config.part_color,8);
                        pts_list = draw_store_pts(pts_list,V2,'*',config.part_color,8);
                    end
                    % Point d'interaction si diffusion
                    if (succ_csr_f || succ_csr_b)
%                         sketch_list = draw_store_pts(sketch_list,V1s,'x',config.part_color);
%                         sketch_list = draw_store_pts(sketch_list,V2s,'x',config.part_color);
                        pts_list = draw_store_pts(pts_list,V1s,'*',config.part_color,8);
                        pts_list = draw_store_pts(pts_list,V2s,'*',config.part_color,8);
                    end
                end

                %disp(config.label)

                test = "";
            % ---------------------------------------------------------------------
            % ---------------------------------------------------------------------
            % ---------------------------------------------------------------------
            % ---------------------------------------------------------------------
        end % switch C.case

        S.results.M_pr(n,:) = M;    % Dans le cas du postrin range, M a été stocké lors de l'appel à la fonction d'émission unifiée (simulation_emission.m)
        S.results.R_pr(n,:) = R_pr;
        % Stockage des données de comptage
        S.results.occ_config_count(config.idx) = S.results.occ_config_count(config.idx) + 1;
        % Stockage dans la structure H-map
        %key_list = find(isinf(S.results.occ_config(config.idx,:)));
        %S.results.occ_config(config.idx,key_list(1)) = n;

        test="";
        %break;
        if (mod(n,verb_lim) == 0 )
            info.R_pr = R_pr;
            info.n = n;
            info.N_allG =S.results.occ_config_count;
            info.pure_evt_idx = pure_evt_idx;
            info.pts = [S.results.M(n,:);M;Dir.D1;Dir.D2];
            info.inf_msg = inf_msg;
            info.config_idx = config.idx;
            print_one_simu(C,P,info);
        end
        

        if (C.draw && ~isempty(sketch_list))
            if (config.idx > 1), pause(C.wait_time); end    % Si on dessine, on s'en fiche des 0G
            erase_all_elts(sketch_list);
        end
        if (C.draw && C.rem_pts && ~isempty(pts_list))
            erase_all_elts(pts_list);
        end

        % ------------------------------------------------------------------
        % Condition d'arr^et de la simulation
        switch C.preset_type
            case "count"
                % prendre dans le range (2:end) car le premier indice du vecteur correspond aux 0-détections.
                %if (sum(S.results.occ_event(2:end)) == C.N), S.terminal=true; break;  end
                if (S.results.occ_config_count(pure_evt_idx) == C.N), S.terminal=true; break;  end
            case "time"
                 disp("A FAIRE - TODO "); S.terminal = false; break;
            otherwise
        end
    % Sécurité 
    [V0f,V0b,V1,V2,V0p,V1p,V2p,V0s,V1s,V2s,E1a,E2a] = reset_interaction_point();
    [n_gamma_511,n_gamma_1157] = reset_detection();
    test ="";
    
    
    end % while (~terminal)
    % on ferme la figure à la fin
    if (C.draw)
        hold off;
    end
    % ------------------------------------------------------------------
    S.results.N_exp = sum(S.results.occ_config_count);
    %{
        Je demande N tirages (preset-count) eg. N=100 ;
        Lors de la simulation, je peux en obtenir en fait M=107 car 7 seront sortis mais je m’en fiche, je veux N.
        Je simule mes N tirages voulus.
        Je stocke les résultats dans une matrice C
            - Est de dim 3 car je travaille avec un fantôme en 3D ;
            - Contient des entiers (+1 à chaque fois qu’un voxel est sélectionné).
        Une fois que j’ai obtenu mes N tirages (je peux en avoir fais M, je m’en fiche, j’en voulais N), je calcul l’activité de MON fantômes  (simulé) tq.
        activité = sum_i,j,k C(i,j,k) 	// i <=> x, j <=> y, k <=> z
        Je calcule alors le facteur de calibration tq.
        ECF = activité/N
    %}
    S.results.calib_factor = sum(S.results.activity,'all')/S.Conf.N;
    % ------------------------------------------------------------------
    % Rabotage des allocations mémoire en trop ...
    [S] = plane_S_struct(S);


    test ="";

end
