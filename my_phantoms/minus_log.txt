./mod_create_phantom.exe -o minus -d 19 19 24 -v 5 5 10 -c 0 0 0 45 240 100 -s 15.907 -15.907 -15.000 10 400 -s 15.907 15.907 -15.000 7.5 400 -s -22.500 0 -15.000 12.5 10 -s -15.907 15.907 15.000 5 400 -s 0 -22.500 15.000 12.5 400 -s 15.907 15.907 15.000 10 10 -x 64 64 1 
  --> Creating image (background: 0)
  --> Inserting cylinder at [0;0;0] of radius 45 and length 240 with value 100
      Number of non-background voxels: 7032
  --> Inserting sphere at [15.907;-15.907;-15] of radius 10 with value 400
      Number of non-background voxels: 7032
  --> Inserting sphere at [15.907;15.907;-15] of radius 7.5 with value 400
      Number of non-background voxels: 7032
  --> Inserting sphere at [-22.5;0;-15] of radius 12.5 with value 10
      Number of non-background voxels: 7032
  --> Inserting sphere at [-15.907;15.907;15] of radius 5 with value 400
      Number of non-background voxels: 7032
  --> Inserting sphere at [0;-22.5;15] of radius 12.5 with value 400
      Number of non-background voxels: 7032
  --> Inserting sphere at [15.907;15.907;15] of radius 10 with value 10
      Number of non-background voxels: 7032
