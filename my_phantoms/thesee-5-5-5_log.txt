./mod_create_phantom.exe -o thesee-5-5-5 -d 14 14 48 -v 5 5 5 -c 0 0 0 45 240 100 -s 15.907 -15.907 -15.000 10 400 -s 15.907 15.907 -15.000 7.5 400 -s -22.500 0 -15.000 12.5 10.000 -s -15.907 15.907 15.000 5 400 -s 0 -22.500 15.000 12.5 400 -s 15.907 15.907 15.000 10 10.000 -x 64 64 1 
  --> Creating image (background: 0)
  --> Inserting cylinder at [0;0;0] of radius 45 and length 240 with value 100
      Number of non-background voxels: 9408
  --> Inserting sphere at [15.907;-15.907;-15] of radius 10 with value 400
      Number of non-background voxels: 9408
  --> Inserting sphere at [15.907;15.907;-15] of radius 7.5 with value 400
      Number of non-background voxels: 9408
  --> Inserting sphere at [-22.5;0;-15] of radius 12.5 with value 10
      Number of non-background voxels: 9408
  --> Inserting sphere at [-15.907;15.907;15] of radius 5 with value 400
      Number of non-background voxels: 9408
  --> Inserting sphere at [0;-22.5;15] of radius 12.5 with value 400
      Number of non-background voxels: 9408
  --> Inserting sphere at [15.907;15.907;15] of radius 10 with value 10
      Number of non-background voxels: 9408
