./mod_create_phantom.exe -o cylindre_sphere -d 14 14 48 -v 5 5 5 -c 0 0 0 25 120 100 -s 7.5 7.5 -13 10 400 -x 64 64 64 
  --> Creating image (background: 0)
  --> Inserting cylinder at [0;0;0] of radius 25 and length 120 with value 100
      Number of non-background voxels: 2112
  --> Inserting sphere at [7.5;7.5;-13] of radius 10 with value 400
      Number of non-background voxels: 2112
