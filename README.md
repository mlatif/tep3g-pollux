# Pollux : simulateur TEP $`3\gamma`$

## Dernière version :
![Simulateur 3D XEMIS](images/phantom_simon_exemple_comptage.png)
```
----------
F	erno_3d_smt
P	Nombre total de Voxel : J := 7 x 7 x 9 = 441
P	Taille d'un voxel 3 x 3 x 3 (mm)
P	Taille de l'image 21 x 21 x 27 (mm)
P	Activité totale : 15409
S	Cas étudié : 3 photon(s)
S	rng = default
S	preset : count
S	positron range :1
S	Nombre de tirages demandés : 15409
S	Nombre de tirages réalisés : 15524
S	Calibration factor : 1.00746
S	Occurences des évènements détectés (en %)
		0G : 0.746317
		1G : 7.1906
		2G : 10.2732
		3G : 82.5362
S	Classification des évènements détectés (en %)
		0G : 0.746317
		1G511 : 0.285547
		1G1157 : 6.90506
		2GLOR : 7.91745
		2GMIX : 2.35577
		3G : 82.5362
----------
```

## Convention de stockage des données :

|  	| **t** 	| **c** 	| **V1** 	|  	|  	| **V2** 	|  	|  	| **V1’** 	|  	|  	| **V2’** 	|  	|  	| **V1’’** 	|  	|  	| **V2’’** 	|  	|  	| **E1a** 	| **E2a** 	| **E1b** 	| **E2b** 	|
|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|:---:	|
| **Cas** 	|  	|  	| x 	| y 	| z 	| x 	| y 	| z 	| x 	| y 	| z 	| x 	| y 	| z 	| x 	| y 	| z 	| x 	| y 	| z 	|  	|  	|  	|  	|
| **1G511** 	| 10 	| '1’ 	|  	|  	|  	|  	|  	|  	| o 	| o 	| o 	| o 	| o 	| o 	|  	|  	|  	|  	|  	|  	| o 	| o 	|  	|  	|
| **1G1157** 	| 10 	| '2’ 	|  	|  	|  	|  	|  	|  	| o 	| o 	| o 	| o 	| o 	| o 	|  	|  	|  	|  	|  	|  	| o 	| o 	|  	|  	|
| **2GLOR** 	| 10 	| '3’ 	| o 	| o 	| o 	| o 	| o 	| o 	|  	|  	|  	|  	|  	|  	|  	|  	|  	|  	|  	|  	|  	|  	|  	|  	|
| **2GMIX** 	| 10 	| '4’ 	|  	|  	|  	|  	|  	|  	| o 	| o 	| o 	| o 	| o 	| o 	| o 	| o 	| o 	| o 	| o 	| o 	| o 	| o 	| o 	| o 	|
| **3G** 	| 10 	| '5’ 	| o 	| o 	| o 	| o 	| o 	| o 	| o 	| o 	| o 	| o 	| o 	| o 	|  	|  	|  	|  	|  	|  	| o 	| o 	|  	|  	|
|  	| **1** 	| **2** 	| **3** 	| **4** 	| **5** 	| **6** 	| **7** 	| **8** 	| **9** 	| **10** 	| **11** 	| **12** 	| **13** 	| **14** 	| **15** 	| **16** 	| **17** 	| **18** 	| **19** 	| **20** 	| **21** 	| **22** 	| **23** 	| **24** 	|

**Types de données:**
- *c* char
- *le reste* float32
- Remarque : normalement *t* devait ^etre un uint32 mais Matlab ne veut pas.

**Quelques refs :**
- [Lien 1 - Exemple de typage multiple avec fwrite](https://fr.mathworks.com/matlabcentral/answers/1679274-how-to-output-multiple-types-of-binary-files-in-one-file?s_tid=ans_lp_feed_leaf)
- [Lien 2 - Exemple de typage multiple avec fwrite - en une seule ligne](https://fr.mathworks.com/matlabcentral/answers/724318-using-fwrite-for-multiple-data-type-at-once)

## Récupération des données liées aux sections efficaces d'un photon:
- [Site du NIST](https://physics.nist.gov/PhysRefData/Xcom/html/xcom1.html)
- Cocher
	- Identify material by: *Element*
	- Method of entering additional energies: *Enter additional energies by hand*
	- Select by Symbol : *Xe* (atomic number *Z=54*)
	- Graphs options
		- *Total Attenuation with Coherent Scattering*
		- *Coherent Scattering* (Effet Rayleigh)
		- *Incoherent Scattering* (Effet Compton)
		- *Photoelectric Absorption*
	- Options for output units: *All quantities in cm2/g*
	- Additional energies in MeV: (optional)
		- *0.511*
		- *1.517*
	- Energy Range:
	 	- Minimum : *0.001* (MeV)
		- Maximum : *1.517* (MeV)
- Export des données
	- Cocher : Scattering (Coherent et Incoherent), Photoelectric Absorption, Total Attenuation with Coherent Scattering  
	- Delimiter : *tab*
	- *Download data*
- Un fichier *.pl* doit apparaitre -> Stocker les données dans le répertoire *data/nist_data_xenon.txt*


## Utilisation de l'application générée à partir de Matlab
POLLUX - Simulateur 3 photons  
Simulateur d'activité radioactive dans la caméra Xemis2.
### Documentations
**Paramètres :**
- filename [string] : le nom du fichier contenant les données générée à l'aide du créateur de phantom modifié (simulator/mod_create_phantom.cc)
	- Remarques :
		- ne pas mettre l'extention *.txt (Pollux s'en charge)
		- préfixer filename du chemin où sont stockés les données relatives au phantom généré : eg. filename = "my_phantoms/phantom_emission_simon_3d"
- nb_photon [string] : le nombre de photons qui sont émis lors d'un tirage; prend ses valeurs dans l'ensemble {1,2,3}
	- Remarques:
		- pour les cas 2 et 3 photons, les évènements où l'on détecte un nombre inférieur de photons sont également considérés dans la classification.
		- le générateur de code matlab est capricieux, obligé de passer nb_photon sous forme d’une chaîne de caractères eg. "3"
- N [string] : nombre de tirage réalisés lors de la simulation.
	- Remarques :
		- si N est inférieur est à 250, l'ensemble des résultats des tirages sont affichés; au délà, l'affichage est moins verbeux.
		- le générateur de code matlab est capricieux, obligé de passer nb_photon sous forme d’une chaîne de caractères eg. "1000"

**Retour :** affichage console du résultat de la simulation.  
Remarque :  
- Convention de notation pour la classification des évènements détectés :
	- 0G : aucune détection ;
	- 1GX : un photon détecté possédant une signature énergétique de X KeV ;
	- 2GY : deux photons détectés avec Y = LOR (resp. Y=MIX) pour signifier que leurs signatures énergétique sont identiques (resp. différentes) ;
	- 3G : trois photons sont détectés.

**Exemple :**
Matlab speaking :

```
POLLUX("../my_phantoms/phantom_emission_simon_3d","3","1000")
```

Va générer 1000 tirages d’évènement type 3 photons.  
L'équivalent avec le script généré :

```
./run_POLLUX.sh ../v911/ phantom_emission_simon_3d 3 1000
```

### Installation rapide de l'application :
Tout est stocké dans le répertoire `share_code`.
- Aller dans `share_code/POLLUX/for_redistribution/`
- Lancer le script : `"MyAppInstaller_web.install"` (ou cliquer dessus).

Une fenetre d'installation va s'ouvrir; du texte reprenant la documentation ci-dessus est affiché :
- Cliquer sur Next
- Choisir un répertoire pour l'installation de l'application eg. `/home/user/Bureau/share_pollux` (nommé `PATH` dans la suite)
- Cliquer sur Next
- Choisir un répertoire pour l'installation du Matlab Runtime (obligatoire) eg. `PATH`.  **Remarque :** j'ai choisi le meme et cela ne semble pas poser de problème.
- Cliquer sur Next
- Dire `Yes` pour les conditions d'utilisation de Matlab
- Cliquer sur Next
- Lancer l'installation (~193Mb)

Maintenant, l'application s'est installée :

- Se rendre dans `PATH`
- Aller dans le répertoire `application` ie. `PATH/application/`
- On doit avoir par exemple :

```
user@[...]:~PATH/application$ pwd
PATH/application
user@[...]:~/PATH/application$ ls
default_icon_48.png  POLLUX  readme.txt  run_POLLUX.sh  splash.png
```

- Placer un fichier de données texte (*.txt) générées à l'aide du créateur de phantom modifié eg. `phantom_emission_simon_3d`
- Lancer la ligne de commande suivante :

```
./run_POLLUX.sh ../v911/ phantom_emission_simon_3d 3 1000
```

- Si tout marche bien, un truc de ce type doit apparaitre

```
user@pc-chu-208404:~/Bureau/share_pollux/application$ ./run_POLLUX.sh ../v911/ phantom_emission_simon_3d 3 1
------------------------------------------
Setting up environment variables
---
LD_LIBRARY_PATH is .:../v911//runtime/glnxa64:../v911//bin/glnxa64:../v911//sys/os/glnxa64:../v911//sys/opengl/lib/glnxa64
----------
F	phantom_emission_simon_3d
P	Nombre total de Voxel : J := 24 x 24 x 30 = 17280
P	Taille d'un voxel 4 x 4 x 4 (mm)
P	Taille de l'image 96 x 96 x 120 (mm)

```





## Prises de notes : Création de phantoms
A partir de l'archive **simulator_2021_11_15.zip**
```
$ unzip simulator_2021_11_15.zip ; cd simulator/ ; ls
create_castor_data.cc  create_crystal_map.cc  create_phantom.cc  include  Makefile  README  simulator.cc  src
$ cat README
# To compile, simply run 'make'.
# It will create a 'bin' directory with the 4 executables.
$ cd bin;  ls
create_castor_data.exe  create_crystal_map.exe  create_phantom.exe  simulator.exe
```
---
```
$ ./simulator/bin/create_phantom.exe

Usage: create_phantom  -o  name  -d  voxX  voxY  voxZ  -v  sizeX  sizeY  sizeZ  [Options]
This program creates an interfile image in float 32bits format.
The image is made of 'voxX', 'voxY', 'voxZ' voxels of size 'sizeX', 'sizeY', sizeZ' given in mm.
[...]
```
---
```
../simulator/bin/create_phantom.exe -d 112 112 1 -v 4. 4. 4. -o phantom_emission -c 0. 0. 0. 150. 4. 100. -c 50. 10. 0. 20. 4. 400. -c -40. -40. 0. 40. 4. 0. -x 32 32 1
```
*-o  name : is the base name of the output interfile image*  
*-d  vox[XYZ]  : is the number of voxels in the 3 dimensions*:  `-d 112 112 1`  
*-v  size[XYZ] : is the voxel dimensions (in mm)*: `-v 4. 4. 4.`  
*-c  pos[XYZ] rad len val : to insert a cylinder of radius 'rad' and length 'len' and centered in position [posX,posY,posZ], given in mm. The point [0,0,0] is the center of the image. 'val' will be the value inside the cylinder.*  
  - $`C_1 = \{ (0,0,0), 150mm,4mm\}`$ et la valeur de densité dedans est 100: `-c 0. 0. 0. 150. 4. 100.`
  - $`C_2 = \{ (50,10,0), 20mm,4mm\}`$ et la valeur de densité dedans est 400: `-c 50. 10. 0. 20. 4. 400.`
  - $`C_3 = \{ (-40,-40,0), 40mm,4mm\}`$ et la valeur de densité dedans est 0: `-c -40. -40. 0. 40. 4. 0. ` aka. zone froide  

*-x  nb[XYZ] : perform a surpixelation to get smoother volume (default: 1 1 1)* `-x 32 32 1`


```
mkdir my_phantoms; cd my_phantoms
$ ../simulator/bin/create_phantom.exe -d 112 112 1 -v 4. 4. 4. -o phantom_emission_simon -c 0. 0. 0. 150. 4. 100. -c 50. 10. 0. 20. 4. 400. -c -40. -40. 0. 40. 4. 0. -x 32 32 1
[...]
$ ls
phantom_emission_simon.hdr  phantom_emission_simon.img
```
**Dans le fichier .hdr** regarder les champs suivants :
- !number format := float ( équivalent à 32 bit - Real)
- !matrix size [1] := 112
- !matrix size [2] := 112
- !matrix size [3] := 1

Dans **ImageJ**
- Sélectionner dans  *file/import/raw* le fichier *.**img**
- Remplir les champs suivant avec les données du fichier .hdr
	- Image type = 32 bit - Real
	- Width = !matrix size [1] := 112
	- Height = !matrix size [2] := 112
	- Number of images = !matrix size [3] := 1
- Sélectionner les checkbox suivantes :
	- White is zero
	- Little edian byte order yuridsti_rsh


## Un exemple :
### Phantom alaSimon

```
../simulator/bin/mod_create_phantom.exe -d 24 24 1 -v 4. 4. 4. -o phantom_emission_simon -c 0. 0. 0. 46. 4. 100. -c 15. 15. 0. 18. 4. 400. -c -20. -20. 0. 10. 4. 0. -x 32 32 1
```
![Simulateur 2D XEMIS](images/phantom_simon_post_simu.png)

### Phantom alaDebora
```
../simulator/bin/mod_create_phantom.exe -d 24 24 1 -v 4. 4. 4. -o phantom_emission_debora -c 0. 0. 0. 46. 4. 100. -c 0. 17.5 0. 2. 4. 400. -c -8.8 15.1 0. 4. 4. 400. -c -17.5 0. 0. 8. 4. 400. -c 0. -17.5 0. 10. 4. 400. -c 17.2 3.7 0. 12. 4. 400. -x 64 64 1
```
![Simulateur : Phantom DG](images/phantom_debora_post_simu.png)


### Phantom Ernő
```
../simulator/bin/mod_create_phantom.exe -d 3. 3. 9. -v 10. 10. 15. -o phantom_emission_long_erno -c 0. 0. 0. 7. 25s. 100. -x 64 64 1
```

![Simulateur : Phantom Ernő 1](images/phantom_erno_post_simu.png)

### Phantom Ernő - Un peu plus long :

```
../simulator/bin/mod_create_phantom.exe -d 7. 7. 9. -v 3. 3. 3. -o phantom_emission_long_erno_2 -c 0. 0. 0. 7. 25s. 100. -x 64 64 1
```
![Simulateur : Phantom Ernő 2](images/phantom_erno2_post_simu.png)


## Inversion de la fonction de répartition
{:toc}

Soit $`F`$ une fonction de répartition. On appelle fonction quantile la fonction

```math
F^{−}(u) = \textrm{inf}\{x : F(x) \geq u\},\textrm{ } u \in ]0, 1[
```

Alors si $`U\sim\mathcal{U}([0,1])`$, la variable $`X = F^{−}(U)`$ a pour fonction de répartition $`F`$.

## Mantra git
```
cd existing_repo
git remote add origin https://gitlab.com/mlatif/tep3g-pollux.git
git branch -M master
git push -uf origin master
```
