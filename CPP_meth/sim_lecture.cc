#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;

#define FLTNBDATA float

// g++ sim_lecture.cc -o sim_lecture -O3
//
int main(int argc, char * argv[]) {
	int nb_evt = 100;
	FILE* fin = fopen("phantom_3d_raw.cdf","rb");
	// Compteur de données lues
	int nb_read = 0;


	//
	for (int e=0;e<nb_evt;e++){
		// Buffers
		uint32_t t = 0;
		char c;
		FLTNBDATA vertex[18] ={0};
		FLTNBDATA energy[4] ={0};
		//
	  nb_read += fread(&t,sizeof(uint32_t),1,fin);
		nb_read += fread(&c,sizeof(char),1,fin);
		cout << "t = " << t << endl;
		cout << "c = " << c << endl;
		//
		for (int i=0; i<18; i++) {
		  nb_read += fread(&vertex[i],sizeof(FLTNBDATA),1,fin);
		}
		//
		for (int i=0; i<4; i++){
			nb_read += fread(&energy[i],sizeof(FLTNBDATA),1,fin);
		}
	}
	fclose(fin);
	cout << "nb_read = " << nb_read << endl;



	return 0;
}
