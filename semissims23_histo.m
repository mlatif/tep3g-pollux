close all; 
figure; 
hold on; 
x = categorical(1:5);
y = [22.7 30.9 11.2 20.9 14.3]; 
b=bar(x,y); 
b.FaceColor = "#91A3B0";
b.EdgeColor = "#91A3B0";
b.CData(3,:) = "r"; 
lab = ["${1\gamma}^{511}_{\textrm{COR}}$";"${1\gamma}^{1157}_{\textrm{COR}}$";"${2\gamma}^{}_{\textrm{LOR}}$"; "${2\gamma}^{}_{\textrm{COR}}$";"${3\gamma}^{}_{}$" ]; 
set(gca,'TickLabelInterpreter','latex')
set(gca,'xticklabel',lab,'FontSize',15);
xtips2 = b(1).XEndPoints;
ytips2 = b(1).YEndPoints;
labels2 = strcat(string(b(1).YData)," \%");
text(xtips2,ytips2,labels2,'HorizontalAlignment','center','VerticalAlignment','bottom','Interpreter','latex','FontSize',20);
