#!/bin/bash
# VOIR  https://www.geogebra.org/m/mexed5ej
if [ "$#" -ne 3 ]; then
    echo -e "[!!!] \t Je vais continuer à te cracher à la figure si tu ne me donnes pas les 3 dims Dx Dy Dz en argument "
    exit 1
fi
# DEFINITION DU FANTOME
NAME=dedale
# 
CFov=70;       # Controle du coté maximal du FOV souhaité (on prendre le cercle circonscrit au carré) - Limite fixée à CmaxFov=70.721
HFov=240;      # Controle de la hauteur maximale du FOV souhaité 
# CONTRAINTES DU FOV
RadmaxFov=50;     # Rayon maximal du champ de vue
HmaxFov=240;      # Hauteur maximale du champs de vue 
# Définition du Fantome voulu 
CylRadPh=45    # Rayon du cylindre dans lequel on place les sphères
HPh=240;       # Hauteur du cylindre dans lequel on place les sphères
D=8             # Controle de l'espacement de le long de l'axe z (eg les sphères sont situées à 1/8 * (H/2) du centre) eg. H=240, D=8, les sphères sont situées à z=+/-15 mm 
ACT=100         # Background activity
Dx=$1;   Dy=$2;   Dz=$3;
FULL_NAME="$NAME-$Dx-$Dy-$Dz"
echo $FULL_NAME
# ------------------------------------------------------------------------
# Nombres de voxel dans Dedale
Nx=$(echo "scale=0 ; (sqrt(2)*$CFov)/$Dx" | bc);
Ny=$(echo "scale=0 ; (sqrt(2)*$CFov)/$Dy" | bc); 
Nz=$(echo "scale=0 ; ($HFov)/$Dz" | bc);
# Tambouille cancereuse 
P=$(echo "scale=3 ; $HPh/2" | bc -l);                     # Demi plan autorisé
HOT=$(echo "scale=3 ; $ACT*4" | bc -l);                     # Valeur des sphères chaudes
COL=$(echo "scale=3 ; $ACT/10" | bc -l);                    # Valeur des sphères froides 
Z1=$(echo "scale=3 ; -1*$P/$D" | bc -l)                     # Position z1 (espacement le long de l'axe z par valeur neg)
Z2=$(echo "scale=3 ; $P/$D" | bc -l)                        # Position z2 (espacement le long de l'axe z par valeur pos)
A=$(echo "scale=3 ; ($CylRadPh/2) * (sqrt(2)/2)" | bc -l)    # coordonnée de positionnement des centre de sphère type A
I=$(echo "scale=3 ; ($CylRadPh/2)" | bc -l)                  # coordonnée de positionnement des centre de sphère type I
CmaxFov=$(echo "scale=3 ; (2*$RadmaxFov)/sqrt(2)" | bc -l)
# ------------------------------------------------------------------------
echo -e "Limites de XEMIS\n\tRadmaxFov=$RadmaxFov ~> CmaxFov=$CmaxFov\n\tHmaxFov=$HmaxFov\nActivité bg:$ACT / hot: $HOT / cold: $COL\nPosition D=$D des sphères d'activité Z1=$Z1, Z2=$Z2" 
echo -e "Dimensions/nb voxels générés\n\t(Dx = $Dx, Nx = $Nx)\n\t(Dy = $Dy, Nx = $Ny)\n\t(Dz = $Dz, Nz = $Nz)"
PATH2CREATOR=$(find ~ -name "mod_create_phantom.exe")
#echo $PATH2CREATOR
#-s  pos[XYZ] rad val : to insert a sphere of radius 'rad' and centered in position [posX,posY,posZ],  given in mm. The point [0,0,0] is the center of the image. 'val' will be the  value inside the sphere.
$PATH2CREATOR -o $FULL_NAME -d $Nx $Ny $Nz -v $Dx $Dy $Dz -c 0 0 0 $CylRadPh $HPh $ACT -s $A -$A $Z1 10 $HOT -s $A $A $Z1 7.5 $HOT -s -$I 0 $Z1 12.5 $COL -s -$A $A $Z2 5 $HOT -s 0 -$I $Z2 12.5 $HOT -s $A $A $Z2 10 $COL -x 64 64 1
# echo "cp $FULL_NAME* ../../my_phantoms/"
# cp $FULL_NAME* ../../my_phantoms/

