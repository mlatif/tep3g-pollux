Reprise POLLUX

V = *émission* + t *  *vecteur directionnel* 

Cas 1G

- t1_min :  intersection du rayon avec le bord intérieur du photon incident 
- V0p : point d'entrée du photon dans le détecteur
- t1_max : intersection du rayon avec le bord extérieur / plans $\pm z$ du photon incident 
- t_V1p  \in [t1_min,t1_max] : associé premier point d'interaction du photon dans le Xénon
- E1a : énergie déposée par le photon lors de la première interaction
- V1p : premier point d'interaction  
- csr_dir : vecteur directionnel de la génératrice du cône
- CSR 
  - Sommet V1p
  - Axe MV1p
  - Variable : beta, omega 
- t2_min : point de départ du rayon diffusion n°1
- t2_max : intersection avec l'un des bords du cylindre 
- E2a : énergie déposée par le photon  lors de la seconde interaction
- V2p: le second point d'interaction

Cas 2G

* t_min_f :   intersection du rayon avec le bord intérieur du photon incident dans le sens de propagation du rayon 
* t_min_b :   intersection du rayon avec le bord intérieur du photon incident dans le sens inverse de propagation du rayon 
* V0_f : point d'entrée du photon dans le détecteur dans le sens de propagation du rayon
* t_max_f : intersection du rayon avec le bord extérieur / plans $\pm z$ du photon incident dans le sens de propagation du rayon
* t_V1_f  \in [t_min_f,t_max_f] : associé premier point d'interaction du photon dans le Xénon dans le sens de propagation du rayon 
* E1a :  énergie déposée par le photon lors de la première interaction
* V1 : premier point d'interaction 
* SI t_min_b n'est pas défini
  * csr_dir : vecteur directionnel de la génératrice du cône (à partir de M et V1)
  * t2_min : point de départ du rayon diffusion n°1
  * t2_max:  intersection avec l'un des bords du cylindre
  * V2p : le second point d'interaction
  * t_V2p_f \in [t2_min,t2_max] associé second point d'interaction du photon dans le Xénon dans le sens de propagation du rayon 