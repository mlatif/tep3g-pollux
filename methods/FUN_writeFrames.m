function FUN_writeFrames( frames, VideoFilename, FrameRate )
%FUN_WRITEFRAMES Summary of this function goes here
%   Detailed explanation goes here
%	Author : Marceau METILLON (marceau.metillon@ls2n.fr)
%	Project : CRAFT_UC1
%	Date : 07/12/2021

% create the video writer with 1 fps
writerObj = VideoWriter(VideoFilename);
% set the seconds per image
writerObj.FrameRate = FrameRate;

% open the video writer
open(writerObj);
% write the frames to the video
for u=1:length(frames)
    % convert the image to a fra
    
    writeVideo(writerObj, frames(u));
end
% close the writer object
close(writerObj);

end

