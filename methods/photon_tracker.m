function [track,pts_list] = photon_tracker(X,C,track,E0,M,Dir,t,type)
% PHOTON_TRACKER
% INPUT
% X: l'objet caméra;
% C: la configuration de l'expérimentation (accès à draw et verbose)
% track: le tracker que l'on va remplir (ou non)
% E0: l'énergie du photon incident;
% M: le point d'émission dans le FOV
% Dir: vecteur directionnel initial;
% t: valeur initiale tq V0+t*Dir atteint le bord Rin;
% OUTPUT
% track: structure contenant les points d'interactions et dépots d'énergie

mark = ['x','.','*'];
col = [	"#4DBEEE","#7E2F8E","#EDB120"];
pts_list = [];

lcl_inter_pts = inf(100,3);
lcl_dep_energ = inf(100,1);
lcl_effect_id = inf(100,1);

is_out = false;
cross_rin = false;
it = 0;
% -------------------------------------------------------------------------
% Point d'entrée dans la zone active 
V0 = get_point(M,Dir,t);
% Direction de propagation lors de l'entrée dans la zone active 
[t_max] = get_intersect_coord_Rout(X,M,Dir,t);
% -------------------------------------------------------------------------
Ecur = E0;
Vcur = V0;
Mcur = M;
Dircur = Dir;
Tcur = t_max;
% -------------------------------------------------------------------------
if(C.verbose), fprintf("################# START TRACK %g #################\n",E0); end
if (C.draw)
    pts_list = draw_store_pts(pts_list,Vcur,'.','k',1); % Point d'entrée dans la zone active
end
while(Ecur > 0 && ~is_out && ~cross_rin)
    E1 = 0; % On réinitialise le dépot d'énergie à l'itéraction courante;
    it = it + 1;
    if(C.verbose),fprintf("It = %d \t Ecur = %g\n",it,Ecur);end
    % ---------------------------------------------------------------------
    if (C.draw)
        [t_extrem] = get_intersect_coord_csr(X,Vcur,Dircur);
        Vextrem = get_point(Vcur,Dircur,t_extrem);
        pts_list = draw_store_lin(pts_list,Vcur,Vextrem,'#808080',"--",0.25);
    end
    % ---------------------------------------------------------------------
    % Déplacement du photon jusqu'à interaction
    [t_V] = distance_xenon_attenuation(X,Ecur,Dircur,Tcur);
    Vcur = get_point(Vcur,Dircur,t_V);
    % Dessin
    if (C.draw)
        col_photon = "r"; if (strcmp(type,"511")), col_photon = "b"; end 
        pts_list = draw_store_lin(pts_list,Mcur,Vcur,col_photon,"-",0.5);
        pts_list = draw_store_pts(pts_list,Vcur,'o','#808080',0.001);
    end
    % Test d'appartenance du point courant au champs de vue
    msg_1 = ""; if(C.verbose),msg_1 = strcat("[Vcur it",num2str(it),"]"); end 
    if (~is_in_FOD(X,Vcur,msg_1))
        is_out = true;
        break;
    end
    % Test de traversée du Rin
    % Nécessairement, le premier segment d'interaction va croiser les cylindres Rfov et Rin
    if (it > 1)
        % Pour le segment [Mcur,Vcur], on cherche à savoir si il intersecte
        % le cylindre Rin (fonction get_intersect_coord_Rin);
        % Si il y a intersection dans la direction de propagation du segment,
        % la fonction retourne deux coefficients st positifs (inf si pas
        % d'intersection et negatif si intersection dans la direction opposée)
        % Si le coefficient de propagation t_V associé à Vcur est supérieur
        % au max des deux coefficients d'intersection, alors le photon est
        % sortir du LXe.
        vec_cross = (Vcur-Mcur)/norm(Vcur-Mcur,2);
        [t_cross_rin1,t_cross_rin2] = get_intersect_coord_Rin(X,Mcur,vec_cross);
        if (t_cross_rin1 >= 0 && ~isinf(t_cross_rin1) && t_cross_rin2 >= 0 && ~isinf(t_cross_rin2))
            if (t_V >= max([t_cross_rin1,t_cross_rin2]))
                cross_rin = true;
                %warning('cross Rin 1')
                break;
            end
        end
    end
    % Simulation d'un effet au point courant
    [eff_id,eff_name] = get_interaction_type(X,Ecur);
    if(C.verbose), fprintf("[INTER_%d] %s \n",it,eff_name); end
    % Dessin
    if (C.draw)
        pts_list = draw_store_pts(pts_list,Vcur,mark(eff_id),col(eff_id),8);
    end
    % Application des modifications associées
    %   - PE : dépot total d'énergie (cdt° while Ecur = 0)
    %   - Compton : dépot partiel et modification de la trajectoire (new Dir,M,Tcur)
    switch eff_id
        case 1 % Rayleigh
        case 2 % Photo-electrique
            E1 = Ecur;
            Ecur = 0;
            Tcur = 0;
            if(C.verbose), fprintf("\tEcur= %g\n\tDépot E1 = %g\n\t=> Ecur = %g\n",Ecur+E1,E1,Ecur); end
        case 3 % Compton
            [E_sc,E1,beta,rad_beta,~] = klein_nishina_reject_method(X,Ecur);
            Ecur = E_sc;
            omega = 360*rand(); rad_omega = deg2rad(omega);
            if(C.verbose),
                tok_diff = "diff"; if (beta >=90), tok_diff = "retro-diff"; end
                fprintf("Angle Compton: [%g](deg) <=> %g(rad)\t=> %s \n",beta,rad_beta,tok_diff);
                fprintf("\tEcur= %g\n\tDépot E1 = %g\n\t=> Ecur = %g\n",Ecur+E1,E1,Ecur);
                fprintf("\tAngle de directrice = [%g] (deg) <=> %g (rad) \n",omega,rad_omega);
            end
            Dircur = get_generatrix_unit_vector(Mcur,Vcur,rad_beta,rad_omega,t_V);
            %
            Mcur = Vcur;
            % Tcur est non nul que pour assurer la direction de propagation d'un photon à 511 (f ou b) 
            Tcur = 0;
    end
    lcl_inter_pts(it,:) = Vcur;
    lcl_dep_energ(it,:) = E1;
    lcl_effect_id(it,:) = eff_id;
    if(C.verbose),fprintf("---\n"); end
end

if(C.verbose),fprintf("it = %d \t is_out = %d \t cross_rin = %d \n",it,is_out,cross_rin); end

if (~is_out && ~cross_rin)
    % Rabotage
    lcl_inter_pts(it+1:end,:) = [];
    lcl_dep_energ(it+1:end,:) = [];
    lcl_effect_id(it+1:end,:) = [];
    % Affectation
    track.N = it;
    track.inter_pts = lcl_inter_pts;
    track.dep_energ = lcl_dep_energ;
    track.effect_id = lcl_effect_id;
    % Sinon, on retourne la structure vide
end

if(C.verbose),fprintf("################# STOP TRACK %g ##################\n",E0); end
test = "";

end

% function [cross_rin] = test_cross_Rin(X,P1,P2)
%     % CF demo cahier 5
%     cross_rin = false; 
% 
%     C1 = [0,0,-X.Ax/2];
%     C2 = [0,0,+X.Ax/2];
%     v = C2 - C1;
% 
%     % Calcul des distance à l'axe du cylindre 
%     dist_P1 = norm(cross((P1-C1),v))/norm(v);
%     dist_P2 = norm(cross((P2-C1),v))/norm(v);
% 
%     fprintf("dist_P1 = %g\ndist_P2 = %g\nRin=%g\n",dist_P1,dist_P2,X.Rin); 
%     if ((dist_P1 > X.Rin && dist_P2 <= X.Rin) || (dist_P1 <= X.Rin && dist_P2 > X.Rin) )
%         cross_rin = true; 
%     end
%     % Les deux points d'interaction doivent ^etre dans la zone de détection
%     if (dist_P1 > X.Rin && dist_P2 > X.Rin)
%         v = v / norm(v);
%         dP = P2 - P1; 
%         u0 = P1 - C1;  
%         dPvz_dzvy = dP(2) * v(3) - dP(3) * v(2); % dy * vz - dz * vy
%         dzvx_dxvz = dP(3) * v(1) - dP(1) * v(3); % dz * vx - dx * vz
%         dxvy_dyvx = dP(1) * v(2) - dP(2) * v(1); % dx * vy - dy * v
%         u0vz_u0zvy = u0(2) * v(3) - u0(3) * v(2); % u0y * vz - u0z * vy
%         u0vx_u0xvz = u0(3) * v(1) - u0(1) * v(3); % u0z * vx - u0x * vz
%         u0vy_u0yvx = u0(1) * v(2) - u0(2) * v(1); % u0x * vy - u0y * vx
%         A = dPvz_dzvy^2 + dzvx_dxvz^2 + dxvy_dyvx^2;
%         B = 2 * (u0vz_u0zvy * dPvz_dzvy + u0vx_u0xvz * dzvx_dxvz + u0vy_u0yvx * dxvy_dyvx);
%         C = u0vz_u0zvy^2 + u0vx_u0xvz^2 + u0vy_u0yvx^2 - X.Rin^2;
%         delta = B^2 - 4 * A * C;
%         if delta < 0
%             cross_rin = false;
%         else
%             t1 = (-B + sqrt(delta)) / (2 * A);
%             t2 = (-B - sqrt(delta)) / (2 * A);
%             if (t1 >= 0 && t1 <= 1) || (t2 >= 0 && t2 <= 1)
%                 cross_rin = true;
%                 i2 = true; 
%             else
%                 cross_rin = false;
%             end
%         end
%     end
%     
% 
% 
%     %X1 = v(3)*(P2(2)-P1(2))
% 
%     test=""; 
% end