function [S] = plane_S_struct(S)
    bound = S.results.N_exp + 1;
    % Rabotage de results; 
    S.results.vox_idx(bound:end,:)=[]; 
    S.results.u_set(bound:end,:) = []; 
    S.results.M(bound:end,:) = []; 
    S.results.Vox_Re_coord(bound:end,:) = []; 
    S.results.M_pr(bound:end,:) = []; 
    S.results.R_pr(bound:end,:) = []; 
    S.results.D1(bound:end,:) = []; 
    S.results.D_pr(bound:end,:) = []; 
    S.results.D2(bound:end,:) = []; 
    S.results.cpt_rm_kn(bound:end,:) = [];
    S.results.csr_gen_dir(bound:end,:) = []; 
    S.results.nb_intersection(bound:end,:) = []; 
    S.results.inter_status(bound:end,:) = []; 
    % Rabotage de export 
    S.export.t(bound:end,:) = [];  
    S.export.chr(bound:end,:) = []; 
    S.export.data(bound:end,:) = []; 
    S.export.intersection_points(bound:end,:) = []; 
end