function [ret] = is_in_FOD(X,V,name)
    verb=true;
    if (nargin<3 || (nargin == 3 && name == ""))
        name = "";  verb = false;
    end
    x = V(1); y = V(2); z = V(3);
    ok = 0;
    msg="Ok!!";
    % ---
    if (z > +X.Ax/2 && ok == 0)
        ok = 1;     msg = "z > +H/2";
    end
    if (z < -X.Ax/2 && ok == 0)
        ok = 2;     msg = "z < -H/2";
    end
    % ---
    if (x^2+y^2 > X.Rout^2 && ok == 0)
        ok = 3;     msg = "x^2+y^2 > Rout";
    end
    if (x^2+y^2 < X.Rin^2 && ok == 0)
        ok = 4;     msg = "x^2+y^2 < Rin";
    end
    % ---
    ret = (ok==0);
    if (ret == false)
        test ="";
    end
    % ---------------------------------------------------------------------
    if (verb)
        fprintf("%s in FOD ? (%d) %s \n",name,ok,msg)
    end


end
