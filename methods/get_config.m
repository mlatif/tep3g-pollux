function [config,nb_detection,inf_msg] = get_config(n_gamma_511,n_gamma_1157)
    % nb_detect_E0=511 = {0,1,2}
    % nb_detect_E0=1.157 = {0,1} U {-1}
    % RMQ:
    %   - 1 détection de type 1157 est composée de 2 intersection;
    %   - le calcul se fait telle que: n_gamma_1157= length(find([naii(V1p),naii(V2p)]))-1;
    %   - si aucune détection V1p ou V2p, alors on aura n_gamma_1157 <= 0

    if (n_gamma_1157 <= 0)
        if (n_gamma_511 == 0)
            config.idx=1;
            config.chr='0';
            config.label = "0G";
            config.part_color = 'w';
        elseif (n_gamma_511 == 1)
            config.idx=2;
            config.chr='1';
            config.label="1G511";
            config.part_color=[0 0.4470 0.7410];
        else % nb_detect_E0=511 = 2
            config.idx=4;
            config.chr='3';
            config.label="2GLOR";
            config.part_color=[0.9290 0.6940 0.1250];
        end
    else% % Sous entendu nb_detect_E0=1.157=1
        if (n_gamma_511 == 0)
            config.idx=3;
            config.chr='2';
            config.label = "1G1157";
            config.part_color = [0.4940 0.1840 0.5560];
        elseif (n_gamma_511 == 1)
            config.idx=5;
            config.chr='4';
            config.label="2GMIX";
            config.part_color=[0.4660 0.6740 0.1880];
        else
            config.idx=6;
            config.chr='5';
            config.label="3G";
            config.part_color=[0.3010 0.7450 0.9330];
        end
    end
    nb_detection = n_gamma_1157 + n_gamma_511;
    inf_msg=strcat("Config[",num2str(config.idx),"]\t#(detect)=",num2str(nb_detection),"\t#(.511)=",num2str(n_gamma_511),"\t#(1.157)=",num2str(n_gamma_1157),"\t~> ",config.label," [",config.chr,"]\n");
    fprintf(inf_msg);
end
