function [eff_id,eff_name] = get_interaction_type(X,E)
% get_interaction_type
% Retourne le type d'interaction tiré aléatoirement
% INPUT:
%   X : la caméra et les grandeurs physiques
%   E : l'énergie du photon incident  
% OUTPUT:
%   eff_id : l'indice de l'effet simulé
%       1 : Rayleigh
%       2 : Photo-Electrique
%       3 : Compton 
%   eff_name : le nom de l'effet simulé 

    mu_hat = mu_interpolation_energie(X,E); 
    sigma_hat = mu_hat./X.rho; 
    sigma_total = sigma_hat*X.cs_data.is_considered;
    u = rand();
    eff_id = 0;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Est-ce que l'effet Rayleigh est considéré
    if (X.cs_data.is_considered(1) == 1)
        % Oui l'effet Rayleigh est considéré 
        sigma_R_norm = sigma_hat(1)/sigma_total;
        sigma_RP_norm = (sigma_hat(1)+sigma_hat(3))/sigma_total; 
        if (u < sigma_R_norm)
            eff_name ="Rayleigh";
            eff_id = 1;
        elseif (sigma_R_norm < u && u < sigma_RP_norm)
            eff_name = "Photo-electrique";
            eff_id = 2;
        else
            eff_name = "Compton";
            eff_id = 3;
        end
    else
        % Non l'effet Rayleigh est négligé 
        % => Y'a s^urement un moyen plus malin de se dire que nécessairement
        % l'effet Rayleigh sera négligé dès lors que l'on fixe la totalité
        % de sa cellule à 0 et qu'on tire U(]0,1[)
        sigma_P_norm = sigma_hat(3)/sigma_total; 
        if ( u < sigma_P_norm)
            eff_name = "Photo-electrique";
            eff_id = 2;
        else
            eff_name = "Compton";
            eff_id = 3;
        end
    end




end
