function [t,E] = distance_xenon_attenuation_2GLOR(X,E0,d,t_init_min)
    at_E0 = find(X.cs_data.Energy==E0);    
    mu_total = X.cs_data.mu(at_E0,:)*X.cs_data.is_considered; %X.cs_data.mu(at_E0,4);                  
    % Expression du Free Mean Path 
    u = rand();     
    p = - log(u); 
    % Tentative d'expression de la distance suivant l'axe depuis le point V1
    dist_gamma = p/mu_total;
    t = sign(t_init_min)*(dist_gamma/norm(d,2));
    % L'énergie est totalement déposée
    E = E0; 
    tok = "F";  if (t_init_min<0), tok = "B"; end 
    
    % ---------------------------------------------------------------------
    %fprintf("DIST_ATT_2GLOR (%s) \t : t_init = %g\tp = %g\tt = %g\n",tok,t_init_min,p,t);
end