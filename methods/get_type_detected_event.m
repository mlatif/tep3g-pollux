function [config,inf_msg] = get_type_detected_event(n_gamma_511,n_gamma_511_diff,n_gamma_1157_diff,tok_dir_511)
    config.idx = 0;
    config.label ="err";
    config.chr='0';
    config.part_color=[0 0 0];
    inf_msg = "";
    % ----
    dir_511 = ""; 
    if (nargin >3)
        dir_511 = "_"+tok_dir_511; 
    end
    % ----
    %fprintf("#(V) = %d \t #(V*) = %d \t #(V') = %d \n",n_gamma_511,n_gamma_511_diff,n_gamma_1157_diff);
    if (n_gamma_511 == 0 && n_gamma_511_diff == 0 && n_gamma_1157_diff == 0)
        config.idx=1;
        config.chr='0';
        config.label = "0G";
        config.part_color = 'w';
    end

    if (n_gamma_511 == 0 && n_gamma_511_diff == 2 && n_gamma_1157_diff == 0)
        config.idx=2;
        config.chr='1';
        config.label="1G511"+dir_511;
        config.part_color=[0 0.4470 0.7410];
    end

    if (n_gamma_511 == 2 && n_gamma_511_diff == 0 && n_gamma_1157_diff == 0)
        config.idx=4;
        config.chr='3';
        config.label="2GLOR";
        config.part_color=[0.9290 0.6940 0.1250];
    end

    if (n_gamma_511 == 0 && n_gamma_511_diff == 0 && n_gamma_1157_diff == 2)
        config.idx=3;
        config.chr='2';
        config.label = "1G1157";
        config.part_color = [0.4940 0.1840 0.5560];
    end

    if (n_gamma_511 == 0 && n_gamma_511_diff == 2 && n_gamma_1157_diff == 2)
        config.idx=5;
        config.chr='4';
        config.label="2GMIX"+dir_511;
        config.part_color=[0.4660 0.6740 0.1880];
    end

    if (n_gamma_511 == 2 && n_gamma_511_diff == 0 && n_gamma_1157_diff == 2)
        config.idx=6;
        config.chr='5';
        config.label="3G";
        config.part_color=[0.3010 0.7450 0.9330];
    end
    nb_detection =  n_gamma_511 + n_gamma_511_diff + n_gamma_1157_diff;
    inf_msg=strcat("Config[",num2str(config.idx),"]\t#(detect)=",num2str(nb_detection),"\t#(.511)=",num2str(n_gamma_511),"\t#(.511|diff)=",num2str(n_gamma_511_diff),"\t#(1.157|diff)=",num2str(n_gamma_1157_diff),"\t~> ",config.label," [",config.chr,"]\n");
    %fprintf(inf_msg);
    test = "";
end
