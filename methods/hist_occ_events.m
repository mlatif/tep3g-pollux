function [] = hist_occ_events(C,S,class_list,cur_class)
    show_cur_class = true;
    if (nargin < 4), show_cur_class = false;    end
    data = S.results.occ_config_count;
    lab = C.Classif(:,3);
    th_class_idx = (1:length(data)-1);
    %
    sub_data = data(class_list);
    sub_lab = lab(class_list);
    if (show_cur_class)
        sub_cur_class_idx = find(class_list==cur_class);
    end
    

     
   



    figure; 
        hold on; %box on;
        idx = categorical(1:length(sub_data));
        b = bar(idx,sub_data);
        b.FaceColor = 'flat';
        if (show_cur_class)
            b.CData(sub_cur_class_idx,:) = [.5 0 .5];
        end
        set(gca,'xticklabel',sub_lab,'FontSize',12);
        ylim([0,max(S.results.occ_config_count)+10])
        %
        xtips2 = b(1).XEndPoints;
        ytips2 = b(1).YEndPoints;
        labels2 = string(b(1).YData);
        text(xtips2,ytips2,labels2,'HorizontalAlignment','center','VerticalAlignment','bottom','Interpreter','latex','FontSize',12);

        
    % print -depsc ZProf_FR
      
end