if (C.draw)
    draw_pts(M,'x','r',4);
    % Demi-LOR f
    if (succ_lor_f)
        pts_list = draw_store_pts(pts_list,V0_f,'.','k');    % Point d'éntrée dans la zone active (f)
        i1_f = get_point(M,Dir.D1,t_max_f);
        pts_list = draw_store_lin(pts_list,M,i1_f,'#808080',"--",1);
    end
    % Demi-LOR b
    if (succ_lor_b)
        pts_list = draw_store_pts(pts_list,V0_b,'.','k');    % Point d'éntrée dans la zone active (b)
        i1_b = get_point(M,Dir.D1,t_max_b);
        pts_list = draw_store_lin(pts_list,M,i1_b,'#808080',"--",1);
    end
    % generatrice Cone - f
    if (succ_csr_f)
        t2_hat = get_intersect_coord_csr(X,V1s,csr_gen_dir);
        i2_f = get_point(V1s,csr_gen_dir,t2_hat);
        pts_list = draw_store_lin(pts_list,V1s,i2_f,'#808080',"--",1);
    end
    % generatrice Cone - b
    if (succ_csr_b)
        t2_hat = get_intersect_coord_csr(X,V1s,csr_gen_dir);
        i2_b = get_point(V1s,csr_gen_dir,t2_hat);
        pts_list = draw_store_lin(pts_list,V1s,i2_b,'#808080',"--",1);
    end
    % Point d'interaction sans diffusion
    if (succ_lor_f && succ_lor_b)
%                         pts_list = draw_store_pts(pts_list,V1,'x',config.part_color(1,:));
%                         pts_list = draw_store_pts(pts_list,V2,'x',config.part_color(1,:));
        draw_pts(V1,'*',config.part_color(1,:),8);
        draw_pts(V2,'*',config.part_color(1,:),8);
    end
    % Point d'interaction si diffusion
    if (succ_csr_f || succ_csr_b)
%                         pts_list = draw_store_pts(pts_list,V1s,'x',config.part_color(1,:));
%                         pts_list = draw_store_pts(pts_list,V2s,'x',config.part_color(2,:));
        draw_pts(V1s,'*',config.part_color(1,:),8);
        draw_pts(V2s,'*',config.part_color(2,:),8);
    end
end
