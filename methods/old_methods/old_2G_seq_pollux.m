%{
% Remarque :
%   - Dans le cas où je ne détecte qu'un photon sur les deux 511 émis, je dois calculer un second point. Je peux override les variables de stockages d'un cas à l'autre car normalement, on s'en fiche des énergies dans le cas 2GLOR.
% SIMULATION:
% Dans ce cas là, on vient écraser la valeur de M avec celle après le positron range La valeur réelle de l'émission a été stockée avant
%
t_min_f = inf; t_max_f = inf; t_min_b =inf; t_max_b = inf;
t2_min = inf ; t2_max =inf;
csr_dir = inf;
csr_list = [];
[M,R_pr] = positron_range(M,Dir.D_pr,S.Conf.Pos_rng);

% b : back, f : forward
[t_min_f,t_min_b] = get_intersect_coord_Rin(X,M,Dir.D1);
% Revient à traiter séquentiellement les deux intersection rayon/cylindre rin
%   - D'abord vers l'avant - avec t>=0
fprintf("t_min_f = %g \t t_min_b = %g \n",t_min_f,t_min_b)

if (~isinf(t_min_f))
    V0_f = get_point(M,Dir.D1,t_min_f);
    pts_list = draw_store_pts(pts_list,V0_f,'^','c');
    %
    [t_max_f] = get_intersect_coord_Rout(X,M,Dir.D1,t_min_f);
    %
    if (~isinf(t_min_b))    % Je sais que je peux tracer une LOR (car j'ai son opposé)
        [t_V1_f,E1a] = distance_xenon_attenuation_2GLOR(X,E0,Dir.D1,t_min_f);
    else                    % Je sais que c'est nécessairment une CSR
        [t_V1_f,E1a] = distance_xenon_naive(t_min_f,t_max_f,E0);
    end
    %
    V1 = get_point(V0_f,Dir.D1,t_V1_f);
    %
    if (~is_in_FOD(n,X,V1,"V1 - ETAPE 1 - f"))
        V1 = inf(3,1);   t_V1_f = inf;
        % ATTENTION Dans le cas où le point de sortie est en dehors de la zone active => Je peux quand m^eme
        % récupérer l'autre point (ici ça sera t_min_b) pour tracer un cone ... non ??
        t_min_f = inf;
    end
    %
    if (isinf(t_min_b) && ~isinf(t_V1_f))      % Je sais que je n'aurai pas de LOR => CSR si je ne suis pas hors de la zone active

        csr_dir = get_generatrix_unit_vector(M,V1,beta_fixe,omega_fixe,t_V1_f);     % !!!!!!!! beta_fixe et omega_fixe
        t2_min = 0;
        [t2_max] = get_intersect_coord_csr(X,V1,csr_dir,t_V1_f,beta_fixe,omega_fixe,n);
        [t_V2p_f,E2a_f] = distance_xenon_naive(t2_min,t2_max,E1a);
        V2p = get_point(V1,csr_dir,t_V2p_f);
        if (~is_in_FOD(n,X,V2p,"V2p - ETAPE 2 - f"))
            V2p = inf(3,1); t_V2p_f = inf;
        end
        if (POKA_TODO),fprintf("[2|...|att_si_mix|t_min_f]\n\tV1p<-V1\tV2p<-V2p\n\tE1a<-E1a\tE2a<-E2a\n\t=> Calculer V2p,E2a à partir de V1p,i_ri_f/t_V1p/E1a\n");end
        [csr_list] = draw_store_csr(pts_list,X,M,V1,beta_fixe,t_V1_f,n,'#77AC30');

    end

end
%   - Puis vers l'arrière - avec t<=0
if (~isinf(t_min_b))
    V0_b = get_point(M,Dir.D1,t_min_b);
    pts_list = draw_store_pts(pts_list,V0_b,'v','c');
    [t_max_b] = get_intersect_coord_Rout(X,M,Dir.D1,t_min_b);
    if (~isinf(t_min_f))    % Je sais que je peux tracer une LOR (car j'ai son opposé)
        [t_V1_b,E1a] = distance_xenon_attenuation_2GLOR(X,E0,Dir.D1,t_min_b);
    else % Je sais que c'est nécessairment une CSR
        [t_V1_b,E1a] = distance_xenon_naive(t_min_b,t_max_b,E0);
    end
    V2 = get_point(V0_b,Dir.D1,t_V1_b);

    if (~is_in_FOD(n,X,V2,"V2 - ETAPE 1 - b"))
        V2 = inf(3,1);   t_V1_b = inf;
        % ATTENTION Dans le cas où le point de sortie est en dehors de la zone active => Je peux quand m^eme
        % récupérer l'autre point (ici ça sera t_min_f) pour tracer un cone ... non ??
        % Bon là, ça aide pas puisque l'on aura déjà jeté l'autre point.
        t_min_b = inf;
    end

    if (isinf(t_min_f) && ~isinf(t_V1_b))   % Je sais que je n'aurai pas de LOR => CSR si je ne suis pas hors de la zone active
        csr_dir = get_generatrix_unit_vector(M,V2,beta_fixe,omega_fixe,t_V1_b);     % !!!!!!!! beta_fixe et omega_fixe
        t2_min = 0;
        [t2_max] = get_intersect_coord_csr(X,V2,csr_dir,t_V1_b,beta_fixe,omega_fixe,n);
        [t_V2p_b,E2a_b] = distance_xenon_naive(t2_min,t2_max,E1a);
        V2p = get_point(V2,csr_dir,t_V2p_b);
        if (~is_in_FOD(n,X,V2p,"V2p - ETAPE 2 - b"))
            V2p = inf(3,1); t_V2p_b = inf;
        end


        if (POKA_TODO),fprintf("[2|...|att_si_mix|t_min_b]\n\tV1p<-V2\tV2p<-V2p\n\tE1a<-E1a\tE2a<-E2a\n\t=> Calculer V2p,E2a à partir de V2,i_ri_b/t_V1b/E1a\n");end
        [csr_list] = draw_store_csr(pts_list,X,M,V2,beta_fixe,t_V1_b,n,'y');
    end

end

draw_point(M,'x','r',4);
% Demi-LOR f
i1_f = get_point(M,Dir.D1,t_max_f);
pts_list = draw_store_lin(pts_list,M,i1_f,'#808080',"--",1);

% Demi-LOR b
i1_b = get_point(M,Dir.D1,t_max_b);
pts_list = draw_store_lin(pts_list,M,i1_b,'#808080',"--",1);

% Axe Cone - f
i2_f = get_point(V1,csr_dir,t2_max);
pts_list = draw_store_lin(pts_list,V1,i2_f,'#808080',"--",1);
% Axe Cone _ b
i2_b = get_point(V2,csr_dir,t2_max);
pts_list = draw_store_lin(pts_list,V2,i2_b,'#808080',"--",1);

% Point d'interaction
pts_list = draw_store_pts(pts_list,V1,'^','m');
pts_list = draw_store_pts(pts_list,V2,'v','m');
% Le point d'interaction diffusé
pts_list = draw_store_pts(pts_list,V2p,'s','m');


%
if (~isempty(csr_list))
    test="";
    erase_all(csr_list)
end
%}
