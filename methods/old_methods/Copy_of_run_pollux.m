function [S] = run_pollux(C,X,P)

    POKA_TODO = true;
    REM_RAY = C.rem_ray;
    REM_POINT = C.rem_pts;
    POZ_NOT_IN_FOD = C.poz_niFOD;
    ANIM = C.create_anim;
    verb_lim = 1;
    % Transfert des informations du Phantom :
    S.Ph.D = P.D;
    S.Ph.J = P.J;
    S.Ph.Rc = P.Rc;
    S.Ph.V = P.V;
    % Transfert des informations de la config
    S.Conf.N = C.N;
    S.Conf.Case = C.Case;
    S.Conf.rng = C.rng;
    S.Conf.Pos_rng = C.Pos_rng;
    %
    S.Classif = C.Classif;
    S.preset_type = C.preset_type;
    %
    S.results.activity = zeros(P.D(1),P.D(2),P.D(3));
    %
    S.results.M = 0;                        % M : nombre de mesures observées (qui peut ^etre différent du nombre N si on a eu des pertes)
    S.results.calib_factor = 1.;
    S.results.lambda = zeros(1,P.J);

    S.results.vox_idx = zeros(C.N,1);
    S.results.u_set = zeros(C.N,1);         % stocker les tirages aléa
    S.results.M = zeros(C.N,3);             % Point d'émission;
    S.results.Vox_Re_coord = zeros(C.N,3);      % Coordonnées réelles du voxel contenant le point d'émission
    S.results.M_pr = zeros(C.N,3);          % Point d'émission après positron range;
    S.results.R_pr = zeros(C.N,3);          % Valeur du positron range
    S.results.D1 = zeros(C.N,3);            % Le vecteur directionnel 1 - Pour la LOR
    S.results.D_pr = zeros(C.N,3);          % Le vecteur directionnel pour le positron range
    S.results.D2 = zeros(C.N,3);            % Le vecteur directionnel 2 - Pour le 3ème photon
    S.results.axe_cone = zeros(C.N,6);      % Les vecteur unitaires des axes MV1 ->  1:3 vers l'avant, 3:6 vers l'arrière
    S.results.ort_plan = zeros(C.N,4);      % Coordonnées du plan ortho le long de l'axe_cone

    S.results.occ_event = zeros(1,4);       % 0...3 detections

    S.results.occ_config = inf(6,C.N);     % une pseudo H-map pour stocker les configurations détectées - s'aider d'une TDV pour vérifier la classif
    S.results.occ_config_count = zeros(1,6);
    % -----------------------------------------------------------------
    foc_idx = 0;                            % Un indice d'évènements "purs" que l'on doit regarder
    switch C.Case
        case "1", foc_idx = 3;              % 1G1157 - par défaut, en "1" on ne tire que des 1157
        case "2", foc_idx = 4;              % 2GLOR
        case "3", foc_idx = 6;              % 3G
    end

    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    S.export.t = zeros(C.N,1,'uint32');
    S.export.chr = strings(C.N,1);          % TODO Voir pour potentiel pb à la conversion
    S.export.data = zeros(C.N,22,'double');
    % Sommets à LOR
    V1_rng = (1:3);         % Interaction type 2 photons - Sommet 1
    V2_rng = (4:6);         % Interaction type 2 photons - Sommet 2
    % Sommets à CSR - Cas général
    V1p_rng = (7:9);        % Interaction type 1 photon - Sommet 1
    V2p_rng = (10:12);      % Interaction type 1 photon - Sommet 2
    % Sommets à CSR - Cas mix
    V1s_rng = (13:15);      % Interaction type 1 photon - Sommet 1
    V2s_rng = (16:18);      % Interaction type 1 photon - Sommet 2
    % Dép^ot d'energie - Cas général et mix
    E1a_rng = 19;   E2a_rng = 20;   %  type 1 photons - Sommets 1 et 2
    E1b_rng = 21;   E2b_rng = 22;   %  type 1 photons - Sommets 1 et 2


    S.export.idx.V1_rng = V1_rng;  S.export.idx.V2_rng = V2_rng;
    S.export.idx.V1p_rng = V1p_rng;  S.export.idx.V2p_rng = V2p_rng;
    S.export.idx.V1s_rng = V1s_rng;  S.export.idx.V2s_rng = V2s_rng;
    S.export.idx.E1a = E1a_rng;    S.export.idx.E2a = E2a_rng;
    S.export.idx.E1b = E1b_rng;    S.export.idx.E2b = E2b_rng;
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------

    S.terminal = false;                     % condition d'arr^et retournée en dehors de la boucle
    % -----------------------------------------------------------------
    if (C.draw && C.N >= C.N_max), C.draw = false;  verb_lim = C.N_max; end
    if (C.preset_type == "time"), C.draw = false;end
    if (C.create_anim && C.N>C.N_max && C.draw)
        ANIM = false;
    else
        if (C.draw && C.create_anim)
            manage_anim(C,0);
        end
    end
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    % bloc de dessin
    if (C.draw)
        figure;
        draw_xemis(X,P,ANIM)
        if (ANIM), [frames] = manage_anim(C,1);    end
    end
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    terminal = false;
    n = 0;
    % ---
    while (~terminal)
        n = n + 1;
        % -----------------------------------------------------------------
        [S,M,Dir,info] = simulation_emission(P,C,S,n);
        % -----------------------------------------------------------------
        nb_detection = 0;
        n_gamma_511 = 0;  % nb de particules détectées avec une énergue à 511
        n_gamma_1157= 0;  % nb de particules détectées avec une énergie à 1.157
        % ETAPE 1 : INITIALISATION
        [E0,E0_prime,P_gamma_511,P_gamma_1157] = init_photon_energy_count(C.Case);
        % -----------------------------------------------------------------
        R_pr = inf;
        % -----------------------------------------------------------------
        V1 = inf(3,1);  V2 = inf(3,1);  V1p = inf(3,1); V2p = inf(3,1); V1s = inf(3,1); V2s = inf(3,1);
        axe_rng = inf(3,1);
        plan_coord = inf(4,1);
        inter_1 = inf(2,3);
        inter_2 = inf(2,3);
        inter_3 = inf(2,3);
        in_fod_2G_b = true;
        in_fod_2G_f = true;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % ATTENTION - IL FAUT BIEN FAIRE LA DIFFERENCE ENTRE I{123} qui
        % est le point d'interserction et V{123} qui est le point de
        % dépot d'énergie (interaction)
        switch C.Case
            case "1"
                % ETAPE 2 : SIMULATION
                % i1_ri : intersection n°1 avec cylindre intérieur
                % i1_ro : intersection n°1 avec cylindre exterieur
                % Intersection avec le bord intérieur (Rin)
                [i1_ri,~,t1_min,~] = get_intersect_coord_Rin(X,M,Dir.D1,1);
                % On ne s'amuse à tester chercher la seconde intersection ssi la première est valide
                if (naii(i1_ri))
                    % Intersection avec le bord exterieur (Rout)
                    [i1_ro,t1_max] = getIntersectCoord_3D_Rout(X,M,Dir.D1,t1_min);
                    % P(t) = E + t * V avec t \in [t1_min,t1_max] => En déduire la position du point V1 avec t qq part dans entre t1_min et t1_max;
                    [t_V1p,E1a] = distance_xenon_naive(t1_min,t1_max,E0_prime);
                    V1p = i1_ri + t_V1p*Dir.D1;
                    % ---
                    inter_1 = [i1_ri;i1_ro];
                    % ---
                    %%% >>>>
                    %{
                    beta_fixe = deg2rad(15);
                    omega_fixe = deg2rad(45);
                    m = get_generatrix_unit_vector(M,V1p,beta_fixe,omega_fixe,t_V1p)
                    [i2_ri,~,t2_min,~] = getIntersectCoord_3D_Rin(X,V1p,m,1)
                    [i2_ro,t2_max] = getIntersectCoord_3D_Rout(X,V1p,m,t2_min)
                    [t_V2p,E2a] = distance_xenon_naive(t2_min,t2_max,E1a)

                    inter_11 = [i2_ri;i2_ro];

                    V2p = V1p + t_V2p*m
                    %}
                    %%% <<<<
                    % Maintenant faire la m^eme chose pour V2p et E2a modulo la cohérence "sur le cone"
                    if (POKA_TODO), fprintf("[1|1G...]\n\tV1p<-V1p\tV2p<-V2p\n\tE1a<-E1a\tE2a<-E2a\n\t=> Calculer V2p,E2a à partir de V1p/t_V1p/E1a\n"); end
                    %{
                        [...]
                    %}
                    C_axe = get_unit_vector(M,V1p);
                    axe_rng = 1:3;

                    plan_coord = get_orth_plane(V1p,C_axe);


                    test ="";
                end
                % ETAPE 3 : INFERENCE
                n_gamma_511 = 0;
                n_gamma_1157= length(find(naii(V1p)));
                [config,nb_detection,inf_msg] = get_config(n_gamma_511,n_gamma_1157);
                % ETAPE 4 : STOCKAGE
                S.export.chr(n)= config.chr;
                S.export.data(n,V1p_rng) = V1p;
                S.export.data(n,E1a_rng) = E1a;
                % et stocket les V2p et E2a
                %{
                    [...]
                %}

                % ---------------------------------------------------------
                % ---------------------------------------------------------
                % ---------------------------------------------------------
            case "2"
                % Remarque :
                %   - Dans le cas où je ne détecte qu'un photon sur les deux 511 émis, je dois calculer un second point. Je peux override les variables de stockages d'un cas à l'autre car normalement, on s'en fiche des énergies dans le cas 2GLOR.
                % ETAPE 2 : SIMULATION
                % Dans ce cas là, on vient écraser la valeur de M avec celle après le positron range
                % La valeur réelle de l'émission a été stockée avant
                [M,R_pr] = positron_range(M,Dir.D_pr,S.Conf.Pos_rng);

                % b : back, f : forward
                [i_ri_f,i_ri_b,t_min_f,t_min_b] = getIntersectCoord_3D_Rin(X,M,Dir.D1,2);

                % Revient à traiter séquentiellement les deux données
                %   - D'abord vers l'avant - avec t>=0
                if (naii(i_ri_f))
                    [i_ro_f,t_max_f] = getIntersectCoord_3D_Rout(X,M,Dir.D1,t_min_f);
                    inter_1 = [i_ri_f;i_ro_f];
                    % ---
                    if (naii(i_ri_b))
                        % Je suis dans un cas 2GLOR -> Je peux calculer
                        %distance_xenon_attenuation_2GLOR(X,E_init,v,t_init_min,t_init_max)
                        [t_V1_f,E1a] = distance_xenon_attenuation_2GLOR(X,E0,Dir.D1,t_min_f);
                    else
                        [t_V1_f,E1a] = distance_xenon_naive(t_min_f,t_max_f,E0);
                    end
                    % ---
                    V1 = i_ri_f + t_V1_f*Dir.D1;
                    % ---
                    % Est ce qu'après le parcours dans le Xénon, je suis encore dans la caméra
                    in_fod_2G_f = is_in_FOD(n,X,V1);
                    if (~in_fod_2G_f)
                        V1 = inf(3,1);  i_ri_f = inf;
                    end
                    % ---
                    % Si je sais dejà que je n'aurai pas de LOR avec le point opposé :
                    if (~naii(i_ri_b))
                        if (POKA_TODO),fprintf("[2|...|att_si_mix]\n\tV1p<-V1\tV2p<-V2p\n\tE1a<-E1a\tE2a<-E2a\n\t=> Calculer V2p,E2a à partir de V1p,i_ri_f/t_V1p/E1a\n");end
                        C_axe = get_unit_vector(M,V1);
                        axe_rng = 1:3;
                        % Maintenant faire la m^eme chose pour V2p et E2a modulo la cohérence "sur le cone" au départ du point V1
                        %{
                            [...]
                        %}



                    end


                end
                %   - Puis vers l'arrière - avec t<=0
                if (naii(i_ri_b))
                    [i_ro_b,t_max_b] = getIntersectCoord_3D_Rout(X,M,Dir.D1,t_min_b);
                    inter_2 = [i_ri_b;i_ro_b];
                    if (naii(i_ri_f))
                        % Je suis dans un cas 2GLOR -> Je peux calculer
                        %distance_xenon_attenuation_2GLOR(X,E_init,v,t_init_min,t_init_max)
                        [t_V1_b,E1a] = distance_xenon_attenuation_2GLOR(X,E0,Dir.D1,t_min_b);
                    else
                        [t_V1_b,E1a] = distance_xenon_naive(t_min_b,t_max_b,E0);    % On s'en fiche des énergies en LOR - sauf si on doit recalculer un c^one après
                    end
                    % ---
                    V2 = i_ri_b + t_V1_b*Dir.D1;
                    % ---
                    % Est ce qu'après le parcours dans le Xénon, je suis encore dans la caméra
                    in_fod_2G_b = is_in_FOD(n,X,V2);
                    if (~in_fod_2G_b)
                        V2 = inf(3,1);  i_ri_b = inf;
                    end
                    % ---
                    if (~naii(i_ri_f))
                        if (POKA_TODO),fprintf("[2|...|att_si_mix]\n\tV1p<-V2\tV2p<-V2p\n\tE1a<-E1a\tE2a<-E2a\n\t=> Calculer V2p,E2a à partir de V2,i_ri_b/t_V1b/E1a\n");end
                        C_axe = get_unit_vector(M,V2);
                        axe_rng =4:6;
                        % Maintenant faire la m^eme chose pour V2p et E2a modulo la cohérence "sur le cone" au départ du point V2
                        %{
                            [...]
                        %}
                    end

                end
                % ETAPE 3 : INFERENCE
                n_gamma_511 =  length(find([naii(V1),naii(V2)]));
                n_gamma_1157= 0;
                [config,nb_detection,inf_msg] = get_config(n_gamma_511,n_gamma_1157);

                % ETAPE 4 : STOCKAGE
                %   ATTENTION : Disjonction de cas possibles 2GLOR, 1G511 ou 0G
                S.export.chr(n)= config.chr;
                % type 2GLOR (chr='3')
                if (config.chr=='3')
                    S.export.data(n,V1_rng) = V1;
                    S.export.data(n,V2_rng) = V2;
                elseif (config.chr=='1')
                    if (naii(i_ri_f) && ~naii(i_ri_b))
                        % On a un CSR au départ du point V1 car on a pas de LOR (il manque V2)
                        S.export.data(n,V1p_rng) = V1;
                        S.export.data(n,E1a_rng) = E1a;
                        % et stocker les V2p et E2a
                        %{
                            [...]
                        %}

                    elseif (~naii(i_ri_f)&&naii(i_ri_b))
                        % On a un CSR au départ du point V2 car on a pas de LOR (il manque V1)
                        S.export.data(n,V1p_rng) = V2;
                        S.export.data(n,E1a_rng) = E1a;
                        % et stocker les V2p et E2a
                        %{
                            [...]
                        %}
                    end
                else % Necessairement 0G
                    % Nothing to do here my Lord
                end

                test ="";
                % ---------------------------------------------------------
                % ---------------------------------------------------------
                % ---------------------------------------------------------
            case "3"
                % ETAPE 1 : INITIALISATION
                %   Remarque : tout ce qui est relatif au photon 1.157 est marqué par un "prime"
                %   Pour la CSR : V1p, V2p, E1a, E2a
                %   Pour la LOR : V1,V2 si LOR détectée
                %                 V1, (V1s,V2s,E1b,E2b) si DEMI-LOR avec V1 détecté
                %                 V2, (V1s,V2s,E1b,E2b) si DEMI-LOR avec V2 detecté
                % ETAPE 2 : SIMULATION
                % ETAPE 2.1 : on commence par émettre le photon 1.157 pour déterminer la CSR (arbitrairement)
                [i1_ri,~,t1_min,~] = getIntersectCoord_3D_Rin(X,M,Dir.D1,1);
                if (naii(i1_ri))
                    [i1_ro,t1_max] = getIntersectCoord_3D_Rout(X,M,Dir.D1,t1_min);
                    [t_V1p,E1a] = distance_xenon_naive(t1_min,t1_max,E0_prime);
                    V1p = i1_ri + t_V1p*Dir.D1;
                    inter_1 = [i1_ri;i1_ro];
                    % Maintenant faire la m^eme chose pour V2p et E2a modulo la cohérence "sur le cone"
                    if (POKA_TODO),fprintf("[3|1G1157]\n\tV1p<-V1p\tV2p<-V2p\n\tE1a<-E1a\tE2a<-E2a\n\t=> Calculer V2p à partir de V1p/t_V1p/E1a\n");end
                    C_axe = get_unit_vector(M,V1p);
                    axe_rng = 1:3;
                    %{
                        [...]
                    %}

                end

                % --- *** ---
                % ETAPE 2.2 : on enchaine ensuite avec le calcul de la LOR
                % Dans ce cas là, on vient écraser la valeur de M avec celle après le positron range
                % La valeur réelle de l'émission a été stockée avant
                % On aura géré le cas cone avant donc on peut écraser la valeur de M
                [M,R_pr] = positron_range(M,Dir.D_pr,S.Conf.Pos_rng);
                % b : back, f : forward
                [i_ri_f,i_ri_b,t_min_f,t_min_b] = getIntersectCoord_3D_Rin(X,M,Dir.D2,2);
                % Revient à traiter séquentiellement les deux données
                % ETAPE 2.2.1 : D'abord vers l'avant - avec t>=0
                if (naii(i_ri_f))
                    [i_ro_f,t_max_f] = getIntersectCoord_3D_Rout(X,M,Dir.D2,t_min_f);
                    inter_2 = [i_ri_f;i_ro_f];
                    if (naii(i_ri_b))
                        % Je suis dans un cas 2GLOR -> Je peux calculer
                        [t_V1_f,E1b] = distance_xenon_attenuation_2GLOR(X,E0,Dir.D2,t_min_f);
                    else
                        [t_V1_f,E1b] = distance_xenon_naive(t_min_f,t_max_f,E0);
                    end
                    V1 = i_ri_f + t_V1_f*Dir.D2;
                    in_fod_2G_f = is_in_FOD(n,X,V1);
                    if (~in_fod_2G_f)
                        V1 = inf(3,1); i_ri_f = inf;
                    end
                    % ---
                    % Si je sais dejà que je n'aurai pas de LOR avec le point opposé :
                    if (~naii(i_ri_b))
                        if (POKA_TODO),fprintf("[3|2|i_ri_f,i_ri_b=inf]\n\tV1s<-V1\t V2s<-V2s\n\tE1b<-E1b\tE2b<-E2b\n\t=>Calculer V2s à partir de V1,i_ri_f/t_V1_f/E1b\n");end
                        C_axe = get_unit_vector(M,V1);
                        axe_rng = 1:3;
                        % Maintenant faire la m^eme chose pour V2s et E2b modulo la cohérence "sur le cone" au départ du point V1
                        %{
                            [...]
                        %}
                    end


                end
                % ETAPE 2.2.2 : Puis vers l'arrière - avec t<=0
                if (naii(i_ri_b))
                    [i_ro_b,t_max_b] = getIntersectCoord_3D_Rout(X,M,Dir.D2,t_min_b);
                    inter_3 = [i_ri_b;i_ro_b];
                    if (naii(i_ri_f))
                        % Je suis dans un cas 2GLOR -> Je peux calculer
                        [t_V1_b,E1b] = distance_xenon_attenuation_2GLOR(X,E0,Dir.D2,t_min_b);
                    else
                        [t_V1_b,E1b] = distance_xenon_naive(t_min_b,t_max_b,E0);
                    end

                    V2 = i_ri_b + t_V1_b*Dir.D2;
                    in_fod_2G_b = is_in_FOD(n,X,V2);
                    if (~in_fod_2G_b)
                        V2 = inf(3,1); i_ri_b = inf;
                    end


                    if (~naii(i_ri_f))
                        if (POKA_TODO),fprintf("[3|2|i_ri_f=inf,i_ri_b]\n\tV1s<-V2\t V2s<-V2s\n\tE1b<-E1b\tE2b<-E2b\n\t=>Calculer V2s à partir de V2,i_ri_b/t_V1_b/E1b\n");end
                        C_axe = get_unit_vector(M,V2);
                        axe_rng = 4:6;
                        % Maintenant faire la m^eme chose pour V2s et E2b modulo la cohérence "sur le cone" au départ du point V2
                        %{
                            [...]
                        %}
                    end

                end

                % ETAPE 3 : INFERENCE
                n_gamma_511 =  length(find([naii(V1),naii(V2)]));
                n_gamma_1157= length(find(naii(V1p)));
                [config,nb_detection,inf_msg] = get_config(n_gamma_511,n_gamma_1157);
                % ETAPE 4 : STOCKAGE
                S.export.chr(n)= config.chr;
                switch config.chr
                    case '0'
                        % Nothing to do my Lord

                    case '1' %1G511
                        if (naii(i_ri_f) && ~naii(i_ri_b))
                            % On a un CSR au départ du point V1 car on a pas de LOR (il manque V2)
                            % -> Définition de la CSR 511 vers l'avant: V1 (dans V1p), V2s (dans V2p), E1b (dans E1a), E2b (dans E2a)
                            S.export.data(n,V1p_rng) = V1;
                            %S.export.data(n,V2p_rng) = V2s;
                            S.export.data(n,E1a_rng) = E1b;
                            %S.export.data(n,E2a_rng) = E2b;
                            test ="";
                        elseif(~naii(i_ri_f) && naii(i_ri_b))
                            % On a un CSR au départ du point V2 car on a pas de LOR (il manque V1)
                            % -> Définition de la CSR 511 vers l'arriere: V2 (dans V1p), V2s (dans V2p), E1b (dans E1a), E2b (dans E2a)
                            S.export.data(n,V1p_rng) = V2;
                            %S.export.data(n,V2p_rng) = V2s;
                            S.export.data(n,E1a_rng) = E1b;
                            %S.export.data(n,E2a_rng) = E2b;
                            test ="";
                        end

                    case '2'    %1G1157
                        % -> Définition de la CSR: V1p, V2p, E1a, E2a
                        S.export.data(n,V1p_rng) = V1p;
                        % S.export.data(n,V2p_rng) = V2p;
                        S.export.data(n,E1a_rng) = E1a;
                        % S.export.data(n,E2b_rng) = E2a;
                        test ="";
                    case '3'    %2GLOR
                        S.export.data(n,V1_rng) = V1;
                        S.export.data(n,V2_rng) = V2;
                    case '4'    %2GMIX
                        % -> Définition de la CSR 1.157: V1p, V2p, E1a, E2a
                        S.export.data(n,V1p_rng) = V1p;
                        % S.export.data(n,V2p_rng) = V2p;
                        S.export.data(n,E1a_rng) = E1a;
                        % S.export.data(n,E2b_rng) = E2a;
                        if (naii(i_ri_f) && ~naii(i_ri_b))
                            % On a un CSR au départ du point V1 car on a pas de LOR (il manque V2)
                            % -> Définition de la CSR 511 vers l'avant: V1 (dans V1s), V2s (dans V2s), E1b, E2b
                            S.export.data(n,V1s_rng) = V1;
                            %S.export.data(n,V2s_rng) = V2s;
                            S.export.data(n,E1b_rng) = E1b;
                            %S.export.data(n,E2b_rng) = E2b;
                            test="";
                        elseif(~naii(i_ri_f) && naii(i_ri_b))
                            % On a un CSR au départ du point V2 car on a pas de LOR (il manque V1)
                            % -> Définition de la CSR 511 vers l'arriere: V2 (dans V1s), V2s (dans V2s), E1b, E2b
                            S.export.data(n,V1s_rng) = V2;
                            %S.export.data(n,V2s_rng) = V2s;
                            S.export.data(n,E1b_rng) = E1b;
                            %S.export.data(n,E2b_rng) = E2b;
                            test="";
                        end
                    case '5'    %3G
                        % -> Définition de la LOR: V1, V2
                        S.export.data(n,V1_rng) = V1;
                        S.export.data(n,V2_rng) = V2;
                        % -> Définition de la CSR: V1p, V2p, E1a, E2a
                        S.export.data(n,V1p_rng) = V1p;
                        % S.export.data(n,V2p_rng) = V2p;
                        S.export.data(n,E1b_rng) = E1a;
                        % S.export.data(n,E2b_rng) = E2a;
                        test ="";
                end

                test ="";
        end
        % ---
        % On ne met à jour la matrice d'activité que si le photon est
        % détecté (donc n'est pas 0G)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


        test ="";
        % ---
        % bloc de dessin
        if (C.draw)


            pause(C.wait_time);
            % --------------------------------------------
            % !!!!!!!!!!!!!!!!!!!!!!

            if (POZ_NOT_IN_FOD && (~in_fod_2G_f || ~in_fod_2G_b))
                pause;
            end

            if (ANIM)
                [frames] = manage_anim(C,2,frames);
            end

            % MENAGE DANS L'AFFICHAGE

        end % if (C.draw)

        % ------------------------------------------------------------------
        % Condition d'arr^et de la simulation
        switch C.preset_type
            case "count"
                % prendre dans le range (2:end) car le premier indice du vecteur correspond aux 0-détections.
                %if (sum(S.results.occ_event(2:end)) == C.N), S.terminal=true; break;  end
                if (S.results.occ_config_count(foc_idx) == C.N), S.terminal=true; break;  end



            case "time"
                 disp("A FAIRE - TODO "); S.terminal = false; break;
            otherwise
        end

%         if (n==C.N_max)
%             disp("BREAK SECU")
%             break;
%         end



    end % while (~terminal)
    % ---
    % on ferme la figure à la fin
    if (C.draw)
        hold off;
    end
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    % -----------------------------------------------------------------
    S.results.N_exp = sum(S.results.occ_event);
    %{
        Je demande N tirages (preset-count) eg. N=100 ;
        Lors de la simulation, je peux en obtenir en fait M=107 car 7 seront sortis mais je m’en fiche, je veux N.
        Je simule mes N tirages voulus.
        Je stocke les résultats dans une matrice C
            - Est de dim 3 car je travaille avec un fantôme en 3D ;
            - Contient des entiers (+1 à chaque fois qu’un voxel est sélectionné).
        Une fois que j’ai obtenu mes N tirages (je peux en avoir fais M, je m’en fiche, j’en voulais N), je calcul l’activité de MON fantômes  (simulé) tq.
        activité = sum_i,j,k C(i,j,k) 	// i <=> x, j <=> y, k <=> z
        Je calcule alors le facteur de calibration tq.
        ECF = activité/N
    %}
    S.results.calib_factor = sum(S.results.activity,'all')/S.Conf.N;

    if (ANIM)
        clf;
        draw_xemis(X,P,ANIM);
        for (i = 1:size(S.results.M,1))
            rectangle('Position',[S.results.Vox_Re_coord(i,1),S.results.Vox_Re_coord(i,2),P.V(1),P.V(2)],'EdgeColor','k');
            plot3(S.results.M(i,1),S.results.M(i,2),S.results.M(i,3),'x',Color="r");
        end
        [frames] = manage_anim(C,3,frames);
    end
    test ="";
end
