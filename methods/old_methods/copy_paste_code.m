%% VENANT DE MAIN 
% Tirer aléatoirement un point d'émission E dans le voxel considéré 
% REMARQUE : on s'autorise à tirer entre [0,1[; et rand() tire entre ]0,1[
% => Potentiel problème;  intmax('uint32') = 4294967295
% https://fr.mathworks.com/matlabcentral/answers/414929-how-do-i-create-a-random-number-from-a-range-that-includes-zero
% Idée : trouver 0 là dedans : 
% Y=[]; for (i=1:1000000000000), Y = [Y,(randi([0, 4294967295],1,3) / 4294967296.0)]; fprintf("%d/1000000000000\t min = %g\n",i,min(Y)); if (ismember(0,Y)), break; end; end

% d = rand(1,3).*P.V; 00
d =  (randi([0, 4294967295],1,3) / 4294967296.0).*P.V;
[a,b,c] = get_axes_voxel_idx(P,100);           
[r_a,r_b,r_c] = get_axes_voxel_coord(P,a,b,c);
E = [r_a + d(1),r_b + d(2), r_c + d(3)];
% -------------------------------------------------------------------------
% Tracé d'une droite passant par le point E d'angle theta 
% ATTENTION - TAN matlab prend des radians
theta = -pi/3; 

nb_req_inter = 2;
V =  [0.0001,0.0001,0.00005];
if (P.D(3) == 1)
    [V1,V2] = getIntersectCoord_2D(X,E,theta);
else 
    [V1,V2] = getIntersectCoord_3D_Rin(X,E,V,nb_req_inter);
end
    
% --------------------------------------../simulator/bin/mod_create_phantom.exe -d 24 24 30 -v 4. 4. 4. -o phantom_emission_simon_3d -c 0. 0. 0. 46. 120. 100. -c 15. 15. 0. 18. 120. 400. -c -20. -20. 0. 10. 60. 0. -x 32 32 1-----------------------------------
% Tracer d'une simulation naive de Pollux
voxel_set = [1:750:P.J];
theta_in = [];
V_in = [];  % 0.0001,0.0001,0.00005]
N_req_inter = 2;
if (P.D(3)==1)
    % naive_pollux_2D(X,P,voxel_set,theta_in)
else
    % naive_pollux_3D(X,P,voxel_set,V_in,N_req_inter)
end
% -------------------------------------------------------------------------
clear('a','b','c','V1','V2','lambda_hat','d','E','j','theta','r_a','r_b','r_c','r','x','y','z')