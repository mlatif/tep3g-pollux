function [] = naive_pollux_2D(X,P,voxel_set,theta_in)
    figure;
    hold on;    grid on;    box on;
    xlim([-X.Rin-10,+X.Rin+10])
    ylim([-X.Rin-10,+X.Rin+10])
        % Ce qui est lié à la caméra (en noir)
        plot(X.O(1),X.O(2),'kx');
        th = 0:pi/50:2*pi;  xunit = X.Rin * cos(th) + X.O(1);    yunit = X.Rin * sin(th) + X.O(2); h = plot(xunit, yunit,'k'); axis equal;
        rectangle('Position',[-X.L,-X.L,2*X.L,2*X.L],'EdgeColor','k','LineStyle','--');
        % Ce qui est lié à l'image (en bleu)
        rectangle('Position',[-P.T(1)/2,-P.T(2)/2,P.T(1),P.T(2)],'EdgeColor','b','LineStyle','-');
        
        for (j=1:P.J)
            [a,b,c] = get_axes_voxel_idx(P,j);           % %fprintf("%d \t (%g,%g,%g)\n",j,a,b,c);
            [r_a,r_b,r_c] = get_axes_voxel_coord(P,a,b,c);
            % -------------------------------------------------------------
            % Ce qui est lié à l'image (en bleu)
            if (ismember(j,voxel_set))
                plot(r_a,r_b,'.',Color="b"); 
                rectangle('Position',[r_a,r_b,P.V(1),P.V(2)],'EdgeColor','b');
            end
            
            % -------------------------------------------------------------
            % Ce qui est lié à une émission (en rouge)
            d =  (randi([0, 4294967295],1,3) / 4294967296.0).*P.V;
            E = [r_a + d(1),r_b + d(2), r_c + d(3)];
            if (ismember(j,voxel_set))
                h1 = plot(E(1),E(2),'x',Color="r"); 
            end
            % -------------------------------------------------------------
            if (isempty(theta_in))
                theta = 0 + rand()*pi;
            else
                theta = theta_in;
            end
            I1 = zeros(3,1);
            I2 = zeros(3,1);
            [I1,I2] = getIntersectCoord_2D(X,E,theta);
            % -------------------------------------------------------------
            if (ismember(j,voxel_set))
                fprintf("j = %d \t E = (%g,%g,%g)\n",j,E(1),E(2),E(3))
                if (theta == 0)
                    fprintf("\ttheta = %g \t cas y = %g \n", theta,E(2))
                    h2 = yline(E(2),'r--');
                elseif (abs(tan(theta))>1e10)
                    fprintf("\ttheta = %g \t cas x = %g \n", theta, E(1))
                    h2 = xline(E(1),'r--');
                else
                    m = tan(theta);
                    n = E(2)-E(1)*tan(theta);
                    fprintf("\ttheta = %g \t cas y = %g x + %g \n", theta, m,n)
                    x = linspace(-X.Rin,X.Rin,100);
                    y = m .* x + n;
                    h2 = line(x,y,'Color','red','LineStyle','--');
                end
            
                if (~isempty(I1) && ~isempty(I2))
                    h3 = plot(I1(1),I1(2),'o',Color="r"); 
                    h4 = plot(I2(1),I2(2),'o',Color="r");
                end
                fprintf("\t\tI1 = (%g,%g,%g)\n\t\tI2 = (%g,%g,%g)\n",I1(1),I1(2),I1(3),I2(1),I2(2),I2(3))
            end

            if (ismember(j,voxel_set))
                pause(0.1)
            end
        end
    
    hold off;

end