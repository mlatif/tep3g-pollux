function [I1,I2] = getIntersectCoord_2D(X,E,theta)
% GETINTERSECTCOORD_2D Retourne les coordonnées d'intersection I1,I2 dans R^3 d'une droite passant par un point d'émission E et inclinée d'un angle theta avec le bord intérieur de la caméra X.
    I1 = inf*ones(3,1); I2 = inf*ones(3,1);
    if (theta == 0)
        %fprintf("theta = %g \t cas y = %g \n", theta,E(2))
        A = 1;
        B = -2*X.O(1);
        C = E(2)^2 - 2*E(2)*X.O(2) + X.O(1)^2 + X.O(2)^2 - X.Rin^2;
        delta = B^2 -4*A*C;
        I1 = [(-B-sqrt(delta))/(2*A),E(2),0];
        I2 = [(-B+sqrt(delta))/(2*A),E(2),0];
    elseif (abs(tan(theta))>1e10)
        %fprintf("theta = %g \t cas x = %g \n", theta, E(1))
        A = 1;
        B = -2*X.O(2);
        C = E(1)^2 - 2*E(1)*X.O(1) + X.O(1)^2 + X.O(2)^2 - X.Rin^2;
        delta = B^2-4*A*C;
        I1 = [E(1),(-B-sqrt(delta))/(2*A),0];
        I2 = [E(1),(-B+sqrt(delta))/(2*A),0];
    else
        m = tan(theta);
        n = E(2)-E(1)*m;
        %fprintf("theta = %g \t cas y = %g x + %g \n", theta, m,n)
        A = 1+m^2;
        B = 2*(-X.O(1) + m*n - m*X.O(2));
        C = X.O(1)^2 + n^2 - 2*n*X.O(2) + X.O(2)^2 - X.Rin^2;
        delta = B^2-4*A*C;
        x1 = (-B-sqrt(delta))/(2*A);
        y1 = m*x1+n;
        x2 = (-B+sqrt(delta))/(2*A);
        y2 = m*x2+n;
        I1 = [x1,y1,0];
        I2 = [x2,y2,0];
    end
end