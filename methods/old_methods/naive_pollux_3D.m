function [] = naive_pollux_3D(X,P,voxel_set,V_in,nb_req_inter)
    figure;
        hold on;    grid on;    box on;
        %set(gca, 'LineWidth',2, 'XGrid','on', 'GridLineStyle','--')
        xlim([-X.Rin-10,+X.Rin+10]);        xlabel('x');
        ylim([-X.Rin-10,+X.Rin+10]);         ylabel('y');
        zlim([-ceil(X.Ax/2)-10,+ceil(X.Ax/2)+10]);  zlabel('z');
        plot3(X.O(1),X.O(2),X.O(3),'kx')
        
        % https://fr.mathworks.com/matlabcentral/answers/285880-how-to-plot-a-3d-bounding-box-with-specific-size-around-a-3d-point
        
        plotcube(P.T,X.O-P.T/2,0.005,[0,0,1])
        
        for (j=1:P.J)
            [a,b,c] = get_axes_voxel_idx(P,j);           % %fprintf("%d \t (%g,%g,%g)\n",j,a,b,c);
            [r_a,r_b,r_c] = get_axes_voxel_coord(P,a,b,c);

            % Ce qui est lié à une émission (en rouge)
            d =  (randi([0, 4294967295],1,3) / 4294967296.0).*P.V;
            E = [r_a + d(1),r_b + d(2), r_c + d(3)];
            if (ismember(j,voxel_set))
                h1 = plot3(E(1),E(2),E(3),'x',Color="r");
            end
            
            if (isempty(V_in))
                % ]a,b[ = a + (b-a)*rand()
                % V = -1 + (1 + 1)*rand(3,1);
                V = -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
            else
                V = V_in;
            end
            
            [I1,I2] = getIntersectCoord_3D(X,E,V,nb_req_inter);
            fprintf("j = %d\n\tE = (%g,%g,%g)\n\tV = (%g,%g,%g)\n",j,E(1),E(2),E(3),V(1),V(2),V(3));
            fprintf("\t\tI1 = (%g,%g,%g)\n\t\tI2 = (%g,%g,%g)\n",I1(1),I1(2),I1(3),I2(1),I2(2),I2(3));

            % C'est pour que tu t'en souviennes parce que là, tu aurais juste besoin de tester la première composantes =?= inf 
            if (~all(isinf(I1)) && ismember(j,voxel_set))
                plot3(I1(1),I1(2),I1(3),'*',Color='b');
            end

            if (~all(isinf(I2)) && ismember(j,voxel_set))
                plot3(I2(1),I2(2),I2(3),'*',Color='g');
            end
            
            if (~all(isinf(I1)) && ~all(isinf(I2)) && ismember(j,voxel_set))
                pts = [I1;I2];
                plot3(pts(:,1), pts(:,2), pts(:,3),Color='#808080',LineStyle=":",LineWidth=0.25)
            end

            if (~all(isinf(I1))&&~all(isinf(I2)) && ismember(j,voxel_set))
                pause(0.001)
            end
            test ="";
        end


     test ="";
end







% -------------------------------------------------------------------------------------------------------------------------
% -------------------------------------------------------------------------------------------------------------------------
% -------------------------------------------------------------------------------------------------------------------------
% -------------------------------------------------------------------------------------------------------------------------
% -------------------------------------------------------------------------------------------------------------------------
% -------------------------------------------------------------------------------------------------------------------------
% -------------------------------------------------------------------------------------------------------------------------


%{
% https://fr.mathworks.com/matlabcentral/fileexchange/15161-plotcube

P = [10,5,10] ;   % you center point
L = [20,20,20] ;  % your cube dimensions
O = P-L/2 ;       % Get the origin of cube so that P is at center
plotcube(L,O,.8,[1 0 0]);   % use function plotcube
hold on
plot3(P(1),P(2),P(3),'*k')
%}


