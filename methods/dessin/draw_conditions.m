function [C,verb_lim,ANIM] = draw_conditions(C)
    if (C.method == "POLLUX" || C.method == "TRACKER" || C.method == "TRACKER_MC"), C.N = C.pollux_N; end 
    if (C.method == "HECTOR"), C.N = C.hector_N; end
    verb_lim = 1;
    ANIM = true;
    if (C.draw == false), verb_lim = C.N_max; end 
    if (C.draw && C.N >= C.N_max), C.draw = false;  verb_lim = C.N_max; end
    if (C.preset_type == "time"), C.draw = false;end
    if (C.create_anim && C.N>C.N_max && C.draw)
        ANIM = false;
    else
        if (C.draw && C.create_anim)
            manage_anim(C,0);
        end
    end
end