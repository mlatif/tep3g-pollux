function [h1,h2,h3,h4] = external_point_plot(E,V,t_in_1,t_in_2,t_top,t_bot,t_in,t_out,t_p)
    if (nargin <9)
        t_in = inf; t_out = inf; t_p = inf;
    end
    if (nargin <10)
        t_out = inf; t_p = inf; 
    end
    if (nargin <11)
        t_p = inf; 
        if (nargin < 10)
            t_out = inf; 
            if (nargin < 9)
                t_in = inf; 
            end
        end
    end
    % Traitement des cas vide 
    if (isempty(t_in)), t_in = 0; end 
    if (isempty(t_p)), t_p = 0; end


    test = "";
    if (t_in_1~=0 && (t_in_1 == t_in || t_in == inf))
        P = E + t_in_1* V;
        h1 = plot3(P(1),P(2),P(3),'+',Color='blue',MarkerSize=12,DisplayName=strcat("Rin t1 = ",num2str(t_in_1,"%g")));
    else
        h1 = line(E(1),E(2),E(3),Color="white",MarkerSize=1,DisplayName=strcat("Rin t1 = ",num2str(t_in_1,"%g")));
    end
    
    if (t_in_2~=0 && (t_in_2 == t_in || t_in == inf))
        P = E + t_in_2* V;
        h2 = plot3(P(1),P(2),P(3),'*',Color='blue',MarkerSize=12,DisplayName=strcat("Rin t2 = ",num2str(t_in_2,"%g")));
    else
        %h2 = line();
        h2 = line(E(1),E(2),E(3),Color="white",MarkerSize=1,DisplayName=strcat("Rin t2 = ",num2str(t_in_2,"%g")));
    end
        
    %
    if (t_top~=0 && (t_top == t_p || t_p == inf))
        P = E + t_top* V;
        h3 = plot3(P(1),P(2),P(3),'d',Color='magenta',MarkerSize=12,DisplayName=strcat("Top ttop = ",num2str(t_top,"%g")));
    else
        h3 = plot3(E(1),E(2),E(3),Color='white',MarkerSize=1,DisplayName=strcat("Top t = ",num2str(t_top,"%g")));
    end
    % 
    if (t_bot~=0 && (t_bot == t_p || t_p == inf))
        P = E + t_bot* V;
        h4 = plot3(P(1),P(2),P(3),'s',Color='magenta',MarkerSize=12,DisplayName=strcat("Bot t = ",num2str(t_bot,"%g")));
    else
        h4 = plot3(E(1),E(2),E(3),Color='white',MarkerSize=1,DisplayName=strcat("Bot t = ",num2str(t_bot,"%g")));
    end
end