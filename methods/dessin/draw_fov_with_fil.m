function [] = draw_fov_with_fil(X,A,V1,V2,beta,base)
    draw_list = []; 
    t_axe = 150;
    n = sign(tan(beta))*(V1-V2)/norm(V1-V2,2);    %
    draw_list=draw_store_pts(draw_list,V1,'*',"k",5);
    draw_list=draw_store_pts(draw_list,V2,'*',"k",5);
    draw_list = draw_store_lin(draw_list,V1,V2,'k',"--",1.5);
    draw_list = draw_store_lin(draw_list,V1,V1 + t_axe*n,'k',"-",1.5);
    for (o = 10:5:360)
        m = get_generatrix_unit_vector(V2,V1,beta,deg2rad(o),"0"); % !!!
        t_csr = get_intersect_rev_coord_csr(X,V1,m);
        I_csr = get_point(V1,m,t_csr);
        draw_list = draw_store_lin(draw_list,V1,I_csr,'#8C92AC',"--",0.0005);
    end
    %
    Ox = [1,0,0]; Oy = [0,1,0]; Oz = [0,0,1]; 
    draw_list = draw_store_lin(draw_list,V1,V1 + 25*Ox,'b',"-",1.5);    
    draw_list = draw_store_lin(draw_list,V1,V1 - 25*Ox,'b',"-",1.5);    
    draw_list = draw_store_lin(draw_list,V1,V1 + 25*Oy,'r',"-",1.5);    
    draw_list = draw_store_lin(draw_list,V1,V1 - 25*Oy,'r',"-",1.5);   
    draw_list = draw_store_lin(draw_list,V1,V1 + 25*Oz,'g',"-",1.5);    
    draw_list = draw_store_lin(draw_list,V1,V1 - 25*Oz,'g',"-",1.5);  
    
    test = ""; 
end