function [] = erase_all_elts(draw_list)
    gcf;
    for (d = 1:length(draw_list))
        set(draw_list(d),'Visible','off')
    end
end