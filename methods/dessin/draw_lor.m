function [draw_list] = draw_lor(draw_list,X,P,V1,V2,col_lor)
    draw_list = draw_store_pts(draw_list,V1,'*',col_lor,15) ;
    draw_list = draw_store_pts(draw_list,V2,'*',col_lor,15) ;
    pts = [V1;V2];
    [draw_list] = draw_store_lin(draw_list,V1,V2,col_lor,"-",2);
end
