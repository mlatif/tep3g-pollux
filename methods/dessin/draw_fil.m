function [fil] = draw_fil(X,P,A,V1s,V2s,beta)
    draw_xemis(X,P,true);
    draw_fov_with_fil(X,A,V1s,V2s,beta,'C'); 
    [fil] = get_fil(A,V1s,V2s,beta,'C',true); 
end