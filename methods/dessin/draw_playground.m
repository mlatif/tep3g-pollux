function [] = draw_playground(P,A,X,S)
    Lx = [A.yid.max,A.zid.min];
    Ly = [A.xid.max,A.zid.min];
    Lz = [A.xid.max,A.yid.max]; 
    draw_xemis(X,P,true);
    draw_fov_with_vox(P,A,Lx,Ly,Lz);
end