function [draw_list] = draw_cor(draw_list,X,P,V1,V2,beta,col,len_gen,ech_start,ech_step,hint,match,gen_dot_size)
    if (nargin<13), gen_dot_size = 0; end
    if (nargin<12), match = false; end
    if (nargin<11), hint = 0; end
    if (nargin <10),  ech_step = 5;
        if (nargin<9), ech_start = 0; end
        if (nargin<8), len_gen = 75; end
    end
    n = sign(tan(beta))*(V1-V2)/norm(V1-V2,2);    %
    % Points et axe
    draw_list=draw_store_pts(draw_list,V1,'*',col.pts,5);
    draw_list=draw_store_pts(draw_list,V2,'*',col.pts,5);
    draw_list = draw_store_lin(draw_list,V1,V1 + 75*n,'#848482',"-",0.5);
    % Génératrices
    for (o = ech_start:ech_step:360)
        m = get_generatrix_unit_vector(V2,V1,beta,deg2rad(o),"0"); % !!!
        if (match==true)
            [t_csr] = get_intersect_rev_coord_csr(X,V1,m);
        else
            t_csr = len_gen;
        end
        
        I_csr = get_point(V1,m,t_csr);
        if (hint ~= 0), col_gen = col.gen(hint); else col_gen = col.pts; end
        draw_list = draw_store_lin(draw_list,V1,I_csr,col_gen,"-",0.5);
        if (gen_dot_size ~= 0)
            draw_store_pts([],I_csr,'.','#808080',gen_dot_size);
        end
    end
    test = ""; 
end













