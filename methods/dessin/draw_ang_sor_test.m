function [voxel_list] = draw_ang_sor_test(X,A,P,V1s,V2s,beta,col,dbeta,ech_sor_param,verbose)
    % https://fr.mathworks.com/matlabcentral/answers/351573-how-to-link-2-3d-subplots-to-sync-zoom-and-pan
    draw_list = []; 
    figure; 
        S1 = subplot(1,2,1); 
        draw_xemis_wo_fig(X,P,true); 
        draw_sor(draw_list,X,P,V1s,V2s,beta,col,dbeta,ech_sor_param,verbose); 
        S2 = subplot(1,2,2);
        draw_xemis_wo_fig(X,P,true);
        [voxel_list] = test_angle_to_sor(P,A,V1s,V2s,beta,dbeta,true);
        Link = linkprop([S1, S2],{'CameraUpVector', 'CameraPosition', 'CameraTarget', 'XLim', 'YLim', 'ZLim'});
        setappdata(gcf, 'StoreTheLink', Link);
        fprintf("#(voxel) in SOR = %g / %g \n", sum(voxel_list),P.J)
end