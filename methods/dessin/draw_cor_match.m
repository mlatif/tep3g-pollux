function [] = draw_cor_match(X,P,V1s,V2s,beta,col)
    draw_list = []; 
    draw_xemis(X,P,false)
    [draw_list] = draw_cor(draw_list,X,P,V1s,V2s,beta,col);
end