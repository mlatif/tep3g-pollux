function [] = draw_phantom(P,F,X)
    %imagesc(squeeze(sum(P.activity,3))); axis equal; colorbar; colormap(jet);
        print_phantom_info(P,F)
        set(0,'DefaultFigureWindowStyle','docked') 
        ANIM = true;
        f = figure; 
        hold on;      box on;   %  grid on; 
        set(gca,'color','none')
        % Simulateur en 3D 
        xlim([-X.Rin-10,+X.Rin+10]);                    xlabel('$x$','Interpreter','latex','FontSize',15);
        ylim([-X.Rin-10,+X.Rin+10]);                    ylabel('$y$','Interpreter','latex','FontSize',15);
        zlim([-ceil(X.Ax/2)-10,+ceil(X.Ax/2)+10]);      zlabel('$z$','Interpreter','latex','FontSize',15); hZLabel = get(gca,'ZLabel'); set(hZLabel,'rotation',0,'VerticalAlignment','middle')
        plot3(X.O(1),X.O(2),X.O(3),'kx'); 
        plotcube(P.T,X.O-P.T/2,0.25,[.7 .7 .7])
        % Bords des detecteurs
        th = 0:pi/50:2*pi;
        % Bord intérieur en 3D
        xunit = X.Rin * cos(th) + X.O(1);    yunit = X.Rin * sin(th) + X.O(2); zunit = ones(1,length(xunit)); 
        plot3(xunit, yunit,0*zunit,'k'); axis equal;
        plot3(xunit, yunit,(-X.Ax/2)*zunit,'k'); axis equal;
        plot3(xunit, yunit,(+X.Ax/2)*zunit,'k'); axis equal;
        % Bord exterieur en 3D
        xunit = X.Rout * cos(th) + X.O(1);    yunit = X.Rout * sin(th) + X.O(2); zunit = ones(1,length(xunit));
        plot3(xunit, yunit,0*zunit,'k'); axis equal;
        plot3(xunit, yunit,(-X.Ax/2)*zunit,'k'); axis equal;
        plot3(xunit, yunit,(+X.Ax/2)*zunit,'k'); axis equal;
        % Limite FOV en 3D
        xunit = X.Rfov * cos(th) + X.O(1);    yunit = X.Rfov * sin(th) + X.O(2); zunit = ones(1,length(xunit));
        plot3(xunit, yunit,0*zunit,'k--'); axis equal;
        plot3(xunit, yunit,(-X.Ax/2)*zunit,'k--'); axis equal;
        plot3(xunit, yunit,(+X.Ax/2)*zunit,'k--'); axis equal;



        if (~ANIM)
            xline(0,"k--");
            yline(0,"k--");
        end



        for j=1:1
            [a,b,c] = get_axes_voxel_idx(P,j);
            [r_a,r_b,r_c] = get_axes_voxel_coord(P,a,b,c);
            plotcube(P.V,[r_a,r_b,r_c],1,[1,0,0]);
            test = "";
        end
        print -depsc ULYSSE_FR


        % -----------------------------------------------------------------
        if (~all(P.activity==0,'all'))
            if (P.J > 50000)
                fprintf("Trop de voxel pour faire une jolie image !!\n")
            else
                % Activitée de background récupérée comme étant le mode de la
                % distribution d'activité 
                bg_act = mode(P.lambda); 
                f = figure;
                hold on;      box on;   %  grid on;
                set(gca,'color','none')
                % Simulateur en 3D
                xlim([-X.Rin-10,+X.Rin+10]);                    xlabel('$x$','Interpreter','latex','FontSize',15);
                ylim([-X.Rin-10,+X.Rin+10]);                    ylabel('$y$','Interpreter','latex','FontSize',15);
                zlim([-ceil(X.Ax/2)-10,+ceil(X.Ax/2)+10]);       zlabel('$z$','Interpreter','latex','FontSize',15); hZLabel = get(gca,'ZLabel'); set(hZLabel,'rotation',0,'VerticalAlignment','middle')
                plot3(X.O(1),X.O(2),X.O(3),'kx');
                plotcube(P.T,X.O-P.T/2,0.25,[.7 .7 .7])
                % Bords des detecteurs
                th = 0:pi/50:2*pi;
                % Bord intérieur en 3D
                xunit = X.Rin * cos(th) + X.O(1);    yunit = X.Rin * sin(th) + X.O(2); zunit = ones(1,length(xunit));
                plot3(xunit, yunit,0*zunit,'k'); axis equal;
                plot3(xunit, yunit,(-X.Ax/2)*zunit,'k'); axis equal;
                plot3(xunit, yunit,(+X.Ax/2)*zunit,'k'); axis equal;
                % Bord exterieur en 3D
                xunit = X.Rout * cos(th) + X.O(1);    yunit = X.Rout * sin(th) + X.O(2); zunit = ones(1,length(xunit));
                plot3(xunit, yunit,0*zunit,'k'); axis equal;
                plot3(xunit, yunit,(-X.Ax/2)*zunit,'k'); axis equal;
                plot3(xunit, yunit,(+X.Ax/2)*zunit,'k'); axis equal;
                % Limite FOV en 3D
                xunit = X.Rfov * cos(th) + X.O(1);    yunit = X.Rfov * sin(th) + X.O(2); zunit = ones(1,length(xunit));
                plot3(xunit, yunit,0*zunit,'k--'); axis equal;
                plot3(xunit, yunit,(-X.Ax/2)*zunit,'k--'); axis equal;
                plot3(xunit, yunit,(+X.Ax/2)*zunit,'k--'); axis equal;
                % -----
                if (~ANIM)
                    xline(0,"k--");
                    yline(0,"k--");
                end
                % -----
                for j=1:P.J
                    [a,b,c] = get_axes_voxel_idx(P,j);
                    [r_a,r_b,r_c] = get_axes_voxel_coord(P,a,b,c);
                    cond_a = (a == min(P.Ic(:,1)) || a == max(P.Ic(:,1)));
                    cond_b = (b == min(P.Ic(:,2)) || b == max(P.Ic(:,2)));
                    cond_c = (c == min(P.Ic(:,3)) || c == max(P.Ic(:,3)));   
                    if (cond_a || cond_b || cond_c)
                        % EST SUR UN BORD
                        %fprintf("j = %d /a = %d /t b = %d / c = %d \n",j,a,b,c)
                    else
                        if (P.activity(a,b,c) > bg_act)
                            plotcube(P.V,[r_a,r_b,r_c],1,[1,0,0]);
                        end
                        if (P.activity(a,b,c) < bg_act && P.activity(a,b,c) > 0)
                            plotcube(P.V,[r_a,r_b,r_c],1,[1,1,1]);
                            test = "";
                        end
                    end                 
                end
            end
        end
   fprintf(">> print -depsc ... \n")
end