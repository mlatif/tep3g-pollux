function [draw_list] = draw_store_lin(draw_list,P1,P2,col,style,width)
    gcf;
    p=line();   % Retour par défaut de la fonction plot3()
    if (naii(P1) && naii(P2))
        pts = [P1;P2];
        p = plot3(pts(:,1), pts(:,2), pts(:,3),Color=col,LineStyle=style,LineWidth=width);
        draw_list = [draw_list,p];
    end
end