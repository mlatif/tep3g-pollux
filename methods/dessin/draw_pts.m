function [p] = draw_pts(P,mark,col,marksize)
    
    gcf;
    p=line();   % Retour par défaut de la fonction plot3()
    if (naii(P))
        p = plot3(P(1),P(2),P(3),mark,Color=col,Markersize=marksize);
    end
end