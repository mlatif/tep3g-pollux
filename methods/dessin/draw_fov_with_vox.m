function [] = draw_fov_with_vox(P,A,Lx,Ly,Lz)
    fprintf("WARNING!! utiliser d'abord draw_xemis(X,P,[true,false]);\n")
    % Update X,Y,Z labels
    xlabel({'$x$'; strcat('$n_{x}$=',num2str(A.D(1),'%d'))},'Interpreter','latex','FontSize',15); hXLabel = get(gca,'XLabel'); set(hXLabel,'HorizontalAlignment','left','Fontsize',15)
    ylabel({'$y$'; strcat('$n_{y}$=',num2str(A.D(2),'%d'))},'Interpreter','latex','FontSize',15); hYLabel = get(gca,'YLabel'); set(hYLabel,'HorizontalAlignment','right','Fontsize',15)
    zlabel({'$z$'; strcat('$n_{z}$=',num2str(A.D(3),'%d'))},'Interpreter','latex','FontSize',15); hZLabel = get(gca,'ZLabel'); set(hZLabel,'rotation',0,'VerticalAlignment','middle','Fontsize',15)
    
    X_vox = squeeze(A.idx_matrix(:,Lx(1),Lx(2))); 
    Y_vox = squeeze(A.idx_matrix(Ly(1),:,Ly(2))); 
    Z_vox = squeeze(A.idx_matrix(Lz(1),Lz(2),:)); 
    

    for(j = 1:length(Z_vox))
        [a,b,c] = get_axes_voxel_idx(P,Z_vox(j));
        [r_a,r_b,r_c] = get_axes_voxel_coord(P,a,b,c);
        plotcube(P.V,[r_a,r_b,r_c],0.05,[1,0,0]);
    end
    for(j = 1:length(Y_vox))
        [a,b,c] = get_axes_voxel_idx(P,Y_vox(j));
        [r_a,r_b,r_c] = get_axes_voxel_coord(P,a,b,c);
        plotcube(P.V,[r_a,r_b,r_c],0.05,[0,1,0]);
    end
    for(j = 1:length(X_vox))
        [a,b,c] = get_axes_voxel_idx(P,X_vox(j));
        [r_a,r_b,r_c] = get_axes_voxel_coord(P,a,b,c);
        plotcube(P.V,[r_a,r_b,r_c],0.05,[0,0,1]);
    end
    fprintf(">> print -depsc ... \n");
end




% 

% 


% for (j = 1:A.T)
%     [a,b,c] = get_axes_voxel_idx(P,j); 
%     [r_a,r_b,r_c] = get_axes_voxel_coord(P,a,b,c);
%     plotcube(P.V,[r_a,r_b,r_c],0,[0,0,1]);
% end