function [draw_list] = draw_store_pts(draw_list,P,mark,col,siz)
    if (nargin < 5)
        siz = 6;
    end
    gcf;
    p=line();   % Retour par défaut de la fonction plot3()
    if (naii(P))
        p = plot3(P(1),P(2),P(3),mark,Color=col,Markersize=siz);
        draw_list = [draw_list,p];
    end
end
