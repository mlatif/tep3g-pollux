function [draw_list] = draw_store_csr(draw_list,X,M,V,beta,t,col)
    if (nargin < 7)
        col = '#808080';
    end
    if (naii(M))
        % Composante de dessin d'un cone
        for (o = 0:10:360)
            m = get_generatrix_unit_vector(M,V,beta,deg2rad(o),t);
            [t_csr] = get_intersect_coord_csr(X,V,m);
            if (t_csr==0)
                test ="";
            end
            I_csr = get_point(V,m,t_csr);
            draw_list = draw_store_lin(draw_list,V,I_csr,col,"-",1);
            draw_list = draw_store_pts(draw_list,I_csr,'.','b');
        end
    end
end