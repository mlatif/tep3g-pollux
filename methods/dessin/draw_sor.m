function [draw_list] = draw_sor(draw_list,X,P,V1,V2,beta,col,dbeta,ech_param,match,verbose)
    if(nargin < 11), verbose = false; end
    if(nargin < 10), match = false; end 
    if (verbose),fprintf("beta = %g (rad) ~ %g (deg) \n\t beta+dbeta=%g (rad) ~ %g (deg)\n\t beta-dbeta=%g (rad) ~ %g (deg)\n",beta,rad2deg(beta),beta+dbeta,rad2deg(beta+dbeta),beta-dbeta,rad2deg(beta-dbeta)); end
    [k_int,k_ext] = get_sor_angle_interval(beta,dbeta); 
    if (dbeta~=0)
%        ech_param.ech_step_ext = 15;
 %       ech_param.ech_step_int = 10;
  %      ech_param.start_step_ext = 5; 
  %      ech_param.start_step_int = 10;
        if (k_int)
            hint = 1;   % On parle du bord intérieur (beta - dbeta)
            [draw_list] = draw_cor(draw_list,X,P,V1,V2,beta-dbeta,col,ech_param.len_gen,ech_param.start_step_int,ech_param.ech_step_int,hint,match,ech_param.size_point);
        end
        if (k_ext)
            hint = 2;   % On parle du bord extérieur (beta + dbeta)
            [draw_list] = draw_cor(draw_list,X,P,V1,V2,beta+dbeta,col,ech_param.len_gen,ech_param.start_step_ext,ech_param.ech_step_ext,hint,match,ech_param.size_point);
        end
    else 
        [draw_list] = draw_cor(draw_list,X,P,V1,V2,beta,col,ech_param.len_gen,ech_param.start_step_ext,ech_param.ech_step_ext,0,match,ech_param.size_point);
    end
end