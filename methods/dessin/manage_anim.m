function [frames] = manage_anim(C,part,frames)
    anim_savefoldere = strcat(C.save_folder,"_anim/");
    % ---------------------------------------------------------------------
    switch part
        case 0
            % Initilisation du répertoire pour stocker l'animation 
            % Ne doit rien retourner.
            if (exist(anim_savefoldere)),delete(anim_savefoldere); end
            mkdir(anim_savefoldere);
        case 1
            % 
            frame = getframe(1);
            frames(1) = frame;
            test ="";
        case 2
            % 
            n = length(frames);
            frame = getframe(1);
            frames(n+1) = frame;   % Car on stock en 1 la caméra vide 
            test ="";
        case 3
            n = length(frames);
            frame = getframe(1);
            frames(n+2) = frame;   % Car on stock en n+1 la mesures
            frame = getframe(1);
            frames(n+3) = frame;   % Car on stock en n+1 la mesures
            frame = getframe(1);
            frames(n+4) = frame;   % Car on stock en n+1 la mesures
            % ---
            FUN_writeFrames(frames, strcat(anim_savefoldere, 'pollux_anim.avi'), 20);
    end
end