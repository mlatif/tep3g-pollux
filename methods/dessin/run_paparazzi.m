function [] = run_paparazzi(P,A,X,S,instance,classe,liste_3g,dbeta,ech_sor_param,col,match)
    draw_list = [];
    path = "PAPARAZZI/";
    if (~isfolder(path)), mkdir PAPARAZZI/; end
    for (e = 1:length(liste_3g))
        id = liste_3g(e);
        Sp = split(instance,'_');
        name = strcat(path,Sp(2),'_',Sp(4),'_',Sp(5),'_C',num2str(classe),'_',num2str(e),'_',num2str(id),'_db',num2str(dbeta,'%d'));
        fprintf('e = %g \t id = %d \t %s \n',e,id, name);
        [V1,V2,V1p,V2p,V1s,V2s,E0,E0_prime,E1a,E2a,E1b,E2b,I1,I2,beta,betap] = load_event(S,liste_3g,e);
        f = figure;
        draw_xemis(X,P,true);
        draw_event(draw_list,X,P,A,classe,V1,V2,V1p,V2p,V1s,V2s,E1a,E2a,E1b,E2b,I1,I2,beta,betap,dbeta,ech_sor_param,col,match);
        saveas(gcf,strcat(name,'.eps'),'epsc');
        test = "";
        close all;
    end
end
