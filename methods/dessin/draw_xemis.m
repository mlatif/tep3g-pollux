function [f] = draw_xemis(X,P,ANIM)
        f = figure; 
        hold on;      box on;   %  grid on; 
        set(gca,'color','none')
        set(gca,'TickLabelInterpreter','Latex')
        % Simulateur en 3D 
        xlim([-X.Rin-10,+X.Rin+10]);                    xlabel('$x$','Interpreter','latex','FontSize',15);
        ylim([-X.Rin-10,+X.Rin+10]);                    ylabel('$y$','Interpreter','latex','FontSize',15);
        zlim([-ceil(X.Ax/2)-10,+ceil(X.Ax/2)+10]);      zlabel('$z$','Interpreter','latex','FontSize',15); hZLabel = get(gca,'ZLabel'); set(hZLabel,'rotation',0,'VerticalAlignment','middle')
        plot3(X.O(1),X.O(2),X.O(3),'kx'); 
        plotcube(P.T,X.O-P.T/2,0.005,[0,0,1])
        % Bords des detecteurs
        th = 0:pi/50:2*pi;
        % Bord intérieur en 3D
        xunit = X.Rin * cos(th) + X.O(1);    yunit = X.Rin * sin(th) + X.O(2); zunit = ones(1,length(xunit)); 
        plot3(xunit, yunit,0*zunit,'k'); axis equal;
        plot3(xunit, yunit,(-X.Ax/2)*zunit,'k'); axis equal;
        plot3(xunit, yunit,(+X.Ax/2)*zunit,'k'); axis equal;
        % Bord exterieur en 3D
        xunit = X.Rout * cos(th) + X.O(1);    yunit = X.Rout * sin(th) + X.O(2); zunit = ones(1,length(xunit));
        plot3(xunit, yunit,0*zunit,'k'); axis equal;
        plot3(xunit, yunit,(-X.Ax/2)*zunit,'k'); axis equal;
        plot3(xunit, yunit,(+X.Ax/2)*zunit,'k'); axis equal;
        % Limite FOV en 3D
        xunit = X.Rfov * cos(th) + X.O(1);    yunit = X.Rfov * sin(th) + X.O(2); zunit = ones(1,length(xunit));
        plot3(xunit, yunit,0*zunit,'k'); axis equal;
        plot3(xunit, yunit,(-X.Ax/2)*zunit,'k--'); axis equal;
        plot3(xunit, yunit,(+X.Ax/2)*zunit,'k--'); axis equal;

        if (~ANIM)
            xline(0,"k--");
            yline(0,"k--");
        end
end