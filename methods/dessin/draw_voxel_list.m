function [draw_list] = draw_voxel_list(draw_list,P,A,V1,V2,voxel_list,col)
    for (j=1:A.J)
        if (voxel_list(j)) 
            V1Cj =  A.Cv(j,:) - V1; 
            V2V1 = V1 - V2; 
            Cj = get_corresponding_point(P,j);
            if (isstruct(col))
                [~,i] = min([dot(V1Cj,-V2V1),dot(V1Cj,V2V1)]);
                choix = col(i);
            else
                choix = col; 
            end
            draw_list = draw_store_pts(draw_list,Cj,'.',choix,3);
        end
    end
end