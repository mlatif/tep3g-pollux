function [V0f,V0b,V1,V2,V0p,V1p,V2p,V0s,V1s,V2s,E1a,E2a] = reset_interaction_point()
    V1 = inf(3,1);  V2 = inf(3,1);  V1p = inf(3,1); V2p = inf(3,1); V1s = inf(3,1); V2s = inf(3,1);
    V0f = inf(3,1);  V0p = inf(3,1);  V0s = inf(3,1);
    V0b = inf(3,1);
    E1a = inf; E2a = inf; 
end