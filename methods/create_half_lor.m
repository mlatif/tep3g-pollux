function [succ,V0,V1,E1a,t_V1,t_min,t_max] = create_half_lor(X,M,Dir,t_min,E0,name)
%   INPUT
%   X : la caméra
%   M : le point d'émission 
%   Dir : le vecteur directionnel 
%   t_min : le paramètre associé à l'entrée dans la zone active 
%   E0: l'énergie initiale 
%   name : poka va disparaitre
%   OUTPUT
%   succ : si l'on a réussi à créer une LOR
%   V0 : le point d'entrée dans la zone active 
%   V1 : le point d'interaction
%   E1a : l'énergie déposée (0) par défaut dans le cas d'une LOR
%   t_V1 : le paramètre associé à V1
%   [t_min,t_max] : les bornes pour sélectionner le point V1 dans la zone active
    verbose = true; if (nargin < 6 || name == ""), verbose = false; name = ""; end 
    succ = false; 
    V0 = get_point(M,Dir,t_min); % Point d'éntrée dans la zone active (f)
    [t_max] = get_intersect_coord_Rout(X,M,Dir,t_min);
    [t_V1,E1a] = distance_xenon_attenuation_2GLOR(X,E0,Dir,t_min);
    V1 = get_point(V0,Dir,t_V1);
    if (~is_in_FOD(X,V1,name))
        V1 = inf(3,1);   t_V1 = inf;  t_min = inf;  t_max = inf;
    else
        succ = true;
    end
end

%{
%
%                     V0_f = get_point(M,Dir,t_min_f); % Point d'éntrée dans la zone active (f)        
%                     [t_max_f] = get_intersect_coord_Rout(X,M,Dir,t_min_f);
%                     [t_V1_f,E1a] = distance_xenon_attenuation_2GLOR(X,E0,Dir,t_min_f);
%                     V1 = get_point(V0_f,Dir,t_V1_f);
%                     if (~is_in_FOD(X,V1,"[LOR - V1 - f]"))
%                         V1 = inf(3,1);   t_V1_f = inf;  t_min_f = inf;  t_max_f = inf; 
%                     end
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                     V0_b = get_point(M,Dir,t_min_b); % Point d'éntrée dans la zone active (b)
%                     [t_max_b] = get_intersect_coord_Rout(X,M,Dir,t_min_b);
%                     [t_V1_b,E1a] = distance_xenon_attenuation_2GLOR(X,E0,Dir,t_min_b);
%                     V2 = get_point(V0_b,Dir,t_V1_b);
%                     if (~is_in_FOD(X,V2,"[LOR - V2 - b]"))
%                         V2 = inf(3,1);   t_V1_b = inf;  t_min_b = inf; t_max_b = inf; 
%                     end
%}