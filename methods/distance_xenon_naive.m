function [t,E] = distance_xenon_naive(t_init_min,t_init_max,E_init)
    % Retourne la distance parcourue suivant la trajectoire obtenue par les
    % points d'intersection détectés. 
    %   - Remarque : le t est appliqué à partir du premier points
    %   d'intersection et st. P(t) = V1 + t*D, t\in [t_init_min,t_init_max]
    %   - Remarque : 
    %       - fixer t = 0 si on veut se ramener au cas Dirac sur Rin
    %       - fixer t = t_init_max - t_init si on veut se ramener au cas Dirac sur Rout
    t = 1/2*(t_init_max-t_init_min);
    E = 1/2*(E_init);
end
