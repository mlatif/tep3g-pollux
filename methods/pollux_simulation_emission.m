function [S,M,Dir,info] = pollux_simulation_emission(P,C,S,n)
        %r = (randi([0, 4294967295]) / 4294967296.0);
        r = rand(); % U(]0,1[) ?? Ou l'argument st. on est en proba continu donc on s'en fiche de la proba en un point => Inclure ou Exclure 0,1 est pareil ??
        rand_j = 0;
        % Revient à chercher l'infimum
        for j=1:P.J
            if (r < P.cdf_lambda(j)),  rand_j = j; break;   end
        end
        % -----------------------------------------------------------------
        [a,b,c] = get_axes_voxel_idx(P,rand_j);
        % -----------------------------------------------------------------
        [r_a,r_b,r_c] = get_axes_voxel_coord(P,a,b,c);
        switch C.Emi_pts
            case "IN"
                % Tirer un point d'émission dans le voxel.
                d =  (randi([0, 4294967295],1,3) / 4294967296.0).*P.V; % Je veux inclure 0 comme borne possible de mon tirage
                M = [r_a + d(1),r_b + d(2), r_c + d(3)];    
            case "CV"
                % Tirer un point depuis le centre du voxel.
                M = [r_a,r_b,r_c] + P.V./2; 
        end
        % -----------------------------------------------------------------
        S.export.t(n) = uint32(10); %10 ms par défaut    
        % -----------------------------------------------------------------
        Dir.D1 =inf(1,3);   Dir.D2 = inf(1,3);  Dir.D_pr = inf(1,3);
        switch C.pollux_case
            case "1"
                D1 = -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
                Dir.D1 = D1/norm(D1,2); 
            case "2"
                D2 = -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
                Dir.D2 = D2/norm(D2,2); 
                D_pr = -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
                Dir.D_pr = D_pr/norm(D_pr,2); 
            case "3"
                D1 = -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
                Dir.D1 = D1/norm(D1,2);
                D2 = -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
                Dir.D2 = D2/norm(D2,2);
                D_pr =  -1+(randi([0, 4294967295],1,3) / 4294967296.0).*2;
                Dir.D_pr = D_pr/norm(D_pr,2); 
        end
        % -----------------------------------------------------------------
        S.results.lambda(rand_j) = S.results.lambda(rand_j)+1;
        S.results.vox_idx(n) = rand_j;
        S.results.u_set(n) = r; 
        S.results.activity(a,b,c)= S.results.lambda(rand_j);
        S.results.Vox_Re_coord(n,:) = [r_a,r_b,r_c]; 
        S.results.M(n,:) = M;
        S.results.D1(n,:) = Dir.D1;
        S.results.D2(n,:) = Dir.D2;
        S.results.D_pr(n,:) = Dir.D_pr;
        % -----------------------------------------------------------------
        info.rand_j=rand_j;
        info.abc = [a,b,c;r_a,r_b,r_c];
        info.r = r; 
        
end