function [t_hat] = get_intersect_coord_csr(X,E,V)
% get_intersect_coord_Cyl
%   Retourne les n coordonnées d'intersection d'un rayon défini par P = E + t_hat * V avec les deux bords du cylindre
% INPUT:
%   X : l'objet caméra
%   M : le point d'émission
%   V : le vecteur directionnel
%   t: le sens de propagation du du rayon initial (t<0 = backward, t>0 forward)
% OUTPUT
%
% REMARK
%   Le signe de t dans P = E + t * V traduit le sens de propagation du rayon - Il est utilisé pour discriminer certains points qui sont contraires au sens de propagation du rayon.
%   L'information de l'orientation du rayon est portée par le vecteur V 
%   On ne s'intéresse donc qu'aux valeurs de t>=0 
%   Pour se rapprocher des maths, on converti localement les valeurs d'erreur (inf) en 0.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    

    t_hat = 0; 
    % ---------------------------------------------------------------------
    t_in = 0;   t_out = 0;  t_r = 0;    t_p = 0;
    L_in = [];  L_out = []; L_r = [];   L_p = [];
    % ---------------------------------------------------------------------
    [t_in_1,t_in_2] = intersection_rayon_cylindre(E,V,X.Rin,X.Ax/2,0);   
    [t_out_1,t_out_2] = intersection_rayon_cylindre(E,V,X.Rout,X.Ax/2,0);
    [t_top] = intersection_rayon_plan(X,E,V,+X.Ax/2,0);
    [t_bot] = intersection_rayon_plan(X,E,V,-X.Ax/2,0);
     

    % Détermination des points candidats -> seuls les points dans le sens de propagation du rayon nous intéressent.
    if (t_in_1>0), L_in = [L_in,t_in_1]; end
    if (t_in_2>0), L_in = [L_in,t_in_2]; end
    if (t_out_1>0), L_out = [L_out,t_out_1]; end
    if (t_out_2>0), L_out = [L_out,t_out_2]; end
    if (t_top>0), L_p = [L_p, t_top]; end
    if (t_bot>0), L_p = [L_p, t_bot]; end
    % CHOIX d'un point d'intersection rayon/Rin 
    %   - Si pas d'intersection => point non présent dans la liste L_in
    %   - Si dans le sens opposé de la propagation <=> t<=0
    %   - Si deux points possibles et dans le sens de propagation du rayon (t>0) 
    %       => On prends le min.
    [~,i_in] = min(L_in(L_in>0));
    t_in = L_in(i_in);
    % CHOIX d'un point d'intersection rayon/Rout
    %   - m^eme principe
    [~,i_out] = min(L_out(L_out>0));
    t_out = L_out(i_out);
    % FINALE DES BORDS => t_r
    L_r = [t_in, t_out];
    t_r = min(L_r);
    % FINALE DES PLANS => CHOIX d'un point d'intersection rayon/plan 
    [~,i_p] = min(L_p(L_p>0));
    t_p = L_p(i_p);
    % FINALE => t_r/t_p
    L_hat = [t_r,t_p];
    t_hat = min(L_hat);
        
    %{
    fprintf("INTER_CSR\n"); 
    fprintf("R_IN \tt_in_1 = %g\tt_in_2 = %g\t=> t_in = %g\n",t_in_1,t_in_2,t_in);
    fprintf("R_OUT \tt_out_1 = %g\tt_out_2 = %g\t=> t_out = %g\n",t_out_1,t_out_2,t_out);
    fprintf("P \tt_top = %g\tt_bot = %g\t=> t_p = %g\n",t_top,t_bot,t_p);
    fprintf("M1 => t_r =%g \t t_p = %g \n",t_r,t_p);
    fprintf("M2 => t_hat =%g\n",t_hat);
    %}
    
%     A = E + 50*V; 
%     pts = [E;A];
%     h0 = plot3(pts(:,1), pts(:,2), pts(:,3),Color='k',LineStyle='-',LineWidth=1);
%     [h1,h2,h3,h4,h5,h6] = external_point_plot(E,V,t_in_1,t_in_2,t_out_1,t_out_2,t_top,t_bot); 
%     % [h1,h2,h3,h4,h5,h6] = external_point_plot(E,V,t_in_1,t_in_2,t_out_1,t_out_2,t_top,t_bot,t_in,t_out,t_p); 
%     legend([h1,h2,h3,h4,h5,h6])   % 
%     test="";
%     set(h0,'Visible','off')
%     set(h1,'Visible','off')
%     set(h2,'Visible','off')
%     set(h3,'Visible','off')
%     set(h4,'Visible','off')
%     set(h5,'Visible','off')
%     set(h6,'Visible','off')

 
end




function [h1,h2,h3,h4,h5,h6] = external_point_plot(E,V,t_in_1,t_in_2,t_out_1,t_out_2,t_top,t_bot,t_in,t_out,t_p)
    if (nargin <9)
        t_in = inf; t_out = inf; t_p = inf;
    end
    if (nargin <10)
        t_out = inf; t_p = inf; 
    end
    if (nargin <11)
        t_p = inf; 
        if (nargin < 10)
            t_out = inf; 
            if (nargin < 9)
                t_in = inf; 
            end
        end
    end
    % Traitement des cas vide 
    if (isempty(t_in)), t_in = 0; end 
    if (isempty(t_out)), t_out = 0; end 
    if (isempty(t_p)), t_p = 0; end


    test = "";
    if (t_in_1~=0 && (t_in_1 == t_in || t_in == inf))
        P = E + t_in_1* V;
        h1 = plot3(P(1),P(2),P(3),'+',Color='blue',MarkerSize=12,DisplayName=strcat("Rin t1 = ",num2str(t_in_1,"%g")));
    else
        h1 = line(E(1),E(2),E(3),Color="white",MarkerSize=1,DisplayName=strcat("Rin t1 = ",num2str(t_in_1,"%g")));
    end
    
    if (t_in_2~=0 && (t_in_2 == t_in || t_in == inf))
        P = E + t_in_2* V;
        h2 = plot3(P(1),P(2),P(3),'*',Color='blue',MarkerSize=12,DisplayName=strcat("Rin t2 = ",num2str(t_in_2,"%g")));
    else
        %h2 = line();
        h2 = line(E(1),E(2),E(3),Color="white",MarkerSize=1,DisplayName=strcat("Rin t2 = ",num2str(t_in_2,"%g")));
    end
    
    %
    if (t_out_1~=0 && (t_out_1 == t_out || t_out == inf)) % 
        P = E + t_out_1* V;
        h3 = plot3(P(1),P(2),P(3),'+',Color='red',MarkerSize=12,DisplayName=strcat("Rout t1 = ",num2str(t_out_1,"%g")));
    else
        h3 = plot3(E(1),E(2),E(3),Color='white',MarkerSize=1,DisplayName=strcat("Rout t1 = ",num2str(t_out_1,"%g")));
    end
    
    if (t_out_2~=0 && (t_out_2 == t_out || t_out == inf)) % 
        P = E + t_out_2* V;
        h4 = plot3(P(1),P(2),P(3),'*',Color='red',MarkerSize=12,DisplayName=strcat("Rout t2 = ",num2str(t_out_2,"%g")));
    else
        h4 = plot3(E(1),E(2),E(3),Color='white',MarkerSize=1,DisplayName=strcat("Rout t2 = ",num2str(t_out_2,"%g")));
    end
    
    %
    if (t_top~=0 && (t_top == t_p || t_p == inf))
        P = E + t_top* V;
        h5 = plot3(P(1),P(2),P(3),'d',Color='magenta',MarkerSize=12,DisplayName=strcat("Top ttop = ",num2str(t_top,"%g")));
    else
        h5 = plot3(E(1),E(2),E(3),Color='white',MarkerSize=1,DisplayName=strcat("Top t = ",num2str(t_top,"%g")));
    end
    % 
    if (t_bot~=0 && (t_bot == t_p || t_p == inf))
        P = E + t_bot* V;
        h6 = plot3(P(1),P(2),P(3),'s',Color='magenta',MarkerSize=12,DisplayName=strcat("Bot t = ",num2str(t_bot,"%g")));
    else
        h6 = plot3(E(1),E(2),E(3),Color='white',MarkerSize=1,DisplayName=strcat("Bot t = ",num2str(t_bot,"%g")));
    end
end


