function [j] = get_voxel_idx(P,x,y,z)
% get_voxel_idx
% Retourne l'indice du voxel dans le vecteur de densité correspondant au triplet de coordonnées réelles.
% INPUT
%   P : L'objet phantom
%   (x,y,z) : Les coordonnées réelles
% OUPUT
%   j : l'indice du voxel correspondant dans le vecteur de densité

    j = x+(y-1)*P.D(1)+(z-1)*P.D(1)*P.D(2);
end