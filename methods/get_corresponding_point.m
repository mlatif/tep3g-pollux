function [V] = get_corresponding_point(P,j)
    [a,b,c] = get_axes_voxel_idx(P,j);
    [r_a,r_b,r_c] = get_axes_voxel_coord(P,a,b,c);
    V = [r_a,r_b,r_c]; 
end