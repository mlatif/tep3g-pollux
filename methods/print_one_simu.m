function [] = print_one_simu(C,P,info)   
    clc; 
    % info: Vox info, info : Tirage info; info : Simulation Info  
    N_focG = info.N_allG(info.pure_evt_idx);
    N_remG = sum(info.N_allG) - N_focG; 
    fprintf("n = %d/%d\t #(othG) = %g\t (%g %%)\n",N_focG,C.N,N_remG,((N_focG)/C.N)*100);
    fprintf("\tr=%.2g\tcdf(%d) = %g\n",info.r,info.rand_j,P.cdf_lambda(info.rand_j));
    fprintf("\t[a,b,c]=(%d,%d,%d) \t R[a,b,c]=(%.2f,%.2f,%.2f)\n",info.abc(1,1),info.abc(1,2),info.abc(1,3),info.abc(2,1),info.abc(2,2),info.abc(2,3));
    Em=info.pts(1,:);
    fprintf("\tM = (%g,%g,%g)\n",Em(1),Em(2),Em(3));   
    Em_pr = info.pts(2,:);
    if (naii(info.R_pr))
        fprintf("\tPR\tM' = (%g,%g,%g)\t R = %g \n",Em_pr(1),Em_pr(2),Em_pr(3),info.R_pr);
    end

    D1=info.pts(3,:);
    if (naii(D1)), fprintf("\tD1 = (%g,%g,%g)\n",D1(1),D1(2),D1(3)); end
    D2=info.pts(4,:);
    if (naii(D2)), fprintf("\tD2 = (%g,%g,%g)\n",D2(1),D2(2),D2(3)); end
    fprintf(strcat("\t",info.inf_msg));
    fprintf("\n");
    %{        
        if (P.D(3) == 1)
            fprintf("\ttheta = %g\n", theta)
        else      
        end
        if (naii(I1)), fprintf("\t\tI1 = (%g,%g,%g)\n",I1(1),I1(2),I1(3)) ; end
        if (naii(I2)), fprintf("\t\tI2 = (%g,%g,%g)\n",I2(1),I2(2),I2(3)) ; end
        if (naii(I3)), fprintf("\t\tI3 = (%g,%g,%g)\n",I3(1),I3(2),I3(3)) ; end
        % fprintf("\t\tI1 = (%g,%g,%g)\n\t\tI2 = (%g,%g,%g)\n\t\tI3 = (%g,%g,%g)\n",I1(1),I1(2),I1(3),I2(1),I2(2),I2(3),I3(1),I3(2),I3(3))
        fprintf("\t=> nb detect = %d\n",nb_detection);
        if(nargin>18)
            fprintf("\t=> classification : %s \n",C.Classif(config,1))
        end

        %}
end