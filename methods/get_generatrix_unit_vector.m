function [m] = get_generatrix_unit_vector(M,V1,beta,omega,t)
%     disp("M")
%     disp(M)
%     disp("V1")
%     disp(V1)
    % ---
    c1 = sign(tan(beta));
    c2 = abs(tan(beta))*cos(omega);
    c3 = abs(tan(beta))*sin(omega);
    % ---
    p = V1 - M;
    q = [p(2),-p(1),0];
    n = p/norm(p,2);
    u = cross(p,q) / norm(cross(p,q),2);
    v = q/norm(q,2) ;
    % ---
    d = c1^2 + c2^2 + c3^2;
    m = (c1*n+ c2*u + c3*v)/sqrt(d) ;
%     disp('m')
%     disp(m)
    test="";
end