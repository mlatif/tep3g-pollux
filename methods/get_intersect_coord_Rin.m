function [t_f,t_b] = get_intersect_coord_Rin(X,E,V)
% get_intersect_coord_Rin
%   Retourne les n coordonnées d'intersection d'un rayon défini par R = M + t * V avec le bord intérieur du cylindre
%   Note: f = forward, b = backward
% INPUT: 
%   X : l'objet caméra
%   E : le point d'émission
%   V : le vecteur directionnel 
% OUTPUT
%   t_f: le paramètre t de l'équation du rayon vérifiant I_f = M + t_f * V
%   t_b: le paramètre t de l'équation du rayon vérifiant I_b = M + t_b * V
% REMARQUES
%   Les racines du polynome sont ordrées telles que : d'abord le max (ie le sens de propagation du rayon) puis le min (dans le sens inverse de propagation du rayon)
%   Si une intersection n'existe pas, alors est retounée par défaut un vecteur inf(1,3)
    [t_f,t_b] = intersection_rayon_cylindre(E,V,X.Rin,X.Ax/2);
end
