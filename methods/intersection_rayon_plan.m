function [t_1] = intersection_rayon_plan(X,E,V,z,def_value)
    dv = inf; 
    if (nargin == 5), dv = def_value; end
    %%% -------------------------------------------------------------------
    t_1 = inf;
    % Recherche du point d'intersection avec le bord supérieur avec N Vecteur normal d'un plan parallèle aux axes xy et Q un point appartenant au plan
    N = [0 0 1];
    Q = [0 0 z];
    if (dot(N,V)~=0)
        t_p = -dot(N,(E-Q))/(dot(N,V));
    else
        t_p = inf;     % Le rayon et le plan sont parallèles
    end
    % ETAPE 4 : Validation (ou non) du point obtenu dans l'étape 3
    if (((E(1)+t_p*V(1))^2+(E(2)+t_p*V(2))^2>X.Rout^2) || ((E(1)+t_p*V(1))^2+(E(2)+t_p*V(2))^2<X.Rin^2) )
        t_1 = inf;
    else
        t_1 = t_p;
    end
    test = "";
    %%% -------------------------------------------------------------------
    if (isinf(t_1) && ~isinf(dv)), t_1 = dv; end
end