function [ret] = is_in_FOV(X,V,name)
    verb=true;
    ret = false;
    msg1 = ""; msg2 = "";
    if (nargin<3 || (nargin == 3 && name == ""))
        name = "";  verb = false;
    end
    x = V(1); y = V(2); z = V(3);
    if (abs(z)<X.Ax/2 && x^2+y^2 < X.Rfov^2)
        ret = true; msg1 = "Ok";
    else
        if (x^2+y^2 > X.Rfov^2)
            msg1 = "x^2+y^2 > X.Rfov^2 ";
        end
        if (abs(z)>X.Ax/2)
            msg2 = "abs(z)>X.Ax/2 ";
        end
    end
    if (verb)
        fprintf("%s in FOV ? %s %s \n",name,msg1,msg2)
    end
end
