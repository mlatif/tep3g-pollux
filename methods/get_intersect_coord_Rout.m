function [t_opt] = get_intersect_coord_Rout(X,E,V,t)
% get_intersect_coord_Rout
%   Retourne les n coordonnées d'intersection d'un rayon défini par R = M + t * V avec le bord extérieur du cylindre
%   Note: f = forward, b = backward
% INPUT: 
%   X : l'objet caméra
%   E : le point d'émission
%   V : le vecteur directionnel 
%   t: le sens de propagation du du rayon initial (t<0 = backward, t>0 forward)
% OUTPUT
%   t_opt: le paramètre t de l'équation du rayon vérifiant I = M + t_opt * V
% REMARQUES
%   Si une intersection n'existe pas, alors est retounée par défaut un vecteur inf(1,3)


    t_opt = inf;
    T = inf(1,4);       % Pour stocker, les points possibles (et retourner le minimum non négatif tp)
    z_min = -X.Ax/2;    z_max = +X.Ax/2;
    
    [t_f,t_b] = intersection_rayon_cylindre(E,V,X.Rout,X.Ax/2);
    T(1) = t_f;
    T(2) = t_b;    
    [t_top] = intersection_rayon_plan(X,E,V,z_max);
    T(3) = t_top;     
    [t_bot] = intersection_rayon_plan(X,E,V,z_min);
    T(4) = t_bot;
    
    % ETAPE 7 : Prendre le minimum non négatif dans le cas t>=0 (forward) et le maximum non positif dans le cas t<=0 (backward) 
    % => On pourra faire plus malin quand on cherchera à ajouter des points négatifs (resp. positifs) 
    if (t>=0)
        T(T<0)=inf;
        t_opt = min(T);
    else
        T(T>0)=-inf;
        t_opt = max(T);
        test ="";
    end

    test ="";
end



