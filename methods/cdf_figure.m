clear all; 
close all; 
% Créer le fichier cdf_data à partir d'une première itération de run_pollux
% En mettant un break point après le premier tirage aléatoire.
% https://fr.mathworks.com/help/matlab/ref/xline.html#xline_sep_mw_a3e9a047-b69c-4d3e-8106-0713e34b50ef

load('cdf_data.mat');
close(cur_fig); 
clear("anim_savefoldere","ANIM","cur_fig",'foc_idx',"frames","frames",'once_anim_empty_camera',"POKA_TODO");
clear("E1a_rng","E2a_rng","E1b_rng","E2b_rng","frame")

Fj = P.cdf_lambda(rand_j);
l1 = [rand_j,0;rand_j,r];
l2 = [0,r;rand_j,r];
figure; 
    hold on; box on; 
    plot(P.cdf_lambda,'k-');
    ylim([-0.05,1.05]);
    xlim([-1,length(P.cdf_lambda)+1]);
    x1 = xline(rand_j,'r:','$j=14175$','LabelOrientation','horizontal',LabelVerticalAlignment='middle',FontSize=12,Interpreter='latex',LineWidth=2);
    x2 = yline(Fj,"b:",'$u=0.8147$',FontSize=12,LabelHorizontalAlignment='left',Interpreter='latex',LineWidth=2);
    yline(0,'k--');
    yline(1,'k--'); 

%    xlabel("Voxel indices");
%    ylabel("")
