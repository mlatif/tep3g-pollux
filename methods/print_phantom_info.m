function [] = print_phantom_info(P,F,S)
    fprintf("----------\n")
    if (nargin > 1), 
        fprintf("F\tfile: %s\n\tphantom_name=%s\n",F.file,F.phantom_name); 
    end 
    fprintf("P\tNombre total de Voxel : J := %d x %d x %d = %d \n",P.D(1),P.D(2),P.D(3),P.J);
    fprintf("P\tTaille d'un voxel %d x %d x %d (mm)\n",P.V(1),P.V(2),P.V(3));
    fprintf("P\tTaille de l'image %d x %d x %d (mm)\n",P.T(1),P.T(2),P.T(3));
    fprintf("P\tActivité totale : %g\n",sum(P.activity,'all'));
    %fprintf("C\tTaille de la demie longueur atteignale L = %g (mm)\n",X.L);
    if (nargin > 2)
        fprintf("S\tLocalisation émission : %s \n",S.Conf.Emi_pts)
        fprintf("S\tCas étudié : %s photon(s)\n",S.Conf.Case)
        fprintf("S\trng = %s\n",S.Conf.rng);
        fprintf("S\tpreset : %s\n",S.preset_type);
        fprintf("S\tpositron range : %d\n",S.Conf.Pos_rng.active)
        fprintf("S\tNombre de tirages demandés : %d\n",S.Conf.N); 
        fprintf("S\tNombre de tirages réalisés : %d\n",S.results.N_exp); 
        fprintf("S\tCalibration factor : %g\n",S.results.calib_factor);
        %{
        fprintf("S\tOccurences des évènements détectés (en %%)\n");
        for (i=0:3)
            prop = 0;
            for j=1:length(S.Classif)
                if (S.Classif(j,2)==num2str(i))
                    prop = prop + length(find(~isinf(S.results.occ_config(j,:))));
                end
            end
            fprintf("\t\t%dG : %g \n",i,prop/S.Conf.N*100);
        end
        %}
        fprintf("S\tClassification des évènements détectés (en %%) \n");
        for (i=1:length(S.Classif))
            %prop = length(find(~isinf(S.results.occ_config(i,:))));
            prop = S.results.occ_config_count(i);
            fprintf("\t\t%s : %g \n",S.Classif(i),prop/S.results.N_exp*100);
        end
        fprintf("S\tNombre de type d'intersection en 3G (en %%) \n");
        L = find(S.export.chr=="5");
        nb_inter_tab = zeros(1,3);
        for (i=0:2)
            nb_inter_tab(i+1) = sum(S.results.nb_intersection(L)==i);
            fprintf("\t\t %d Inter(s) = %d (%g %%) \n",i,nb_inter_tab(i+1),(100.*nb_inter_tab(i+1))./length(L));
        end
        test ="";
        % ----
        %{
        figure; hold on; box on; grid on;
            sup_t = sgtitle([strcat(strrep(F.file,"_"," ")," $(",num2str(P.D(1)),",",num2str(P.D(2)),",",num2str(P.D(3)),")$")]);
            sup_t.Interpreter = 'latex';
            sup_t.FontSize = 14;
            subplot(1,2,1)
                imagesc(squeeze(sum(P.activity,3))); axis equal; colorbar; colormap(flipud(gray));  sub_t = title(strcat("$N=",num2str(S.Conf.N),"$"),'Interpreter','latex');   sub_t.FontSize = 14;
            subplot(1,2,2)
                imagesc(squeeze(sum(S.results.activity.*S.results.calib_factor,3))); axis equal; colorbar; colormap("jet"); sub_t = title(strcat("$N_{exp}=",num2str(S.results.N_exp),"$"),'Interpreter','latex');    sub_t.FontSize = 14;
            hold off;
        %}
    end
    fprintf("----------\n");
    




    %fprintf("C\tNombre de voxel par dim : %d %d %d\n",X.nVpL(1),X.nVpL(2),X.nVpL(3))
end
