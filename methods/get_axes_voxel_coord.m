function [x,y,z] = get_axes_voxel_coord(P,i,j,k)
% get_axes_voxel_coord
% Retourne les coordoonées réelles (x,y,z) d'un voxel à partir de ses indices d'un voxel (i,j,k) le long des axes dans le phantom P;
% REMARK
%   - Fonction inverse de la méthode get_voxel_idx
%   - Fonction à utiliser en plus de la fonction get_axes_voxel_idxselon les axes (x,y,z)
% INPUT 
%   P: L'objet phantom
%   i: l'indice de voxel le long de l'axe X
%   j: l'indice de voxel le long de l'axe Y
%   k: l'indice de voxel le long de l'axe Z
% OUTPUT
%   x: la coordonnée de voxel le long de l'axe X
%   y: la coordonnée de voxel le long de l'axe Y
%   z: la coordonnée de voxel le long de l'axe Z
    x = -P.T(1)/2 + (i-1)*P.V(1);
    y = -P.T(2)/2 + (j-1)*P.V(2);
    z = -P.T(3)/2 + (k-1)*P.V(3);
end