function [P] = get_point(E,V,t) 
    
    if (~isinf(t))
        P = E + t*V;
    else 
        P = inf(3,1);
    end
end