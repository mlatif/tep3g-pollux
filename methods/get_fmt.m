function [fmt] = get_fmt(x)
    fmt = repmat('%g ', 1, length(x));
    fmt(end) = [];
end