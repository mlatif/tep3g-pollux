function [S] = update_S_struct(S)
    S.cur_N_limit = S.cur_N_limit + S.Conf.N; 
    % Définition des tableaux à concaténer dans S.results
    Rz1 = zeros(S.Conf.N,1);
    Rz2 = zeros(S.Conf.N,2); 
    Rz3 = zeros(S.Conf.N,3);
    Rz7 = zeros(S.Conf.N,7); 
    % Définition des tableaux à concaténer dans S.export 
    Ez1 = zeros(S.Conf.N,1,'uint32');
    Es1 = strings(S.Conf.N,1); 
    Ez6 = zeros(S.Conf.N,6,'double'); 
    Ez22 = zeros(S.Conf.N,22,'double');
    % Update S.results 
    S.results.vox_idx = [S.results.vox_idx;Rz1]; 
    S.results.u_set = [S.results.u_set;Rz1]; 
    S.results.M = [S.results.M; Rz3]; 
    S.results.Vox_Re_coord = [S.results.Vox_Re_coord; Rz3]; 
    S.results.M_pr = [S.results.M_pr; Rz3]; 
    S.results.R_pr = [S.results.R_pr; Rz3]; 
    S.results.D1 = [S.results.D1; Rz3]; 
    S.results.D_pr = [S.results.D_pr; Rz3]; 
    S.results.D2 = [S.results.D2; Rz3]; 
    S.results.cpt_rm_kn = [S.results.cpt_rm_kn; Rz2]; 
    S.results.csr_gen_dir = [S.results.csr_gen_dir; Rz3];
    S.results.nb_intersection = [S.results.nb_intersection; Rz1];
    S.results.inter_status = [S.results.inter_status; Rz7];
    % Update S.export 
    S.export.t = [S.export.t; Ez1];
    S.export.chr = [S.export.chr; Es1]; 
    S.export.data = [S.export.data; Ez22]; 
    S.export.intersection_points = [S.export.intersection_points; Ez6]; 
end