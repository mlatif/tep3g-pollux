function [b] = naii(I)
    b = ~all(isinf(I));
end