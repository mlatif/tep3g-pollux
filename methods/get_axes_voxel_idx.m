function [i,j,k] = get_axes_voxel_idx(P,v)
% get_axes_voxel_idx
% Retourne les indices (i,j,k) selon les axes (x,y,z) d'un voxel v dans le phantom P;
% INPUT
%   P: L'objet phantom
%   v: l'indice du voxel étudié
% OUTPUT
%   i: l'indice de voxel le long de l'axe x
%   j: l'indice de voxel le long de l'axe y
%   k: l'indice de voxel le long de l'axe z

    k = ceil(v/(P.D(1)*P.D(2)));
    t = mod(v,P.D(1)*P.D(2)); if (t==0), t = P.D(1)*P.D(2); end
    j = ceil(t/P.D(1));
    i = mod(t,P.D(1)); if (i==0), i = P.D(1); end

end