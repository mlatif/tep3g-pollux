function [t1,t2,status_tab] = intersection_lor_cor(X,V1,axe_lor,apex,axe_csr,beta,verbose)
    name = "";  
    if (nargin<7)
        verb = false;
    else
        verb = verbose; 
    end

    t1 = 0; 
    t2 = 0;
    % [sgn(delta),t1,t1::in_fov,t1::sign(beta),t2,t2::in_fov,t2::sign(beta)]
    status_tab = zeros(1,7); 
%     fprintf("beta = %g (deg)\t cos(beta) = %g\t sign(tan(beta))=%g\n",rad2deg(beta),cos(beta),sign(tan(beta))) 
%     fprintf("D.V =%g\t (O-C).V=%g\t D.O-C=%g\t (O-C).O-C=%g\n",dot(axe_lor,axe_csr),dot((V1-apex),axe_csr),dot(axe_lor,(V1-apex)),dot((V1-apex),(V1-apex)));
    
    a = (dot(axe_lor,axe_csr))^2 - cos(beta)^2;
    b = 2 * ( dot(axe_lor,axe_csr)*dot((V1-apex),axe_csr) - dot(axe_lor,(V1-apex)) * cos(beta)^2);
    c = (dot((V1-apex),axe_csr))^2 - dot((V1-apex),(V1-apex))*cos(beta)^2;
    delta = b^2-4*a*c;

    if (verb), fprintf("Delta(inter) = %g \n",delta); end
    status_tab(1) = sign(delta); 
    if (delta >0 )
       t1 = (-b-sqrt(delta))/(2*a) ;
       t2 = (-b+sqrt(delta))/(2*a) ;
       % WARNING : ATTENTION - TOUT DOUX BIJOU 
       % Losqu'on élève au carré, on n'a plus un cone mais deux (pour le prix d'un !!)
       % il faut donc rejeter les solutions qui coupent le c^one qui résulte du calcul 
       % En testant la condition : Si beta < 90 => dot(P-C,V)>0, P doit vérifier cette 
       % condition (modulo le signe de la tangente de l'angle de diffusion) 
       % ------------------------------------------------------------------
       if (verb), name = "X1"; end
       if (verb),  fprintf("sign(tan(beta))*dot(get_point(O,D,t1)-C,V) > 0 ? %d \n",sign(tan(beta))*dot(get_point(V1,axe_lor,t1)-apex,axe_csr) > 0); end
       % 
       status_tab(2) = t1; 
       status_tab(3) = is_in_FOV(X,get_point(V1,axe_lor,t1)); 
       status_tab(4) = sign(tan(beta))*dot(get_point(V1,axe_lor,t1)-apex,axe_csr) > 0 ; 
       if (sign(tan(beta))*dot(get_point(V1,axe_lor,t1)-apex,axe_csr) > 0 && is_in_FOV(X,get_point(V1,axe_lor,t1),name) ) 
            % C'est ok, on valide t1
       else 
           t1 = 0;
       end
       % ------------------------------------------------------------------
       if (verb), name = "X2"; end
       if (verb),  fprintf("sign(tan(beta))*dot(get_point(O,D,t2)-C,V) > 0 ? %d \n",sign(tan(beta))*dot(get_point(V1,axe_lor,t2)-apex,axe_csr) > 0); end
       %
       status_tab(5) = t2;
       status_tab(6) = is_in_FOV(X,get_point(V1,axe_lor,t2));
       status_tab(7) = sign(tan(beta))*dot(get_point(V1,axe_lor,t2)-apex,axe_csr) > 0 ; 
       if (sign(tan(beta))*dot(get_point(V1,axe_lor,t2)-apex,axe_csr) > 0 && is_in_FOV(X,get_point(V1,axe_lor,t2),name)) 
           % C'est ok, on valide t2
       else 
           t2 = 0;
       end 
       

    elseif (delta == 0)
        t1 = -b/(2*a); 
        if (verb), name = "X1"; end
        if (sign(tan(beta))*dot(get_point(V1,axe_lor,t1)-apex,axe_csr) > 0 && is_in_FOV(X,get_point(V1,axe_lor,t1),"X1"))
            % C'est ok, on valide t1
        else
            t1 = 0; 
        end
    else
        % delta = 0
    end
    % -----
%     disp(status_tab)
%     test ="";
    % Petit swap des valeurs de t quand on a t1 = 0 et t2 != 0   
    % Comme ça on ne stock que sur le premier range.
    if (t1 == 0 && t2 ~= 0)
        t1 = t2; t2 = 0; 
    end
    
    % [sgn(delta),t1::in_fov,t1::sign(beta),t2::in_fov,t2::sign(beta)]
    
    
    
    
end