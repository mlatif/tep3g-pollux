function [rho] = get_rho_max(beta,t)
    rho = sign(tan(beta))*abs(cos(beta))*t
end