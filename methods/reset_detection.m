function [n_gamma_511,n_gamma_1157] = reset_detection()
    n_gamma_511 = 0;  % nb de particules détectées avec une énergue à 511
    n_gamma_1157= 0;  % nb de particules détectées avec une énergie à 1.157
end
