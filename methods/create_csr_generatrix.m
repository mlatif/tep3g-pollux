function [succ,V0,V1,V2,E1,E2,csr_gen_dir,cpt_kn_rm] = create_csr_generatrix(X,M,Dir,t_min,t_V1,E0,name)
%   INPUT
%   X : la caméra
%   M : le point d'émission
%   Dir : le vecteur directionnel
%   t_min : le paramètre associé à l'entrée dans la zone active
%   t_V_init : Si il est connu
%   E0: l'énergie initiale
%   name : poka va disparaitre
    verbose = true; if (nargin < 7 || name == ""), verbose = false; end 

    succ = true;
    V1 = inf(3,1);
    V2 = inf(3,1);
    E1 =inf;
    E2 = inf;

    t_V2=inf;
    csr_gen_dir = inf(3,1);
    csr_list = [];
    cpt_kn_rm = 0;
    % -------------------------------------------------------------------------
    % -------------------------------------------------------------------------
    if (verbose), fprintf("[RES] CSR%s\n\tE0 = %g \n",name,E0); end
    %
    V0 = get_point(M,Dir,t_min); % Point d'éntrée dans la zone active (f)
    %
    if (~isinf(t_V1))
        [eff_id_1,eff_name_1] = get_interaction_type(X,E0);
        if (verbose),fprintf("\t[DIFF1] %s \n",eff_name_1);  end
        if (eff_id_1 > 2)
            % EFFET COMPTON => on peut continuer
            V1 = get_point(V0,Dir,t_V1);
            msg_1 = ""; if (verbose), msg_1 = strcat("[CSR",name," V1]"); end 
            if (is_in_FOD(X,V1,msg_1))

                % Angle Compton
                %{
                beta = 180*rand();
                rad_beta = deg2rad(beta);                
                % Réaliser un dép^ot d'énergie
                %   E0 : énergie initiale, E1 : énergie déposée, E_sc : energie du photon après diffusion
                E_sc = E0/(1+(E0/X.mec2)*(1-cos(rad_beta)));
                E1 = E0-E_sc;
                %}

                [E_sc,E1,beta,rad_beta,cpt_kn_rm] = klein_nishina_reject_method(X,E0);
                %   ATTENTION - l'énergie E0 devient l'énergie post diffusion
                E0 = E_sc;
                % Angle de rotation pour la directrice
                omega = 360*rand();
                rad_omega = deg2rad(omega);
                if (verbose)
                    tok_diff = "diff"; if (beta >=90), tok_diff = "retro-diff"; end
                    fprintf("Angle Compton: [%g](deg) <=> %g(rad)\t=> %s \n",beta,rad_beta,tok_diff);
                    fprintf("\tE0= %g  \n\t\tDépot E1 = %g \n\t\t=> E_sc = %g => E0 = E_sc\n",E0+E1,E1,E_sc);
                    fprintf("\tAngle de directrice = [%g] (deg) <=> %g (rad) \n",omega,rad_omega);
                end
                % Génératrice du cone
                csr_gen_dir = get_generatrix_unit_vector(M,V1,rad_beta,rad_omega,t_V1);
                %
                t2_min = 0;
                [t2_max] = get_intersect_coord_csr(X,V1,csr_gen_dir);
                %
                [t_V2] = distance_xenon_attenuation(X,E0,csr_gen_dir,t2_min);
                if (~isinf(t_V2))
                    V2 = get_point(V1,csr_gen_dir,t_V2);
                    msg_2 = ""; if (verbose), msg_2 = strcat("[CSR",name," V2]"); end 
                    if (is_in_FOD(X,V2,msg_2))
                        % % >>>>
%                         col_csr = '#0072BD'; if (beta >= 90), col_csr = '#A2142F'; end
%                         [csr_list] = draw_store_csr(csr_list,X,M,V1,rad_beta,t_V1,col_csr);
                        % % <<<<
                        [eff_id_2,eff_name_2] = get_interaction_type(X,E0);
                        if(verbose), fprintf("\t[DIFF2] %s \n",eff_name_2); end
                        if (eff_id_2 > 1)
                            % EFFET COMPTON ou PHOTO_ELEC => on peut continuer
                            is_compton =false; 
                            if (eff_id_2 == 2)
                                % Cas d'un effet PE -> Dép^ot de toute l'énergie
                                E2 = E0;
                                E_sc = 0;
                            else
                                is_compton = true; 
                                % ATTENTION - ON S'EN FICHE DE CE DEPOT => C'est surtout pour différencier le dép^ot lié à l'effet PE
                                % Cas d'un effet Compton -> Dép^ot d'énergie partielle
                                beta = 180*rand();
                                rad_beta = deg2rad(beta);
                                E_sc = E0/(1+(E0/X.mec2)*(1-cos(rad_beta)));
                                E2 =E0-E_sc;
                                % to be continued ...
                            end % if (eff_id_2 == 2)
                            if (verbose)
                                if (is_compton)
                                    tok_diff = "diff"; if (beta >=90), tok_diff = "retro-diff"; end
                                    fprintf("\tAngle Compton: [%g](deg) <=> %g(rad)\t=> %s \n",beta,rad_beta,tok_diff);
                                end
                                fprintf("\tE0= %g  \n\t\tDépot E2 = %g \n\t\t=> E_sc = %g => E0 = E_sc\n",E0,E2,E_sc);
                            end
                            %   ATTENTION - l'énergie E0 devient l'énergie post diffusion
                            E0 = E_sc;
                            % to be continued ...
                            succ = true;
                        else
                            succ = false;
                            V0 = inf(3,1); V1 = inf(3,1);  V2 = inf(3,1);
                            t_V1 = inf;    t_V2 = inf;
                            E1 = inf;
                        end %  if (eff_id_2 > 1)
                    else
                        succ = false;
                        V0 = inf(3,1); V1 = inf(3,1);  V2 = inf(3,1);
                        t_V1 = inf;    t_V2 = inf;
                        E1 = inf;
                    end %  if (is_in_FOD(X,V2))
                else
                    succ = false;
                    V0 = inf(3,1); V1 = inf(3,1);
                    t_V1 = inf;    t_V2 = inf;
                    E1 = inf;
                end % if (~isinf(t_V2))
            else
                % V1 est hors de la zone active => STOP
                succ = false;
                V1 = inf(3,1);     V0 = inf(3,1);
                t_V1 = inf;
            end %  if (is_in_FOD(X,V1))
        else
            % EFFET PE OU RAYLEIGH => stop
            succ = false;
            V1 = inf(3,1);
            V0 = inf(3,1);
            t_V1 = inf;
        end % if (eff_id_1 == 3)
    end %  if (~isinf(t_V1))
    %
    if (~isempty(csr_list))
        erase_all_elts(csr_list)
    end
end






