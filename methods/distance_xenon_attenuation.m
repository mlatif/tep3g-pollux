function [t] = distance_xenon_attenuation(X,E0,d,t_init_min)
    t = 0; 

    mu_at_E0 = mu_interpolation_energie(X,E0); 

    mu_total = mu_at_E0*X.cs_data.is_considered; %sum(mu_at_E0(:,1:3)) avec la contrainte de négliger où non l'effet Rayleigh ie sum(mu_at_E0(:,2:3));
    % Expression du Free Mean Path 
    u = rand();     
    p = - log(u);
    % Tentative d'expression de la distance suivant l'axe depuis le point V1
    dist_gamma = p/mu_total;
    % Normalement, à ce moment, on a pas besoin de connaitre le signe de
    % t_init pour donner le sens de propagation. On est déjà dans lee sens
    % de propagation - cela était utile lorsque nous étions dans le cas
    % 2GLOR
    if (t_init_min ~= 0)
        t = sign(t_init_min)*(dist_gamma/norm(d,2));
    else
        t = dist_gamma/norm(d,2);
    end
end