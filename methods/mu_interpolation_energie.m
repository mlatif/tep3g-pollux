function [mu_hat] = mu_interpolation_energie(X,E)
% mu_interpolation_energie
% Retourne les coefficients d'attenuation (mu) pour une énergie passée en paramètre
% INPUT:
%   X : la caméra et les grandeurs physiques
%   E : l'énergie à interpoler 
% OUTPUT:
%   mu_hat : les coefficients d'attenuation associés à l'énergie E_hat

    Energy = X.cs_data.Energy;
    Mu = X.cs_data.mu;
    % Test de validité - l'énergie recherché est-elle bien dans les dans le range des données extraites (entre 0.001 et 1200 Mev) 
    if (E >= Energy(1) && E <= Energy(end))
        E_min = inf; E_max = inf;
        % Recherche des indices d'énergies pour l'interpolation 
        b_min = find(Energy <= E);  
        b_max = find(Energy >= E);
        i_min = b_min(end);
        i_max = b_max(1);
        % L'énergie est entre des bornes ? ou sur une borne ??
        if (i_min ~= i_max)
            % L'énergie est entre des bornes (eg. après dép^ot)
            E_min = Energy(i_min);
            E_max = Energy(i_max);
            mu_min = Mu(i_min,:);
            mu_max = Mu(i_max,:);
            mu_hat = mu_min+(E - E_min)/(E_max-E_min)*(mu_max - mu_min); 
        else
            % L'énergie est connue
            mu_hat = Mu(i_max,:);
        end
    else
        mu_hat = inf(3,1);
    end
end