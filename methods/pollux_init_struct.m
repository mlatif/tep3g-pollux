function [S] = pollux_init_struct(C,P)
    % Sommets à LOR
    V1_rng = (1:3);         % Interaction type 2 photons - Sommet 1 
    V2_rng = (4:6);         % Interaction type 2 photons - Sommet 2
    % Sommets à CSR - Cas général 
    V1p_rng = (7:9);        % Interaction type 1 photon - Sommet 1
    V2p_rng = (10:12);      % Interaction type 1 photon - Sommet 2
    % Sommets à CSR - Cas mix
    V1s_rng = (13:15);      % Interaction type 1 photon - Sommet 1
    V2s_rng = (16:18);      % Interaction type 1 photon - Sommet 2
    % Dép^ot d'energie - Cas général et mix
    E1a_rng = 19;   E2a_rng = 20;   %  type 1 photons - Sommets 1 et 2
    E1b_rng = 21;   E2b_rng = 22;   %  type 1 photons - Sommets 1 et 2
    inter_I1_rng = (1:3);   inter_I2_rng = (4:6);   % Stocker les points d'intersection
    % ---------------------------------------------------------------------
    S.rng.V1_rng = V1_rng;  S.rng.V2_rng = V2_rng;
    S.rng.V1p_rng = V1p_rng;  S.rng.V2p_rng = V2p_rng;
    S.rng.V1s_rng = V1s_rng;  S.rng.V2s_rng = V2s_rng;
    S.rng.E1a_rng = E1a_rng;    S.rng.E2a_rng = E2a_rng;
    S.rng.E1b_rng = E1b_rng;    S.rng.E2b_rng = E2b_rng;
    S.rng.inter_I1_rng = inter_I1_rng;  S.rng.inter_I2_rng = inter_I2_rng; 
    S.export.rng = S.rng; 
    S.export.t = zeros(C.pollux_N,1,'uint32');
    S.export.chr = strings(C.pollux_N,1);          % TODO Voir pour potentiel pb à la conversion
    S.export.data = zeros(C.pollux_N,22,'double');
    S.export.intersection_points = zeros(C.pollux_N,6,'double'); 
    % ---------------------------------------------------------------------
    % condition d'arr^et retournée en dehors de la boucle
    S.terminal = false;   
    % ---------------------------------------------------------------------
    % nombre initial de détection (pour la mise à jour des tableaux
    % d'export en cas de dépassement)
    S.cur_N_limit = C.pollux_N;
    % ---------------------------------------------------------------------
    S.nb_detection = 0; 
    % ---------------------------------------------------------------------
    % Transfert des informations du Phantom : 
    S.Ph.D = P.D;
    S.Ph.J = P.J;
    S.Ph.Rc = P.Rc; 
    S.Ph.V = P.V;
    % ---------------------------------------------------------------------
    % Transfert des informations de la config 
    S.Conf.N = C.pollux_N;
    S.Conf.Case = C.pollux_case;
    S.Conf.rng = C.rng;
    S.Conf.Pos_rng = C.Pos_rng;
    S.Conf.Emi_pts = C.Emi_pts; 
    % ---------------------------------------------------------------------
    S.Classif = C.Classif;
    S.preset_type = C.preset_type;
    % ---------------------------------------------------------------------
    S.results.activity = zeros(P.D(1),P.D(2),P.D(3));
    S.results.M = 0;                        % M : nombre de mesures observées (qui peut ^etre différent du nombre N si on a eu des pertes)
    S.results.calib_factor = 1.;
    S.results.lambda = zeros(1,P.J);
    S.results.vox_idx = zeros(C.pollux_N,1);
    S.results.u_set = zeros(C.pollux_N,1);         % stocker les tirages aléa
    S.results.M = zeros(C.pollux_N,3);             % Point d'émission;
    S.results.Vox_Re_coord = zeros(C.pollux_N,3);      % Coordonnées réelles du voxel contenant le point d'émission 
    S.results.M_pr = zeros(C.pollux_N,3);          % Point d'émission après positron range;
    S.results.R_pr = zeros(C.pollux_N,3);          % Valeur du positron range 
    S.results.D1 = zeros(C.pollux_N,3);            % Le vecteur directionnel 1 - Pour la LOR
    S.results.D_pr = zeros(C.pollux_N,3);          % Le vecteur directionnel pour le positron range 
    S.results.D2 = zeros(C.pollux_N,3);            % Le vecteur directionnel 2 - Pour le 3ème photon
    S.results.occ_config = inf(6,1000*C.N_max   );     % une pseudo H-map pour stocker les configurations détectées - s'aider d'une TDV pour vérifier la classif
    S.results.occ_config_count = zeros(1,6);
    S.results.cpt_rm_kn = zeros(C.pollux_N,2);        % Compteur du nombre de tirages effectués pour KN ( 1) pour les 1157, 2) pour les 511)
    S.results.csr_gen_dir = zeros(C.pollux_N,3); 
    % 
    S.results.nb_intersection = zeros(C.pollux_N,1);
    % Tracker le résultat de l'intersection.
    % [sgn(delta),t1::in_fov,t1::sign(beta),t2::in_fov,t2::sign(beta)]
    S.results.inter_status = zeros(C.pollux_N,7);
end