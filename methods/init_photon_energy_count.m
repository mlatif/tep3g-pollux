function [E0,E0_prime,P_gamma_511,P_gamma_1157] = init_photon_energy_count(config)
    E0=inf;
    E0_prime=inf;
    P_gamma_511 =0;     % nb de particules émises avec une énergie à 511 
    P_gamma_1157=0;     % nb de particules émises avec une énergie à 1.157 
    switch config
        case "1"         
            E0_prime = 1.157;
            P_gamma_511 = 0;
            P_gamma_1157= 1;
        case "2"
            E0 = 0.511;
            P_gamma_511 = 2;
            P_gamma_1157= 0;
        case "3"
            E0 = 0.511;
            E0_prime = 1.157;
            P_gamma_511 = 2;
            P_gamma_1157= 1;
    end
end