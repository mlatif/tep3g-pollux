function [E_sc,E1,beta,rad_beta,cpt] = klein_nishina_reject_method(X,E0)
    e_z = (X.mec2)/(X.mec2 + 2*E0);
    a1 = log(1/e_z);
    a2 = 0.5*(1-e_z^2);
    ratio = a1/(a1+a2);
    % ---------------------------------------------------------------------
    rejected = true; 
    % ---------------------------------------------------------------------
    cpt = 0;
    while(rejected)
        cpt = cpt+1; 
        R = rand(3,1); 
        if (R(1)<ratio)
            e = exp(-R(2)*a1);
        else
            e = sqrt(e_z^2 + R(2)*(1-e_z^2));
        end
        % ---
        t = (X.mec2*(1-e))/(E0*e);
        sin2theta = t*(2-t);
        % ---
        geps = (1-sin2theta*((e)/(1+e^2)));
        if (R(3)<= geps)
            % On accepte e 
            rejected = false;
        end
    end
    % ---------------------------------------------------------------------
    E_sc = E0*e;
    % ---------------------------------------------------------------------
    E1 = E0 - E_sc;
    cosbeta = 1-X.mec2 * (E1/(E0*(E0-E1)));
    beta = acosd(cosbeta); 
    rad_beta = deg2rad(beta);
    % ---------------------------------------------------------------------
    %{
    E_sc_TEST = E0/(1+(E0/X.mec2)*(1-cos(deg2rad(beta))))
    disp(E_sc == E_sc_TEST)
    disp(abs(E_sc - E_sc_TEST))
    %}
end