function [pure_evt_idx] = get_pure_event_idx(C)
    pure_evt_idx = 0;                            % Un indice d'évènements "purs" que l'on doit regarder
    switch C.pollux_case
        case "1", pure_evt_idx = 3;              % 1G1157 - par défaut, en "1" on ne tire que des 1157
        case "2", pure_evt_idx = 4;              % 2GLOR
        case "3", pure_evt_idx = 6;              % 3G
    end
end