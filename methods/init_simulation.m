function [X,P,stop_init] = init_simulation(input_data,cs_file,neg_rayleigh)
    stop_init = false; 
    if (nargin < 3), neg_rayleigh = false; end 
    % X : Xemis
    X.O = [0 0 0];          % centre du repère
    X.Rin = 75;             % Rayon intérieur LXe actif (mm)
    X.Rout = 190;           % Rayon extérieur LXe actif (mm)
    X.Rfov = 50;            % Rayon extérieur FOV (mm)
    X.Ax = 240;             % Longueur axiale (mm); 
    X.L = X.Rin*cos(pi/4);  % demi longueur atteignable
    X.nVpL = 0;             % nb Voxel par demi longueur atteignable
    % Dans l'ordre de lecture [Energy, Rayleigh, Compton, Photo Electrique, Att totale with Compton]
    % Remarque : Si tu tentes de vérifier que la somme des trois effets est égale à l'attenuation totale, tu risques d'^etre déçu ... (perte de précision dans les données importées); 
    X.rho = 3.057;  % densité volumique de masse [g.cm^-3]
    %nist_data = importdata(cs_file,"\t").data(:,1:4);
    parsed_data = importdata(cs_file,"\t");
    nist_data = parsed_data(:,1:4);
    X.cs_data.Energy = nist_data(:,1);
    nist_data = nist_data(:,2:end);                     % coefficient d'atténuation massique [cm^2.g^-1]
    X.cs_data.mu = X.rho.*nist_data./10;                % ATTENTION : là tu es en [cm^-1], et toute tes données sont en [mm] -> il faut diviser par 10 
    % coefficient d'atténuation linéaire [cm^-1]   
    %X.cs_data.mu = [X.cs_data.mu, sum(X.cs_data.mu,2)];  
    %
    X.cs_data.is_considered = ones(3,1);
    if (neg_rayleigh), X.cs_data.is_considered(1) = 0; end
    % 
    X.mec2 = 0.51099895; % (MeV) masse d'un électron au repos en MeV - https://en.wikipedia.org/wiki/Electron_mass#cite_note-physconst-mec2MeV-3 
    test ="";
    % -------------------------------------------------------------------------
    D = input_data(1,1:3);                  % D : nb de voxel;
    V = input_data(2,1:3);                  % V : taille d'un voxel (mm)
    T = D .* V;                             % T : taille de l'image (mm)
    % 
    stop_init = is_valid_phantom(X,T);
    if (stop_init)
        P = []; 
    else
        %
        P.D = input_data(1,1:3);                  % D : nb de voxel;
        P.J = prod(P.D);                    % J : nb de voxel total;
        P.Rc = zeros(P.J,3);                % Rc : liste des coordonnées réelles bas gauche d'un voxel
        P.Ic = zeros(P.J,3);
        P.V = input_data(2,1:3);                  % V : taille d'un voxel (mm)
        P.T = P.D .* P.V;                   % T : taille de l'image (mm)
        %
        P.S = ones(1,3);                    % X : coeff de surpixelisation de l'image
        P.activity = zeros(P.D(1),P.D(2),P.D(3));  % G : matrice des gain (D pour densité est déjà pris)
        %X.nVpL = 2*ceil(X.L ./ P.V);          % ATTENTION - HYPOTHESE je prend la valeur supp pour le nombre de voxels
        input_data(1:2,:) = [];
        input_data(:,1:3) = input_data(:,1:3)+1;      % Resol pb indiçage
        % -------------------------------------------------------------------------
        % Remplissage avec la matrice d'activitée 
        for r = 1:size(input_data,1)
            P.activity(input_data(r,1),input_data(r,2),input_data(r,3))=input_data(r,4);
            %if (input_data(r,4)~=0),    fprintf("%d\t (x=%d,y=%d,z=%d)\t v=%g\n",r,input_data(r,1),input_data(r,2),input_data(r,3),input_data(r,4));    end
        end
        %{
        figure;   imagesc(squeeze(sum(P.activity,3))); axis equal; colorbar; colormap(flipud(gray));
        %}
        % -------------------------------------------------------------------------
        % % Lister les coordonnées réelles de l'ensemble des voxels d'une image
        for j=1:P.J
            [a,b,c] = get_axes_voxel_idx(P,j);          
            [r_a,r_b,r_c] = get_axes_voxel_coord(P,a,b,c);
            P.Rc(j,:) = [r_a,r_b,r_c];
            P.Ic(j,:) = [a,b,c]; 
        end
        % -------------------------------------------------------------------------
        P.lambda = zeros(P.J,1);
        for x=1:P.D(1)
            for y=1:P.D(2)
                for z=1:P.D(3)
                    P.lambda(get_voxel_idx(P,x,y,z))=P.activity(x,y,z);
                end
            end
        end
        lambda_hat = P.lambda./sum(P.lambda);
        P.cdf_lambda = cumsum(lambda_hat);
        if (all(isnan(lambda_hat)) && all(isnumeric(lambda_hat)))
            error_message = [ 'ATTENTION - Pour pollux, tu vas avoir besoin d une activité ... je t aurai prévenu ;) !!!! \n' , ...
                  'Risque de crash dans S.results.lambda(rand_j) = S.results.lambda(rand_j)+1 car P.lambda = 0; .\n' ];
            warning('u:stuffed:it',error_message);

        end
    end
    %{
    figure; 
        subplot(1,2,1)
            plot(lambda_hat)
        subplot(1,2,2)
            plot(P.cdf_lambda)
    %}
    % -------------------------------------------------------------------------
    % if (nargin <3), check_corr_idx = false; end
%     if (check_corr_idx)
%         % % Tester les correspondances variables
%         passe_test_1 = true;
%         passe_test_2 = true;
%         for (r = 1:size(input_data,1))
%             j = get_voxel_idx(P,input_data(r,1),input_data(r,2),input_data(r,3));
%             if (j ~= r), disp(r); passe_test_1 = false; end
%         end
%         for (r=1:size(input_data,1))
%             [x,y,z] = get_axes_voxel_idx(P,r);
%             if (x ~= input_data(r,1) || y ~= input_data(r,2) || z ~= input_data(r,3)), fprintf("%d \t (%g,%g,%g) \t (%g,%g,%g)\n",r,x,y,z,input_data(r,1),input_data(r,2),input_data(r,3)); passe_test_2 = false; end
%         end
%         fprintf("TEST_1 : (x,y,z) -> j: %d \nTEST_2 : j -> (x,y,z): %d \n",passe_test_1,passe_test_2);
%     end

    % Tester la numéroration des voxels en 3D - checker l'ordre de numérotation
    % for (j=1:P.J)
    %     [x,y,z] = get_axes_voxel_idx(P,j);
    %     fprintf("%d \t (%g,%g,%g)\n",j,x,y,z);
    % end

end

function [stop] = is_valid_phantom(X,T)
    L_max = (2*X.Rfov)/sqrt(2) ;
    stop = ~(T(1) <= L_max && T(2) <= L_max && T(3) <= X.Ax); 
    if (stop)
        fprintf("[ATTENTION] Phantom non valide:\n\tTaille(X)=%g et Taille Max(X)=%g\n\tTaille(Y)=%g et Taille Max(Y)=%g\n\tTaille(Z)=%g et Taille Max(Z)=%g\n",T(1),L_max,T(2),L_max,T(3),X.Ax)
    end
    %if (P.T(1) > )
end