function [t_1,t_2] = intersection_rayon_cylindre(E,V,R,z,def_value)
    dv = inf; 
    if (nargin == 5), dv = def_value; end
    %%% -------------------------------------------------------------------
    t_1 = inf;
    t_2 = inf;
    if ((V(1)^2 + V(2)^2) ~= 0)
        A = V(1)^2 + V(2)^2;
        B = 2*E(1)*V(1) + 2*E(2)*V(2);
        C = E(1)^2 + E(2)^2 - R^2;
        delta = B^2-4*A*C;
        if (delta > 0)
            r1 = (-B - sqrt(delta))/(2*A);
            r2 = (-B + sqrt(delta))/(2*A);
            t_1 = max(r1,r2);       % Le maximum des deux est dans le sens du vecteur directionnel 
            t_2 = min(r1,r2);       % Le minimum des deux est dans le sens contraire du vecteur directionnel
        elseif (delta==0) % delta = 0
            r1 = (-B)/(2*A);
            if (r1 <0), t_2 = r1; else, t_1 = r1; end
        else
            t_1 = inf; t_2 = inf; 
        end
    else
        disp('!!!! x_D^2 + y_D^2 = 0 !!!!');
    end % if ((V(1)^2 + V(2)^2) ~= 0)
    % ---
    if (~isinf(t_1))
        if (E(3) + t_1 * V(3) <= -z || E(3) + t_1 * V(3) >= +z)
            t_1 = inf;
%         else
%             t_1 = t_in;
        end
    end    
    % ---
    if (~isinf(t_2))
        if (E(3) + t_2 * V(3) <= -z || E(3) + t_2 * V(3) >= +z)
            t_2 = inf;
%         else
%             t_1 = t_in;
        end
    end
    %%% -------------------------------------------------------------------
    if (isinf(t_1) && ~isinf(dv)), t_1 = dv; end
    if (isinf(t_2) && ~isinf(dv)), t_2 = dv; end

end