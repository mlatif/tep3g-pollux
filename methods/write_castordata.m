function [] = write_castordata(F,S,msg)
    % On est jamais trop prudent
    delete(strcat(F.save_folder,F.phantom_name,".*"));
    if (nargin <3)
        suffix = "";
    else
        suffix = "_"+msg;
    end
    % ---
    %{
    fprintf("Data filename: %s.cdf\n",F.file);
    fprintf("Number of events: %d\n",S.Conf.N);
    fprintf("Detector type: continuous\n");     % {discrete, continous}
    fprintf("Data mode: list-mode\n");
    fprintf("Data type: 3gt\n");
    fprintf("Start time (s): 0\n");
    fprintf("Duration (s): 1 \n");
    fprintf("Calibration factor: %.10f\n",S.results.calib_factor);
    fprintf("Isotope: Sc-44\n");     % Xing p33 (3.97h et Br 1.) + https://www.nndc.bnl.gov/nudat3/ 44Sc 
    %}
    % ---
    % CASTOR DATA HEADER
    ext = ".cdh";
    name_file_cdh = strcat(F.save_folder,F.phantom_name,suffix,ext); 
    fileID = fopen(name_file_cdh,'w');
        fprintf(fileID,"Data filename: %s.cdf\n",F.phantom_name+suffix);
        fprintf(fileID,"Number of events: %d\n",sum(S.export.chr~='0'));
        fprintf(fileID,"Scanner name: XEMIS2\n");
        %fprintf(fileID,"Detector type: continuous\n");     % {discrete, continous}
        fprintf(fileID,"Data mode: list-mode\n");
        fprintf(fileID,"Data type: XEMIS\n");
        class_in_exp = S.export.chr(S.export.chr~='0'); 
        fprintf(fileID,"Number of class: %d\n",length(unique(class_in_exp)));
        fprintf(fileID,"Start time (s): 0\n");
        fprintf(fileID,"Duration (s): 1 \n");
        %fprintf(fileID,"Calibration factor: %.10f\n",S.results.calib_factor);
        fprintf(fileID,"Isotope: Sc-44\n");     % Xing p33 (3.97h et Br 1.) + https://www.nndc.bnl.gov/nudat3/ et taper : 44Sc
    fclose(fileID);
    fprintf("SAVE: %s \n",name_file_cdh);
    % ---
    % CASTOR DATA FILE 
    ext = ".cdf";
    %ext = ".bin";
    % https://fr.mathworks.com/help/matlab/ref/fwrite.html
    % Permission : 'w' : Open or create new file for writing. Discard existing contents, if any.
    % machinefmt : Little-endian ordering (HYP car c'est ce qu'on utilise dans la lecture avec ImageJ )
    % [WIKI] Le boutisme (endianness en anglais) ou plus rarement endianisme désigne l'ordre dans lequel ces octets sont placés. Il existe deux conventions opposées : l'orientation gros-boutiste (ou gros-boutienne) qui démarre avec les octets de poids forts, et l'orientation inverse petit-boutiste (ou petit-boutienne).
    % REMARQUES : 
    %   - Typer t = uint32(.) ne permet pas d'assurer l'écriture dans le fichier lorsque je tente de l'écrire... Donc je l'écris en float32 et là, ça passe. 
    name_file_cdf = strcat(F.save_folder,F.phantom_name,suffix,ext); 
    fileID = fopen(name_file_cdf,'w','l');
    for n = 1:length(S.export.t)      %length(S.export.t)
        if (S.export.chr(n)~="0")
            %t = uint32(S.export.t(n));  
            t = uint32(10);
            %c = S.export.chr(n);
            %c_num = str2num(c); 
            % 
            %fwrite(fileID,t,"uint32")            
            fwrite(fileID,uint32(10),"uint32");     % stocké sur 4 bytes ie 32 bits
            %fwrite(fileID,c,"char");
            switch S.export.chr(n)
                case "1"
                    fwrite(fileID,uint16(1),"uint16");
                case "2"
                    fwrite(fileID,uint16(2),"uint16");
                case "3"
                    fwrite(fileID,uint16(3),"uint16");
                case "4"
                    fwrite(fileID,uint16(4),"uint16");
                case "5"
                    fwrite(fileID,uint16(5),"uint16");
            end

            %fwrite(fileID,uint8(c_num),"uint8");
            % -------------------------------------------------------------
            % d = [V1](1:3) | [V2](4:6) | [V1'](7:9) | [V2'](10:12) | [E1](13) | [E1'](14)
            d = zeros(1,14); 
            switch S.export.chr(n)
                case "1"
                    d(7:9) = S.export.data(n,S.rng.V1s_rng);
                    d(10:12) = S.export.data(n,S.rng.V2s_rng);
                    d(14) = S.export.data(n,S.rng.E1b_rng);
                case "2"
                    d(1:3) = S.export.data(n,S.rng.V1p_rng);
                    d(4:6) = S.export.data(n,S.rng.V2p_rng); 
                    d(13) = S.export.data(n,S.rng.E1a_rng);
                case "3"
                    % Données 2GLOR - sans dépot d'énergie
                    d(7:9) = S.export.data(n,S.rng.V1_rng);
                    d(10:12) = S.export.data(n,S.rng.V2_rng);
                case "4"
                    % Données 1G1157
                    d(1:3) = S.export.data(n,S.rng.V1p_rng);
                    d(4:6) = S.export.data(n,S.rng.V2p_rng);
                    d(13) = S.export.data(n,S.rng.E1a_rng);
                    % Données 1G511
                    d(7:9) = S.export.data(n,S.rng.V1s_rng);
                    d(10:12) = S.export.data(n,S.rng.V2s_rng);
                    d(14) = S.export.data(n,S.rng.E1b_rng);
                case "5"
                    % Donnée 1G1157
                    d(1:3) = S.export.data(n,S.rng.V1p_rng);
                    d(4:6) = S.export.data(n,S.rng.V2p_rng);
                    d(13) = S.export.data(n,S.rng.E1a_rng);
                    % Données 2GLOR - sans dépot d'énergie
                    d(7:9) = S.export.data(n,S.rng.V1_rng);
                    d(10:12) = S.export.data(n,S.rng.V2_rng);
            end
            %d = S.export.data(n,:);
            fwrite(fileID,d,"float32");
            % Champs des énergies incidentes pour les CORs [E0 = 1.157, E0'=0.511]
            e =zeros(1,2); 
            switch S.export.chr(n)
                case "1"
                    e(1) = 0.; e(2) = 0.511; 
                case "2"
                    e(1) = 1.157; e(2) = 0.; 
                case "3"
                    e(1) = 0.; e(2) = 0.;
                case "4"
                    e(1) = 1.157; e(2) = 0.511;
                case "5"
                    e(1) = 1.157; e(2) = 0.;
            end
            %fprintf("%d \t %s \t E = [%g,%g] \n",n,S.export.chr(n),e);
            fwrite(fileID,e,"float32");
            % Champ du poids W (arbitraitement fixé à 1. pour nos données)
            W=1.; 
            fwrite(fileID,W,"float32");% Champ du poids W (arbitraitement fixé à 1. pour nos données)
            test="";
        end        
    end
    fclose(fileID);
    fprintf("SAVE: %s \n",name_file_cdf);
end