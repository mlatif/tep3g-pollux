clear all; 
close all; 
set(0,'DefaultFigureWindowStyle','docked');
load("/home/user/Bureau/THESE/POLLUX/res_simu/pollux_tracker_cortex_sim_1000000_3G_PR.mat")

%%
close all

% ETAPE 1 - On prends tous les counts obtenus et on somme;
lab = ["0\gamma","1\gamma_{COR}^{511}","1\gamma_{COR}^{1157}","2\gamma_{LOR}","2\gamma_{COR}","3\gamma"];
COLOR_LIB = [
    0.7500, 0.7500, 0.7500;     % 0G (grey); 
    0.6350, 0.0780, 0.1840;     % 1G511
    0.9290, 0.6940, 0.1250;     % 1G1157
    0.4940, 0.1840, 0.5560;     % 2GLOR
    0.3010, 0.7450, 0.9330;     % 2GMIX
    0.4660, 0.6740, 0.1880;     % 3G
    0.8500, 0.3250, 0.0980;     % AUTRE
    0, 0.4470, 0.7410;          % 0G;     
];

occ_config_count = S.results.occ_config_count;
% -------------------------------------------------------------------------
fprintf("Nombre total d'émissions générées: %.2e \n",sum(occ_config_count));
% -------------------------------------------------------------------------
data = occ_config_count/sum(occ_config_count)*100; 
figure; 
    set(gca,'color','none')
    %set(gca,'TickLabelInterpreter','Latex');
    hold on; %box on;
    idx = categorical(1:length(data));
    b = bar(idx,data);
    b.FaceColor = 'flat';
    b.EdgeColor = 'none';
    for (idx=1:6)
        b.CData(idx,:) = COLOR_LIB(idx,:);
    end    
    xtips2 = b(1).XEndPoints;
    ytips2 = b(1).YEndPoints;
    labels2 = string(b(1).YData);
    labels2_formatted = cell(size(labels2));
    for i = 1:numel(labels2)
        labels2_formatted{i} = sprintf('%.2f', str2double(labels2{i}));
    end
    ylabel("\% of detections",'Interpreter','latex','FontSize',18)
    text(xtips2,ytips2,labels2_formatted,'HorizontalAlignment','center','VerticalAlignment','bottom','Interpreter','latex','FontSize',18);
    set(gca,'xticklabel',lab,'FontSize',15);
    %title("D\'et\'ections sur l'ensemble des simulations",'Interpreter','latex')
saveas(gcf,"TAB_CORTEX_1",'epsc');

% -------------------------------------------------------------------------    
list = 2:6; 
occ_det_config_count = S.results.occ_config_count(list);

% -------------------------------------------------------------------------
fprintf("Nombre total de détections: %.2e \n",sum(occ_det_config_count));
% -------------------------------------------------------------------------
COLOR_LIB_prime = COLOR_LIB; 
COLOR_LIB_prime(1,:) = []; 
lab_prime = lab; 
lab_prime(1) = []; 
data = occ_det_config_count/sum(occ_det_config_count)*100; 
figure; 
    set(gca,'color','none')
    %set(gca,'TickLabelInterpreter','Latex');
    hold on; %box on;
    idx = categorical(1:length(data));
    b = bar(idx,data);
    b.FaceColor = 'flat';
    b.EdgeColor = 'none';
    for (idx=1:length(list))
        b.CData(idx,:) = COLOR_LIB_prime(idx,:);
    end    
    xtips2 = b(1).XEndPoints;
    ytips2 = b(1).YEndPoints;
    labels2 = string(b(1).YData);
    save_labels2 = labels2; 
    labels2_formatted = cell(size(labels2));
    for i = 1:numel(labels2)
        labels2_formatted{i} = sprintf('%.2f', str2double(labels2{i}));
    end
    ylabel("\% of detections",'Interpreter','latex','FontSize',18)
    text(xtips2,ytips2,labels2_formatted,'HorizontalAlignment','center','VerticalAlignment','bottom','Interpreter','latex','FontSize',18);
    set(gca,'xticklabel',lab_prime,'FontSize',15);
    %title("Classes d'\'ev\'enement d\'et\'ect\'es sur l'ensemble des simulations",'Interpreter','latex')

saveas(gcf,"TAB_CORTEX_2",'epsc');

%%
fprintf("Hybride: total des classes rapportées aux détections excluant le 0G\n")
save_labels2

data = occ_config_count/sum(occ_config_count)*100; 
figure; 
    set(gca,'color','none')
    %set(gca,'TickLabelInterpreter','Latex');
    hold on; %box on;
    idx = categorical(1:length(data));
    b = bar(idx,data);
    b.FaceColor = 'flat';
    b.EdgeColor = 'none';
    for (idx=1:6)
        b.CData(idx,:) = COLOR_LIB(idx,:);
    end    
    xtips2 = b(1).XEndPoints;
    ytips2 = b(1).YEndPoints;
%    labels2 = string(b(1).YData);
    labels2 = ["",save_labels2]; 
    labels2_formatted = cell(size(labels2));
    for i = 1:numel(labels2)
        if (labels2{i}~= "")
        labels2_formatted{i} = sprintf('%.2f', str2double(labels2{i}));
        end
    end
    ylabel("\% of detections",'Interpreter','latex','FontSize',18)
    text(xtips2,ytips2,labels2_formatted,'HorizontalAlignment','center','VerticalAlignment','bottom','Interpreter','latex','FontSize',18);
    set(gca,'xticklabel',lab,'FontSize',15);
    %title("D\'et\'ections sur l'ensemble des simulations",'Interpreter','latex')
saveas(gcf,"TAB_CORTEX_3",'epsc');


